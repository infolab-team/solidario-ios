//
//  ProjectCardView.h
//  Solidario
//
//  Created by Pan on 22/11/16.
//  Copyright © 2016 Paula Vasconcelos Gueiros. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KAProgressLabel.h"

@interface ProjectCardView : UIView

@property (weak, nonatomic) IBOutlet UIView *topHiddenView;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIView *imageContainerView;
@property (weak, nonatomic) IBOutlet UIView *bottomHiddenView;
@property (weak, nonatomic) IBOutlet UIView *gradientSuperview;

@property (weak, nonatomic) IBOutlet UICollectionView *bannerCollectionView;
@property (weak, nonatomic) IBOutlet UIButton *favoriteButton;
@property (weak, nonatomic) IBOutlet UIPageControl *bannerPageControl;
@property (weak, nonatomic) IBOutlet UILabel *goalLabel;

@property (weak, nonatomic) IBOutlet UILabel *projectTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *projectDescriptionLabel;
@property (weak, nonatomic) IBOutlet UIButton *showMoreButton;
@property (weak, nonatomic) IBOutlet KAProgressLabel *projectProgressLabel;
@property (weak, nonatomic) IBOutlet UILabel *projectNumberOfDonorsLabel;
@property (weak, nonatomic) IBOutlet UILabel *projectPeopleDonatedLabel;
@property (weak, nonatomic) IBOutlet UIButton *numberOfDonorsButton;

@end

//
//  UserUpdated.m
//
//  Created by Paula Vasconcelos on 24/07/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "UserUpdated.h"


NSString *const kUserUpdatedEmail = @"email";
NSString *const kUserUpdatedPicture = @"picture";
NSString *const kUserUpdatedSurname = @"surname";
NSString *const kUserUpdatedName = @"name";


@interface UserUpdated ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation UserUpdated

@synthesize email = _email;
@synthesize picture = _picture;
@synthesize surname = _surname;
@synthesize name = _name;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.email = [self objectOrNilForKey:kUserUpdatedEmail fromDictionary:dict];
            self.picture = [self objectOrNilForKey:kUserUpdatedPicture fromDictionary:dict];
            self.surname = [self objectOrNilForKey:kUserUpdatedSurname fromDictionary:dict];
            self.name = [self objectOrNilForKey:kUserUpdatedName fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.email forKey:kUserUpdatedEmail];
    [mutableDict setValue:self.picture forKey:kUserUpdatedPicture];
    [mutableDict setValue:self.surname forKey:kUserUpdatedSurname];
    [mutableDict setValue:self.name forKey:kUserUpdatedName];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.email = [aDecoder decodeObjectForKey:kUserUpdatedEmail];
    self.picture = [aDecoder decodeObjectForKey:kUserUpdatedPicture];
    self.surname = [aDecoder decodeObjectForKey:kUserUpdatedSurname];
    self.name = [aDecoder decodeObjectForKey:kUserUpdatedName];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_email forKey:kUserUpdatedEmail];
    [aCoder encodeObject:_picture forKey:kUserUpdatedPicture];
    [aCoder encodeObject:_surname forKey:kUserUpdatedSurname];
    [aCoder encodeObject:_name forKey:kUserUpdatedName];
}

- (id)copyWithZone:(NSZone *)zone
{
    UserUpdated *copy = [[UserUpdated alloc] init];
    
    if (copy) {

        copy.email = [self.email copyWithZone:zone];
        copy.picture = [self.picture copyWithZone:zone];
        copy.surname = [self.surname copyWithZone:zone];
        copy.name = [self.name copyWithZone:zone];
    }
    
    return copy;
}


@end

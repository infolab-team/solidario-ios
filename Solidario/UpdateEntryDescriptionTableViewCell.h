//
//  UpdateEntryDescriptionTableViewCell.h
//  Solidario
//
//  Created by Pan on 16/02/17.
//  Copyright © 2017 Paula Vasconcelos Gueiros. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PlaceholderTextView.h"

@interface UpdateEntryDescriptionTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet PlaceholderTextView *descriptionTextView;

@end

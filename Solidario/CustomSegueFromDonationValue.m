//
//  CustomSegueFromDonationValue.m
//  Solidario
//
//  Created by Pan on 31/05/17.
//  Copyright © 2017 Paula Vasconcelos Gueiros. All rights reserved.
//

#import "CustomSegueFromDonationValue.h"

#import "DonationValueTableViewController.h"
#import "DonationDataTableViewController.h"
#import "AntifraudDataTableViewController.h"

#import "DonationGoalView.h"
#import "DonationValueView.h"

#import "UIColor+Utils.h"

@implementation CustomSegueFromDonationValue

- (void)perform {
    DonationValueTableViewController *sourceTVC = (DonationValueTableViewController *)self.sourceViewController;
    UITableView *sourceTableView = sourceTVC.tableView;
    
    CGFloat screenWidth = UIScreen.mainScreen.bounds.size.width;
    CGFloat screenHeight = UIScreen.mainScreen.bounds.size.height;
    CGFloat yOffset = self.sourceViewController.navigationController.navigationBar.frame.size.height + 20.f;
    
    // add destination view to current view, initially transparent
    UIView *destinationView = self.destinationViewController.view;
    [destinationView setFrame:CGRectMake(0, yOffset, screenWidth, screenHeight)];
    [destinationView setAlpha:0];
    
    // hide current banner, that will be slided down with tableview
    [sourceTVC.donationGoalView setHidden:YES];
    
    // add donation superview on top of the current hidden banner
    DonationValueView *donationSuperview = [[DonationValueView alloc] initWithFrame:sourceTVC.donationGoalView.frame];
    [donationSuperview setFrame:CGRectOffset(donationSuperview.frame, 0, yOffset)];
    
    // setup donation superview data
    NSNumberFormatter *currencyFormatter = [NSNumberFormatter new];
    [currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    NSLocale *currentLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"pt_BR"];
    [currencyFormatter setLocale:currentLocale];
    
    if ([self.destinationViewController isKindOfClass:[DonationDataTableViewController class]]) {
        donationSuperview.donationValueLabel.text = [currencyFormatter stringFromNumber:[NSNumber numberWithFloat:((DonationDataTableViewController *)self.destinationViewController).donation.value/100.f]];
    } else if ([self.destinationViewController isKindOfClass:[AntifraudDataTableViewController class]]) {
        donationSuperview.donationValueLabel.text = [currencyFormatter stringFromNumber:[NSNumber numberWithFloat:((AntifraudDataTableViewController *)self.destinationViewController).donation.value/100.f]];
    }

    // add goal superview on top of donation superview
    DonationGoalView *goalSuperview = [[DonationGoalView alloc] initWithFrame:sourceTVC.donationGoalView.frame];
    [goalSuperview setFrame:CGRectOffset(goalSuperview.frame, 0, yOffset)];
    
    // setup goal superview data
    goalSuperview.goalValueLabel.text = sourceTVC.project.goal;
    goalSuperview.mainImageView.image = sourceTVC.mainImage;
    
    CGFloat viewHeight, viewWidth;
    viewWidth = [[UIScreen mainScreen]bounds].size.width - (2 * defaultViewPadding);
    viewHeight = viewWidth * bannerViewProportion;
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = CGRectMake(0, 0, viewWidth, viewHeight);
    gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor defaultColor:DefaultColorBannerShadowTop] CGColor], (id)[[UIColor defaultColor:DefaultColorBannerShadowTop] CGColor], (id)[[UIColor blackColor] CGColor], nil];
    [goalSuperview.mainImageView.layer insertSublayer:gradient atIndex:0];
    
    // add views to window
    UIWindow *window = UIApplication.sharedApplication.keyWindow;
    [window insertSubview:destinationView aboveSubview:sourceTVC.view];
    [window insertSubview:donationSuperview aboveSubview:sourceTVC.view];
    [window insertSubview:goalSuperview aboveSubview:sourceTVC.view];
    
    // calculate height of tableview slide
    CGFloat heightToSlide = sourceTVC.donationGoalView.frame.size.height;
    UIEdgeInsets tableViewInset = sourceTableView.contentInset;
    tableViewInset.top = heightToSlide;
    
    // animate views
    [UIView animateWithDuration: 0.3f animations:^{
        //no need to set contentOffset, setting contentInset will change contentOffset accordingly.
        sourceTableView.contentInset = tableViewInset;
        [donationSuperview setFrame:CGRectOffset(donationSuperview.frame, 0, heightToSlide)];
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.3f animations:^{
            [destinationView setAlpha:1];
            
        } completion:^(BOOL finished) {
            [destinationView removeFromSuperview];
            [donationSuperview removeFromSuperview];
            [goalSuperview removeFromSuperview];
            [sourceTVC.navigationController pushViewController:self.destinationViewController animated:NO];
        }];
    }];
}

@end


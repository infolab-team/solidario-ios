//
//  User.m
//
//  Created by Pan  on 15/12/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "User.h"

static NSString *const kUserEmail = @"email";
static NSString *const kUserImage = @"image";
static NSString *const kUserSurname = @"surname";
static NSString *const kUserUserId = @"userId";
static NSString *const kUserAuthToken = @"authToken";
static NSString *const kUserName = @"name";

@interface User ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end


@implementation User

@synthesize email = _email;
@synthesize image = _image;
@synthesize surname = _surname;
@synthesize userId = _userId;
@synthesize authToken = _authToken;
@synthesize name = _name;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
        self.email = [self objectOrNilForKey:kUserEmail fromDictionary:dict];
        self.image = [self objectOrNilForKey:kUserImage fromDictionary:dict];
        self.surname = [self objectOrNilForKey:kUserSurname fromDictionary:dict];
        self.userId = [self objectOrNilForKey:kUserUserId fromDictionary:dict];
        self.authToken = [self objectOrNilForKey:kUserAuthToken fromDictionary:dict];
        self.name = [self objectOrNilForKey:kUserName fromDictionary:dict];

    }
    
    return self;
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.email forKey:kUserEmail];
    [mutableDict setValue:self.image forKey:kUserImage];
    [mutableDict setValue:self.surname forKey:kUserSurname];
    [mutableDict setValue:self.userId forKey:kUserUserId];
    [mutableDict setValue:self.authToken forKey:kUserAuthToken];
    [mutableDict setValue:self.name forKey:kUserName];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}


#pragma mark - Helper Method

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.email = [aDecoder decodeObjectForKey:kUserEmail];
    self.image = [aDecoder decodeObjectForKey:kUserImage];
    self.surname = [aDecoder decodeObjectForKey:kUserSurname];
    self.userId = [aDecoder decodeObjectForKey:kUserUserId];
    self.authToken = [aDecoder decodeObjectForKey:kUserAuthToken];
    self.name = [aDecoder decodeObjectForKey:kUserName];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {

    [aCoder encodeObject:_email forKey:kUserEmail];
    [aCoder encodeObject:_image forKey:kUserImage];
    [aCoder encodeObject:_surname forKey:kUserSurname];
    [aCoder encodeObject:_userId forKey:kUserUserId];
    [aCoder encodeObject:_authToken forKey:kUserAuthToken];
    [aCoder encodeObject:_name forKey:kUserName];
}

- (id)copyWithZone:(NSZone *)zone {
    User *copy = [[User alloc] init];
    
    if (copy) {
        copy.email = [self.email copyWithZone:zone];
        copy.image = [self.image copyWithZone:zone];
        copy.surname = [self.surname copyWithZone:zone];
        copy.userId = [self.userId copyWithZone:zone];
        copy.authToken = [self.authToken copyWithZone:zone];
        copy.name = [self.name copyWithZone:zone];
    }
    return copy;
}


@end

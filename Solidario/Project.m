//
//  Project.m
//
//  Created by Paula Vasconcelos on 02/05/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "Project.h"
#import "ProjectCreator.h"
#import "ProjectContact.h"
#import "ProjectSocial.h"


NSString *const kProjectSlug = @"slug";
NSString *const kProjectStatus = @"status";
NSString *const kProjectTitle = @"title";
NSString *const kProjectTimeleft = @"timeleft";
NSString *const kProjectCreator = @"creator";
NSString *const kProjectDuration = @"duration";
NSString *const kProjectId = @"_id";
NSString *const kProjectCategory = @"category";
NSString *const kProjectCity = @"city";
NSString *const kProjectContact = @"contact";
NSString *const kProjectMedia = @"media";
NSString *const kProjectInformation = @"information";
NSString *const kProjectDonators = @"donators";
NSString *const kProjectState = @"state";
NSString *const kProjectPercent = @"percent";
NSString *const kProjectVideo = @"video";
NSString *const kProjectSocial = @"social";
NSString *const kProjectDonatorsCount = @"donatorsCount";
NSString *const kProjectDonated = @"donated";
NSString *const kProjectGoal = @"goal";
NSString *const kProjectDescription = @"description";


@interface Project ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation Project

@synthesize slug = _slug;
@synthesize status = _status;
@synthesize title = _title;
@synthesize timeleft = _timeleft;
@synthesize creator = _creator;
@synthesize duration = _duration;
@synthesize internalBaseClassIdentifier = _internalBaseClassIdentifier;
@synthesize category = _category;
@synthesize city = _city;
@synthesize contact = _contact;
@synthesize media = _media;
@synthesize information = _information;
@synthesize donators = _donators;
@synthesize state = _state;
@synthesize percent = _percent;
@synthesize video = _video;
@synthesize social = _social;
@synthesize donatorsCount = _donatorsCount;
@synthesize donated = _donated;
@synthesize goal = _goal;
@synthesize internalBaseClassDescription = _internalBaseClassDescription;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.slug = [self objectOrNilForKey:kProjectSlug fromDictionary:dict];
            self.status = [self objectOrNilForKey:kProjectStatus fromDictionary:dict];
            self.title = [self objectOrNilForKey:kProjectTitle fromDictionary:dict];
            self.timeleft = [[self objectOrNilForKey:kProjectTimeleft fromDictionary:dict] doubleValue];
            self.creator = [ProjectCreator modelObjectWithDictionary:[dict objectForKey:kProjectCreator]];
            self.duration = [[self objectOrNilForKey:kProjectDuration fromDictionary:dict] doubleValue];
            self.internalBaseClassIdentifier = [self objectOrNilForKey:kProjectId fromDictionary:dict];
            self.category = [self objectOrNilForKey:kProjectCategory fromDictionary:dict];
            self.city = [self objectOrNilForKey:kProjectCity fromDictionary:dict];
            self.contact = [ProjectContact modelObjectWithDictionary:[dict objectForKey:kProjectContact]];
            self.media = [self objectOrNilForKey:kProjectMedia fromDictionary:dict];
            self.information = [self objectOrNilForKey:kProjectInformation fromDictionary:dict];
            self.donators = [self objectOrNilForKey:kProjectDonators fromDictionary:dict];
            self.state = [self objectOrNilForKey:kProjectState fromDictionary:dict];
            self.percent = [[self objectOrNilForKey:kProjectPercent fromDictionary:dict] doubleValue];
            self.video = [self objectOrNilForKey:kProjectVideo fromDictionary:dict];
            self.social = [ProjectSocial modelObjectWithDictionary:[dict objectForKey:kProjectSocial]];
            self.donatorsCount = [[self objectOrNilForKey:kProjectDonatorsCount fromDictionary:dict] doubleValue];
            self.donated = [[self objectOrNilForKey:kProjectDonated fromDictionary:dict] doubleValue];
            self.goal = [self objectOrNilForKey:kProjectGoal fromDictionary:dict];
            self.internalBaseClassDescription = [self objectOrNilForKey:kProjectDescription fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.slug forKey:kProjectSlug];
    [mutableDict setValue:self.status forKey:kProjectStatus];
    [mutableDict setValue:self.title forKey:kProjectTitle];
    [mutableDict setValue:[NSNumber numberWithDouble:self.timeleft] forKey:kProjectTimeleft];
    [mutableDict setValue:[self.creator dictionaryRepresentation] forKey:kProjectCreator];
    [mutableDict setValue:[NSNumber numberWithDouble:self.duration] forKey:kProjectDuration];
    [mutableDict setValue:self.internalBaseClassIdentifier forKey:kProjectId];
    [mutableDict setValue:self.category forKey:kProjectCategory];
    [mutableDict setValue:self.city forKey:kProjectCity];
    [mutableDict setValue:[self.contact dictionaryRepresentation] forKey:kProjectContact];
    NSMutableArray *tempArrayForMedia = [NSMutableArray array];
    for (NSObject *subArrayObject in self.media) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForMedia addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForMedia addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForMedia] forKey:kProjectMedia];
    [mutableDict setValue:self.information forKey:kProjectInformation];
    NSMutableArray *tempArrayForDonators = [NSMutableArray array];
    for (NSObject *subArrayObject in self.donators) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForDonators addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForDonators addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForDonators] forKey:kProjectDonators];
    [mutableDict setValue:self.state forKey:kProjectState];
    [mutableDict setValue:[NSNumber numberWithDouble:self.percent] forKey:kProjectPercent];
    [mutableDict setValue:self.video forKey:kProjectVideo];
    [mutableDict setValue:[self.social dictionaryRepresentation] forKey:kProjectSocial];
    [mutableDict setValue:[NSNumber numberWithDouble:self.donatorsCount] forKey:kProjectDonatorsCount];
    [mutableDict setValue:[NSNumber numberWithDouble:self.donated] forKey:kProjectDonated];
    [mutableDict setValue:self.goal forKey:kProjectGoal];
    [mutableDict setValue:self.internalBaseClassDescription forKey:kProjectDescription];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.slug = [aDecoder decodeObjectForKey:kProjectSlug];
    self.status = [aDecoder decodeObjectForKey:kProjectStatus];
    self.title = [aDecoder decodeObjectForKey:kProjectTitle];
    self.timeleft = [aDecoder decodeDoubleForKey:kProjectTimeleft];
    self.creator = [aDecoder decodeObjectForKey:kProjectCreator];
    self.duration = [aDecoder decodeDoubleForKey:kProjectDuration];
    self.internalBaseClassIdentifier = [aDecoder decodeObjectForKey:kProjectId];
    self.category = [aDecoder decodeObjectForKey:kProjectCategory];
    self.city = [aDecoder decodeObjectForKey:kProjectCity];
    self.contact = [aDecoder decodeObjectForKey:kProjectContact];
    self.media = [aDecoder decodeObjectForKey:kProjectMedia];
    self.information = [aDecoder decodeObjectForKey:kProjectInformation];
    self.donators = [aDecoder decodeObjectForKey:kProjectDonators];
    self.state = [aDecoder decodeObjectForKey:kProjectState];
    self.percent = [aDecoder decodeDoubleForKey:kProjectPercent];
    self.video = [aDecoder decodeObjectForKey:kProjectVideo];
    self.social = [aDecoder decodeObjectForKey:kProjectSocial];
    self.donatorsCount = [aDecoder decodeDoubleForKey:kProjectDonatorsCount];
    self.donated = [aDecoder decodeDoubleForKey:kProjectDonated];
    self.goal = [aDecoder decodeObjectForKey:kProjectGoal];
    self.internalBaseClassDescription = [aDecoder decodeObjectForKey:kProjectDescription];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_slug forKey:kProjectSlug];
    [aCoder encodeObject:_status forKey:kProjectStatus];
    [aCoder encodeObject:_title forKey:kProjectTitle];
    [aCoder encodeDouble:_timeleft forKey:kProjectTimeleft];
    [aCoder encodeObject:_creator forKey:kProjectCreator];
    [aCoder encodeDouble:_duration forKey:kProjectDuration];
    [aCoder encodeObject:_internalBaseClassIdentifier forKey:kProjectId];
    [aCoder encodeObject:_category forKey:kProjectCategory];
    [aCoder encodeObject:_city forKey:kProjectCity];
    [aCoder encodeObject:_contact forKey:kProjectContact];
    [aCoder encodeObject:_media forKey:kProjectMedia];
    [aCoder encodeObject:_information forKey:kProjectInformation];
    [aCoder encodeObject:_donators forKey:kProjectDonators];
    [aCoder encodeObject:_state forKey:kProjectState];
    [aCoder encodeDouble:_percent forKey:kProjectPercent];
    [aCoder encodeObject:_video forKey:kProjectVideo];
    [aCoder encodeObject:_social forKey:kProjectSocial];
    [aCoder encodeDouble:_donatorsCount forKey:kProjectDonatorsCount];
    [aCoder encodeDouble:_donated forKey:kProjectDonated];
    [aCoder encodeObject:_goal forKey:kProjectGoal];
    [aCoder encodeObject:_internalBaseClassDescription forKey:kProjectDescription];
}

- (id)copyWithZone:(NSZone *)zone
{
    Project *copy = [[Project alloc] init];
    
    if (copy) {

        copy.slug = [self.slug copyWithZone:zone];
        copy.status = [self.status copyWithZone:zone];
        copy.title = [self.title copyWithZone:zone];
        copy.timeleft = self.timeleft;
        copy.creator = [self.creator copyWithZone:zone];
        copy.duration = self.duration;
        copy.internalBaseClassIdentifier = [self.internalBaseClassIdentifier copyWithZone:zone];
        copy.category = [self.category copyWithZone:zone];
        copy.city = [self.city copyWithZone:zone];
        copy.contact = [self.contact copyWithZone:zone];
        copy.media = [self.media copyWithZone:zone];
        copy.information = [self.information copyWithZone:zone];
        copy.donators = [self.donators copyWithZone:zone];
        copy.state = [self.state copyWithZone:zone];
        copy.percent = self.percent;
        copy.video = [self.video copyWithZone:zone];
        copy.social = [self.social copyWithZone:zone];
        copy.donatorsCount = self.donatorsCount;
        copy.donated = self.donated;
        copy.goal = [self.goal copyWithZone:zone];
        copy.internalBaseClassDescription = [self.internalBaseClassDescription copyWithZone:zone];
    }
    
    return copy;
}


@end

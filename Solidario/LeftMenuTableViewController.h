//
//  LeftMenuTableViewController.h
//  Solidario
//
//  Created by Paula Vasconcelos Gueiros on 1/8/16.
//  Copyright © 2016 Paula Vasconcelos Gueiros. All rights reserved.
//

typedef enum {UserCellIn = 0, CreatedCellIn, DonatedCellIn, FavoriteCellIn, CategoryCellIn, LocationCellIn, ActivityCellIn, CardCellIn, TermsCellIn, AboutCellIn, LogoutCellIn} LeftMenuCellLoggedIn;
typedef enum {LoginCellOut = 0, CategoryCellOut, LocationCellOut, TermsCellOut, AboutCellOut} LeftMenuCellLoggedOut;

#import <UIKit/UIKit.h>

@interface LeftMenuTableViewController : UITableViewController

@property (strong, nonatomic) UIColor *tintColor;

@end

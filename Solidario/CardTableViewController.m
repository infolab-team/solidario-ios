//
//  CardTableViewController.m
//  Solidario
//
//  Created by Pan on 06/03/17.
//  Copyright © 2017 Paula Vasconcelos Gueiros. All rights reserved.
//

#import "CardTableViewController.h"
#import "CardScanViewController.h"
#import "UIView+Utils.h"
#import "NSString+Utils.h"
#import "CustomTextField.h"
#import "Singleton.h"

@interface CardTableViewController ()

@property (weak, nonatomic) IBOutlet UIButton *scanCardButton;

@property (weak, nonatomic) IBOutlet CustomTextField *cardNumber;
@property (weak, nonatomic) IBOutlet UILabel *cardFlagLabel;
@property (weak, nonatomic) IBOutlet UITextField *cardholderName;
@property (weak, nonatomic) IBOutlet CustomTextField *cardValidation;
@property (weak, nonatomic) IBOutlet CustomTextField *cardCVN;

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (strong, nonatomic) NSArray *textFields;
@property (strong, nonatomic) NSArray *acceptedCardFlags;

@end


@implementation CardTableViewController

static const CGFloat defaultViewPadding = 20.f;

#pragma mark - Initialization

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setup];
}

- (void)setup {
    // buttons
    CGFloat viewWidth;
    viewWidth = [[UIScreen mainScreen]bounds].size.width - (2 * defaultViewPadding);
    [self.scanCardButton addDashedBorderWithSize:CGSizeMake(viewWidth, self.scanCardButton.frame.size.height)];
    
    // card flag label - make initially blank
    self.cardFlagLabel.text = @"";
    
    // text fields
    self.cardholderName.delegate = self;
    
    // fetch accepted card flags from singleton
    self.acceptedCardFlags = [Singleton acceptedCardFlags];
    
    self.textFields = @[self.cardNumber, self.cardholderName, self.cardValidation, self.cardCVN];
    for (UITextField *textField in self.textFields) {
        [self addInputAccessoryViewToTextField:textField];
    }
    
    // setup selector for card number text field event editing changed
    [self.cardNumber addTarget:self action:@selector(textFieldDidEndEditing:) forControlEvents:UIControlEventEditingDidEnd];
}

- (void)addInputAccessoryViewToTextField:(UITextField *)textField {
    //Next Text Field Button
    UINavigationBar *inputAccessoryView = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0,
                                                                                            self.view.frame.size.width, INPUT_ACCESSORY_VIEW_HEIGHT)];
    UIBarButtonItem *barButtonNext = [[UIBarButtonItem alloc] initWithTitle:@"Próximo"
                                                                      style:UIBarButtonItemStyleDone target:self
                                                                     action:@selector(nextTextField:)];
    
    UIBarButtonItem *barButtonPrevious = [[UIBarButtonItem alloc] initWithTitle:@"Anterior"
                                                                          style:UIBarButtonItemStyleDone target:self
                                                                         action:@selector(previousTextField:)];
    barButtonNext.tag = textField.tag+1;
    barButtonPrevious.tag = textField.tag-1;
    if (barButtonPrevious.tag < 0) [barButtonPrevious setEnabled:NO];
    if (barButtonNext.tag > self.textFields.count) [barButtonNext setEnabled:NO];
    UINavigationItem *navItem = [[UINavigationItem alloc] initWithTitle:@""];
    navItem.leftBarButtonItem = barButtonPrevious;
    navItem.rightBarButtonItem = barButtonNext;
    [inputAccessoryView setItems:@[navItem]];
    [textField setInputAccessoryView:inputAccessoryView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // setup activityIndicator position
    UIWindow *frontWindow = [[UIApplication sharedApplication] keyWindow];
    [frontWindow addSubview:self.activityIndicator];
    self.activityIndicator.center = frontWindow.center;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    // stop activityIndicator on cases in which it's still animating
    [self.activityIndicator stopAnimating];
    [self.activityIndicator removeFromSuperview];
}


#pragma mark - UITextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidEndEditing:(CustomTextField *)textField {
    if ([textField isEqual:self.cardNumber]) {
        NSString *cardNumber = [NSString plainStringFromString:textField.text];
        CreditCardFlag flag = [NSString creditCardFlagFromNumber:cardNumber];
        if (flag != CreditCardNone) {
            NSString *cardFlagName = [NSString creditCardFlagNameFromType:flag];
            self.cardFlagLabel.text = cardFlagName;
        } else self.cardFlagLabel.text = @"";
    }
}


#pragma mark - UIScrollView Delegate

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [self.view endEditing:YES];
}


#pragma mark - Button Actions

- (void)previousTextField:(UIBarButtonItem *)sender {
    if (sender.tag >= 0 && sender.tag < self.textFields.count) {
        [self focusTextFieldWithTag:sender.tag];
    } else {
        [sender setEnabled:NO];
    }
}

- (void)nextTextField:(UIBarButtonItem *)sender {
    if (sender.tag >= 0 && sender.tag < self.textFields.count) {
        [self focusTextFieldWithTag:sender.tag];
    } else {
        [sender setEnabled:NO];
    }
}

- (void)focusTextFieldWithTag:(NSInteger)tag {
    for (UITextField *textField in self.textFields) {
        if (textField.tag == tag) {
            [textField becomeFirstResponder];
        }
    }
}

- (IBAction)cvnHelp:(id)sender {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Ajuda"
                                                                   message:@"O CVN, ou número de verificação, é um número de três dígitos que geralmente fica na parte de trás do cartão."
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"OK"
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action) {}];
    
    [alert addAction:okButton];
    [self presentViewController:alert animated:YES completion:nil];
}


#pragma mark - Navigation

- (IBAction)save:(id)sender {
    [self.view endEditing:YES];
    [self performInputValidations];
}

- (void)performInputValidations {
    
    // alert setup
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Atenção"
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"OK"
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action) {}];
    [alert addAction:okButton];
    
    
    // mandatory input confirmation
    __block BOOL allFilled = YES;
    [self.textFields enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([((UITextField *)obj).text isEqualToString:@""]) {
            allFilled = NO;
            *stop = YES;
        }
    }];
    if (!allFilled) {
        [alert setMessage:@"Preencha todos os campos antes de prosseguir."];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    
    // valid credit card number
    CreditCardFlag flag = [NSString creditCardFlagFromNumber:[NSString plainStringFromString:self.cardNumber.text]];
    if (flag == CreditCardNone) {
        [alert setMessage:@"Número do cartão inválido."];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    
    // accepted credit card number
    NSNumber *flagNumber = [NSNumber numberWithInteger:flag];
    NSInteger cardFlagIndex = [self.acceptedCardFlags indexOfObject:flagNumber];
    if (cardFlagIndex == NSNotFound) {
        [alert setMessage:@"Bandeira do cartão não suportada."];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    
    // valid expiration date
    if ([self.cardValidation.text length] != self.cardValidation.maxLength) {
        [alert setMessage:@"Vencimento no formato incorreto. Utilize o formato mm/aa."];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    NSDateComponents *dateComponents = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
    NSArray *dateArray = [self.cardValidation.text componentsSeparatedByString:@"/"];
    NSInteger validationYear = [dateArray[1] integerValue] + 2000;
    if (validationYear < [dateComponents year]) {
        [alert setMessage:@"Vencimento anterior à data atual."];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    NSInteger validationMonth = [dateArray[0] integerValue];
    if (validationMonth < 1 || validationMonth > 12) {
        [alert setMessage:@"Data inválida."];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    if (validationYear == [dateComponents year] && validationMonth < [dateComponents month]) {
        [alert setMessage:@"Vencimento anterior à data atual."];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    
    // valid CVN
    if ([self.cardCVN.text length] != self.cardCVN.maxLength) {
        [alert setMessage:@"Número CVN deve ter 3 dígitos."];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    
    [self callSaveCardService];
}

- (void)callSaveCardService {
#warning complete call when ready

    [self.activityIndicator startAnimating];
//    [Service saveCardXForUserY... andCompletion:^(id results, BOOL hasError, NSString *errorMessage) {
    
        [self.activityIndicator stopAnimating];
//        if (hasError) {
//            // alert setup
//            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Erro"
//                                                                           message:nil
//                                                                    preferredStyle:UIAlertControllerStyleAlert];
//            UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"OK"
//                                                               style:UIAlertActionStyleDefault
//                                                             handler:nil];
//            [alert addAction:okButton];
//            
////            if (errorMessage != nil) {
////                [alert setMessage:errorMessage];
////            } else {
//                [alert setMessage:@"Não foi possível salvar os dados do cartão."];
//            }
//            [self presentViewController:alert animated:YES completion:nil];
//            return;
//        } else {
        NSLog(@"💳 Credit Card data saved");
        [self performSegueWithIdentifier:@"unwindToCardList" sender:self];
//        }
//    }];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"modalToCardScan"]) {
        // setup properties for Card Scan View Controller
        UINavigationController *navController = (UINavigationController *)[segue destinationViewController];
        CardScanViewController *cardScanViewController = (CardScanViewController *)[navController topViewController];
        cardScanViewController.userIsSavingCard = YES;
    }
}

- (IBAction)unwindToBeforeCardScan:(UIStoryboardSegue*)unwindSegue {
    
}

@end

//
//  NSString+Utils.h
//  Solidario
//
//  Created by Paula Vasconcelos Gueiros on 31/8/16.
//  Copyright © 2016 Paula Vasconcelos Gueiros. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Utils.h"

@interface NSString(Utils)

+ (NSString *)plainStringFromString:(NSString *)string;
+ (NSString *)maskedStringFromString:(NSString *)string andMask:(TextMask)mask;
+ (NSString *)encodedStringFromString:(NSString *)string;

+ (NSString *)monthShortNameFromInteger:(NSInteger)monthInteger;

+ (BOOL)isValidEmail:(NSString *)email;
+ (BOOL)isValidName:(NSString *)name;
+ (BOOL)isValidCPF:(NSString *)cpf;
//+ (BOOL)isValidCNPJ:(NSString *)cnpj;

+ (CreditCardFlag)creditCardFlagFromNumber:(NSString *)cardNumber;
+ (NSString *)creditCardFlagNameFromType:(CreditCardFlag)flag;

@end

//
//  MainViewController.m
//  Solidario
//
//  Created by Paula Vasconcelos Gueiros on 11/3/16.
//  Copyright © 2016 paulinhavgueiros. All rights reserved.
//

#import "MainViewController.h"
#import "LeftMenuTableViewController.h"
#import "UIColor+Utils.h"

@interface MainViewController ()

@property (strong, nonatomic) LeftMenuTableViewController *leftViewController;
@property (assign, nonatomic) NSUInteger type;

@end


@implementation MainViewController

static const CGFloat menuMargin = 20.f;

#pragma mark - Initialization

- (void)setup {
    
    self.leftViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"leftViewController"];

    [self setLeftViewEnabledWithWidth:LEFT_MENU_WIDTH
                    presentationStyle:LGSideMenuPresentationStyleSlideBelow
                 alwaysVisibleOptions:LGSideMenuAlwaysVisibleOnNone];
    
    self.leftViewStatusBarStyle = UIStatusBarStyleLightContent;
    self.leftViewStatusBarVisibleOptions = LGSideMenuStatusBarVisibleOnAll;
    [self.leftViewController.tableView reloadData];
    [self.leftView addSubview:self.leftViewController.tableView];
}


#pragma mark - Helper Methods

- (void)leftViewWillLayoutSubviewsWithSize:(CGSize)size {
    
    [super leftViewWillLayoutSubviewsWithSize:size];
    
    if (![UIApplication sharedApplication].isStatusBarHidden && (self.type == 2 || self.type == 3))
        self.leftViewController.tableView.frame = CGRectMake(0.f , menuMargin, size.width, size.height-menuMargin);
    else
        self.leftViewController.tableView.frame = CGRectMake(0.f , 0.f, size.width, size.height);
}

@end

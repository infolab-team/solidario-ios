//
//  IntroPageItemViewController.m
//  Solidário
//
//  Created by Allan Alves on 7/4/16.
//  Copyright © 2016 Infosolo. All rights reserved.
//

#import "IntroPageItemViewController.h"
#import "UIView+Utils.h"

@interface IntroPageItemViewController ()

@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (weak, nonatomic) IBOutlet UIImageView *stepImageView;

@end

@implementation IntroPageItemViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setup];
}

- (void)setup {
    self.view.backgroundColor = [UIColor clearColor];
    
    _descriptionLabel.text = _pageDescription;
    if (![_pageTitle isEqualToString:@""]) {

        UIFont *boldFont = [UIFont fontWithName:@"HelveticaNeue-Bold" size:_descriptionLabel.font.pointSize];
        NSDictionary *attributes = @{NSForegroundColorAttributeName: _descriptionLabel.textColor,
                                     NSFontAttributeName: boldFont};
        NSMutableAttributedString *title = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n\n", _pageTitle] attributes:attributes];
        [title appendAttributedString:_descriptionLabel.attributedText];
        _descriptionLabel.attributedText = title;
    }
    _stepImageView.image = [UIImage imageNamed:_pageImageName];
}

@end

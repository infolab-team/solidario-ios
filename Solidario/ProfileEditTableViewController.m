//
//  ProfileEditTableViewController.m
//  Solidario
//
//  Created by Paula Vasconcelos Gueiros on 30/8/16.
//  Copyright © 2016 Paula Vasconcelos Gueiros. All rights reserved.
//

#import "ProfileEditTableViewController.h"
#import "NSString+Utils.h"
#import "UIView+Utils.h"
#import "User.h"
#import "Singleton.h"
#import "Service.h"
#import "UIColor+Utils.h"
#import "UIImage+Utils.h"

#import <SDWebImage/UIButton+WebCache.h>

@import Photos;

@interface ProfileEditTableViewController ()

@property (weak, nonatomic) IBOutlet UIButton *userPicture;
@property (weak, nonatomic) IBOutlet UITextField *name;
@property (weak, nonatomic) IBOutlet UITextField *surname;
@property (weak, nonatomic) IBOutlet UITextField *email;
@property (weak, nonatomic) IBOutlet UITextField *updatedPassword;
@property (weak, nonatomic) IBOutlet UITextField *updatedPasswordConfirm;

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (strong, nonatomic) NSArray *textFields;
@property (strong, nonatomic) UIImage *bufferedImage;
@property (strong, nonatomic) User *loggedUser;


/*!
 * @brief BOOL used to verify if user has picture, in order to display user picture menu options
 */
@property BOOL userHasPicture;
@property BOOL pictureHasBeenChanged;
@property BOOL photoLibrarySource;
@property BOOL needsAuthentication;

@end

@implementation ProfileEditTableViewController

static const CGFloat userPictureSquareSize = 500.f;

#pragma mark - Initialization

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setup];
}

- (void)setup {
    // current logged user
    self.loggedUser = [Singleton loggedUser];
    
    // text field delegates
    self.name.delegate = self;
    self.surname.delegate = self;
    self.email.delegate = self;
    self.updatedPassword.delegate = self;
    self.updatedPasswordConfirm.delegate = self;
    
    // properties
    self.needsAuthentication = NO;

    // data in text fields
    [self setupData];
}

- (void)setupData {
    self.name.text = self.loggedUser.name;
    self.surname.text = self.loggedUser.surname;
    if ([Singleton isFacebookUser]) {
        self.email.enabled = NO;
        self.email.placeholder = self.loggedUser.email;
        self.updatedPassword.tag = 2;
        self.updatedPasswordConfirm.tag = 3;
        self.textFields = @[self.name, self.surname, self.updatedPassword, self.updatedPasswordConfirm];
    } else {
        self.email.text = self.loggedUser.email;
        self.textFields = @[self.name, self.surname, self.email, self.updatedPassword, self.updatedPasswordConfirm];
    }
    for (UITextField *textField in self.textFields) {
        [self addInputAccessoryViewToTextField:textField];
    }
    
    // user profile picture
    if (![self.loggedUser.image isEqualToString:@""]) {
        NSURL *imageURL = [NSURL URLWithString:[NSString encodedStringFromString:self.loggedUser.image]];
        
        // check if image src is base64 or url
        if ([self.loggedUser.image hasPrefix:@"data:image"]) {
            NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
            [self.userPicture setBackgroundImage:[UIImage imageWithData:imageData] forState:UIControlStateNormal];
            self.bufferedImage = [self.userPicture backgroundImageForState:UIControlStateNormal];
            self.userHasPicture = YES;
        } else {
            [self.userPicture sd_setBackgroundImageWithURL:imageURL forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"defaultUser"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                if (!error) {
                    self.bufferedImage = [self.userPicture backgroundImageForState:UIControlStateNormal];
                    self.userHasPicture = YES;
                } else {
                    self.bufferedImage = nil;
                    self.userHasPicture = NO;
                }
            }];
        }
    } else {
        [self.userPicture setBackgroundImage:[UIImage imageNamed:@"defaultUser"] forState:UIControlStateNormal];
        self.bufferedImage = nil;
        self.userHasPicture = NO;
    }
    
    // for deciding if picture should be sent to server when saving
    self.pictureHasBeenChanged = NO;
}

- (void)addInputAccessoryViewToTextField:(UITextField *)textField {
    //Next Text Field Button
    UINavigationBar *inputAccessoryView = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0,
                                                                                            self.view.frame.size.width, INPUT_ACCESSORY_VIEW_HEIGHT)];
    UIBarButtonItem *barButtonNext = [[UIBarButtonItem alloc] initWithTitle:@"Próximo"
                                                                      style:UIBarButtonItemStyleDone target:self
                                                                     action:@selector(nextTextField:)];
    
    UIBarButtonItem *barButtonPrevious = [[UIBarButtonItem alloc] initWithTitle:@"Anterior"
                                                                          style:UIBarButtonItemStyleDone target:self
                                                                         action:@selector(previousTextField:)];
    barButtonNext.tag = textField.tag+1;
    barButtonPrevious.tag = textField.tag-1;
    if (barButtonPrevious.tag < 0) [barButtonPrevious setEnabled:NO];
    if (barButtonNext.tag > self.textFields.count) [barButtonNext setEnabled:NO];
    UINavigationItem *navItem = [[UINavigationItem alloc] initWithTitle:@""];
    navItem.leftBarButtonItem = barButtonPrevious;
    navItem.rightBarButtonItem = barButtonNext;
    [inputAccessoryView setItems:@[navItem]];
    [textField setInputAccessoryView:inputAccessoryView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // setup activityIndicator position
    UIWindow *frontWindow = [[UIApplication sharedApplication] keyWindow];
    [frontWindow addSubview:self.activityIndicator];
    self.activityIndicator.center = frontWindow.center;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self.activityIndicator stopAnimating];
    [self.activityIndicator removeFromSuperview];
}


#pragma mark - Button Actions

- (IBAction)changePicture:(id)sender {
#warning temporary: only until picture from facebook retrieval fix
    if ([Singleton isFacebookUser]) {
        return;
    }

    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleActionSheet];
    if (self.userHasPicture) {
        UIAlertAction *removeButton = [UIAlertAction actionWithTitle:@"Remover Foto"
                                                               style:UIAlertActionStyleDestructive
                                                             handler:^(UIAlertAction * action) {
                                                                 NSLog(@"❌ Remove profile picture");
                                                                 [self removeUserPictureInView];
                                                             }];
        [alert addAction:removeButton];
    }
    UIAlertAction *useLast = [UIAlertAction actionWithTitle:@"Usar a Última Foto"
                                                      style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction * action) {
                                                        NSLog(@"🍄 Use last picture in Library");
                                                        [self selectLastPictureInLibrary];
                                                    }];
    UIAlertAction *take = [UIAlertAction actionWithTitle:@"Tirar Foto"
                                                   style:UIAlertActionStyleDefault
                                                 handler:^(UIAlertAction * action) {
                                                     NSLog(@"📸 Take picture");
                                                     self.photoLibrarySource = NO;
                                                     [self openPicker];
                                                 }];
    UIAlertAction *choose = [UIAlertAction actionWithTitle:@"Escolher na Biblioteca"
                                                     style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action) {
                                                       NSLog(@"🖼 Choose from Library");
                                                       self.photoLibrarySource = YES;
                                                       [self openPicker];
                                                   }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancelar"
                                                     style:UIAlertActionStyleCancel
                                                   handler:^(UIAlertAction * action) {}];
    [alert addAction:useLast];
    [alert addAction:take];
    [alert addAction:choose];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:^{}];
}

- (void)selectLastPictureInLibrary {
    
    PHFetchOptions *fetchOptions = [[PHFetchOptions alloc] init];
    fetchOptions.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:YES]];
    PHFetchResult *fetchResult = [PHAsset fetchAssetsWithMediaType:PHAssetMediaTypeImage options:fetchOptions];
    PHAsset *lastAsset = [fetchResult lastObject];
    [[PHImageManager defaultManager] requestImageDataForAsset:lastAsset
                                                      options:PHImageRequestOptionsVersionCurrent
                                                resultHandler:^(NSData * _Nullable imageData, NSString * _Nullable dataUTI, UIImageOrientation orientation, NSDictionary * _Nullable info) {
        
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        UIImage *helperImage = [UIImage imageWithData:imageData];
                                                        helperImage = [UIImage cropToSquareWithImage:helperImage];
                                                        self.bufferedImage = [UIImage shrunkenImageWithImage:helperImage toLargerSideSize:userPictureSquareSize];
                                                        [self.userPicture setBackgroundImage:self.bufferedImage forState:UIControlStateNormal];
                                                        self.userHasPicture = YES;
                                                        self.pictureHasBeenChanged = YES;
                                                    });
                                                }];
}

- (void)openPicker {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    
    if (self.photoLibrarySource) {
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        picker.modalPresentationStyle = UIModalPresentationCurrentContext;
        [picker.navigationBar setTranslucent:NO];
    } else {
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    }
    
    [self presentViewController:picker animated:YES completion:nil];
}

- (void)previousTextField:(UIBarButtonItem *)sender {
    if (sender.tag >= 0 && sender.tag < self.textFields.count) {
        [self focusTextFieldWithTag:sender.tag];
    } else {
        [sender setEnabled:NO];
    }
}

- (void)nextTextField:(UIBarButtonItem *)sender {
    if (sender.tag >= 0 && sender.tag < self.textFields.count) {
        [self focusTextFieldWithTag:sender.tag];
    } else {
        [sender setEnabled:NO];
    }
}

- (void)focusTextFieldWithTag:(NSInteger)tag {
    for (UITextField *textField in self.textFields) {
        if (textField.tag == tag) {
            [textField becomeFirstResponder];
        }
    }
}


#pragma mark - Navigation

- (IBAction)save:(id)sender {
    [self.view endEditing:YES];
    NSLog(@"📤 Checking new user data");
    
    if ([self.updatedPassword.text length] > 0) {
        self.needsAuthentication = YES;
    }
    if (![Singleton isFacebookUser] && ![self.email.text isEqualToString:self.loggedUser.email]) {
        self.needsAuthentication = YES;
    }
    
    [self performInputValidations];
}

- (void)performInputValidations {
    
    // alert setup
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Atenção"
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"OK"
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action) {}];
    [alert addAction:okButton];

    // valid name
    if (![NSString isValidName:self.name.text]) {
        [alert setMessage:@"Nome inválido."];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    
    // valid surname
    if (![NSString isValidName:self.surname.text]) {
        [alert setMessage:@"Sobrenome inválido."];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    
    // if not facebook user, check valid email
    if (![Singleton isFacebookUser]) {
        if (![NSString isValidEmail:self.email.text]) {
            [alert setMessage:@"Email inválido."];
            [self presentViewController:alert animated:YES completion:nil];
            return;
        }
    }
    
    // valid new password length
    if ([self.updatedPassword.text length] > 0 && [self.updatedPassword.text length] < 6) {
        [alert setMessage:@"Nova senha deve ter pelo menos seis caracteres."];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    
    // equal passwords
    if (![self.updatedPassword.text isEqualToString:self.updatedPasswordConfirm.text]) {
        [alert setMessage:@"Confirmação de nova senha diferente da nova senha."];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    
    if (self.needsAuthentication) {
        [self confirmWithCurrentPassword];
    } else {
        [self proceedWithSaveUsingCurrentPassword:nil];
    }
}

- (void)confirmWithCurrentPassword {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Confirmação"
                                                                   message:@"Informe a senha atual"
                                                            preferredStyle:UIAlertControllerStyleAlert];

    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
         textField.placeholder = @"Senha";
         textField.secureTextEntry = YES;
     }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancelar"
                                                     style:UIAlertActionStyleCancel
                                                   handler:^(UIAlertAction *action) {}];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK"
                                                 style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction *action) {
                                                   [self proceedWithSaveUsingCurrentPassword:alert.textFields[0].text];
                                               }];
    [alert addAction:cancel];
    [alert addAction:ok];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)proceedWithSaveUsingCurrentPassword:(NSString *)currentPassword {
    
    [self.activityIndicator startAnimating];
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    
    // check if email has changed
    NSString *emailText = ([self.email.text isEqualToString:self.loggedUser.email]) ? nil : self.email.text;
    NSString *newPasswordText = ([self.updatedPassword.text isEqualToString:@""]) ? nil: self.updatedPassword.text;
    NSString *currentPasswordText = currentPassword;
    NSString *imageBase64Text = (self.pictureHasBeenChanged) ?
                                    ((self.userHasPicture) ?
                                        [UIImagePNGRepresentation(self.bufferedImage) base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed]
                                        : @"")
                                    : nil;
    
    [Service updateUserWithName:self.name.text surname:self.surname.text email:emailText newPassword:newPasswordText currentPassword:currentPasswordText imageString:imageBase64Text andCompletion:^(id results, BOOL hasError, NSString *errorMessage) {

        if (hasError) {
            // alert setup
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Erro"
                                                                           message:nil
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"OK"
                                                               style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction * action) {}];
            [alert addAction:okButton];
            
//            if (errorMessage != nil) {
//                [alert setMessage:errorMessage];
//            } else {
                [alert setMessage:@"Não foi possível alterar os dados."];
//            }
            [self.activityIndicator stopAnimating];
            [[UIApplication sharedApplication] endIgnoringInteractionEvents];
            [self presentViewController:alert animated:YES completion:nil];
            return;
        } else {
            NSLog(@"✅ User data saved");
            
            [Singleton updateLoggedUserWithUser:(UserUpdated *)results];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"userHasBeenUpdated" object:nil];
            [self.activityIndicator stopAnimating];
            [[UIApplication sharedApplication] endIgnoringInteractionEvents];
            
            UIAlertController *successAlert = [UIAlertController alertControllerWithTitle:@"Sucesso"
                                                                                  message:@"Perfil alterado com sucesso"
                                                                           preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *successOkButton = [UIAlertAction actionWithTitle:@"OK"
                                                                      style:UIAlertActionStyleDefault
                                                                    handler:^(UIAlertAction * action) {
                                                                        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
                                                                    }];
            [successAlert addAction:successOkButton];
            [self presentViewController:successAlert animated:YES completion:nil];
        }
    }];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"unwindFromEditToRoot"]) {
        [self.view endEditing:YES];
    }
}


#pragma mark - UIImagePickerController Delegate and Helpers

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *helperImage = info[UIImagePickerControllerEditedImage];
    helperImage = [UIImage cropToSquareWithImage:helperImage];
    self.bufferedImage = [UIImage shrunkenImageWithImage:helperImage toLargerSideSize:userPictureSquareSize];
    
    if (!self.bufferedImage) {
        NSLog(@"❌ Error selecting picture");
        return;
    }
    
    [self.userPicture setBackgroundImage:self.bufferedImage forState:UIControlStateNormal];
    self.userHasPicture = YES;
    self.pictureHasBeenChanged = YES;
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)removeUserPictureInView {
    
    [SDWebImageManager.sharedManager.imageCache clearMemory];
    [self.userPicture setBackgroundImage:[UIImage imageNamed:@"defaultUser"] forState:UIControlStateNormal];
    self.bufferedImage = nil;
    self.userHasPicture = NO;
    self.pictureHasBeenChanged = YES;
}


#pragma mark - UITextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}


#pragma mark - UIScrollView Delegate

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [self.view endEditing:YES];
}
    
    
#pragma mark - UITableView Delegate
    
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, [tableView sectionHeaderHeight])];
    [view setBackgroundColor:[UIColor defaultColor:DefaultColorMainBackground]];
    
    return view;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ([Singleton isFacebookUser] && section == 1) {
        return 1;
    }
    
    return [super tableView:tableView numberOfRowsInSection:section];
}
    
@end




//
//  UnwindSegueDissolve.m
//  Solidario
//
//  Created by Pan on 19/06/17.
//  Copyright © 2017 Paula Vasconcelos Gueiros. All rights reserved.
//

#import "UnwindSegueDissolve.h"
#import "CompleteCardInfoViewController.h"

@implementation UnwindSegueDissolve

- (void)perform {
    CGFloat screenWidth = UIScreen.mainScreen.bounds.size.width;
    CGFloat screenHeight = UIScreen.mainScreen.bounds.size.height;
    
    CGFloat yOffset = self.sourceViewController.navigationController.navigationBar.frame.size.height + 20.f;
    
    // add destination tableview to current view, initially transparent
    UIView *destinationView = self.destinationViewController.view;
    if (![self.destinationViewController isKindOfClass:[CompleteCardInfoViewController class]]) {
        [destinationView setFrame:CGRectMake(0, yOffset, screenWidth, screenHeight)];
    }
    [destinationView setAlpha:0];
    
    // add views to window
    UIWindow *window = UIApplication.sharedApplication.keyWindow;
    [window insertSubview:destinationView aboveSubview:self.sourceViewController.view];
    
    // animate views
    [UIView animateWithDuration: 0.3f animations:^{
        [destinationView setAlpha:1];
        
    } completion:^(BOOL finished) {
        [self.sourceViewController.navigationController popViewControllerAnimated:NO];
    }];
}

@end

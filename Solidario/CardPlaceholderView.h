//
//  CardPlaceholderView.h
//  Solidario
//
//  Created by Pan on 12/12/16.
//  Copyright © 2016 Paula Vasconcelos Gueiros. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CardPlaceholderView : UIView

@property (weak, nonatomic) IBOutlet UIView *topHiddenView;
@property (weak, nonatomic) IBOutlet UIView *imageContainerView;
@property (weak, nonatomic) IBOutlet UIView *bottomHiddenView;
@property (weak, nonatomic) IBOutlet UIImageView *pigImageView;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet UIButton *retryButton;
    
@end

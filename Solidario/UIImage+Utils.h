//
//  UIImage+Utils.h
//  Solidario
//
//  Created by Pan on 27/12/16.
//  Copyright © 2016 Paula Vasconcelos Gueiros. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage(Utils)

+ (UIImage *)imageWithColor:(UIColor *)color;
+ (UIImage *)cropToSquareWithImage:(UIImage *)image;
+ (UIImage *)shrunkenImageWithImage:(UIImage *)largeImage toLargerSideSize:(CGFloat)largerSideSize;

@end

//
//  AntifraudData.h
//
//  Created by Paula Vasconcelos on 05/05/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AntifraudData : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double numero;
@property (nonatomic, strong) NSString *rua;
@property (nonatomic, strong) NSString *complemento;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *bairro;
@property (nonatomic, strong) NSString *cep;
@property (nonatomic, strong) NSString *cpf;
@property (nonatomic, strong) NSString *telefone;
@property (nonatomic, strong) NSString *ddd;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end

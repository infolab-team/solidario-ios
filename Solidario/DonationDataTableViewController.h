//
//  DonationDataTableViewController.h
//  Solidario
//
//  Created by Pan on 28/11/16.
//  Copyright © 2016 Paula Vasconcelos Gueiros. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Donation.h"
#import "DonationGoalView.h"
#import "DonationValueView.h"

@class ProjectShort;

@interface DonationDataTableViewController : UITableViewController <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet DonationGoalView *donationGoalView;
@property (weak, nonatomic) IBOutlet DonationValueView *donationValueView;

@property (strong, nonatomic) ProjectShort *project;
@property (strong, nonatomic) Donation *donation;
@property (strong, nonatomic) UIImage *mainImage;

@end

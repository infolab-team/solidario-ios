//
//  RegisterTableViewController.h
//  Solidario
//
//  Created by Paula Vasconcelos Gueiros on 19/8/16.
//  Copyright © 2016 Paula Vasconcelos Gueiros. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegisterTableViewController : UITableViewController <UITextFieldDelegate>

@end

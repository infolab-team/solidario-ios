//
//  ActionButton.h
//  BelievePrototype
//
//  Created by Allan Alves on 9/30/15.
//  Copyright © 2015 Infosolo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ActionButton : UIButton

- (void)startLoadingAnimated;
- (void)stopLoading;

@end

//
//  AccountViewController.m
//  Solidario
//
//  Created by Paula Vasconcelos Gueiros on 18/8/16.
//  Copyright © 2016 Paula Vasconcelos Gueiros. All rights reserved.
//

#import "AccountViewController.h"
#import "MainViewController.h"
#import "CategoryViewController.h"
#import "ActionButton.h"
#import "Service.h"
#import "User.h"
#import "Singleton.h"

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface AccountViewController ()

@property (weak, nonatomic) IBOutlet ActionButton *facebookLoginButton;

@end

@implementation AccountViewController


#pragma mark - Initialization

- (void)viewDidLoad {
    [super viewDidLoad];
}

    
#pragma mark - Facebook Login
    
- (IBAction)loginWithFacebook:(id)sender {
    [self.facebookLoginButton startLoadingAnimated];
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login setLoginBehavior:FBSDKLoginBehaviorSystemAccount];
    [login logInWithReadPermissions:@[@"public_profile",@"email"] fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        if (error) {
            [self.facebookLoginButton stopLoading];
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Erro"
                                                                           message:[error localizedDescription]
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"OK"
                                                               style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction * action) {}];
            [alert addAction:okButton];
            [self presentViewController:alert animated:YES completion:nil];
        } else if (result.isCancelled) {
            [self.facebookLoginButton stopLoading];
        } else {
            [self facebookEngine];
        }
    }];
}

- (void)facebookEngine {
    
    if ([FBSDKAccessToken currentAccessToken]) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Erro"
                                                                       message:nil
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"OK"
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * action) {}];
        [alert addAction:okButton];
        
        /// get user data
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"id, first_name, last_name, email"}] startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
            
             if (!error) {
                 
                 [Service loginWithFacebookAccessToken:[[FBSDKAccessToken currentAccessToken] tokenString] name:[result objectForKey:@"first_name"] lastName:[result objectForKey:@"last_name"] email:[result objectForKey:@"email"] andCompletion:^(id results, BOOL hasError, NSString *errorMessage) {

                     [self.facebookLoginButton stopLoading];
                     
                     if (hasError) {
//                         if (errorMessage != nil) {
//                             [alert setMessage:errorMessage];
//                         } else {
                             [alert setMessage:@"Não foi possível efetuar o login."];
//                         }
                         [self presentViewController:alert animated:YES completion:nil];
                         return;
                     } else {
                         // save User object to Singleton
//                         [Singleton loginWithUser:(User *)results withFacebookEnabled:YES andPersistent:YES];
                         NSLog(@"👤 Facebook Login successful");
                         [self checkPictureOfUser:(User *)results];
                     }
                 }];
             } else {
                 [self.facebookLoginButton stopLoading];
                 [alert setMessage:@"Não foi possível efetuar o login."];
                 [self presentViewController:alert animated:YES completion:nil];
             }
         }];
    }
}
    
    
#pragma mark - Navigation
    
- (IBAction)unwindToAccount:(UIStoryboardSegue*)unwindSegue {
    
}
    
- (IBAction)enterWithoutAccount:(id)sender {
    [self proceedAfterAccessNoUser];
}
    
- (void)proceedAfterAccessNoUser {
    if ([Singleton isDonating]) {
        [self performSegueWithIdentifier:@"unwindToBeforeDonationValue" sender:self];
    } else {
        [self instantiateSettingsWithViewControllerIdentifier:@"settingsDonationNavigationController"];
    }
}
    
- (void)checkPictureOfUser:(User *)user {
    
    if (user.image == nil) {
        if ([FBSDKAccessToken currentAccessToken]) {
            [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{ @"fields" : @"id,name,picture.width(500).height(500)"}]startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                if (!error) {
                    NSString *imageStringOfLoginUser = [[[result valueForKey:@"picture"] valueForKey:@"data"] valueForKey:@"url"];
                    user.image = imageStringOfLoginUser;
                    [self proceedAfterFacebookLoginWithUser:user];
                }
            }];
        }
    } else {
        
    }
}

- (void)proceedAfterFacebookLoginWithUser:(User *)user {
    
    [Singleton loginWithUser:user withFacebookEnabled:YES andPersistent:YES];
    
    if ([Singleton isDonating]) {
        [self performSegueWithIdentifier:@"unwindToBeforeDonationValue" sender:self];
    } else {
        [self instantiateSettingsWithViewControllerIdentifier:@"settingsNavigationController"];
    }
}

- (void)instantiateSettingsWithViewControllerIdentifier:(NSString *)viewControllerIdentifier {
    NSString *storyboardIdentifier = @"Settings";
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardIdentifier bundle:nil];
    UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:viewControllerIdentifier];
    
    if ([viewControllerIdentifier isEqualToString:@"settingsDonationNavigationController"]) {
        CategoryViewController *categoryViewController = (CategoryViewController *)[navigationController topViewController];
        categoryViewController.showCloseButton = YES;
    }
    
    [self presentViewController:navigationController animated:YES completion:nil];
}

@end

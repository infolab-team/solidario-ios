//
//  StateTableViewCell.m
//  Solidario
//
//  Created by Paula Vasconcelos Gueiros on 23/8/16.
//  Copyright © 2016 Paula Vasconcelos Gueiros. All rights reserved.
//

#import "StateTableViewCell.h"

@implementation StateTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end

//
//  OptionViewController.m
//  Solidario
//
//  Created by Paula Vasconcelos Gueiros on 22/8/16.
//  Copyright © 2016 Paula Vasconcelos Gueiros. All rights reserved.
//

#import "OptionViewController.h"
#import "MainViewController.h"
#import "Singleton.h"
#import "Service.h"

@interface OptionViewController ()

@property (weak, nonatomic) IBOutlet UIButton *donateButton;
@property (weak, nonatomic) IBOutlet UIButton *manageButton;
@property (weak, nonatomic) IBOutlet UIImageView *donateSelectedCover;
@property (weak, nonatomic) IBOutlet UIImageView *manageSelectedCover;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@end

@implementation OptionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}


#pragma mark - Button Actions

- (IBAction)donate:(id)sender {
    if (self.donateButton.selected) {
        return;
    }
    [self invertAll];
}

- (IBAction)manage:(id)sender {
    if (self.manageButton.selected) {
        return;
    }
    [self invertAll];
}

- (void)invertAll {
    
    self.donateButton.selected = !self.donateButton.selected;
    self.manageButton.selected = !self.manageButton.selected;
    
    self.donateSelectedCover.hidden = !self.donateSelectedCover.hidden;
    self.manageSelectedCover.hidden = !self.manageSelectedCover.hidden;
}


#pragma mark - Navigation

- (IBAction)advance:(id)sender {
    NSLog(@"➡️ User pressed advance button");
    if (self.donateButton.selected) {
        [self performSegueWithIdentifier:@"pushToAreas" sender:self];
    } else {
        // call Category and States services and select all, since user wont go through category and states selection pages
        [self callServices];
    }
}

- (void)instantiateMain {
    NSString *storyboardIdentifier = @"Main";
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardIdentifier bundle:nil];
    
    UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"mainNavigationController"];
    MainViewController *mainViewController = [storyboard instantiateViewControllerWithIdentifier:@"mainViewController"];
    mainViewController.rootViewController = navigationController;
    [mainViewController setup];
    
    [Singleton setUpdatingTimeline:YES];
    
    UIWindow *window = [UIApplication sharedApplication].delegate.window;
    window.rootViewController = mainViewController;
    [UIView transitionWithView:window
                      duration:0.5
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:nil
                    completion:nil];
}


#pragma mark - Helper Methods

- (void)callServices {
    [self.activityIndicator startAnimating];
    [self callCategoriesService];
}

- (void)callCategoriesService {
    // call Categories service
    [Service getAllCagetoriesWithCompletion:^(id results, BOOL hasError, NSString *errorMessage) {
        if (hasError) {
            [self.activityIndicator stopAnimating];
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Erro"
                                                                           message:nil
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"OK"
                                                               style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction * action) {}];
            [alert addAction:okButton];
            
//            if (errorMessage != nil) {
//                [alert setMessage:errorMessage];
//            } else {
                [alert setMessage:@"Erro no serviço.  Tente novamente mais tarde."];
//            }
            [self presentViewController:alert animated:YES completion:nil];
        } else {
            // select all categories and save settings to Singleton
            
            NSArray *categories = (NSArray *)results;
            [categories enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                Category *category = (Category *)obj;
                [category setSelected:YES];
            }];
            [Singleton saveCategories:categories];
            NSLog(@"🤡 Categories retrieval and settings definition successful");
            
            // call States service
            [self callStatesService];
        }
        return;
    }];
}

- (void)callStatesService {
    [Service getAllStatesWithCompletion:^(id results, BOOL hasError, NSString *errorMessage) {
        [self.activityIndicator stopAnimating];
        
        if (hasError) {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Erro"
                                                                           message:nil
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"OK"
                                                               style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction * action) {}];
            [alert addAction:okButton];
            
//            if (errorMessage != nil) {
//                [alert setMessage:errorMessage];
//            } else {
                [alert setMessage:@"Erro no serviço.  Tente novamente mais tarde."];
//            }
            [self presentViewController:alert animated:YES completion:nil];
            return;
        } else {
            // select all states and save settings to Singleton
            
            NSArray *states = (NSArray *)results;
            [states enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                State *state = (State *)obj;
                [state setSelected:YES];
            }];
            [Singleton saveStates:states];
            NSLog(@"🇧🇷 States retrieval and settings definition successful");

            // proceed with app flow
            [self instantiateMain];
        }
    }];
}

@end

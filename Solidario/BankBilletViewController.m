//
//  BankBilletViewController.m
//  Solidario
//
//  Created by Pan on 16/05/17.
//  Copyright © 2017 Paula Vasconcelos Gueiros. All rights reserved.
//

#import "BankBilletViewController.h"
#import "TUSafariActivity.h"

@interface BankBilletViewController ()

@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@end

@implementation BankBilletViewController

#pragma mark - Initialization

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setup];
}

- (void)setup {
    // load billet URL
    NSURL *billetURL = [NSURL URLWithString:self.billetStringURL];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:billetURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:30];
    
    self.webView.delegate = self;
    [self.webView loadRequest:urlRequest];
}


#pragma mark - UIWebView Delegate

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [self.activityIndicator stopAnimating];
}

- (void)updateZoomToAspectToFit:(UIWebView*)webView { // disables zoom. use when needed
    CGSize contentSize = webView.scrollView.contentSize;
    CGSize viewSize = webView.bounds.size;
    
    float rw = viewSize.width / contentSize.width;
    
    webView.scrollView.minimumZoomScale = rw;
    webView.scrollView.maximumZoomScale = rw;
    webView.scrollView.zoomScale = rw;
}


#pragma mark - Button Actions

- (IBAction)openSharingActivity:(id)sender {

    NSString *sharingString = @"Boleto de Doação - Solidário Brasil";
    NSURL *sharingURL = [NSURL URLWithString:self.billetStringURL];
    TUSafariActivity *activity = [[TUSafariActivity alloc] init];
    
    UIActivityViewController *activityViewController =
    [[UIActivityViewController alloc] initWithActivityItems:@[sharingString, sharingURL]
                                      applicationActivities:@[activity]];
    [self.navigationController presentViewController:activityViewController
                                            animated:YES
                                          completion:nil];
}

@end

//
//  ProjectDetailVideoCell.h
//  Solidario
//
//  Created by Pan on 05/05/17.
//  Copyright © 2017 Paula Vasconcelos Gueiros. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YTPlayerView.h"

@interface ProjectDetailVideoCell : UITableViewCell

@property (weak, nonatomic) IBOutlet YTPlayerView *playerView;

@end

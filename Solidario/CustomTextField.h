//
//  CustomTextField.h
//  Blue SP
//
//  Created by Allan on 5/6/16.
//  Copyright © 2016 IS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Utils.h"

@interface CustomTextField : UITextField <UITextFieldDelegate>

@property (nonatomic) IBInspectable NSInteger maxLength;
@property (nonatomic) IBInspectable NSInteger maskType;

@end

//
//  ProjectCardViewController.h
//  Solidario
//
//  Created by Paula Vasconcelos Gueiros on 7/11/16.
//  Copyright © 2016 Paula Vasconcelos Gueiros. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProjectCardView.h"

@interface ProjectCardViewController : UIViewController

@property (strong, nonatomic) IBOutlet ProjectCardView *superview;

@end

//
//  UpdateEntryNotifyTableViewCell.h
//  Solidario
//
//  Created by Pan on 16/02/17.
//  Copyright © 2017 Paula Vasconcelos Gueiros. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UpdateEntryNotifyTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *notifyAllLabel;
@property (weak, nonatomic) IBOutlet UISwitch *notifySwitch;

@end

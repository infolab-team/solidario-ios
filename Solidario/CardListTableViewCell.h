//
//  CardListTableViewCell.h
//  Solidario
//
//  Created by Pan on 22/02/17.
//  Copyright © 2017 Paula Vasconcelos Gueiros. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CardListTableViewCell : UITableViewCell
    
@property (weak, nonatomic) IBOutlet UIImageView *flagImageView;
@property (weak, nonatomic) IBOutlet UILabel *cardNumberEndingLabel;
@property (weak, nonatomic) IBOutlet UILabel *isMainLabel;

@end

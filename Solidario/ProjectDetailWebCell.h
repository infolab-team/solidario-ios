//
//  ProjectDetailWebCell.h
//  Solidario
//
//  Created by Pan on 06/12/16.
//  Copyright © 2016 Paula Vasconcelos Gueiros. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProjectDetailWebCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end

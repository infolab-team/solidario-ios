//
//  DonationActivityTableViewCell.h
//  Solidario
//
//  Created by Pan on 26/06/17.
//  Copyright © 2017 Paula Vasconcelos Gueiros. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DonationActivityTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *topLineView;
@property (weak, nonatomic) IBOutlet UILabel *activityDateDayLabel;
@property (weak, nonatomic) IBOutlet UILabel *activityDateMonthLabel;
@property (weak, nonatomic) IBOutlet UILabel *activityDateTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *activityTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *activityDescriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *activityStatusLabel;

@end

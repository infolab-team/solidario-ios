//
//  ProjectDetailFirstCell.m
//  Solidario
//
//  Created by Pan on 05/12/16.
//  Copyright © 2016 Paula Vasconcelos Gueiros. All rights reserved.
//

#import "ProjectDetailFirstCell.h"
#import "UIColor+Utils.h"
#import "BannerCollectionViewCell.h"

@implementation ProjectDetailFirstCell

static const CGFloat pageControlScale = 1.5f;

#pragma mark - Initialization

- (void)layoutSubviews {

    // setup collection view
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    [flowLayout setMinimumInteritemSpacing:0.0f];
    [flowLayout setMinimumLineSpacing:0.0f];
    [self.bannerCollectionView setPagingEnabled:YES];
    [self.bannerCollectionView setCollectionViewLayout:flowLayout];
    
    // setup page control
    [self.bannerPageControl setCurrentPageIndicatorTintColor:[UIColor defaultColor:DefaultColorIntroSelectedPageDot]];
    self.bannerPageControl.transform = CGAffineTransformMakeScale(pageControlScale, pageControlScale);
}



@end

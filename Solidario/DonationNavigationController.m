//
//  DonationNavigationController.m
//  Solidario
//
//  Created by Pan on 09/06/17.
//  Copyright © 2017 Paula Vasconcelos Gueiros. All rights reserved.
//

#import "DonationNavigationController.h"
#import "DonationValueTableViewController.h"
#import "DonationDataTableViewController.h"
#import "AntifraudDataTableViewController.h"
#import "DonationFailViewController.h"

#import "UnwindSegueToDonationValue.h"
#import "UnwindSegueDissolve.h"

@implementation DonationNavigationController

#pragma mark - Initialization

- (void)viewDidLoad {
    [super viewDidLoad];
}


#pragma mark - Navigation

- (UIStoryboardSegue *)segueForUnwindingToViewController:(UIViewController *)toViewController fromViewController:(UIViewController *)fromViewController identifier:(NSString *)identifier {
    
    if ([fromViewController isKindOfClass:[DonationDataTableViewController class]]) {
        return [[UnwindSegueToDonationValue alloc] initWithIdentifier:identifier source:fromViewController destination:toViewController];
        
    } else if ([fromViewController isKindOfClass:[AntifraudDataTableViewController class]]) {
        if ([toViewController isKindOfClass:[DonationValueTableViewController class]]) {
            return [[UnwindSegueToDonationValue alloc] initWithIdentifier:identifier source:fromViewController destination:toViewController];
            
        } else if ([toViewController isKindOfClass:[DonationDataTableViewController class]]) {
            return [[UnwindSegueDissolve alloc] initWithIdentifier:identifier source:fromViewController destination:toViewController];
            
        }
    } else if ([fromViewController isKindOfClass:[DonationFailViewController class]]) {
        return [[UnwindSegueToDonationValue alloc] initWithIdentifier:identifier source:fromViewController destination:toViewController];
        
    }
    return [super segueForUnwindingToViewController:toViewController fromViewController:fromViewController identifier:identifier];
}

@end

//
//  CustomSegueFromCardScan.m
//  Solidario
//
//  Created by Pan on 19/06/17.
//  Copyright © 2017 Paula Vasconcelos Gueiros. All rights reserved.
//

#import "CustomSegueFromCardScan.h"
#import "CardScanViewController.h"
#import "CompleteCardInfoViewController.h"

@implementation CustomSegueFromCardScan

- (void)perform {
    CompleteCardInfoViewController *destinationVC = (CompleteCardInfoViewController *)self.destinationViewController;
    CardScanViewController *sourceVC = (CardScanViewController *)self.sourceViewController;
    
    CGFloat yOffset = self.sourceViewController.navigationController.navigationBar.frame.size.height + 20.f;
    CGFloat screenWidth = UIScreen.mainScreen.bounds.size.width;
    CGFloat screenHeight = UIScreen.mainScreen.bounds.size.height - yOffset;
    
    // add destination view to current view, initially transparent
    UIView *destinationView = destinationVC.view;
    [destinationView setFrame:CGRectMake(0, yOffset, screenWidth, screenHeight)];
    [destinationView setAlpha:0];
    
    // calculate height of scrollview slide
    CGFloat superviewHeight = screenWidth*(4.f/3);
    CGFloat cardHeight = (screenWidth-40)*(270.f/428);
    CGFloat yOriginSource = (superviewHeight-cardHeight)/2;
    
    CGPoint originDestination = destinationVC.cardImageView.frame.origin;
    CGPoint convertedPoint = [destinationVC.cardImageView convertPoint:originDestination toView:destinationVC.scrollView];
    CGFloat yOriginDestination = convertedPoint.y;
    
    // set scroll view offset
    [destinationVC.scrollView setContentInset:UIEdgeInsetsMake(yOriginSource - yOriginDestination, 0, 0, 0)];

    // add views to window
    UIWindow *window = UIApplication.sharedApplication.keyWindow;
    [window insertSubview:destinationView aboveSubview:sourceVC.view];
    
    // animate views
    [UIView animateWithDuration: 0.3f animations:^{
        [destinationView setAlpha:1];
        [destinationVC.scrollView setContentInset:UIEdgeInsetsMake(0, 0, 0, 0)];
        
    } completion:^(BOOL finished) {
        [sourceVC.navigationController pushViewController:self.destinationViewController animated:NO];
    }];
}

@end

//
//  ProjectShort.m
//
//  Created by Paula Vasconcelos on 19/05/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "ProjectShort.h"

NSString *const kProjectShortStatus = @"status";
NSString *const kProjectShortId = @"_id";
NSString *const kProjectShortCategory = @"category";
NSString *const kProjectShortGoal = @"goal";
NSString *const kProjectShortTitle = @"title";
NSString *const kProjectShortApproved = @"approved";
NSString *const kProjectShortMedia = @"media";
NSString *const kProjectShortIsFavorite = @"isFavorite";
NSString *const kProjectShortDescription = @"description";
NSString *const kProjectShortDonators = @"donators";
NSString *const kProjectShortPercent = @"percent";

@interface ProjectShort ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end


@implementation ProjectShort

@synthesize status = _status;
@synthesize internalBaseClassIdentifier = _internalBaseClassIdentifier;
@synthesize category = _category;
@synthesize goal = _goal;
@synthesize title = _title;
@synthesize approved = _approved;
@synthesize media = _media;
@synthesize isFavorite = _isFavorite;
@synthesize internalBaseClassDescription = _internalBaseClassDescription;
@synthesize donators = _donators;
@synthesize percent = _percent;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.status = [self objectOrNilForKey:kProjectShortStatus fromDictionary:dict];
            self.internalBaseClassIdentifier = [self objectOrNilForKey:kProjectShortId fromDictionary:dict];
            self.category = [self objectOrNilForKey:kProjectShortCategory fromDictionary:dict];
            self.goal = [self objectOrNilForKey:kProjectShortGoal fromDictionary:dict];
            self.title = [self objectOrNilForKey:kProjectShortTitle fromDictionary:dict];
            self.approved = [self objectOrNilForKey:kProjectShortApproved fromDictionary:dict];
            self.media = [self objectOrNilForKey:kProjectShortMedia fromDictionary:dict];
            self.isFavorite = [[self objectOrNilForKey:kProjectShortIsFavorite fromDictionary:dict] boolValue];
            self.internalBaseClassDescription = [self objectOrNilForKey:kProjectShortDescription fromDictionary:dict];
            self.donators = [[self objectOrNilForKey:kProjectShortDonators fromDictionary:dict] doubleValue];
            self.percent = [[self objectOrNilForKey:kProjectShortPercent fromDictionary:dict] doubleValue];
    }
    return self;
}

- (instancetype)initWithProject:(Project *)project {
    self = [super init];
    
    // This check serves to make sure that a non-Project object
    // passed into the model class doesn't break the parsing.
    if (self && [project isKindOfClass:[Project class]]) {
        self.status = project.status;
        self.internalBaseClassIdentifier = project.internalBaseClassIdentifier;
        self.category = project.category;
        self.goal = project.goal;
        self.title = project.title;
        self.approved = nil;
        self.media = project.media;
        self.isFavorite = NO;
        self.internalBaseClassDescription = project.internalBaseClassDescription;
        self.donators = project.donatorsCount;
        self.percent = project.percent;
    }
    return self;
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.status forKey:kProjectShortStatus];
    [mutableDict setValue:self.internalBaseClassIdentifier forKey:kProjectShortId];
    [mutableDict setValue:self.category forKey:kProjectShortCategory];
    [mutableDict setValue:self.goal forKey:kProjectShortGoal];
    [mutableDict setValue:self.title forKey:kProjectShortTitle];
    [mutableDict setValue:self.approved forKey:kProjectShortApproved];
    NSMutableArray *tempArrayForMedia = [NSMutableArray array];
    for (NSObject *subArrayObject in self.media) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForMedia addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForMedia addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForMedia] forKey:kProjectShortMedia];
    [mutableDict setValue:[NSNumber numberWithBool:self.isFavorite] forKey:kProjectShortIsFavorite];
    [mutableDict setValue:self.internalBaseClassDescription forKey:kProjectShortDescription];
    [mutableDict setValue:[NSNumber numberWithDouble:self.donators] forKey:kProjectShortDonators];
    [mutableDict setValue:[NSNumber numberWithDouble:self.percent] forKey:kProjectShortPercent];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}


#pragma mark - Helper Method

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.status = [aDecoder decodeObjectForKey:kProjectShortStatus];
    self.internalBaseClassIdentifier = [aDecoder decodeObjectForKey:kProjectShortId];
    self.category = [aDecoder decodeObjectForKey:kProjectShortCategory];
    self.goal = [aDecoder decodeObjectForKey:kProjectShortGoal];
    self.title = [aDecoder decodeObjectForKey:kProjectShortTitle];
    self.approved = [aDecoder decodeObjectForKey:kProjectShortApproved];
    self.media = [aDecoder decodeObjectForKey:kProjectShortMedia];
    self.isFavorite = [aDecoder decodeBoolForKey:kProjectShortIsFavorite];
    self.internalBaseClassDescription = [aDecoder decodeObjectForKey:kProjectShortDescription];
    self.donators = [aDecoder decodeDoubleForKey:kProjectShortDonators];
    self.percent = [aDecoder decodeDoubleForKey:kProjectShortPercent];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {

    [aCoder encodeObject:_status forKey:kProjectShortStatus];
    [aCoder encodeObject:_internalBaseClassIdentifier forKey:kProjectShortId];
    [aCoder encodeObject:_category forKey:kProjectShortCategory];
    [aCoder encodeObject:_goal forKey:kProjectShortGoal];
    [aCoder encodeObject:_title forKey:kProjectShortTitle];
    [aCoder encodeObject:_approved forKey:kProjectShortApproved];
    [aCoder encodeObject:_media forKey:kProjectShortMedia];
    [aCoder encodeBool:_isFavorite forKey:kProjectShortIsFavorite];
    [aCoder encodeObject:_internalBaseClassDescription forKey:kProjectShortDescription];
    [aCoder encodeDouble:_donators forKey:kProjectShortDonators];
    [aCoder encodeDouble:_percent forKey:kProjectShortPercent];
}

- (id)copyWithZone:(NSZone *)zone {
    ProjectShort *copy = [[ProjectShort alloc] init];
    
    if (copy) {
        copy.status = [self.status copyWithZone:zone];
        copy.internalBaseClassIdentifier = [self.internalBaseClassIdentifier copyWithZone:zone];
        copy.category = [self.category copyWithZone:zone];
        copy.goal = [self.goal copyWithZone:zone];
        copy.title = [self.title copyWithZone:zone];
        copy.approved = [self.approved copyWithZone:zone];
        copy.media = [self.media copyWithZone:zone];
        copy.isFavorite = self.isFavorite;
        copy.internalBaseClassDescription = [self.internalBaseClassDescription copyWithZone:zone];
        copy.donators = self.donators;
        copy.percent = self.percent;
    }
    return copy;
}

@end

//
//  ProjectCreator.m
//
//  Created by Paula Vasconcelos on 28/06/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "ProjectCreator.h"

NSString *const kProjectCreatorImage = @"image";
NSString *const kProjectCreatorId = @"id";
NSString *const kProjectCreatorCreatedCount = @"createdCount";
NSString *const kProjectCreatorSurname = @"surname";
NSString *const kProjectCreatorName = @"name";
NSString *const kProjectCreatorDonatedCount = @"donatedCount";


@interface ProjectCreator ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end


@implementation ProjectCreator

@synthesize image = _image;
@synthesize internalBaseClassIdentifier = _internalBaseClassIdentifier;
@synthesize createdCount = _createdCount;
@synthesize surname = _surname;
@synthesize name = _name;
@synthesize donatedCount = _donatedCount;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.image = [self objectOrNilForKey:kProjectCreatorImage fromDictionary:dict];
            self.internalBaseClassIdentifier = [self objectOrNilForKey:kProjectCreatorId fromDictionary:dict];
            self.createdCount = [[self objectOrNilForKey:kProjectCreatorCreatedCount fromDictionary:dict] doubleValue];
            self.surname = [self objectOrNilForKey:kProjectCreatorSurname fromDictionary:dict];
            self.name = [self objectOrNilForKey:kProjectCreatorName fromDictionary:dict];
            self.donatedCount = [[self objectOrNilForKey:kProjectCreatorDonatedCount fromDictionary:dict] doubleValue];

    }
    return self;
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.image forKey:kProjectCreatorImage];
    [mutableDict setValue:self.internalBaseClassIdentifier forKey:kProjectCreatorId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.createdCount] forKey:kProjectCreatorCreatedCount];
    [mutableDict setValue:self.surname forKey:kProjectCreatorSurname];
    [mutableDict setValue:self.name forKey:kProjectCreatorName];
    [mutableDict setValue:[NSNumber numberWithDouble:self.donatedCount] forKey:kProjectCreatorDonatedCount];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}


#pragma mark - Helper Method

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.image = [aDecoder decodeObjectForKey:kProjectCreatorImage];
    self.internalBaseClassIdentifier = [aDecoder decodeObjectForKey:kProjectCreatorId];
    self.createdCount = [aDecoder decodeDoubleForKey:kProjectCreatorCreatedCount];
    self.surname = [aDecoder decodeObjectForKey:kProjectCreatorSurname];
    self.name = [aDecoder decodeObjectForKey:kProjectCreatorName];
    self.donatedCount = [aDecoder decodeDoubleForKey:kProjectCreatorDonatedCount];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {

    [aCoder encodeObject:_image forKey:kProjectCreatorImage];
    [aCoder encodeObject:_internalBaseClassIdentifier forKey:kProjectCreatorId];
    [aCoder encodeDouble:_createdCount forKey:kProjectCreatorCreatedCount];
    [aCoder encodeObject:_surname forKey:kProjectCreatorSurname];
    [aCoder encodeObject:_name forKey:kProjectCreatorName];
    [aCoder encodeDouble:_donatedCount forKey:kProjectCreatorDonatedCount];
}

- (id)copyWithZone:(NSZone *)zone {
    ProjectCreator *copy = [[ProjectCreator alloc] init];
    
    if (copy) {
        copy.image = [self.image copyWithZone:zone];
        copy.internalBaseClassIdentifier = [self.internalBaseClassIdentifier copyWithZone:zone];
        copy.createdCount = self.createdCount;
        copy.surname = [self.surname copyWithZone:zone];
        copy.name = [self.name copyWithZone:zone];
        copy.donatedCount = self.donatedCount;
    }
    return copy;
}

@end

//
//  CustomTextField.m
//  Blue SP
//
//  Created by Allan on 5/6/16.
//  Copyright © 2016 IS. All rights reserved.
//

#import "CustomTextField.h"

@implementation CustomTextField

- (void)awakeFromNib {
    [super awakeFromNib];
    self.delegate = self;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSString * appendString = @"";
    //Placa
    if (_maskType == TextMaskDate) {
        if (!range.length) {
            switch (range.location) {
                case 2:
                case 5:
                    appendString = @"/";
                    break;
                default:
                    break;
            }
        }
    } else if (_maskType == TextMaskPhone) { // (61) 983477341
        if (!range.length) {
            switch (range.location) {
                case 0:
                    appendString = @"(";
                    break;
                case 3:
                    appendString = @") ";
                    break;
                case 9:
                    appendString = @"-";
                    break;
                default:
                    break;
            }
            
            NSMutableString *string = [NSMutableString stringWithString:self.text];
            switch (self.text.length) {
                case 14:
                    string = [NSMutableString stringWithString:[string stringByReplacingOccurrencesOfString:@"-" withString:@""]];
                    [string insertString:@"-" atIndex:10];
                    break;
                default:
                    break;
            }
            self.text = string;
            
        } else { // user is removing characters
            NSMutableString *string = [NSMutableString stringWithString:self.text];
            switch (self.text.length) {
                case 2:
                    [string replaceCharactersInRange:NSMakeRange(0, 1) withString:@""];
                    break;
                case 6:
                    [string replaceCharactersInRange:NSMakeRange(3, 2) withString:@""];
                    break;
                case 11:
                    [string replaceCharactersInRange:NSMakeRange(9, 1) withString:@""];
                    break;
                case 15:
                    string = [NSMutableString stringWithString:[string stringByReplacingOccurrencesOfString:@"-" withString:@""]];
                    [string insertString:@"-" atIndex:9];
                    break;
                default:
                    break;
            }
            self.text = string;
        }
    } else if (_maskType == TextMaskCPFCNPJ) { // 188.302.190-10 & 18.346.150/0001-17
        if (!range.length) { // user is adding characters
            switch (range.location) {
                case 3:
                case 7:
                    appendString = @".";
                    break;
                case 11:
                    appendString = @"-";
                    break;
                default:
                    break;
            }
            
            NSMutableString *string = [NSMutableString stringWithString:self.text];
            switch (self.text.length) {
                case 14:
                    string = [NSMutableString stringWithString:[string stringByReplacingOccurrencesOfString:@"." withString:@""]];
                    string = [NSMutableString stringWithString:[string stringByReplacingOccurrencesOfString:@"-" withString:@""]];
                    [string insertString:@"." atIndex:2];
                    [string insertString:@"." atIndex:6];
                    [string insertString:@"/" atIndex:10];
                    break;
                case 15:
                    [string insertString:@"-" atIndex:15];
                    break;
                default:
                    break;
            }
            self.text = string;
        } else { // user is removing characters
            NSMutableString *string = [NSMutableString stringWithString:self.text];
            switch (self.text.length) {
                case 5:
                    [string replaceCharactersInRange:NSMakeRange(3, 1) withString:@""];
                    break;
                case 9:
                    [string replaceCharactersInRange:NSMakeRange(7, 1) withString:@""];
                    break;
                case 13:
                case 17:
                    string = [NSMutableString stringWithString:[string stringByReplacingOccurrencesOfString:@"-" withString:@""]];
                    break;
                case 15:
                    string = [NSMutableString stringWithString:[string stringByReplacingOccurrencesOfString:@"." withString:@""]];
                    string = [NSMutableString stringWithString:[string stringByReplacingOccurrencesOfString:@"/" withString:@""]];
                    [string insertString:@"." atIndex:3];
                    [string insertString:@"." atIndex:7];
                    [string insertString:@"-" atIndex:11];
                    break;
                default:
                    break;
            }
            self.text = string;
        }
    } else if (_maskType == TextMaskCPF) { // 188.302.190-10
        if (!range.length) { // user is adding characters
            switch (range.location) {
                case 3:
                case 7:
                    appendString = @".";
                    break;
                case 11:
                    appendString = @"-";
                    break;
                default:
                    break;
            }
        } else { // user is removing characters
            NSMutableString *string = [NSMutableString stringWithString:self.text];
            switch (self.text.length) {
                case 5:
                    [string replaceCharactersInRange:NSMakeRange(3, 1) withString:@""];
                    break;
                case 9:
                    [string replaceCharactersInRange:NSMakeRange(7, 1) withString:@""];
                    break;
                case 13:
                    [string replaceCharactersInRange:NSMakeRange(11, 1) withString:@""];
                    break;
                default:
                    break;
            }
            self.text = string;
        }
    } else if (_maskType == TextMaskCurrency) {
        if (!range.length) { // user is adding characters
            switch (range.location) {
                case 0:
                    appendString = @"00,0";
                    break;
                default:
                    appendString = @"";
                    break;
            }
            
            NSMutableString *string = [NSMutableString stringWithString:self.text];
            if (self.text.length == 5) {
                if ([string characterAtIndex:0] == '0') {
                    [string replaceCharactersInRange:NSMakeRange(0, 1) withString:@""];
                    string = [NSMutableString stringWithString:[string stringByReplacingOccurrencesOfString:@"," withString:@""]];
                    [string insertString:@"," atIndex:2];
                } else {
                    string = [NSMutableString stringWithString:[string stringByReplacingOccurrencesOfString:@"," withString:@""]];
                    [string insertString:@"," atIndex:[string length]-1];
                }
                
            } else if (self.text.length > 5) {
                if  (self.text.length < self.maxLength) {
                    
                    string = [NSMutableString stringWithString:[string stringByReplacingOccurrencesOfString:@"," withString:@""]];
                    [string insertString:@"," atIndex:[string length]-1];
                    
                    string = [NSMutableString stringWithString:[string stringByReplacingOccurrencesOfString:@"." withString:@""]];
                    int i = (int)[string length] - 5;
                    
                    while (i > 0) {
                        [string insertString:@"." atIndex:i];
                        i -= 3;
                    }
                }
            }
            self.text = string;
            
        } else { // user is removing characters
            NSMutableString *string = [NSMutableString stringWithString:self.text];
            
            if (self.text.length >= 6) {
                string = [NSMutableString stringWithString:[string stringByReplacingOccurrencesOfString:@"," withString:@""]];
                string = [NSMutableString stringWithString:[string stringByReplacingOccurrencesOfString:@"." withString:@""]];
                
                [string insertString:@"," atIndex:[string length] - 3];
                
                int i = (int)[string length] - 7;
                while (i > 0) {
                    [string insertString:@"." atIndex:i];
                    i -= 3;
                }
            } else {
                string = [NSMutableString stringWithString:[string stringByReplacingOccurrencesOfString:@"," withString:@""]];
                [string insertString:@"0" atIndex:0];
                [string insertString:@"," atIndex:2];
            }
            self.text = string;
        }
        
    } else if (_maskType == TextMaskCreditCardNumber) { // 4444 4444 4444 5555
        if (!range.length) {
            switch (range.location) {
                case 4:
                case 9:
                case 14:
                    appendString = @" ";
                    break;
                default:
                    break;
            }
        } else {
            NSMutableString *string = [NSMutableString stringWithString:self.text];
            switch (self.text.length) {
                case 6:
                    [string replaceCharactersInRange:NSMakeRange(4, 1) withString:@""];
                    break;
                case 11:
                    [string replaceCharactersInRange:NSMakeRange(9, 1) withString:@""];
                    break;
                case 16:
                    [string replaceCharactersInRange:NSMakeRange(14, 1) withString:@""];
                    break;
                default:
                    break;
            }
            self.text = string;
        }
    } else if (_maskType == TextMaskCreditCardValidationDate) { // 12/2019
        if (!range.length) {
            switch (range.location) {
                case 2:
                    appendString = @"/";
                    break;
                default:
                    break;
            }
        } else {
            NSMutableString *string = [NSMutableString stringWithString:self.text];
            switch (self.text.length) {
                case 4:
                    [string replaceCharactersInRange:NSMakeRange(2, 1) withString:@""];
                    break;
                default:
                    break;
            }
            self.text = string;
        }
    } else if (_maskType == TextMaskCEP) { // 71523-123
        if (!range.length) { // user is adding characters
            switch (range.location) {
                case 5:
                    appendString = @"-";
                    break;
                default:
                    break;
            }
        } else { // user is removing characters
            NSMutableString *string = [NSMutableString stringWithString:self.text];
            switch (self.text.length) {
                case 7:
                    [string replaceCharactersInRange:NSMakeRange(5, 1) withString:@""];
                    break;
                default:
                    break;
            }
            self.text = string;
        }
    }
    
    NSString *newString = [textField.text stringByAppendingString:appendString];
    
    [textField setText:newString];
    
    //Ignore if max length is zero
    if (self.maxLength == 0) {
        return true;
    }
    
    NSUInteger oldLength = [textField.text length];
    NSUInteger replacementLength = [string length];
    NSUInteger rangeLength = range.length;
    
    NSUInteger newLength = oldLength - rangeLength + replacementLength;
    
    BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
    
    return newLength <= self.maxLength || returnKey;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

@end

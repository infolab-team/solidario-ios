//
//  UpdateProjectTableViewController.m
//  Solidario
//
//  Created by Pan on 14/02/17.
//  Copyright © 2017 Paula Vasconcelos Gueiros. All rights reserved.
//

typedef enum {UpdateEntryTitleCellNum = 0, UpdateEntryDescriptionCellNum, UpdateEntryAddPictureCellNum, UpdateEntryNotifyCellNum, UpdateEntryPictureListCellNum} ProjectUpdateCellFirstSectionNum;

#import "UpdateProjectTableViewController.h"

#import "PlaceholderTextView.h"
#import "UpdateEntryTitleTableViewCell.h"
#import "UpdateEntryDescriptionTableViewCell.h"
#import "UpdateEntryAddPictureTableViewCell.h"
#import "UpdateEntryNotifyTableViewCell.h"
#import "UpdateEntryPictureListTableViewCell.h"
#import "ProjectUpdateCell.h"

#import "Service.h"

#import "TimelineEntry.h"

#import "UIColor+Utils.h"
#import "UILabel+Utils.h"
#import "UIImage+Utils.h"
#import "NSString+Utils.h"
#import <SDWebImage/UIImageView+WebCache.h>

@import Photos;

@interface UpdateProjectTableViewController ()

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIImageView *projectBannerImageView;
@property (weak, nonatomic) IBOutlet UILabel *projectTitleLabel;
@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *pictureLabelGestureRecognizer;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UIView *footerView;
@property (weak, nonatomic) IBOutlet UIButton *loadMoreButton;

@property (strong, nonatomic) NSMutableArray *rowsInSection;
@property (strong, nonatomic) NSMutableArray *rowHeights;
@property (strong, nonatomic) NSMutableArray *timelineEntries;
@property (strong, nonatomic) NSMutableArray *isButtonTextMoreForEntry;
@property (strong, nonatomic) UIImage *selectedImage;

@property (nonatomic) BOOL isPictureSelected;
@property (nonatomic) BOOL photoLibrarySource;
@property (nonatomic) BOOL restartTableIsActive;
@property (nonatomic) NSInteger timelinePageNumber;

@end


@implementation UpdateProjectTableViewController

static const CGFloat defaultViewPadding = 20.f;
static const CGFloat projectBannerProportion = 64.f / 280.f;

// static row heights (initially)
static const CGFloat updateNewEntryDescriptionCellHeight = 112.f;
static const CGFloat updateNewEntryNotifyCellHeight = 70.f;
static const CGFloat updateNewEntryPictureListFullCellHeight = 140.f;
static const CGFloat updateNewEntryPictureListEmptyCellHeight = 20.f;
static const CGFloat tableFooterHeight = 48;

static const CGFloat entryPictureLargerSideSize = 500.f;

static const NSInteger numberOfEntriesPerPage = 3;

#pragma mark - Initialization

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setup];
}

- (void)setup {
    // setup table view estimated row height
    self.tableView.estimatedRowHeight = 44.f;
    
    // setup headerView height depending on projectBanner height
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat bannerWidth = screenWidth - 2 * defaultViewPadding, bannerHeight = bannerWidth * projectBannerProportion;
    [self.headerView setFrame:CGRectMake(self.headerView.frame.origin.x, self.headerView.frame.origin.y, self.headerView.frame.size.width, bannerHeight + 2 * defaultViewPadding)];
    
    // setup specific project properties
    [self.projectTitleLabel setText:self.project.title];
    NSString *imageUrlString = [NSString encodedStringFromString:self.project.media[0]];
    [self.projectBannerImageView sd_setImageWithURL:[NSURL URLWithString:imageUrlString] placeholderImage:[UIImage imageNamed:@"webImagePlaceholder"]];

    // add gradient layer to banner
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = CGRectMake(0, 0, bannerWidth, bannerHeight);
    gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor defaultColor:DefaultColorBannerShadowTop] CGColor], (id)[[UIColor defaultColor:DefaultColorBannerShadowTop] CGColor], (id)[[UIColor blackColor] CGColor], nil];
    [self.projectBannerImageView.layer insertSublayer:gradient atIndex:0];

    // initialize picture properties
    self.isPictureSelected = NO;
    self.selectedImage = nil;
    
    // setup arrays for number of rows and row heights
    self.rowsInSection = [NSMutableArray arrayWithObjects:[NSNumber numberWithInt:5], [NSNumber numberWithInteger:0], nil];
    
    // row heights for first section: always 5 cells (with dynamic heights)
    self.rowHeights = [NSMutableArray arrayWithCapacity:[self.rowsInSection count]];
    self.rowHeights[0] = [NSMutableArray array];
    for (int j = 0; j < [self.rowsInSection[0] integerValue]; j++) {
        if (j == UpdateEntryDescriptionCellNum) {
            [self.rowHeights[0] addObject:[NSNumber numberWithFloat:updateNewEntryDescriptionCellHeight]];
        } else if (j == UpdateEntryNotifyCellNum) {
            [self.rowHeights[0] addObject:[NSNumber numberWithFloat:updateNewEntryNotifyCellHeight]];
        } else  if (j == UpdateEntryPictureListCellNum) {
            [self.rowHeights[0] addObject:[NSNumber numberWithFloat:updateNewEntryPictureListEmptyCellHeight]];
        } else {
            [self.rowHeights[0] addObject:[NSNumber numberWithFloat:44.f]];
        }
    }
    
    // hide load more button until at least one activity is found
    [self.footerView setHidden:YES];
    [self.loadMoreButton setHidden:YES];
    
    self.timelineEntries = [NSMutableArray array];
    self.isButtonTextMoreForEntry = [NSMutableArray array];
    self.timelinePageNumber = 1; // initialize timeline page number variable
    [self retrieveTimelineEntries]; // fetch first three timeline entries
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // setup activityIndicator position
    UIWindow *frontWindow = [[UIApplication sharedApplication] keyWindow];
    [frontWindow addSubview:self.activityIndicator];
    self.activityIndicator.center = frontWindow.center;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    // stop activityIndicator on cases in which it's still animating
    [self.activityIndicator stopAnimating];
    [self.activityIndicator removeFromSuperview];
}


#pragma mark - Service Requests

- (void)retrieveTimelineEntries {
    [self.activityIndicator startAnimating];
    
    // call service to fetch timeline entries for project
    [Service getTimelineEntriesForProjectWithIdentifier:self.project.internalBaseClassIdentifier onPage:self.timelinePageNumber withItemsPerPage:numberOfEntriesPerPage andCompletion:^(id results, BOOL hasError, NSString *errorMessage) {
        
        [self.activityIndicator stopAnimating];
        
        if (hasError) {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Erro"
                                                                           message:nil
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"OK"
                                                               style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction * action) {}];
            [alert addAction:okButton];
//            if (errorMessage != nil) {
//                [alert setMessage:errorMessage];
//            } else {
                [alert setMessage:@"Não foi possível recuperar publicações do projeto."];
//            }
            [self presentViewController:alert animated:YES completion:nil];
            return;
        } else {
            NSInteger numberOfResults = [(NSMutableArray *)results count];
            NSLog(@"⏳ %lu timeline entry(ies) found on page #️⃣%ld", (unsigned long)numberOfResults, (long)self.timelinePageNumber);
            if (numberOfResults > 0) {
                // increment page number variable if call returned results
                self.timelinePageNumber++;
                
                // check is table is being restarted (ex. after entry deletion)
                if (self.restartTableIsActive) {
                    self.rowsInSection[1] = [NSNumber numberWithInteger:0];
                    self.timelineEntries = [NSMutableArray array];
                    self.isButtonTextMoreForEntry = [NSMutableArray array];
                }
                
                // fetch old value for number of entries
                NSInteger numberOfOldEntries = [self.rowsInSection[1] integerValue];
                
                // update data source
                [self.timelineEntries addObjectsFromArray:(NSMutableArray *)results];
                NSInteger totalNumberOfEntries = [self.timelineEntries count];
                self.rowsInSection[1] = [NSNumber numberWithInteger:totalNumberOfEntries];
                
                // update isButtonTextMoreForEntry array
                for (int i = 0; i < numberOfResults; i++) {
                    // initialize value for more/less button text of new entries
                    [self.isButtonTextMoreForEntry addObject:[NSNumber numberWithBool:YES]];
                }

                // update table using new data
                [self.tableView beginUpdates];
                if (numberOfOldEntries > 0) {
                    NSMutableArray *newIndexPaths = [NSMutableArray arrayWithCapacity:numberOfResults];
                    for (int i = 1; i <= numberOfResults; i++) {
                        NSIndexPath *newRowIndexPath = [NSIndexPath indexPathForRow:numberOfOldEntries-i inSection:1];
                        [newIndexPaths addObject:newRowIndexPath];
                    }
                    [self.tableView insertRowsAtIndexPaths:newIndexPaths withRowAnimation:UITableViewRowAnimationAutomatic];
                } else {
                    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationNone];
                    
                }
                [self.tableView endUpdates];
                
                // unhide load more button (if hidden)
                [self.footerView setHidden:NO];
                [self.loadMoreButton setHidden:NO];
            } else {
                [self.tableView beginUpdates];
                if (self.restartTableIsActive) {
                    self.rowsInSection[1] = [NSNumber numberWithInteger:0];
                    self.timelineEntries = [NSMutableArray array];
                    self.isButtonTextMoreForEntry = [NSMutableArray array];
                    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationAutomatic];
                }
                [self.loadMoreButton setHidden:YES];
                [self.footerView setFrame:CGRectMake(self.footerView.frame.origin.x, self.footerView.frame.origin.y, self.footerView.frame.size.width, 0)];
                [self.tableView endUpdates];
            }
        }
        self.restartTableIsActive = NO;
    }];
}


#pragma mark - UITableView Delegate and Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self.rowsInSection count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.rowsInSection[section] integerValue];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case 0: {
            switch (indexPath.row) {
                case UpdateEntryTitleCellNum: {
                    UpdateEntryTitleTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"updateTitleCell" forIndexPath:indexPath];
                    return cell;
                }
                case UpdateEntryDescriptionCellNum: {
                    UpdateEntryDescriptionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"updateDescriptionCell" forIndexPath:indexPath];
                    return cell;
                }
                case UpdateEntryAddPictureCellNum: {
                    UpdateEntryAddPictureTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"updateAddPictureCell" forIndexPath:indexPath];
                    if (self.isPictureSelected) {
                        [cell.addPictureButton setEnabled:NO];
                        [cell.addPictureLabel setEnabled:NO];
                        [self.pictureLabelGestureRecognizer setEnabled:NO];
                    } else {
                        [cell.addPictureButton setEnabled:YES];
                        [cell.addPictureLabel setEnabled:YES];
                        [self.pictureLabelGestureRecognizer setEnabled:YES];
                    }
                    return cell;
                }
                case UpdateEntryNotifyCellNum: {
                    UpdateEntryNotifyTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"updateNotifyCell" forIndexPath:indexPath];
                    return cell;
                }
                case UpdateEntryPictureListCellNum: {
                    if (self.isPictureSelected) {
                        UpdateEntryPictureListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"updatePictureListCell"];
                        [cell.pictureImageView setImage:self.selectedImage];
                        [cell setImageViewWidthFromProportion:self.selectedImage.size.width/self.selectedImage.size.height];

                        self.rowHeights[indexPath.section][indexPath.row] = [NSNumber numberWithFloat:updateNewEntryPictureListFullCellHeight];
                        
                        return cell;
                    } else {
                        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"voidCell"];
                        self.rowHeights[indexPath.section][indexPath.row] = [NSNumber numberWithFloat:updateNewEntryPictureListEmptyCellHeight];
                        return cell;
                    }
                }
                default: {
                    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
                    self.rowHeights[indexPath.section][indexPath.row] = [NSNumber numberWithFloat:44.f];
                    return cell;
                }
            }
        }
        case 1: {
            ProjectUpdateCell *cell = [tableView dequeueReusableCellWithIdentifier:@"projectUpdateCell" forIndexPath:indexPath];
            TimelineEntry *entry = [self.timelineEntries objectAtIndex:indexPath.row];
            
            NSString *day = (entry.day < 10) ? [NSString stringWithFormat:@"0%.0f", entry.day] :
            [NSString stringWithFormat:@"%.0f", entry.day];
            cell.entryDateDayLabel.text = day;
            cell.entryDateMonthLabel.text = [NSString monthShortNameFromInteger:(NSInteger)entry.month];
            
            // date setup for time
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
            [dateFormatter setDateFormat:@"yyyy'-'MM'-'dd'T'HH':'mm':'ss.SSS'Z'"];
            NSDate *entryDate = [dateFormatter dateFromString:entry.date];
            [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
            [dateFormatter setDateFormat:@"HH:mm"];
            cell.entryDateTimeLabel.text = [dateFormatter stringFromDate:entryDate];
            
            // month label spacing
            NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:cell.entryDateMonthLabel.text];
            float spacing = 4.4f;
            [attributedString addAttribute:NSKernAttributeName
                                     value:@(spacing)
                                     range:NSMakeRange(0, [attributedString length]-1)];
            cell.entryDateMonthLabel.attributedText = attributedString;
            
            cell.entryTitleLabel.text = entry.title;
            cell.entryDescriptionLabel.text = [entry.internalBaseClassDescription stringByTrimmingCharactersInSet:
                                               [NSCharacterSet whitespaceAndNewlineCharacterSet]];
            
            // retrieve and set up image
            if (entry.img != nil && ![entry.img isEqualToString:@""]) {
                [cell.entryImageView sd_setImageWithURL:[NSURL URLWithString:entry.img] placeholderImage:[UIImage imageNamed:@"webImagePlaceholder"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                    
                    if (error) {
                        [cell.entryImageView removeFromSuperview];
                    } else {
                        CGFloat imageViewWidth = cell.entryTitleLabel.frame.size.width;
                        CGFloat imageViewHeight = (image.size.height / image.size.width) * imageViewWidth;
                        CGFloat constant = imageViewHeight - imageViewWidth;
                        cell.imageAspectRatioConstraint.constant = constant;
                    }
                }];
            } else {
                [cell.entryImageView removeFromSuperview];
            }
            
            // timeline top line hidden if necessary
            if (indexPath.row == 0) {
                [cell.topLineView setHidden:YES];
            } else {
                [cell.topLineView setHidden:NO];
            }
            
            // see more/less button
            cell.isButtonTextMore = [self.isButtonTextMoreForEntry[indexPath.row] boolValue];
            [self setupLayoutOfUpdateCell:cell];
            
            // set button tag for identifying which button was selected
            [cell.viewMoreOrLessButton setTag:indexPath.row];
            [cell.deleteEntryButton setTag:indexPath.row];
            
            return cell;
        }
        default: {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
            return cell;
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return [(self.rowHeights[indexPath.section][indexPath.row]) floatValue];
    }
    return UITableViewAutomaticDimension;
}


#pragma mark - Button Actions

- (IBAction)addPicture:(id)sender {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *useLast = [UIAlertAction actionWithTitle:@"Usar a Última Foto"
                                                      style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction * action) {
                                                        NSLog(@"🍄 Use last picture in Library");
                                                        [self selectLastPictureInLibrary];
                                                    }];
    UIAlertAction *take = [UIAlertAction actionWithTitle:@"Tirar Foto"
                                                   style:UIAlertActionStyleDefault
                                                 handler:^(UIAlertAction * action) {
                                                     NSLog(@"📸 Take picture");
                                                     self.photoLibrarySource = NO;
                                                     [self openPicker];
                                                 }];
    UIAlertAction *choose = [UIAlertAction actionWithTitle:@"Escolher na Biblioteca"
                                                     style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action) {
                                                       NSLog(@"🖼 Choose from Library");
                                                       self.photoLibrarySource = YES;
                                                       [self openPicker];
                                                   }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancelar"
                                                     style:UIAlertActionStyleCancel
                                                   handler:^(UIAlertAction * action) {}];
    [alert addAction:useLast];
    [alert addAction:take];
    [alert addAction:choose];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:^{}];
}

- (void)selectLastPictureInLibrary {
    
    PHFetchOptions *fetchOptions = [[PHFetchOptions alloc] init];
    fetchOptions.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:YES]];
    PHFetchResult *fetchResult = [PHAsset fetchAssetsWithMediaType:PHAssetMediaTypeImage options:fetchOptions];
    PHAsset *lastAsset = [fetchResult lastObject];
    [[PHImageManager defaultManager] requestImageDataForAsset:lastAsset
                                                      options:PHImageRequestOptionsVersionCurrent
                                                resultHandler:^(NSData * _Nullable imageData, NSString * _Nullable dataUTI, UIImageOrientation orientation, NSDictionary * _Nullable info) {
                                                    
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        UIImage *helperImage = [UIImage imageWithData:imageData];
                                                        self.selectedImage = [UIImage shrunkenImageWithImage:helperImage toLargerSideSize:entryPictureLargerSideSize];
                                                        self.isPictureSelected = YES;
                                                        [self.tableView reloadData];
                                                    });
                                                }];
//    
//    
//    
//    
//    
//    
//    ALAssetsLibrary *assetsLibrary = [[ALAssetsLibrary alloc] init];
//    [assetsLibrary enumerateGroupsWithTypes:ALAssetsGroupSavedPhotos usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
//        if (nil != group) {
//            // be sure to filter the group so you only get photos
//            [group setAssetsFilter:[ALAssetsFilter allPhotos]];
//
//            if (group.numberOfAssets > 0) {
//                [group enumerateAssetsAtIndexes:[NSIndexSet indexSetWithIndex:group.numberOfAssets - 1] options:0 usingBlock:^(ALAsset *result, NSUInteger index, BOOL *stop) {
//                    
//                    if (nil != result) {
//                        ALAssetRepresentation *repr = [result defaultRepresentation];
//                        CGImageRef imageRef = [repr fullResolutionImage];
//                        
//                        // this is the most recent saved photo - update data
//                        UIImage *helperImage = [UIImage imageWithCGImage:imageRef scale:1 orientation:(UIImageOrientation)[repr orientation]];
//                        self.selectedImage = [UIImage shrunkenImageWithImage:helperImage toLargerSideSize:entryPictureLargerSideSize];
//                        self.isPictureSelected = YES;
//                        [self.tableView reloadData];
//                        
//                        // stop the enumeration
//                        *stop = YES;
//                    }
//                }];
//            }
//        }
//        *stop = NO;
//    } failureBlock:^(NSError *error) {
//        NSLog(@"error: %@", error);
//    }];
}

- (void)openPicker {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = NO;
    
    if (self.photoLibrarySource) {
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        picker.modalPresentationStyle = UIModalPresentationCurrentContext;
        [picker.navigationBar setTranslucent:NO];
    } else {
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    }
    
    [self presentViewController:picker animated:YES completion:nil];
}

- (IBAction)loadMoreUpdateEntries:(id)sender {
    NSLog(@"↪️ Load More Update Entries");
    [self retrieveTimelineEntries];
}

- (IBAction)seeMoreOrLessOfUpdateEntry:(id)sender {
    
    ProjectUpdateCell *cell = (ProjectUpdateCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:((UIButton *)sender).tag inSection:1]];
    
    // show log message for action
    if (cell.isButtonTextMore) {
        NSLog(@"🔃 See More of Update Entry: %ld", (long)cell.viewMoreOrLessButton.tag);
    } else {
        NSLog(@"🔃 See Less of Update Entry: %ld", (long)cell.viewMoreOrLessButton.tag);
    }
    
    // update value for isButtonTextMoreForEntry array
    cell.isButtonTextMore = !cell.isButtonTextMore;
    self.isButtonTextMoreForEntry[cell.viewMoreOrLessButton.tag] = [NSNumber numberWithBool:cell.isButtonTextMore];
    
    [self.tableView beginUpdates];
    [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:(long)cell.viewMoreOrLessButton.tag inSection:1]] withRowAnimation:UITableViewRowAnimationAutomatic];
    [self.tableView endUpdates];
}

- (void)setupLayoutOfUpdateCell:(ProjectUpdateCell *)cell {
    NSMutableAttributedString *mutableAttributedString = [cell.viewMoreOrLessButton.currentAttributedTitle mutableCopy];
    if (cell.isButtonTextMore) {
        [mutableAttributedString.mutableString setString:@"Ver Mais"];
        [cell.entryArrowImage setImage:[UIImage imageNamed:@"updateArrowRight"]];
        [cell.entryDescriptionLabel setNumberOfLines:5];
        [cell.imageHiddenConstraint setPriority:900];
    } else {
        [mutableAttributedString.mutableString setString:@"Ver Menos"];
        [cell.entryArrowImage setImage:[UIImage imageNamed:@"updateArrowUp"]];
        [cell.entryDescriptionLabel setNumberOfLines:0];
        [cell.imageHiddenConstraint setPriority:500];
    }
    [cell.viewMoreOrLessButton setAttributedTitle:mutableAttributedString forState:UIControlStateNormal];
}

- (IBAction)removePicture:(id)sender {

    // confirm deletion
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Atenção"
                                                                   message:[NSString stringWithFormat:@"Tem certeza que deseja excluir essa foto?"]
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"Sim"
                                                       style:UIAlertActionStyleDestructive
                                                     handler:^(UIAlertAction * action) {
                                                         self.isPictureSelected = NO;
                                                         self.selectedImage = nil;
                                                         [self.tableView reloadData];
                                                     }];
    UIAlertAction *cancelButton = [UIAlertAction actionWithTitle:@"Cancelar"
                                                           style:UIAlertActionStyleCancel
                                                         handler:^(UIAlertAction * action) {}];
    
    [alert addAction:okButton];
    [alert addAction:cancelButton];
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)removeUpdateEntry:(id)sender {
    
    NSInteger position = ((UIButton *)sender).tag;
    TimelineEntry *entry = [self.timelineEntries objectAtIndex:position];
    
    // confirm deletion
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Atenção"
                                                                   message:[NSString stringWithFormat:@"Tem certeza que deseja excluir a publicação \"%@\"?", entry.title]
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"Sim"
                                                       style:UIAlertActionStyleDestructive
                                                     handler:^(UIAlertAction * action) {
                                                         NSLog(@"🔃 Deleting Entry %ld: %@", (long)position, entry.title);
                                                         [self removeUpdateEntryWithIdentifier:entry.internalBaseClassIdentifier];
                                                     }];
    UIAlertAction *cancelButton = [UIAlertAction actionWithTitle:@"Cancelar"
                                                           style:UIAlertActionStyleCancel
                                                         handler:^(UIAlertAction * action) {}];
    
    [alert addAction:okButton];
    [alert addAction:cancelButton];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)removeUpdateEntryWithIdentifier:(NSString *)identifier {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleAlert];
    __block UIAlertAction *okButton;
    
    [self.activityIndicator startAnimating];
    [Service deleteEntryFromTimelineWithIdentifier:identifier andCompletion:^(id results, BOOL hasError, NSString *errorMessage) {
        [self.activityIndicator stopAnimating];
        
        if (hasError) {
            // error message
            [alert setTitle:@"Erro"];
//            if (errorMessage != nil) {
//                [alert setMessage:errorMessage];
//            } else {
                [alert setMessage:@"Não foi possível excluir a publicação."];
//            }
            okButton = [UIAlertAction actionWithTitle:@"OK"
                                                style:UIAlertActionStyleDefault
                                              handler:nil];
            [alert addAction:okButton];
            [self presentViewController:alert animated:YES completion:nil];
        } else {
            // success message
            [alert setTitle:@"Sucesso"];
            if ([results isKindOfClass:[NSString class]]) {
                [alert setMessage:(NSString *)results];
            }
            okButton = [UIAlertAction actionWithTitle:@"OK"
                                                style:UIAlertActionStyleDefault
                                              handler:^(UIAlertAction * action) {
                                                  
                                                  // restart data source
                                                  self.restartTableIsActive = YES;
                                                  self.timelinePageNumber = 1; // initialize timeline page number variable
                                                  
                                                  [self.tableView beginUpdates];
                                                  [self.footerView setFrame:CGRectMake(self.footerView.frame.origin.x, self.footerView.frame.origin.y, self.footerView.frame.size.width, tableFooterHeight)];
                                                  [self.tableView endUpdates];
                                                  
                                                  [self retrieveTimelineEntries]; // fetch first three timeline entries
                                              }];
            [alert addAction:okButton];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }];
}


#pragma mark - UIImagePickerController Delegate and Helpers

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *helperImage = (info[UIImagePickerControllerEditedImage]) ? (info[UIImagePickerControllerEditedImage]) : (info[UIImagePickerControllerOriginalImage]);
    self.selectedImage = [UIImage shrunkenImageWithImage:helperImage toLargerSideSize:entryPictureLargerSideSize];
    
    if (!self.selectedImage) {
        NSLog(@"❌ Error selecting picture");
        return;
    }

    self.isPictureSelected = YES;
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.tableView reloadData];
}


#pragma mark - Navigation

- (IBAction)publish:(id)sender {
    
    [self.view endEditing:YES];
    [self performInputValidations];
}

- (void)performInputValidations {
    NSLog(@"📤 Checking input data");
    
    // alert setup
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Atenção"
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"OK"
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action) {}];
    [alert addAction:okButton];
    
    // title not empty
    UpdateEntryTitleTableViewCell *titleCell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:UpdateEntryTitleCellNum inSection:0]];
    if ([titleCell.titleTextField.text length] == 0) {
        [alert setMessage:@"Utilize um título válido."];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    
    UpdateEntryDescriptionTableViewCell *descriptionCell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:UpdateEntryDescriptionCellNum inSection:0]];
    if ([descriptionCell.descriptionTextView.text length] == 0) {
        [alert setMessage:@"Utilize uma descrição válida."];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    
    [self confirmPostWithTitle:titleCell.titleTextField.text andDescription:descriptionCell.descriptionTextView.text];
}

- (void)confirmPostWithTitle:(NSString *)title andDescription:(NSString *)description {
    
    // purchase confirmation
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Confirmação"
                                                                   message:[NSString stringWithFormat:@"Você deseja atualizar o projeto \"%@\"?", self.project.title]
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"Sim"
                                                       style:UIAlertActionStyleDestructive
                                                     handler:^(UIAlertAction * action) {
                                                         [self callUpdateProjectServiceWithTitle:title andDescription:description];
                                                     }];
    UIAlertAction *cancelButton = [UIAlertAction actionWithTitle:@"Cancelar"
                                                           style:UIAlertActionStyleCancel
                                                         handler:^(UIAlertAction * action) {}];
    
    [alert addAction:okButton];
    [alert addAction:cancelButton];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)callUpdateProjectServiceWithTitle:(NSString *)title andDescription:(NSString *)description {
    [self.activityIndicator startAnimating];
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    
    NSString *imageBase64Text = (self.isPictureSelected) ?
                                    [UIImagePNGRepresentation(self.selectedImage) base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed]
                                    : nil;
    
    [Service insertEntryWithTitle:title description:description andImageString:imageBase64Text toProjectWithIdentifier:self.project.internalBaseClassIdentifier andCompletion:^(id results, BOOL hasError, NSString *errorMessage) {
    
        if (hasError) {
            // alert setup
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Erro"
                                                                           message:nil
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"OK"
                                                               style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction * action) {}];
            [alert addAction:okButton];
            
//            if (errorMessage != nil) {
//                [self presentFeedbackSuccessful:NO andMessage:errorMessage];
//            } else {
                [self presentFeedbackSuccessful:NO andMessage:@"Não foi possível atualizar o projeto."];
//            }
            [self.activityIndicator stopAnimating];
            [[UIApplication sharedApplication] endIgnoringInteractionEvents];
            [self presentViewController:alert animated:YES completion:nil];
            return;
        } else {
            NSLog(@"🌱 Update successful!");
            [self.activityIndicator stopAnimating];
            [[UIApplication sharedApplication] endIgnoringInteractionEvents];
            [self presentFeedbackSuccessful:YES andMessage:@"Projeto atualizado com sucesso!"];
        }
    }];
}

- (void)presentFeedbackSuccessful:(BOOL)successful andMessage:(NSString *)message {
    
    // purchase confirmation
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleAlert];
    alert.title = successful ? @"Sucesso" : @"Erro";
    alert.message = message;
    UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"OK"
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action) {
                                                         if (successful) {
                                                             [self.navigationController dismissViewControllerAnimated:YES completion:nil];
                                                         }
                                                     }];
    [alert addAction:okButton];
    [self presentViewController:alert animated:YES completion:nil];
}

@end

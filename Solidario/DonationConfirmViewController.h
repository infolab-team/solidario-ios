//
//  DonationConfirmViewController.h
//  Solidario
//
//  Created by Paula Vasconcelos Gueiros on 20/10/16.
//  Copyright © 2016 Paula Vasconcelos Gueiros. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProjectShort.h"
#import "Donation.h"

@interface DonationConfirmViewController : UIViewController

@property (strong, nonatomic) ProjectShort *project;
@property (strong, nonatomic) Donation *donation;
@property (strong, nonatomic) NSString *billetURL;

@end

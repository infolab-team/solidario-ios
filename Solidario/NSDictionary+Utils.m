//
//  NSDictionary+Utils.m
//  Solidario
//
//  Created by Pan on 02/05/17.
//  Copyright © 2017 Paula Vasconcelos Gueiros. All rights reserved.
//

#import "NSDictionary+Utils.h"

@implementation NSDictionary(Utils)

+ (NSDictionary *)loadPlistWithName:(NSString *)name {
    NSURL *url = [[NSBundle mainBundle] URLForResource:name withExtension:@"plist"];
    return [NSDictionary dictionaryWithContentsOfURL:url];
}

@end

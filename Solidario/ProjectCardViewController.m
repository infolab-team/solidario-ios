//
//  ProjectCardViewController.m
//  Solidario
//
//  Created by Paula Vasconcelos Gueiros on 7/11/16.
//  Copyright © 2016 Paula Vasconcelos Gueiros. All rights reserved.
//

#import "ProjectCardViewController.h"
#import "BannerCollectionViewCell.h"
#import "UIColor+Utils.h"
#import "Utils.h"

@implementation ProjectCardViewController

static const CGFloat bannerViewProportion = 164.f / 280.f;
static const CGFloat defaultViewPadding = 20.f;
static const CGFloat pageControlScale = 1.5f;

#pragma mark - Initialization

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setup];
}

- (void)setup {
    // setup superview
    [self.superview.topHiddenView setBackgroundColor:[UIColor clearColor]];
    [self.superview.imageContainerView setBackgroundColor:[UIColor clearColor]];
    [self.superview.bottomHiddenView setBackgroundColor:[UIColor clearColor]];
    
    // setup collection view
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    [flowLayout setMinimumInteritemSpacing:0.0f];
    [flowLayout setMinimumLineSpacing:0.0f];
    [self.superview.bannerCollectionView setPagingEnabled:YES];
    [self.superview.bannerCollectionView setCollectionViewLayout:flowLayout];
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    CGFloat viewHeight, viewWidth;
    viewWidth = [[UIScreen mainScreen]bounds].size.width - (2 * defaultViewPadding);
    viewHeight = viewWidth * bannerViewProportion;
    gradient.frame = CGRectMake(0, 0, viewWidth, viewHeight);
    gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor clearColor] CGColor], (id)[[UIColor clearColor] CGColor], (id)[[UIColor defaultColor:DefaultColorBannerShadow] CGColor], nil];
    [self.superview.gradientSuperview.layer insertSublayer:gradient atIndex:0];

    // setup page control
    [self.superview.bannerPageControl setCurrentPageIndicatorTintColor:[UIColor defaultColor:DefaultColorIntroSelectedPageDot]];
    self.superview.bannerPageControl.transform = CGAffineTransformMakeScale(pageControlScale, pageControlScale);
}
    


@end

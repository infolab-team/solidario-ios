//
//  DonationActivity.m
//
//  Created by Paula Vasconcelos on 16/06/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "DonationActivity.h"
#import "ProjectBasic.h"

NSString *const kDonationActivityAmount = @"amount";
NSString *const kDonationActivityStatus = @"status";
NSString *const kDonationActivityProjectBasic = @"project";
NSString *const kDonationActivityType = @"type";
NSString *const kDonationActivityDate = @"date";


@interface DonationActivity ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation DonationActivity

@synthesize amount = _amount;
@synthesize status = _status;
@synthesize project = _project;
@synthesize type = _type;
@synthesize date = _date;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
        self.amount = [[self objectOrNilForKey:kDonationActivityAmount fromDictionary:dict] doubleValue];
        self.status = [self objectOrNilForKey:kDonationActivityStatus fromDictionary:dict];
        self.project = [ProjectBasic modelObjectWithDictionary:[dict objectForKey:kDonationActivityProjectBasic]];
        self.type = [self objectOrNilForKey:kDonationActivityType fromDictionary:dict];
        self.date = [self objectOrNilForKey:kDonationActivityDate fromDictionary:dict];
        
    }
    return self;
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.amount] forKey:kDonationActivityAmount];
    [mutableDict setValue:self.status forKey:kDonationActivityStatus];
    [mutableDict setValue:[self.project dictionaryRepresentation] forKey:kDonationActivityProjectBasic];
    [mutableDict setValue:self.type forKey:kDonationActivityType];
    [mutableDict setValue:self.date forKey:kDonationActivityDate];
    
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}


#pragma mark - Helper Method

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    
    self.amount = [aDecoder decodeDoubleForKey:kDonationActivityAmount];
    self.status = [aDecoder decodeObjectForKey:kDonationActivityStatus];
    self.project = [aDecoder decodeObjectForKey:kDonationActivityProjectBasic];
    self.type = [aDecoder decodeObjectForKey:kDonationActivityType];
    self.date = [aDecoder decodeObjectForKey:kDonationActivityDate];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    
    [aCoder encodeDouble:_amount forKey:kDonationActivityAmount];
    [aCoder encodeObject:_status forKey:kDonationActivityStatus];
    [aCoder encodeObject:_project forKey:kDonationActivityProjectBasic];
    [aCoder encodeObject:_type forKey:kDonationActivityType];
    [aCoder encodeObject:_date forKey:kDonationActivityDate];
}

- (id)copyWithZone:(NSZone *)zone {
    DonationActivity *copy = [[DonationActivity alloc] init];
    
    if (copy) {
        
        copy.amount = self.amount;
        copy.status = [self.status copyWithZone:zone];
        copy.project = [self.project copyWithZone:zone];
        copy.type = [self.type copyWithZone:zone];
        copy.date = [self.date copyWithZone:zone];
    }
    return copy;
}

@end

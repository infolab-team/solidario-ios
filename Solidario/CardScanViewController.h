//
//  CardScanViewController.h
//  Solidario
//
//  Created by Pan on 21/12/16.
//  Copyright © 2016 Paula Vasconcelos Gueiros. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CardIO.h"

@class ProjectShort;
@class Donation;

@interface CardScanViewController : UIViewController <CardIOViewDelegate>

@property (strong, nonatomic) ProjectShort *project;
@property (strong, nonatomic) Donation *donation;
@property (strong, nonatomic) UIImage *mainImage;

@property (weak, nonatomic) IBOutlet CardIOView *cardView;

@property BOOL userIsSavingCard;

@end

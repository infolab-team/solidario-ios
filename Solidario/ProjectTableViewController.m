//
//  ProjectTableViewController.m
//  Solidario
//
//  Created by Paula Vasconcelos Gueiros on 12/9/16.
//  Copyright © 2016 Paula Vasconcelos Gueiros. All rights reserved.
//

#import "ProjectTableViewController.h"
#import "ProjectTabsTableViewCell.h"
#import "ProjectTableViewCell.h"
#import "UpdateProjectTableViewController.h"
#import "ProjectDetailViewController.h"
#import "UpdateProjectTableViewController.h"
#import "ProjectDetailViewController.h"
#import "ProjectShort.h"
#import "Utils.h"
#import "KAProgressLabel.h"
#import "UIView+Utils.h"
#import "NSString+Utils.h"
#import "NSDictionary+Utils.h"
#import "UIColor+Utils.h"
#import "Service.h"
#import "Singleton.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/UIButton+WebCache.h>

@interface ProjectTableViewController ()

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UIButton *userPicture;
@property (weak, nonatomic) IBOutlet UILabel *userName;

@property (strong, nonatomic) ProjectTabsTableViewCell *sectionView;

@property (strong, nonatomic) NSMutableArray <ProjectShort *> *projects;
@property (strong, nonatomic) NSMutableArray <ProjectShort *> *projectsCreated;
@property (strong, nonatomic) NSMutableArray <ProjectShort *> *projectsDonated;
@property (strong, nonatomic) NSMutableArray <ProjectShort *> *projectsFavorite;
@property (strong, nonatomic) ProjectShort *selectedProject;
@property (strong, nonatomic) NSDictionary *projectStatusDictionary;
@property (strong, nonatomic) NSMutableArray <NSNumber *> *pageNumberForList;

@property BOOL refreshControlIsActive;
@property BOOL searchingForProjects;
@property BOOL performingFirstLoad;
@property BOOL loadingProjectsFromSameList;

@end


@implementation ProjectTableViewController

static const CGFloat sectionHeight = 64.f;
static const CGFloat projectBigCellHeight = 332.f;
static const CGFloat projectSmallCellHeight = 136.f;
static const CGFloat plainCellHeight = 82.f;

static const CGFloat createdLeadingConstant = 0.f;
static const CGFloat donatedLeadingConstant = 80.f;
static const CGFloat favoriteLeadingConstant = 160.f;

static const NSInteger itemsPerPage = 10;


#pragma mark - Initialization

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setup];
}

- (void)setup {
    // setup header view with user data
    User *loggedUser = [Singleton loggedUser];

    // user profile picture
    if (![loggedUser.image isEqualToString:@""]) {
        NSURL *imageURL = [NSURL URLWithString:[NSString encodedStringFromString:loggedUser.image]];
        
        // check if image src is base64 or url
        if ([loggedUser.image hasPrefix:@"data:image"]) {
            NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
            [self.userPicture setBackgroundImage:[UIImage imageWithData:imageData] forState:UIControlStateNormal];
        } else {
            [self.userPicture sd_setBackgroundImageWithURL:imageURL forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"defaultUser"] completed:nil];
        }
    } else {
        [self.userPicture setBackgroundImage:[UIImage imageNamed:@"defaultUser"] forState:UIControlStateNormal];
    }

    self.userName.text = [NSString stringWithFormat:@"%@ %@", loggedUser.name, loggedUser.surname];
    
    // initialize flow variable
    self.searchingForProjects = YES;
    self.performingFirstLoad = YES;
    
    // setup refresh control view and target
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl setTintColor:[UIColor defaultColor:DefaultColorMain]];
    [self.refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
    
    // setup notification observers
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(favoriteOptionChanged:) name:@"favoriteOptionChanged" object:nil];
    
    // setup project status dictionary
    self.projectStatusDictionary = [NSDictionary loadPlistWithName:@"ProjectStatus"];

    // initialize project lists
    self.projectsCreated = [NSMutableArray array];
    self.projectsDonated = [NSMutableArray array];
    self.projectsFavorite = [NSMutableArray array];
    
    // initialize page number array for each project list for further use in service calls
    self.pageNumberForList = [NSMutableArray arrayWithObjects:[NSNumber numberWithInteger:1],
                                                                [NSNumber numberWithInteger:1],
                                                                [NSNumber numberWithInteger:1], nil];
    
    // setup tableview data
    [self loadProjectArray];
}

- (void)loadProjectArray {
    NSInteger option = [self.projectListOption integerValue];
    
    switch (option) {
        case CreatedOption:
            self.projects = self.projectsCreated;
            break;
        case DonatedOption:
            self.projects = self.projectsDonated;
            break;
        case FavoriteOption:
            self.projects = self.projectsFavorite;
            break;
        default:
            break;
    }
    
    if ([self.projects count] == 0 || self.refreshControlIsActive) {
        if (!self.refreshControlIsActive) { // user is selecting a tab with no results, initially
            self.projects = [NSMutableArray array];
            [self.tableView reloadData];
            [self.activityIndicator startAnimating];
        }
        NSLog(@"👓 Attempting to retrieve projects...");
        [self retrieveProjects];
    } else { // user changed for a tab that already had results
        [self.activityIndicator stopAnimating];
        self.searchingForProjects = NO;
        [self.tableView reloadData];
        [self setupSectionView:YES];
    }
}

- (void)retrieveProjects {
    self.searchingForProjects = YES;
    
    NSInteger option = [self.projectListOption integerValue];
    
    // if user activated refresh control, service calls should restart from first page and corresponding array should be emptied
    if (self.refreshControlIsActive) {
        self.pageNumberForList[option-1] = [NSNumber numberWithInteger:1];
        switch (option) {
            case CreatedOption:
                self.projectsCreated = [NSMutableArray array]; break;
            case DonatedOption:
                self.projectsDonated = [NSMutableArray array]; break;
            case FavoriteOption:
                self.projectsFavorite = [NSMutableArray array]; break;
            default: break;
        }
    }
    
    NSInteger pageNumber = [self.pageNumberForList[option-1] integerValue];

    [Service getMyProjectListWithOptionNumber:option onPage:pageNumber withItemsPerPage:itemsPerPage andCompletion:^(id results, BOOL hasError, NSString *errorMessage) {
        [self.refreshControl endRefreshing];
        [self.activityIndicator stopAnimating];
        
        if (hasError) {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Erro"
                                                                           message:nil
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"OK"
                                                               style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction * action) {}];
            [alert addAction:okButton];
//            if (errorMessage != nil) {
//                [alert setMessage:errorMessage];
//            } else {
                [alert setMessage:@"Não foi possível recuperar os projetos."];
//            }
            [self presentViewController:alert animated:YES completion:nil];
            
        } else {
            NSInteger numberOfResults = [(NSMutableArray *)results count];
            NSLog(@"📁 %lu project(s) found on page #️⃣%ld", (unsigned long)numberOfResults, (long)pageNumber);
            
            if (numberOfResults > 0) {
                // increment page number variable if call returned results
                self.pageNumberForList[option-1] = [NSNumber numberWithInteger:pageNumber+1];
                
                switch (option) {
                    case CreatedOption:
                        [self.projectsCreated addObjectsFromArray:(NSMutableArray *)results];
                        self.projects = self.projectsCreated;
                        break;
                    case DonatedOption:
                        [self.projectsDonated addObjectsFromArray:(NSMutableArray *)results];
                        self.projects = self.projectsDonated;
                        break;
                    case FavoriteOption:
                        [self.projectsFavorite addObjectsFromArray:(NSMutableArray *)results];
                        self.projects = self.projectsFavorite;
                        break;
                    default:
                        break;
                }
                
                // load new results into tableview
                NSInteger numberOfProjects = [self.projects count];
                if (numberOfResults == numberOfProjects) { // if first results, reload all data
                    [self.tableView reloadData];
                } else { // if additional results, append new rows
                    NSMutableArray *newIndexPaths = [NSMutableArray arrayWithCapacity:numberOfResults];
                    for (int i = 1; i <= numberOfResults; i++) {
                        NSIndexPath *newRowIndexPath = [NSIndexPath indexPathForRow:numberOfProjects-i inSection:0];
                        [newIndexPaths addObject:newRowIndexPath];
                    }
                    [self.tableView beginUpdates];
                    [self.tableView insertRowsAtIndexPaths:newIndexPaths withRowAnimation:UITableViewRowAnimationAutomatic];
                    [self.tableView endUpdates];
                }
            }
            
            if (!self.loadingProjectsFromSameList) {
                [self setupSectionView:YES];
            }
        }
        self.refreshControlIsActive = NO;
        self.searchingForProjects = NO;
        self.loadingProjectsFromSameList = NO;
    }];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    if (self.isBeingLoaded) {
        [self setupSectionView:NO];
        self.isBeingLoaded = NO;
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // setup activity indicator
    UIWindow *frontWindow = [[UIApplication sharedApplication] keyWindow];
    [frontWindow addSubview:self.activityIndicator];
    self.activityIndicator.center = frontWindow.center;
    [self.activityIndicator setFrame:CGRectMake(self.activityIndicator.frame.origin.x, self.activityIndicator.frame.origin.y+100, self.activityIndicator.frame.size.width, self.activityIndicator.frame.size.height)];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    // stop activityIndicator on cases in which it's still animating
    [self.activityIndicator stopAnimating];
    [self.activityIndicator removeFromSuperview];
}


#pragma mark - UI Setup

- (void)setupSectionView:(BOOL)animated {
    NSInteger option = [self.projectListOption integerValue];

    switch (option) {
        case CreatedOption:
            if (animated) NSLog(@"🔄 Selected Tab: 🌎 Created projects page");
            self.navigationItem.title = @"Criados por mim";
            self.sectionView.selectedViewLeadingConstraint.constant = createdLeadingConstant;
            break;
        case DonatedOption:
            if (animated) NSLog(@"🔄 Selected Tab: 💰 Supported projects page");
            self.navigationItem.title = @"Projetos que ajudei";
            self.sectionView.selectedViewLeadingConstraint.constant = donatedLeadingConstant;
            break;
        case FavoriteOption:
            if (animated) NSLog(@"🔄 Selected Tab: ❤️ Favorite projects page");
            self.navigationItem.title = @"Meus favoritos";
            self.sectionView.selectedViewLeadingConstraint.constant = favoriteLeadingConstant;
            break;
        default:
            break;
    }
    
    if (animated) {
        [UIView animateWithDuration:0.3f animations:^{
            [self modifySectionView];
            [self.sectionView layoutIfNeeded];
            
        } completion:^(BOOL finished) {
            [self.tableView reloadData];
            
            if (self.performingFirstLoad) {
                self.performingFirstLoad = NO;
                return;
            }

            dispatch_async(dispatch_get_main_queue(), ^{
                [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
            });
        }];
    } else {
        [self modifySectionView];
    }
}

- (void)modifySectionView {
    self.sectionView.createdImage.highlighted = ([self.projectListOption integerValue] == CreatedOption);
    self.sectionView.donatedImage.highlighted = ([self.projectListOption integerValue] == DonatedOption);
    self.sectionView.favoriteImage.highlighted = ([self.projectListOption integerValue] == FavoriteOption);
}


#pragma mark - UITableView Delegate and Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if ([self.projects count]) {
        [self.tableView setAllowsSelection:YES];
    } else {
        [self.tableView setAllowsSelection:NO];
    }
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.projects count] ? [self.projects count] : 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (![self.projects count]) {
        UITableViewCell *cell;
        if (self.searchingForProjects) {
            cell = [tableView dequeueReusableCellWithIdentifier:@"emptyCell" forIndexPath:indexPath];
            return cell;
        }
        cell = [tableView dequeueReusableCellWithIdentifier:@"noResultsCell" forIndexPath:indexPath];
        return cell;
    }

    ProjectShort *project = [self.projects objectAtIndex:indexPath.row];
    ProjectTableViewCell *cell;
    CGFloat percentageFontSize, percentSymbolFontSize;
    
    if ([self.projectListOption integerValue] == FavoriteOption) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"projectSmallTableViewCell" forIndexPath:indexPath];
        percentageFontSize = 13.f;
        percentSymbolFontSize = 8.f;
    } else {
        cell = [tableView dequeueReusableCellWithIdentifier:@"projectBigTableViewCell" forIndexPath:indexPath];
        percentageFontSize = 20.f;
        percentSymbolFontSize = 13.f;
        if ([self.projectListOption integerValue] == DonatedOption) {
            [cell.updateButton setHidden:YES];
        } else {
            [cell.updateButton setHidden:NO];
            [cell.updateButton setTag:indexPath.row];
        }
    }
    
    NSString *imageUrlString = [NSString encodedStringFromString:project.media[0]];
    [cell.projectImageView sd_setImageWithURL:[NSURL URLWithString:imageUrlString] placeholderImage:[UIImage imageNamed:@"webImagePlaceholder"]];
    float raisedPercentage = project.percent;
    
    // animate Progress Label
    cell.pLabel.endDegree = 0.f;
    [cell.pLabel setEndDegree:3.6 * raisedPercentage timing:TPPropertyAnimationTimingEaseInEaseOut duration:1.f delay:0];
    
    NSMutableAttributedString *percentageAttString = [[NSMutableAttributedString alloc] initWithString: [NSString stringWithFormat:@"%.0f%%", raisedPercentage]];
    [percentageAttString addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Helvetica Bold" size:percentageFontSize] range:NSMakeRange(0, percentageAttString.length-1)];
    [percentageAttString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:percentSymbolFontSize] range:NSMakeRange(percentageAttString.length-1, 1)];
    cell.pLabel.attributedText = percentageAttString;
    
    cell.statusLabel.text = [self.projectStatusDictionary objectForKey:project.status];
    cell.projectTitleLabel.text = project.title;
    cell.numberOfDonorsLabel.text = [NSString stringWithFormat:@"%.0f", project.donators];
    cell.peopleDonatedLabel.text = (project.donators == 1) ? @"PESSOA JÁ DOOU" : @"PESSOAS JÁ DOARAM";
    
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    self.sectionView = [tableView dequeueReusableCellWithIdentifier:@"projectTabsTableViewCell"];
    if (self.sectionView == nil){
        [NSException raise:@"headerView == nil.." format:@"No cells with matching CellIdentifier loaded from your storyboard"];
    }
    [self setupSectionView:NO];
    
    UITapGestureRecognizer *tapCreated = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectedCreatedOption)];
    [tapCreated setNumberOfTapsRequired:1];
    [self.sectionView.createdImage addGestureRecognizer:tapCreated];
    
    UITapGestureRecognizer *tapDonated = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectedDonatedOption)];
    [tapDonated setNumberOfTapsRequired:1];
    [self.sectionView.donatedImage addGestureRecognizer:tapDonated];
    
    UITapGestureRecognizer *tapFavorite = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectedFavoriteOption)];
    [tapFavorite setNumberOfTapsRequired:1];
    [self.sectionView.favoriteImage addGestureRecognizer:tapFavorite];

    return self.sectionView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return sectionHeight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return
        (![self.projects count]) ? plainCellHeight :
        ([self.projectListOption integerValue] == FavoriteOption) ? projectSmallCellHeight :
        projectBigCellHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    self.selectedProject = [self.projects objectAtIndex:indexPath.row];
    [self performSegueWithIdentifier:@"modalToProjectDetail" sender:self];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([self.projects count] > 0 && indexPath.row == [self.projects count]-1) {
        if (!self.searchingForProjects) {
            self.loadingProjectsFromSameList = YES;
            NSLog(@"➕ Attempting to retrieve more projects...");
            [self retrieveProjects];
        }
    }
}


#pragma mark - UITableView Helper Methods

- (void)refreshTable {
    self.refreshControlIsActive = YES;
    [self.tableView setContentOffset:CGPointMake(0, -self.refreshControl.frame.size.height) animated:NO];
    [self loadProjectArray];
}


#pragma mark - Button actions

- (void)selectedCreatedOption {
    if (self.projectListOption != [NSNumber numberWithInteger:CreatedOption]) {
        self.projectListOption = [NSNumber numberWithInteger:CreatedOption];
        [self prepareForListChange];
    }
}

- (void)selectedDonatedOption {
    if (self.projectListOption != [NSNumber numberWithInteger:DonatedOption]) {
        self.projectListOption = [NSNumber numberWithInteger:DonatedOption];
        [self prepareForListChange];
    }
}

- (void)selectedFavoriteOption {
    if (self.projectListOption != [NSNumber numberWithInteger:FavoriteOption]) {
        self.projectListOption = [NSNumber numberWithInteger:FavoriteOption];
        [self prepareForListChange];
    }
}

- (void)prepareForListChange {
    [self loadProjectArray];
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    UINavigationController *navController = (UINavigationController *)[segue destinationViewController];
    
    if ([segue.identifier isEqualToString:@"modalToUpdate"]) {
        if ([sender isKindOfClass:[UIButton class]]) {
            UIButton *button = (UIButton *)sender;
            UpdateProjectTableViewController *updateProjectTableViewController = (UpdateProjectTableViewController *)[navController topViewController];
            updateProjectTableViewController.project = self.projects[button.tag];
        }
    } else if ([segue.identifier isEqualToString:@"modalToProjectDetail"]) {
        UINavigationController *navController = (UINavigationController *)[segue destinationViewController];
        ProjectDetailViewController *projectDetailViewController = (ProjectDetailViewController *)[navController topViewController];
        projectDetailViewController.projectShort = [self.selectedProject copy];
        projectDetailViewController.selectedBannerPage = 0;
        projectDetailViewController.isFinalized = ![[self.projectStatusDictionary objectForKey:self.selectedProject.status] isEqualToString:@"Em andamento"];
    }
}

- (IBAction)unwindToProjectList:(UIStoryboardSegue*)unwindSegue {
    
}

- (IBAction)unwindToBeforeProjectDetail:(UIStoryboardSegue*)unwindSegue {
    
}


#pragma mark - Notification Handlers

- (void)favoriteOptionChanged:(NSNotification *)notification {
    
    NSString *projectId = (NSString *)notification.object;
    
    NSLog(@"👀 Searching for project with favorite option change in list: Created Projects...");
    [self changeFavoriteOptionForProjectWithIdentifier:projectId inList:self.projectsCreated];
    
    NSLog(@"👀 Searching for project with favorite option change in list: Donated Projects...");
    [self changeFavoriteOptionForProjectWithIdentifier:projectId inList:self.projectsDonated];
    
    NSLog(@"👀 Searching for project with favorite option change in list: Favorite Projects...");
    [self changeFavoriteOptionForProjectWithIdentifier:projectId inList:self.projectsFavorite];
}

- (void)changeFavoriteOptionForProjectWithIdentifier:(NSString *)identifier inList:(NSArray *)projectList {
    __block NSInteger changedProjectIndex = -1;
    __block ProjectShort *changedProject;
    [projectList enumerateObjectsUsingBlock:^(ProjectShort *project, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([project.internalBaseClassIdentifier isEqualToString:identifier]) {
            changedProject = project;
            changedProjectIndex = idx;
            *stop = YES;
        }
    }];
    
    if (changedProjectIndex >= 0) {
        NSLog(@"🗃 Found changed project in ProjectTVC (for favorite/unfavorite heart touch)");
        NSLog(@"\tProject title: %@", changedProject.title);
        NSLog(@"\tProject index in table view: %ld", (long)changedProjectIndex);
        NSLog(@"\tProjectTVC value for object: isFavorite = %@", (changedProject.isFavorite) ? @"YES" : @"NO");
        
        changedProject.isFavorite = !changedProject.isFavorite;
    }
}

@end

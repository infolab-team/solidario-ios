//
//  StateTableViewController.m
//  Solidario
//
//  Created by Paula Vasconcelos Gueiros on 22/8/16.
//  Copyright © 2016 Paula Vasconcelos Gueiros. All rights reserved.
//

#import "StateTableViewController.h"
#import "MainViewController.h"
#import "StateTableViewCell.h"
#import "UIColor+Utils.h"
#import "Service.h"
#import "Singleton.h"
#import "State.h"

@interface StateTableViewController ()

@property (weak, nonatomic) IBOutlet UIButton *changeAllButton;
@property (weak, nonatomic) IBOutlet UIImageView *changeAllButtonImage;
@property (weak, nonatomic) IBOutlet UILabel *changeAllButtonLabel;
@property (weak, nonatomic) IBOutlet UIView *changeAllSuperview;
@property (weak, nonatomic) IBOutlet UIView *separator1View;
@property (weak, nonatomic) IBOutlet UIView *separator2View;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (strong, nonatomic) NSArray <State *> *states;
@property (nonatomic) BOOL statesRetrieved;
@property (nonatomic) BOOL allSelected;

@end


@implementation StateTableViewController

static const NSInteger constraintPriority1 = 900;
static const NSInteger constraintPriority2 = 500;

#pragma mark - Initialization

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setup];
}

- (void)setup {
    // user feedback message: current controller storyboard location
    NSLog(@"📍 StateTVC Location: %@.storyboard", [self.storyboard valueForKey:@"name"]);
    
    // initialize states array pointer
    self.states = [[NSMutableArray alloc] init];
    
    // hide views until list is shown
    [self setViewsHidden:YES];
    
    [self.activityIndicator startAnimating];
    // call State service and populate states array
    [Service getAllStatesWithCompletion:^(id results, BOOL hasError, NSString *errorMessage) {
        [self.activityIndicator stopAnimating];
        
        if (hasError) {
            self.statesRetrieved = NO;
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Erro"
                                                                           message:nil
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"OK"
                                                               style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction * action) {}];
            [alert addAction:okButton];
            
//            if (errorMessage != nil) {
//                [alert setMessage:errorMessage];
//            } else {
                [alert setMessage:@"Não foi possível salvar as escolhas de localização.  Tente novamente mais tarde."];
//            }
            [self presentViewController:alert animated:YES completion:nil];
            return;
        } else {
            
            // save states to array
            self.statesRetrieved = YES;
            NSLog(@"🇧🇷 States retrieval successful");
            
            if (self.fromLogin) {
                self.states = (NSArray *)results;
            } else {
                [self updateStatesWithArray:(NSMutableArray *)results];
            }
            
            [self setViewsHidden:NO];
            [self checkAllSelected];
            [self.tableView reloadData];
        }
    }];
}

- (void)setViewsHidden:(BOOL)hidden {
    
    // show or hide change all button and separator views
    [self.changeAllSuperview setHidden:hidden];
    [self.separator1View setHidden:hidden];
    [self.separator2View setHidden:hidden];
}

- (void)updateStatesWithArray:(NSMutableArray *)newStates {
    
    NSArray *oldStates = [Singleton states];
    
    [newStates enumerateObjectsUsingBlock:^(id obj1, NSUInteger idx1, BOOL *stop1) {
        State *newState = ((State *)obj1);
        
        [oldStates enumerateObjectsUsingBlock:^(id obj2, NSUInteger idx2, BOOL *stop2) {
            State *oldState = ((State *)obj2);
            
            if ([oldState.internalBaseClassIdentifier isEqualToString:newState.internalBaseClassIdentifier]) {
                [newState setSelected:oldState.selected];
                *stop2 = YES;
            }
        }];
    }];
    
    self.states = newStates;
}

- (void)checkAllSelected {
    self.allSelected = YES;
    for (State *state in self.states) {
        if (!state.selected) {
            self.allSelected = NO;
            break;
        }
    }
    if (self.allSelected) {
        [self invertAllButton];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // setup activityIndicator position
    UIWindow *frontWindow = [[UIApplication sharedApplication] keyWindow];
    [frontWindow addSubview:self.activityIndicator];
    self.activityIndicator.center = frontWindow.center;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    // stop activityIndicator on cases in which it's still animating
    [self.activityIndicator stopAnimating];
    [self.activityIndicator removeFromSuperview];
}


#pragma mark - UITableView Delegate and Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.states count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44.f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    StateTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"stateCell" forIndexPath:indexPath];

    State *state = [self.states objectAtIndex:indexPath.row];
    cell.stateNameLabel.text = state.name;
    
    if (state.count) {
        [cell.projectQuantitySuperview setHidden:NO];
        cell.projectQuantityLabel.text = [[NSNumber numberWithDouble:state.count] stringValue];
    } else {
        [cell.projectQuantitySuperview setHidden:YES];
    }

    if (state.selected) {
        [cell.checkImage setHidden:NO];
        [self.tableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionNone];
        cell.leadingSpaceLabelToSuperviewConstraint.priority = constraintPriority2;
        cell.leadingSpaceLabelToImageConstraint.priority = constraintPriority1;
    } else {
        [cell.checkImage setHidden:YES];
        [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
        cell.leadingSpaceLabelToSuperviewConstraint.priority = constraintPriority1;
        cell.leadingSpaceLabelToImageConstraint.priority = constraintPriority2;
    }

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    State *state = [self.states objectAtIndex:indexPath.row];
    [state setSelected:YES];
    NSLog(@"🔵 Selected: %@", state.name);
    
    StateTableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    [self setLayoutForCell:cell selected:YES];

    // check if all are selected with this option
    [self checkAllSelected];
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    State *state = [self.states objectAtIndex:indexPath.row];
    [state setSelected:NO];
    NSLog(@"🔴 Deselected: %@", state.name);
    
    StateTableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    [self setLayoutForCell:cell selected:NO];
    
    // change button if all were selected
    if (self.allSelected) {
        self.allSelected = NO;
        [self invertAllButton];
    }
}

#pragma mark - Button Actions

- (IBAction)changeAll:(id)sender {

    self.allSelected = !self.allSelected;
    self.allSelected ? NSLog(@"🔃 User selected all 🔵") : NSLog(@"🔃 User deselected all 🔴");
    for (State *state in self.states) {
        [state setSelected:self.allSelected];
    }

    [self.tableView reloadData];
    [self invertAllButton];
}

- (void)invertAllButton {
    if (self.allSelected) {
        self.changeAllButton.backgroundColor = [UIColor defaultColor:DefaultColorMain];
        [self.changeAllButtonImage setHidden:NO];
        self.changeAllButtonLabel.text = @"Desmarcar todos";
    } else {
        self.changeAllButton.backgroundColor = [UIColor clearColor];
        [self.changeAllButtonImage setHidden:YES];
        self.changeAllButtonLabel.text = @"Marcar todos";
    }
}

- (IBAction)save:(id)sender {
    NSLog(@"💾 User pressed save button");
    [self performInputValidations];
}

- (void)performInputValidations {

    // check if no error occurred in Service for states retrieval
    if (self.statesRetrieved) {
        
        // at leats one selected state confirmation
        __block BOOL oneSelected = NO;
        [self.states enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            if (((State *)obj).selected) {
                oneSelected = YES;
                *stop = YES;
            }
        }];
        
        if (!oneSelected) {
            // alert setup
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Atenção"
                                                                           message:@"Você deve selecionar ao menos um Estado."
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"OK"
                                                               style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction * action) {}];
            [alert addAction:okButton];
            [self presentViewController:alert animated:YES completion:nil];
            return;
        }
    }
    
    [self proceedWithSave];
}
    
- (void)proceedWithSave {

    if (self.statesRetrieved) {
        [Singleton saveStates:self.states];
        NSLog(@"📌 Location settings definition successful");
    } else {
        NSLog(@"⚠️ States not retrieved from service: none will be saved in user settings");
    }
    
    if (self.fromLogin) {
        NSString *storyboardIdentifier = @"Main";
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardIdentifier bundle:nil];
        
        UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"mainNavigationController"];
        MainViewController *mainViewController = [storyboard instantiateViewControllerWithIdentifier:@"mainViewController"];
        mainViewController.rootViewController = navigationController;
        [mainViewController setup];
        
        UIWindow *window = [UIApplication sharedApplication].delegate.window;
        window.rootViewController = mainViewController;
        [UIView transitionWithView:window
                          duration:0.5
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:nil
                        completion:nil];
    } else {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"userSettingsChanged" object:nil];
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }
}


#pragma mark - Helper methods

- (void)setLayoutForCell:(StateTableViewCell *)cell selected:(BOOL)selected {
    if (selected) {
        [cell.checkImage setHidden:NO];
        cell.leadingSpaceLabelToSuperviewConstraint.priority = constraintPriority2;
        cell.leadingSpaceLabelToImageConstraint.priority = constraintPriority1;
    } else {
        [cell.checkImage setHidden:YES];
        cell.leadingSpaceLabelToSuperviewConstraint.priority = constraintPriority1;
        cell.leadingSpaceLabelToImageConstraint.priority = constraintPriority2;
    }
}

@end

//
//  UnwindSegueToCardScan.m
//  Solidario
//
//  Created by Pan on 19/06/17.
//  Copyright © 2017 Paula Vasconcelos Gueiros. All rights reserved.
//

#import "UnwindSegueToCardScan.h"
#import "CompleteCardInfoViewController.h"

@implementation UnwindSegueToCardScan

- (void)perform {
    CompleteCardInfoViewController *sourceVC = (CompleteCardInfoViewController *)self.sourceViewController;

    // animate views
    [UIView animateWithDuration: 0.3f animations:^{
        [sourceVC.view setAlpha:0.f];

    } completion:^(BOOL finished) {
        [sourceVC.navigationController popViewControllerAnimated:NO];
    }];
}

@end

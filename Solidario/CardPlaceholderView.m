 //
//  CardPlaceholderView.m
//  Solidario
//
//  Created by Pan on 12/12/16.
//  Copyright © 2016 Paula Vasconcelos Gueiros. All rights reserved.
//

#import "CardPlaceholderView.h"

@implementation CardPlaceholderView

#pragma mark - Initialization
    
- (void)drawRect:(CGRect)rect {
    [self.topHiddenView setBackgroundColor:[UIColor clearColor]];
    [self.imageContainerView setBackgroundColor:[UIColor clearColor]];
    [self.bottomHiddenView setBackgroundColor:[UIColor clearColor]];
}

@end

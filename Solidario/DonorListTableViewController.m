//
//  DonorListTableViewController.m
//  Solidario
//
//  Created by Pan on 03/05/17.
//  Copyright © 2017 Paula Vasconcelos Gueiros. All rights reserved.
//

#import "DonorListTableViewController.h"
#import "DonorTableViewCell.h"

#import "NSString+Utils.h"

#import "DonatorList.h"
#import "Donator.h"

#import "Service.h"

#import <SDWebImage/UIImageView+WebCache.h>

@interface DonorListTableViewController ()

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (strong, nonatomic) DonatorList *donatorList;
@property (nonatomic) double donatorCount;
@property (nonatomic) BOOL emptyList;

@end

@implementation DonorListTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setup];
}

- (void)setup {
    // initialize control variables;
    self.donatorCount = 0;
    self.emptyList = NO;
    
    // setup activityIndicator
    [self.view addSubview:self.activityIndicator];
    self.activityIndicator.center = self.view.center;
    
    // check for available project object, fetch id
    NSString *projectID = (self.projectShort != nil) ? self.projectShort.internalBaseClassIdentifier : self.project.internalBaseClassIdentifier;

    [self.activityIndicator startAnimating];
    [Service getDonatorsForProjectWithIdentifier:projectID andCompletion:^(id results, BOOL hasError, NSString *errorMessage) {
        [self.activityIndicator stopAnimating];
        
        if (hasError) {
            self.emptyList = YES;
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Erro"
                                                                           message:nil
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"OK"
                                                               style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction * action) {}];
            [alert addAction:okButton];
//            if (errorMessage != nil) {
//                [alert setMessage:errorMessage];
//            } else {
                [alert setMessage:@"Não foi possível recuperar lista de doadores do projeto."];
//            }
            [self presentViewController:alert animated:YES completion:nil];
            return;
        } else {
            self.donatorList = (DonatorList *)results;
            self.donatorCount = [self.donatorList.donators count] + self.donatorList.anonCount;
            if (self.donatorCount == 0) {
                self.emptyList = YES;
            }
            [self.tableView reloadData];
        }
    }];
}


#pragma mark - UITableView Delegate and Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (self.donatorCount) {
        [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    } else {
        [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    }
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (!self.donatorCount) {
        if (self.emptyList) {
            return 1;
        }
        return 0;
    }
    return self.donatorCount;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 82.f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    DonorTableViewCell *cell;
    
    if (self.emptyList) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"noResultsCell" forIndexPath:indexPath];
        return cell;
    }
    
    cell = [tableView dequeueReusableCellWithIdentifier:@"donorCell" forIndexPath:indexPath];
    
    if (indexPath.row < [self.donatorList.donators count]) { // not anonymous donors
        Donator *donator = [self.donatorList.donators objectAtIndex:indexPath.row];

        if (![donator.image isEqualToString:@""]) {
            NSURL *imageURL = [NSURL URLWithString:[NSString encodedStringFromString:donator.image]];
            
            // check if image src is base64 or url
            if ([donator.image hasPrefix:@"data:image"]) {
                NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
                cell.donorPictureImageView.image = [UIImage imageWithData:imageData];
            } else {
                [cell.donorPictureImageView sd_setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"defaultUser"]];
            }
        } else {
            cell.donorPictureImageView.image = [UIImage imageNamed:@"defaultUser"];
        }

        cell.donorNameLabel.text = donator.name;
    } else { // anonymous donors
        cell.donorPictureImageView.image = [UIImage imageNamed:@"anonymousDonor"];
        cell.donorNameLabel.text = @"Anônimo";
    }
    
    return cell;
}

@end

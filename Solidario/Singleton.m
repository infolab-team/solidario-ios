//
//  Singleton.m
//  Solidario
//
//  Created by Paula Vasconcelos Gueiros on 17/8/16.
//  Copyright © 2016 Paula Vasconcelos Gueiros. All rights reserved.
//

#define DF_FIRST_TIME_USE               @"firstTimeUse"
#define DF_LOGGED_USER                  @"loggedUser"
#define DF_FACEBOOOK_USER               @"facebookUser"
#define DF_USER_IS_PERSISTENT           @"userIsPersistent"
#define DF_CATEGORIES                   @"categories"
#define DF_STATES                       @"states"

#import "Singleton.h"
#import "Utils.h"

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface Singleton ()

@property BOOL isFirstTimeUse;
@property BOOL isFacebookUser;
@property BOOL isDonating;
@property BOOL isUpdatingTimeline;
@property BOOL userIsPersistent;

@property (strong, nonatomic) NSString *firstTimeString;
@property (strong, nonatomic) NSArray *acceptedCardFlags;
@property (strong, nonatomic) User *loggedUser;
@property (strong, nonatomic) NSString *loggedUserAuthToken;
@property (strong, nonatomic) NSString *loggedUserID;
@property (strong, nonatomic) NSMutableArray <Category *> *categories;
@property (strong, nonatomic) NSMutableArray <State *> *states;

@property double pLabelPercentageFontSize;
@property double pLabelPercentSymbolFontSize;
@property double pLabelTrackWidth;
@property double pLabelProgressWidth;
@property NSInteger cardDescriptionLabelNumberOfLines;

@end


@implementation Singleton

#pragma mark - Singleton instance

static Singleton *instance;

+ (Singleton *)instance {
    @synchronized(self) {
        if (instance == nil) {
            instance = [[super alloc] init];
            
            // define PLabel and Project Card properties
            if (IS_IPHONE_6_7_PLUS) { // iPhone 5.5'
                instance.pLabelPercentageFontSize = 20.5f;
                instance.pLabelPercentSymbolFontSize = 13.3f;
                instance.pLabelTrackWidth = 9.2f;
                instance.pLabelProgressWidth = 12.3f;
                instance.cardDescriptionLabelNumberOfLines = 5;
            } else if (IS_IPHONE_6_7) { // iPhone 4.7'
                instance.pLabelPercentageFontSize = 18.5f;
                instance.pLabelPercentSymbolFontSize = 12.f;
                instance.pLabelTrackWidth = 8.3f;
                instance.pLabelProgressWidth = 11.f;
                instance.cardDescriptionLabelNumberOfLines = 4;
            } else { // iPhone 4'
                instance.pLabelPercentageFontSize = 15.5f;
                instance.pLabelPercentSymbolFontSize = 10.f;
                instance.pLabelTrackWidth = 7.f;
                instance.pLabelProgressWidth = 9.3f;
                instance.cardDescriptionLabelNumberOfLines = 3;
            }
        }
        return instance;
    };
    return nil;
}


#pragma mark - First Time Use

+ (BOOL)isFirstTimeUse {
    @synchronized(self) {
        if ([self instance].firstTimeString == nil) {
            [self fetchFirstTimeFromDefaults];
        }
        return instance.isFirstTimeUse;
    };
}

+ (void)fetchFirstTimeFromDefaults {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    instance.firstTimeString = [defaults objectForKey:DF_FIRST_TIME_USE];
    
    if (instance.firstTimeString == nil || [instance.firstTimeString isEqualToString:@"YES"]) {
        instance.isFirstTimeUse = YES;
    } else {
        instance.isFirstTimeUse = NO;
    }
}

+ (void)setNotFirstTimeUse {
    [self instance].isFirstTimeUse = NO;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:@"NO" forKey:DF_FIRST_TIME_USE];
    [defaults synchronize];
}


#pragma mark - PLabel and Card Project Properties

+ (double)pLabelPercentageFontSize {
    @synchronized(self) {
        return [self instance].pLabelPercentageFontSize;
    };
}

+ (double)pLabelPercentSymbolFontSize {
    @synchronized(self) {
        return [self instance].pLabelPercentSymbolFontSize;
    };
}

+ (double)pLabelTrackWidth {
    @synchronized(self) {
        return [self instance].pLabelTrackWidth;
    };
}

+ (double)pLabelProgressWidth {
    @synchronized(self) {
        return [self instance].pLabelProgressWidth;
    };
}

+ (NSInteger)cardDescriptionLabelNumberOfLines {
    @synchronized(self) {
        return [self instance].cardDescriptionLabelNumberOfLines;
    };
}


#pragma mark - Card Flag Names


+ (NSArray *)acceptedCardFlags {
    @synchronized(self) {
        if ([self instance].acceptedCardFlags == nil) {
            [self initializeAcceptedCardFlags];
        }
        return instance.acceptedCardFlags;
    };
    return nil;
}

+ (void)initializeAcceptedCardFlags {
    instance.acceptedCardFlags = [NSArray arrayWithObjects:
                                  [NSNumber numberWithInteger:CreditCardVisa],
                                  [NSNumber numberWithInteger:CreditCardMasterCard], nil];
}


#pragma mark - App Flow

+ (BOOL)isDonating {
    @synchronized(self) {
        return instance.isDonating;
    };
}

+ (void)setDonating:(BOOL)donating {
    [self instance].isDonating = donating;
}

+ (BOOL)isUpdatingTimeline {
    @synchronized(self) {
        return instance.isUpdatingTimeline;
    };
}

+ (void)setUpdatingTimeline:(BOOL)updatingTimeline {
    [self instance].isUpdatingTimeline = updatingTimeline;
}


#pragma mark - User Data

+ (User *)loggedUser {
    @synchronized(self) {
        if ([self instance].loggedUser == nil) {
            [self fetchLoggedUserFromDefaults];
        }
        return instance.loggedUser;
    };
    return nil;
}

+ (void)fetchLoggedUserFromDefaults {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *encodedObject = [defaults objectForKey:DF_LOGGED_USER];
    if (encodedObject) {
        instance.loggedUser = [NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];
        instance.loggedUserAuthToken = instance.loggedUser.authToken;
        instance.loggedUserID = instance.loggedUser.userId;
    }
    instance.isFacebookUser = [[defaults objectForKey:DF_FACEBOOOK_USER] boolValue];
    instance.userIsPersistent = [[defaults objectForKey:DF_USER_IS_PERSISTENT] boolValue];
}

+ (void)loginWithUser:(User *)user withFacebookEnabled:(BOOL)facebookEnabled andPersistent:(BOOL)persistent {
    [self instance].loggedUser = user;
    instance.loggedUserAuthToken = user.authToken;
    instance.loggedUserID = user.userId;
    instance.isFacebookUser = facebookEnabled;
    if (persistent) {
        instance.userIsPersistent = YES;
        [self setDefaultsWithLoggedUser];
    } else {
        instance.userIsPersistent = NO;
    }
}

+ (void)setDefaultsWithLoggedUser {
    NSData *encodedObject = [NSKeyedArchiver archivedDataWithRootObject:instance.loggedUser];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:encodedObject forKey:DF_LOGGED_USER];
    [defaults setBool:instance.isFacebookUser forKey:DF_FACEBOOOK_USER];
    [defaults setBool:instance.userIsPersistent forKey:DF_USER_IS_PERSISTENT];
    [defaults synchronize];
}

+ (void)logout {
    [self instance].loggedUser = nil;
    instance.loggedUserAuthToken = nil;
    instance.loggedUserID = nil;
    instance.userIsPersistent = NO;
    [FBSDKAccessToken setCurrentAccessToken:nil];
    [self setDefaultsWithLoggedUser];
}

+ (NSString *)loggedUserAuthToken {
    @synchronized(self) {
        return [self instance].loggedUserAuthToken;
    };
    return nil;
}

+ (BOOL)isThereLoggedUserAuthToken {
    if ([self instance].loggedUserAuthToken != nil) {
        return YES;
    }
    return NO;
}

+ (NSString *)loggedUserID {
    @synchronized(self) {
        return [self instance].loggedUserID;
    };
    return nil;
}

+ (BOOL)isThereLoggedUserID {
    if ([self instance].loggedUserID != nil) {
        return YES;
    }
    return NO;
}

+ (BOOL)isFacebookUser {
    @synchronized(self) {
        return instance.isFacebookUser;
    };
}

+ (void)updateLoggedUserWithUser:(UserUpdated *)user {
    [self instance].loggedUser.name = user.name;
    [self instance].loggedUser.surname = user.surname;
    [self instance].loggedUser.email = user.email;
    [self instance].loggedUser.image = user.picture;
    if (instance.userIsPersistent) {
        [self setDefaultsWithLoggedUser];
    }
}

+ (BOOL)userIsPersistent {
    @synchronized(self) {
        return instance.userIsPersistent;
    };
}


#pragma mark - Category

+ (NSArray *)categories {
    @synchronized(self) {
        if ([self instance].categories == nil) {
            [self fetchCategoriesFromDefaults];
        }
        return instance.categories;
    };
    return nil;
}

+ (void)fetchCategoriesFromDefaults {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSMutableArray *dataArray = [[NSMutableArray alloc] initWithArray:[defaults objectForKey:DF_CATEGORIES]];
    instance.categories = [[NSMutableArray alloc] init];
    for (NSData *encodedObject in dataArray) {
        Category *category = [NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];
        [instance.categories addObject:category];
    }
}

+ (void)saveCategories:(NSArray *)categories {
    [self instance].categories = [categories mutableCopy];
    [self setDefaultsWithCategories];
}

+ (void)setDefaultsWithCategories {
    NSMutableArray *dataArray = [[NSMutableArray alloc] init];
    for (Category *category in instance.categories) {
        NSData *encodedObject = [NSKeyedArchiver archivedDataWithRootObject:category];
        [dataArray addObject:encodedObject];
    }
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:dataArray forKey:DF_CATEGORIES];
    [defaults synchronize];
}


#pragma mark - State

+ (NSArray *)states {
    @synchronized(self) {
        if ([self instance].states == nil) {
            [self fetchStatesFromDefaults];
        }
        return instance.states;
    };
    return nil;
}

+ (void)fetchStatesFromDefaults {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSMutableArray *dataArray = [[NSMutableArray alloc] initWithArray:[defaults objectForKey:DF_STATES]];
    instance.states = [[NSMutableArray alloc] init];
    for (NSData *encodedObject in dataArray) {
        State *state = [NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];
        [instance.states addObject:state];
    }
}

+ (void)saveStates:(NSArray *)states {
    [self instance].states = [states mutableCopy];
    [self setDefaultsWithStates];
}

+ (void)setDefaultsWithStates {
    NSMutableArray *dataArray = [[NSMutableArray alloc] init];
    for (State *state in instance.states) {
        NSData *encodedObject = [NSKeyedArchiver archivedDataWithRootObject:state];
        [dataArray addObject:encodedObject];
    }
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:dataArray forKey:DF_STATES];
    [defaults synchronize];
}

@end

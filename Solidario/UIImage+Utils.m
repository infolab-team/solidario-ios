//
//  UIImage+Utils.m
//  Solidario
//
//  Created by Pan on 27/12/16.
//  Copyright © 2016 Paula Vasconcelos Gueiros. All rights reserved.
//

#import "UIImage+Utils.h"

@implementation UIImage(Utils)

+ (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

+ (UIImage *)cropToSquareWithImage:(UIImage *)image {

    // find out smaller side to base square in
    CGFloat squareSideSize = MIN(image.size.height, image.size.width);
    CGSize squareSize = CGSizeMake(squareSideSize, squareSideSize);
    
    // calculate offset for square in original image
    CGPoint offset = CGPointMake((image.size.width - squareSideSize)/2, (image.size.height - squareSideSize)/2);
    
    /// make the final clipping rect based on the calculated values
    CGRect clipSquare = CGRectMake(-offset.x, -offset.y, image.size.width, image.size.height);
    
    /// start a new context, with scale factor 0.0 so retina displays get high quality image
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        UIGraphicsBeginImageContextWithOptions(squareSize, YES, 0.0);
    } else {
        UIGraphicsBeginImageContext(squareSize);
    }
    UIRectClip(clipSquare);
    [image drawInRect:clipSquare];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    return newImage;
}

+ (UIImage *)shrunkenImageWithImage:(UIImage *)largeImage toLargerSideSize:(CGFloat)largerSideSize {

    // actual larger side size for device
    CGFloat largerSize = largerSideSize/[[UIScreen mainScreen] scale];
    CGFloat aspectRatio = largeImage.size.width/largeImage.size.height;

    CGSize newSize = (aspectRatio > 0) ? CGSizeMake(largerSize, largerSize/aspectRatio) : CGSizeMake(largerSize*aspectRatio, largerSize);

    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [largeImage drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *smallImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return smallImage;
}

@end

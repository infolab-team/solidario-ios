//
//  DonationDataTableViewController.m
//  Solidario
//
//  Created by Pan on 28/11/16.
//  Copyright © 2016 Paula Vasconcelos Gueiros. All rights reserved.
//

#import "DonationDataTableViewController.h"
#import "AntifraudDataTableViewController.h"
#import "CardScanViewController.h"

#import "Utils.h"
#import "UIColor+Utils.h"
#import "UIView+Utils.h"
#import "UIImage+Utils.h"
#import "NSString+Utils.h"
#import "CustomTextField.h"

#import "Singleton.h"

#import "PagarMe.h"
#import "PagarMeCreditCard.h"

@interface DonationDataTableViewController ()

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIButton *generateBilletButton;
@property (weak, nonatomic) IBOutlet UIButton *scanCardButton;

@property (weak, nonatomic) IBOutlet CustomTextField *cardNumber;
@property (weak, nonatomic) IBOutlet UILabel *cardFlagLabel;
@property (weak, nonatomic) IBOutlet UITextField *cardholderName;
@property (weak, nonatomic) IBOutlet CustomTextField *cardValidation;
@property (weak, nonatomic) IBOutlet CustomTextField *cardCVN;

@property (weak, nonatomic) IBOutlet UIView *footerView;
@property (weak, nonatomic) IBOutlet UISwitch *memorizeCardSwitch;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (strong, nonatomic) NSArray *textFields;
@property (strong, nonatomic) NSArray *acceptedCardFlags;

@end


@implementation DonationDataTableViewController

static const CGFloat headerViewDefaultHeight = 240.f;

#pragma mark - Initialization

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setup];
}

- (void)setup {
    // project title
    self.titleLabel.text = self.project.title;

    // goal and donation values
    self.donationGoalView.goalValueLabel.text = self.project.goal;
    
    NSNumberFormatter *currencyFormatter = [NSNumberFormatter new];
    [currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    NSLocale *currentLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"pt_BR"];
    [currencyFormatter setLocale:currentLocale];
    
    self.donationValueView.donationValueLabel.text = [currencyFormatter stringFromNumber:[NSNumber numberWithFloat:self.donation.value/100.f]];

    // banner image
    self.donationGoalView.mainImageView.image = self.mainImage;
    
    // gradient layer over banner
    CGFloat viewHeight, viewWidth;
    viewWidth = [[UIScreen mainScreen]bounds].size.width - (2 * defaultViewPadding);
    viewHeight = viewWidth * bannerViewProportion;
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = CGRectMake(0, 0, viewWidth, viewHeight);
    gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor defaultColor:DefaultColorBannerShadowTop] CGColor], (id)[[UIColor defaultColor:DefaultColorBannerShadowTop] CGColor], (id)[[UIColor blackColor] CGColor], nil];
    [self.donationGoalView.mainImageView.layer insertSublayer:gradient atIndex:0];
    
    // header view height
    if (IS_IPHONE_6_7_PLUS) { // iPhone 5.5'
        [self.headerView setFrame:CGRectMake(self.headerView.frame.origin.x, self.headerView.frame.origin.y, self.headerView.frame.size.width, headerViewDefaultHeight + 20.f)];
    } else if (IS_IPHONE_6_7) { // iPhone 4.7'
        [self.headerView setFrame:CGRectMake(self.headerView.frame.origin.x, self.headerView.frame.origin.y, self.headerView.frame.size.width, headerViewDefaultHeight)];
    } else { // iPhone 4'
        [self.headerView setFrame:CGRectMake(self.headerView.frame.origin.x, self.headerView.frame.origin.y, self.headerView.frame.size.width, headerViewDefaultHeight - 30.f)];
    }

    // buttons
    [self.scanCardButton addDashedBorderWithSize:CGSizeMake(viewWidth, self.scanCardButton.frame.size.height)];

    // card flag label - make initially blank
    self.cardFlagLabel.text = @"";
    
    // text fields
    self.cardholderName.delegate = self;
    
    // fetch accepted card flags from singleton
    self.acceptedCardFlags = [Singleton acceptedCardFlags];
    
    self.textFields = @[self.cardNumber, self.cardholderName, self.cardValidation, self.cardCVN];
    for (UITextField *textField in self.textFields) {
        [self addInputAccessoryViewToTextField:textField];
    }
    
    // setup selector for card number text field event editing changed
    [self.cardNumber addTarget:self action:@selector(textFieldDidEndEditing:) forControlEvents:UIControlEventEditingDidEnd];
    
    // hide save card option if service not activated
    if (!CREDIT_CARD_SAVE_SERVICE_ACTIVATED) {
        [self.footerView setFrame:CGRectMake(self.footerView.frame.origin.x, self.footerView.frame.origin.y, self.footerView.frame.size.width, 0)];
        [self.footerView removeFromSuperview];
    }
}

- (void)addInputAccessoryViewToTextField:(UITextField *)textField {
    //Next Text Field Button
    UINavigationBar *inputAccessoryView = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0,
                                                                                            self.view.frame.size.width, INPUT_ACCESSORY_VIEW_HEIGHT)];
    UIBarButtonItem *barButtonNext = [[UIBarButtonItem alloc] initWithTitle:@"Próximo"
                                                                      style:UIBarButtonItemStyleDone target:self
                                                                     action:@selector(nextTextField:)];
    
    UIBarButtonItem *barButtonPrevious = [[UIBarButtonItem alloc] initWithTitle:@"Anterior"
                                                                          style:UIBarButtonItemStyleDone target:self
                                                                         action:@selector(previousTextField:)];
    barButtonNext.tag = textField.tag+1;
    barButtonPrevious.tag = textField.tag-1;
    if (barButtonPrevious.tag < 0) [barButtonPrevious setEnabled:NO];
    if (barButtonNext.tag > self.textFields.count) [barButtonNext setEnabled:NO];
    UINavigationItem *navItem = [[UINavigationItem alloc] initWithTitle:@""];
    navItem.leftBarButtonItem = barButtonPrevious;
    navItem.rightBarButtonItem = barButtonNext;
    [inputAccessoryView setItems:@[navItem]];
    [textField setInputAccessoryView:inputAccessoryView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // activityIndicator
    UIWindow *frontWindow = [[UIApplication sharedApplication] keyWindow];
    [frontWindow addSubview:self.activityIndicator];
    self.activityIndicator.center = frontWindow.center;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    // stop activityIndicator on cases in which it's still animating
    [self.activityIndicator stopAnimating];
    [self.activityIndicator removeFromSuperview];
}


#pragma mark - UITextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidEndEditing:(CustomTextField *)textField {
    if ([textField isEqual:self.cardNumber]) {
        NSString *cardNumber = [NSString plainStringFromString:textField.text];
        CreditCardFlag flag = [NSString creditCardFlagFromNumber:cardNumber];
        if (flag != CreditCardNone) {
            NSString *cardFlagName = [NSString creditCardFlagNameFromType:flag];
            self.cardFlagLabel.text = cardFlagName;
        } else self.cardFlagLabel.text = @"";
    }
}


#pragma mark - UIScrollView Delegate

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [self.view endEditing:YES];
}


#pragma mark - Button Actions

- (void)previousTextField:(UIBarButtonItem *)sender {
    if (sender.tag >= 0 && sender.tag < self.textFields.count) {
        [self focusTextFieldWithTag:sender.tag];
    } else {
        [sender setEnabled:NO];
    }
}

- (void)nextTextField:(UIBarButtonItem *)sender {
    if (sender.tag >= 0 && sender.tag < self.textFields.count) {
        [self focusTextFieldWithTag:sender.tag];
    } else {
        [sender setEnabled:NO];
    }
}

- (void)focusTextFieldWithTag:(NSInteger)tag {
    for (UITextField *textField in self.textFields) {
        if (textField.tag == tag) {
            [textField becomeFirstResponder];
        }
    }
}

- (IBAction)cvnHelp:(id)sender {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Ajuda"
                                                                   message:@"O CVN, ou número de verificação, é um número de três dígitos que geralmente fica na parte de trás do cartão."
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"OK"
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action) {}];
    
    [alert addAction:okButton];
    [self presentViewController:alert animated:YES completion:nil];
}


#pragma mark - Navigation

- (IBAction)generateBillet:(id)sender {
    // set donation payment type
    self.donation.paymentType = PaymentTypeBankBillet;
    
    [self performSegueWithIdentifier:@"customToAntifraudDataFromDonationData" sender:self];
}

- (IBAction)goToNextPage:(id)sender {
    // if user pressed "advance" button, the payment type can only be credit card
    self.donation.paymentType = PaymentTypeCreditCard;
    
    [self.view endEditing:YES];
    [self performInputValidations];
}

- (void)performInputValidations {
    // alert setup
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Atenção"
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"OK"
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action) {}];
    [alert addAction:okButton];
    
    
    // mandatory input confirmation
    __block BOOL allFilled = YES;
    [self.textFields enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([((UITextField *)obj).text isEqualToString:@""]) {
            allFilled = NO;
            *stop = YES;
        }
    }];
    if (!allFilled) {
        [alert setMessage:@"Preencha todos os campos antes de prosseguir."];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    
    // valid credit card number
    CreditCardFlag flag = [NSString creditCardFlagFromNumber:[NSString plainStringFromString:self.cardNumber.text]];
    if (flag == CreditCardNone) {
        [alert setMessage:@"Número do cartão inválido."];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    
    // accepted credit card number
    NSNumber *flagNumber = [NSNumber numberWithInteger:flag];
    NSInteger cardFlagIndex = [self.acceptedCardFlags indexOfObject:flagNumber];
    if (cardFlagIndex == NSNotFound) {
        [alert setMessage:@"Bandeira do cartão não suportada."];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }

    // valid expiration date
    if ([self.cardValidation.text length] != self.cardValidation.maxLength) {
        [alert setMessage:@"Vencimento no formato incorreto. Utilize o formato mm/aa."];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    NSDateComponents *dateComponents = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
    NSArray *dateArray = [self.cardValidation.text componentsSeparatedByString:@"/"];
    NSInteger validationYear = [dateArray[1] integerValue] + 2000;
    if (validationYear < [dateComponents year]) {
        [alert setMessage:@"Vencimento anterior à data atual."];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    NSInteger validationMonth = [dateArray[0] integerValue];
    if (validationMonth < 1 || validationMonth > 12) {
        [alert setMessage:@"Data inválida."];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    
    if (validationYear == [dateComponents year] && validationMonth < [dateComponents month]) {
        [alert setMessage:@"Vencimento anterior à data atual."];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    
    // valid CVN
    if ([self.cardCVN.text length] != self.cardCVN.maxLength) {
        [alert setMessage:@"Número CVN deve ter 3 dígitos."];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    
    // call method for generating cardHash from user card data
    [self generateCardHash];
}

- (void)generateCardHash {
    // alert setup
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Erro"
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"OK"
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action) {
                                                         [self.memorizeCardSwitch setOn:NO animated:YES];
                                                     }];
    [alert addAction:okButton];
    
    NSArray *expirationDateComponents = [self.cardValidation.text componentsSeparatedByString:@"/"];
    PagarMeCreditCard *pagarMeCreditCard = [[PagarMeCreditCard alloc] initWithCardNumber:[NSString plainStringFromString:self.cardNumber.text]
                                                                          cardHolderName:self.cardholderName.text
                                                                     cardExpirationMonth:expirationDateComponents[0]
                                                                      cardExpirationYear:expirationDateComponents[1]
                                                                                 cardCvv:self.cardCVN.text];
    if ([pagarMeCreditCard hasErrorCardNumber]) {
        [alert setMessage:@"Erro no número do cartão."];
    }
    else if ([pagarMeCreditCard hasErrorCardHolderName]) {
        [alert setMessage:@"Erro no nome impresso no cartão."];
    }
    else if ([pagarMeCreditCard hasErrorCardCVV]) {
        [alert setMessage:@"Erro no CVN."];
    }
    else if ([pagarMeCreditCard hasErrorCardExpirationMonth] || [pagarMeCreditCard hasErrorCardExpirationYear]) {
        [alert setMessage:@"Erro na data de validade."];
    }
    else {
        // todos os campos estão válidos - gerar hash do cartãos
        [self.activityIndicator startAnimating];
        
        [pagarMeCreditCard generateHash:^(NSError *error, NSString *cardHash) {
            [self.activityIndicator stopAnimating];
            if (error) {
                NSLog(@"❗️ Error generating card hash: %@", error);
                [alert setMessage:@"Erro na geração do hash do cartão."];
                [self presentViewController:alert animated:YES completion:nil];
                return;
            }
            
            // this point is only reached in case of transaction success
            self.donation.cardHash = cardHash;
            
            // proceed with flow
            if (self.memorizeCardSwitch.isOn) {
                [self callSaveCardService];
            } else {
                [self performSegueWithIdentifier:@"customToAntifraudDataFromDonationData" sender:self];
            }
        }];
        return;
    }
    
    // this point is only reached in case of error in card data
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)callSaveCardService {
#warning complete call when ready

    [self.activityIndicator startAnimating];
//    [Service saveCardXForUserY... andCompletion:^(id results, BOOL hasError, NSString *errorMessage) {
//    
        [self.activityIndicator stopAnimating];
//
//        if (hasError) {
//            // alert setup
//            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Erro"
//                                                                           message:nil
//                                                                    preferredStyle:UIAlertControllerStyleAlert];
//            UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"OK"
//                                                               style:UIAlertActionStyleDefault
//                                                             handler:^(UIAlertAction * action) {
//                                                                 [self.memorizeCardSwitch setOn:NO animated:YES];
//                                                             }];
//            [alert addAction:okButton];
//            
////            if (errorMessage != nil) {
////                [alert setMessage:errorMessage];
////            } else {
//                [alert setMessage:@"Não foi possível salvar os dados do cartão."];
////            }
//            [self presentViewController:alert animated:YES completion:nil];
//            return;
//        } else {
            NSLog(@"💳 Credit Card data saved");
            [self performSegueWithIdentifier:@"customToAntifraudDataFromDonationData" sender:self];
//        }
//    }];
}

- (IBAction)returnToPreviousPage:(UIBarButtonItem *)sender {
    [self performSegueWithIdentifier:@"unwindToDonationValue" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"customToAntifraudDataFromDonationData"]) {
        AntifraudDataTableViewController *antifraudDataTableViewController = (AntifraudDataTableViewController *)[segue destinationViewController];
        
        antifraudDataTableViewController.project = self.project;
        antifraudDataTableViewController.mainImage = self.mainImage;
        antifraudDataTableViewController.donation = self.donation;
    } else if ([segue.identifier isEqualToString:@"modalToCardScan"]) {
        // if user pressed "card scan" button, the payment type can only be credit card
        self.donation.paymentType = PaymentTypeCreditCard;
        
        UINavigationController *navController = (UINavigationController *)[segue destinationViewController];
        CardScanViewController *cardScanViewController = (CardScanViewController *)[navController topViewController];
        cardScanViewController.project = self.project;
        cardScanViewController.mainImage = self.mainImage;
        cardScanViewController.donation = self.donation;
    }
}

- (IBAction)unwindToDonationData:(UIStoryboardSegue*)unwindSegue {
    
}

- (IBAction)unwindToBeforeCardScan:(UIStoryboardSegue*)unwindSegue {
    
}

@end

//
//  ProjectTableViewCell.h
//  Solidario
//
//  Created by Paula Vasconcelos Gueiros on 5/10/16.
//  Copyright © 2016 Paula Vasconcelos Gueiros. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KAProgressLabel.h"

@interface ProjectTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *projectImageView;
@property (weak, nonatomic) IBOutlet KAProgressLabel *pLabel;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UILabel *projectTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *numberOfDonorsLabel;
@property (weak, nonatomic) IBOutlet UILabel *peopleDonatedLabel;
@property (weak, nonatomic) IBOutlet UIButton *updateButton;

@end

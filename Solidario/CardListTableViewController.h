//
//  CardListTableViewController.h
//  Solidario
//
//  Created by Pan on 21/02/17.
//  Copyright © 2017 Paula Vasconcelos Gueiros. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CardListTableViewController : UITableViewController

@property BOOL userCameFromDonationPage;

@end

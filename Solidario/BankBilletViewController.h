//
//  BankBilletViewController.h
//  Solidario
//
//  Created by Pan on 16/05/17.
//  Copyright © 2017 Paula Vasconcelos Gueiros. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BankBilletViewController : UIViewController <UIWebViewDelegate>

@property (strong, nonatomic) NSString *billetStringURL;

@end

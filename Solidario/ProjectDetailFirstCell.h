//
//  ProjectDetailFirstCell.h
//  Solidario
//
//  Created by Pan on 05/12/16.
//  Copyright © 2016 Paula Vasconcelos Gueiros. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KAProgressLabel.h"

@interface ProjectDetailFirstCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *gradientSuperview;
@property (weak, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UIView *metadataView;
@property (weak, nonatomic) IBOutlet UIView *leftView;
@property (weak, nonatomic) IBOutlet UIView *rightView;

@property (weak, nonatomic) IBOutlet UICollectionView *bannerCollectionView;
@property (weak, nonatomic) IBOutlet UIButton *favoriteButton;
@property (weak, nonatomic) IBOutlet UIPageControl *bannerPageControl;
@property (weak, nonatomic) IBOutlet UILabel *goalLabel;

@property (weak, nonatomic) IBOutlet UILabel *projectTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *projectDescriptionLabel;
@property (weak, nonatomic) IBOutlet KAProgressLabel *projectProgressLabel;
@property (weak, nonatomic) IBOutlet UILabel *projectNumberOfDonorsLabel;
@property (weak, nonatomic) IBOutlet UILabel *projectPeopleDonatedLabel;
@property (weak, nonatomic) IBOutlet UILabel *projectDonatedValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *collectedStringLabel;
@property (weak, nonatomic) IBOutlet UILabel *projectTotalValueLabel;

@property NSInteger selectedBannerPage;
@property (weak, nonatomic) CAGradientLayer *gradient;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leftViewBottomConstraint;
@property (weak, nonatomic) IBOutlet UIButton *numberOfDonorsButton;

@end

//
//  DonatorList.h
//
//  Created by Paula Vasconcelos on 11/05/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface DonatorList : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double anonCount;
@property (nonatomic, strong) NSArray *donators;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end

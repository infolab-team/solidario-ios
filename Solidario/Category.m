//
//  Category.m
//
//  Created by Pan  on 20/01/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "Category.h"

NSString *const kCategoryName = @"name";
NSString *const kCategoryId = @"id";
NSString *const kCategorySelected = @"selected";

@interface Category ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end


@implementation Category

@synthesize name = _name;
@synthesize internalBaseClassIdentifier = _internalBaseClassIdentifier;
@synthesize selected = _selected;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
        self.name = [self objectOrNilForKey:kCategoryName fromDictionary:dict];
        self.internalBaseClassIdentifier = [self objectOrNilForKey:kCategoryId fromDictionary:dict];
        self.selected = NO;
    }
    return self;
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.name forKey:kCategoryName];
    [mutableDict setValue:self.internalBaseClassIdentifier forKey:kCategoryId];
    [mutableDict setValue:[NSNumber numberWithBool:self.selected] forKey:kCategorySelected];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}


#pragma mark - Helper Method

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.name = [aDecoder decodeObjectForKey:kCategoryName];
    self.internalBaseClassIdentifier = [aDecoder decodeObjectForKey:kCategoryId];
    self.selected = [[aDecoder decodeObjectForKey:kCategorySelected] boolValue];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {

    [aCoder encodeObject:_name forKey:kCategoryName];
    [aCoder encodeObject:_internalBaseClassIdentifier forKey:kCategoryId];
    [aCoder encodeObject:[NSNumber numberWithBool:_selected] forKey:kCategorySelected];
}

- (id)copyWithZone:(NSZone *)zone {
    Category *copy = [[Category alloc] init];
    
    if (copy) {
        copy.name = [self.name copyWithZone:zone];
        copy.internalBaseClassIdentifier = [self.internalBaseClassIdentifier copyWithZone:zone];
        copy.selected = [[[NSNumber numberWithBool:self.selected] copyWithZone:zone] boolValue];
    }
    return copy;
}

@end

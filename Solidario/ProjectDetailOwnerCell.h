//
//  ProjectDetailOwnerCell.h
//  Solidario
//
//  Created by Pan on 07/12/16.
//  Copyright © 2016 Paula Vasconcelos Gueiros. All rights reserved.
//

#import <UIKit/UIKit.h>

// static row heights
static const CGFloat projectDetailOwnerCellHeight = 120.f;


@interface ProjectDetailOwnerCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *ownerImage;
@property (weak, nonatomic) IBOutlet UILabel *ownerNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *subtitleLabel;
@property (weak, nonatomic) IBOutlet UIButton *facebookButton;
@property (weak, nonatomic) IBOutlet UIButton *twitterButton;

@end

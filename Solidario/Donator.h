//
//  Donator.h
//
//  Created by Paula Vasconcelos on 11/05/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Donator : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *donatorIdentifier;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *image;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end

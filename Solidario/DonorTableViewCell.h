//
//  DonorTableViewCell.h
//  Solidario
//
//  Created by Pan on 03/05/17.
//  Copyright © 2017 Paula Vasconcelos Gueiros. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DonorTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *donorPictureImageView;
@property (weak, nonatomic) IBOutlet UILabel *donorNameLabel;

@end

//
//  SearchViewController.m
//  Solidario
//
//  Created by Paula Vasconcelos Gueiros on 29/8/16.
//  Copyright © 2016 Paula Vasconcelos Gueiros. All rights reserved.
//

#import "SearchViewController.h"
#import "SearchTableViewCell.h"
#import "ProjectDetailViewController.h"
#import "Service.h"
#import "NSArray+Utils.h"
#import "NSString+Utils.h"
#import "ProjectShort.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface SearchViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UIView *footerView;

@property (strong, nonatomic) NSArray <ProjectShort *> *projects;
@property (strong, nonatomic) NSString *searchArgument;
@property (strong, nonatomic) ProjectShort *selectedProject;

@property BOOL searchPerformed;

@end


@implementation SearchViewController

static const CGFloat searchResultCellHeight = 108.f;

#pragma mark - Initialization

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setup];
}

- (void)setup {
    // setup tableview
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.projects = [NSArray array];
    self.searchPerformed = NO;
    
    // setup notification observers
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(favoriteOptionChanged:) name:@"favoriteOptionChanged" object:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(presentSearchResults:) name:@"presentSearchResults" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(clearSearchResults:) name:@"clearSearchResults" object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"presentSearchResults" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"clearSearchResults" object:nil];
}


#pragma mark - UITableView Delegate and Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if ([self.projects count]) {
        [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
        [self.tableView setAllowsSelection:YES];
        [self.footerView setHidden:NO];
    } else {
        [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        [self.tableView setAllowsSelection:NO];
        [self.footerView setHidden:YES];
    }
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.projects count] ? [self.projects count] : 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (![self.projects count]) {
        if (self.searchPerformed) {
            return [tableView dequeueReusableCellWithIdentifier:@"noResultsCell"];
        }
        return [tableView dequeueReusableCellWithIdentifier:@"emptyCell"];
    }
    
    SearchTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"searchTableViewCell"];
    
    ProjectShort *project = [self.projects objectAtIndex:indexPath.row];
    cell.titleLabel.text = project.title;
    cell.titleLabel.numberOfLines = 0;
    [cell.titleLabel sizeToFit];
    
    NSString *imageUrlString = [NSString encodedStringFromString:project.media[0]];
    [cell.projectImageView sd_setImageWithURL:[NSURL URLWithString:imageUrlString] placeholderImage:[UIImage imageNamed:@"webImagePlaceholder"]];
    
    cell.goalLabel.text = project.goal;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return searchResultCellHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:[tableView indexPathForSelectedRow] animated:YES];
    self.selectedProject = [self.projects objectAtIndex:indexPath.row];
    [self performSegueWithIdentifier:@"modalToProjectDetail" sender:self];
}


#pragma mark - Project Results Methods

- (void)searchProjects {
    
    [self.activityIndicator startAnimating];
    [Service getProjectsOnPage:1 withItemsPerPage:15 searchArgument:self.searchArgument andCompletion:^(id results, BOOL hasError, NSString *errorMessage) {
        [self.activityIndicator stopAnimating];
        self.searchPerformed = YES;
        
        if (hasError) {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Erro"
                                                                           message:nil
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"OK"
                                                               style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction * action) {}];
            [alert addAction:okButton];
//            if (errorMessage != nil) {
//                [alert setMessage:errorMessage];
//            } else {
                [alert setMessage:@"Nenhum projeto encontrado."];
//            }
            [self presentViewController:alert animated:YES completion:nil];
            return;
        } else {
            self.projects = (NSArray *)results;
            [self.tableView reloadData];
        }
    }];
    [self.tableView reloadData];
}

- (void)clearProjects {
    self.projects = [NSArray array];
    self.searchPerformed = NO;
    [self.tableView reloadData];
}


#pragma mark - Notification Handlers

- (void)presentSearchResults:(NSNotification *)notification {
    if ([notification.object isKindOfClass:[NSString class]]) {
        self.searchArgument = (NSString *)notification.object;
        [self searchProjects];
    }
}

- (void)clearSearchResults:(NSNotification *)notification {
    [self clearProjects];
}

- (void)favoriteOptionChanged:(NSNotification *)notification {
    
    NSString *projectId = (NSString *)notification.object;
    __block NSInteger changedProjectIndex = -1;
    __block ProjectShort *changedProject;
    [self.projects enumerateObjectsUsingBlock:^(ProjectShort *project, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([project.internalBaseClassIdentifier isEqualToString:projectId]) {
            changedProject = project;
            changedProjectIndex = idx;
            *stop = YES;
        }
    }];
    
    if (changedProjectIndex >= 0) {
        NSLog(@"🔍 Found changed project in SearchVC (for favorite/unfavorite heart touch)");
        NSLog(@"\tProject title: %@", changedProject.title);
        NSLog(@"\tProject index in table view: %ld", (long)changedProjectIndex);
        NSLog(@"\tSearchVC value for object: isFavorite = %@", (changedProject.isFavorite) ? @"YES" : @"NO");
        
        changedProject.isFavorite = !changedProject.isFavorite;
    }
}
    
    
#pragma mark - Navigation
    
- (IBAction)unwindToBeforeProjectDetail:(UIStoryboardSegue*)unwindSegue {
    
}
    
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"modalToProjectDetail"]) {
        UINavigationController *navController = (UINavigationController *)[segue destinationViewController];
        ProjectDetailViewController *projectDetailViewController = (ProjectDetailViewController *)[navController topViewController];
        projectDetailViewController.projectShort = [self.selectedProject copy];
        projectDetailViewController.selectedBannerPage = 0;
    }
}

@end

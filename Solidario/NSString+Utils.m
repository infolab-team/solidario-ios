//
//  NSString+Utils.m
//  Solidario
//
//  Created by Paula Vasconcelos Gueiros on 31/8/16.
//  Copyright © 2016 Paula Vasconcelos Gueiros. All rights reserved.
//

#import "NSString+Utils.h"

@implementation NSString(Utils)

static NSArray *monthNames;

+ (NSString *)plainStringFromString:(NSString *)string {
    NSMutableString *plain = [NSMutableString stringWithString:string];
    plain = [NSMutableString stringWithString:[plain stringByReplacingOccurrencesOfString:@"-" withString:@""]];
    plain = [NSMutableString stringWithString:[plain stringByReplacingOccurrencesOfString:@"." withString:@""]];
    plain = [NSMutableString stringWithString:[plain stringByReplacingOccurrencesOfString:@"," withString:@""]];
    plain = [NSMutableString stringWithString:[plain stringByReplacingOccurrencesOfString:@"/" withString:@""]];
    plain = [NSMutableString stringWithString:[plain stringByReplacingOccurrencesOfString:@" " withString:@""]];
    plain = [NSMutableString stringWithString:[plain stringByReplacingOccurrencesOfString:@"(" withString:@""]];
    plain = [NSMutableString stringWithString:[plain stringByReplacingOccurrencesOfString:@")" withString:@""]];
    return plain;
}

+ (NSString *)maskedStringFromString:(NSString *)string andMask:(TextMask)mask {
    NSMutableString *masked = [NSMutableString stringWithString:string];
    
    switch (mask) {
        case TextMaskCPFCNPJ: {
            if (masked.length == 11) {
                [masked insertString:@"." atIndex:3];
                [masked insertString:@"." atIndex:7];
                [masked insertString:@"-" atIndex:11];
            } else if (masked.length == 14) {
                [masked insertString:@"." atIndex:2];
                [masked insertString:@"." atIndex:6];
                [masked insertString:@"/" atIndex:10];
                [masked insertString:@"-" atIndex:15];
            }
        }
        case TextMaskCreditCardNumber: {
            [masked insertString:@" " atIndex:4];
            [masked insertString:@" " atIndex:9];
            [masked insertString:@" " atIndex:14];
        }
        default: {}
    }
    return masked;
}

+ (NSString *)encodedStringFromString:(NSString *)string {
    NSString *encodedString = [string stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
    encodedString = [encodedString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    return encodedString;
}

+ (NSString *)monthShortNameFromInteger:(NSInteger)monthInteger {
    if (monthNames == nil) {
        monthNames = @[@"JAN", @"FEV", @"MAR", @"ABR", @"MAI", @"JUN", @"JUL", @"AGO", @"SET", @"OUT", @"NOV", @"DEZ"];
    }
    
    return monthNames[monthInteger];
}

+ (BOOL)isValidEmail:(NSString *)email {
    NSString *emailRegex =
    @"(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"
    @"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"
    @"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"
    @"z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"
    @"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"
    @"9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"
    @"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES[c] %@", emailRegex];
    
    return [emailTest evaluateWithObject:email];
}

+ (BOOL)isValidName:(NSString *)name {
    NSString *nameRegex = @"[a-zA-ZáàâãéèêíïóôõöúçñÁÀÂÃÉÈÍÏÓÔÕÖÚÇÑ][a-zA-ZáàâãéèêíïóôõöúçñÁÀÂÃÉÈÍÏÓÔÕÖÚÇÑ ]*";
    NSPredicate *nameTest = [NSPredicate predicateWithFormat:@"SELF MATCHES[c] %@", nameRegex];
    
    return [nameTest evaluateWithObject:name];
}

+ (BOOL)isValidCPF:(NSString *)cpf {

    if ([cpf length] != 11) {
        return NO;
    }
    
    if ([self sameCharsInString:cpf]) {
        return NO;
    }
    
    int firstDigitVerifier = 0, secondDigitVerifier = 0;
    
    int multiplier = 10;
    for (int i = 0; i < 9; i++) {
        char c = [cpf characterAtIndex:i];
        int numAtIndex = c - '0';
        
        firstDigitVerifier += (multiplier * numAtIndex);
        multiplier--;
    }

    firstDigitVerifier *= 10;
    firstDigitVerifier = firstDigitVerifier%11;
    if (firstDigitVerifier == 10) firstDigitVerifier = 0;
    
    if (([cpf characterAtIndex:9] - '0') != firstDigitVerifier) {
        return NO;
    }

    multiplier = 11;
    for (int i = 0; i < 10; i++) {
        char c = [cpf characterAtIndex:i];
        int numAtIndex = c - '0';
        
        secondDigitVerifier += (multiplier * numAtIndex);
        multiplier--;
    }
    
    secondDigitVerifier *= 10;
    secondDigitVerifier = secondDigitVerifier%11;
    if (secondDigitVerifier == 10) secondDigitVerifier = 0;
    
    if (([cpf characterAtIndex:10] - '0') != secondDigitVerifier) {
        return NO;
    }
    return YES;
}

+ (BOOL)sameCharsInString:(NSString *)string {
    if ([string length] == 0) return NO;
    return [[string stringByReplacingOccurrencesOfString:[string substringToIndex:1] withString:@""] length] == 0 ? YES : NO;
}

//+ (BOOL)isValidCNPJ:(NSString *)cnpj { }

+ (CreditCardFlag)creditCardFlagFromNumber:(NSString *)cardNumber {
    
    NSError *error = NULL;
    NSRegularExpression *regex;
    NSArray <NSString *> *regexStringForFlag = @[@"", // None
                                                @"^4[0-9]{12}(?:[0-9]{3})?", // Visa
                                                @"^5[1-5][0-9]{14}", // MasterCard
                                                @"^3[47][0-9]{13}", // Amex
                                                @"^3(?:0[0-5]|[68][0-9])[0-9]{11}", // Diners Club
                                                @"^6(?:011|5[0-9]{2})[0-9]{12}", // Discover
                                                @"^(?:2131|1800|35\\d{3})\\d{11}"]; // JCB
    
    for (NSString *regexString in regexStringForFlag) {
        regex  = [NSRegularExpression regularExpressionWithPattern:regexString
                                                           options:NSRegularExpressionCaseInsensitive
                                                             error:&error];
        if ([regex numberOfMatchesInString:cardNumber options:0 range:NSMakeRange(0, [cardNumber length])])
            return (CreditCardFlag)[regexStringForFlag indexOfObject:regexString] ;
    }

    return CreditCardNone;
}

+ (NSString *)creditCardFlagNameFromType:(CreditCardFlag)flagType {
    switch (flagType) {
        case CreditCardVisa:
            return @"Visa";

        case CreditCardMasterCard:
            return @"MasterCard";

        case CreditCardAmex:
            return @"Amex";

        case CreditCardDinersClub:
            return @"Diners";

        case CreditCardDiscover:
            return @"Discover";

        case CreditCardJCB:
            return @"JCB";
            
        default:
            return @"None";
    }
}

@end


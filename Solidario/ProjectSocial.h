//
//  ProjectSocial.h
//
//  Created by Paula Vasconcelos on 02/05/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface ProjectSocial : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *twitter;
@property (nonatomic, strong) NSString *facebook;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end

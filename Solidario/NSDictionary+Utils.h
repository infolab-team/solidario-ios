//
//  NSDictionary+Utils.h
//  Solidario
//
//  Created by Pan on 02/05/17.
//  Copyright © 2017 Paula Vasconcelos Gueiros. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary(Utils)

+ (NSDictionary *)loadPlistWithName:(NSString*)name;

@end

//
//  IntroPageItemViewController.h
//  Solidário
//
//  Created by Allan Alves on 7/4/16.
//  Copyright © 2016 Infosolo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IntroPageItemViewController : UIViewController

@property (nonatomic) NSInteger pageIndex;
@property (nonatomic) NSString *pageImageName;
@property (nonatomic) NSString *pageDescription;
@property (nonatomic) NSString *pageTitle;

@end

//
//  ProjectCreator.h
//
//  Created by Paula Vasconcelos on 28/06/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ProjectCreator : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *image;
@property (nonatomic, strong) NSString *internalBaseClassIdentifier;
@property (nonatomic, assign) double createdCount;
@property (nonatomic, strong) NSString *surname;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, assign) double donatedCount;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end

//
//  UILabel+Utils.m
//  Solidario
//
//  Created by Pan on 28/12/16.
//  Copyright © 2016 Paula Vasconcelos Gueiros. All rights reserved.
//

#import "UILabel+Utils.h"

@implementation UILabel (Utils)

+ (CGFloat)heightForLabelWithText:(NSString*)text font:(UIFont*)font andWidth:(CGFloat)width {
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0.f, 0.f, width, 0.f)];
    label.font = font;
    label.numberOfLines = 0;
    label.text = text;
    [label sizeToFit];
    
    return label.frame.size.height;
}

@end

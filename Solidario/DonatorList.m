//
//  DonatorList.m
//
//  Created by Paula Vasconcelos on 11/05/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "DonatorList.h"
#import "Donator.h"

NSString *const kDonatorListAnonCount = @"anonCount";
NSString *const kDonatorListDonators = @"donators";


@interface DonatorList ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end


@implementation DonatorList

@synthesize anonCount = _anonCount;
@synthesize donators = _donators;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.anonCount = [[self objectOrNilForKey:kDonatorListAnonCount fromDictionary:dict] doubleValue];
        NSObject *receivedDonators = [dict objectForKey:kDonatorListDonators];
        NSMutableArray *parsedDonators = [NSMutableArray array];
        if ([receivedDonators isKindOfClass:[NSArray class]]) {
            for (NSDictionary *item in (NSArray *)receivedDonators) {
                if ([item isKindOfClass:[NSDictionary class]]) {
                    [parsedDonators addObject:[Donator modelObjectWithDictionary:item]];
                }
           }
        } else if ([receivedDonators isKindOfClass:[NSDictionary class]]) {
           [parsedDonators addObject:[Donator modelObjectWithDictionary:(NSDictionary *)receivedDonators]];
        }

        self.donators = [NSArray arrayWithArray:parsedDonators];
    }
    return self;
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.anonCount] forKey:kDonatorListAnonCount];
    NSMutableArray *tempArrayForDonators = [NSMutableArray array];
    for (NSObject *subArrayObject in self.donators) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForDonators addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForDonators addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForDonators] forKey:kDonatorListDonators];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}


#pragma mark - Helper Method

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.anonCount = [aDecoder decodeDoubleForKey:kDonatorListAnonCount];
    self.donators = [aDecoder decodeObjectForKey:kDonatorListDonators];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {

    [aCoder encodeDouble:_anonCount forKey:kDonatorListAnonCount];
    [aCoder encodeObject:_donators forKey:kDonatorListDonators];
}

- (id)copyWithZone:(NSZone *)zone {
    DonatorList *copy = [[DonatorList alloc] init];
    
    if (copy) {
        copy.anonCount = self.anonCount;
        copy.donators = [self.donators copyWithZone:zone];
    }
    return copy;
}

@end

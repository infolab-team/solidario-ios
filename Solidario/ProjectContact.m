//
//  ProjectContact.m
//
//  Created by Paula Vasconcelos on 02/05/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "ProjectContact.h"


NSString *const kProjectContactEmail = @"email";
NSString *const kProjectContactTel = @"tel";


@interface ProjectContact ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation ProjectContact

@synthesize email = _email;
@synthesize tel = _tel;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.email = [self objectOrNilForKey:kProjectContactEmail fromDictionary:dict];
            self.tel = [self objectOrNilForKey:kProjectContactTel fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.email forKey:kProjectContactEmail];
    [mutableDict setValue:self.tel forKey:kProjectContactTel];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.email = [aDecoder decodeObjectForKey:kProjectContactEmail];
    self.tel = [aDecoder decodeObjectForKey:kProjectContactTel];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_email forKey:kProjectContactEmail];
    [aCoder encodeObject:_tel forKey:kProjectContactTel];
}

- (id)copyWithZone:(NSZone *)zone
{
    ProjectContact *copy = [[ProjectContact alloc] init];
    
    if (copy) {

        copy.email = [self.email copyWithZone:zone];
        copy.tel = [self.tel copyWithZone:zone];
    }
    
    return copy;
}


@end

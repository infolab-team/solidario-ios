//
//  RootViewController.h
//  Solidario2.0
//
//  Created by Paula Vasconcelos Gueiros on 11/3/16.
//  Copyright © 2016 paulinhavgueiros. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "iCarousel.h"

@interface RootViewController : UIViewController <UISearchBarDelegate, iCarouselDataSource, iCarouselDelegate, UICollectionViewDataSource, UICollectionViewDelegate>

@property (nonatomic, strong) IBOutlet iCarousel *carousel;

@end

//
//  UpdateProjectTableViewController.h
//  Solidario
//
//  Created by Pan on 14/02/17.
//  Copyright © 2017 Paula Vasconcelos Gueiros. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProjectShort.h"

@interface UpdateProjectTableViewController : UITableViewController <UINavigationControllerDelegate, UIImagePickerControllerDelegate>

@property (strong, nonatomic) ProjectShort *project;

@end

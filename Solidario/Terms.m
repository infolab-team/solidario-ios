//
//  Terms.m
//
//  Created by Pan  on 08/03/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "Terms.h"

NSString *const kTermsTitle = @"title";
NSString *const kTermsContent = @"content";

@interface Terms ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end


@implementation Terms

@synthesize title = _title;
@synthesize content = _content;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.title = [self objectOrNilForKey:kTermsTitle fromDictionary:dict];
            self.content = [self objectOrNilForKey:kTermsContent fromDictionary:dict];

    }
    return self;
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.title forKey:kTermsTitle];
    [mutableDict setValue:self.content forKey:kTermsContent];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}


#pragma mark - Helper Method

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.title = [aDecoder decodeObjectForKey:kTermsTitle];
    self.content = [aDecoder decodeObjectForKey:kTermsContent];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {

    [aCoder encodeObject:_title forKey:kTermsTitle];
    [aCoder encodeObject:_content forKey:kTermsContent];
}

- (id)copyWithZone:(NSZone *)zone {
    Terms *copy = [[Terms alloc] init];
    
    if (copy) {

        copy.title = [self.title copyWithZone:zone];
        copy.content = [self.content copyWithZone:zone];
    }
    return copy;
}

@end

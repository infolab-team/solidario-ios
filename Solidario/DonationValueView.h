//
//  DonationValueView.h
//  Solidario
//
//  Created by Pan on 02/06/17.
//  Copyright © 2017 Paula Vasconcelos Gueiros. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE
@interface DonationValueView : UIView

@property (weak, nonatomic) IBOutlet UILabel *donationValueLabel;

@end

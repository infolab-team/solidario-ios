//
//  Project.h
//
//  Created by Paula Vasconcelos on 02/05/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ProjectCreator, ProjectContact, ProjectSocial;

@interface Project : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *slug;
@property (nonatomic, strong) NSString *status;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, assign) double timeleft;
@property (nonatomic, strong) ProjectCreator *creator;
@property (nonatomic, assign) double duration;
@property (nonatomic, strong) NSString *internalBaseClassIdentifier;
@property (nonatomic, strong) NSString *category;
@property (nonatomic, strong) NSString *city;
@property (nonatomic, strong) ProjectContact *contact;
@property (nonatomic, strong) NSArray *media;
@property (nonatomic, strong) NSString *information;
@property (nonatomic, strong) NSArray *donators;
@property (nonatomic, strong) NSString *state;
@property (nonatomic, assign) double percent;
@property (nonatomic, strong) NSString *video;
@property (nonatomic, strong) ProjectSocial *social;
@property (nonatomic, assign) double donatorsCount;
@property (nonatomic, assign) double donated;
@property (nonatomic, strong) NSString *goal;
@property (nonatomic, strong) NSString *internalBaseClassDescription;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end

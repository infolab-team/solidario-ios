//
//  RegisterTableViewController.m
//  Solidario
//
//  Created by Paula Vasconcelos Gueiros on 19/8/16.
//  Copyright © 2016 Paula Vasconcelos Gueiros. All rights reserved.
//

#import "RegisterTableViewController.h"
#import "Service.h"
#import "Singleton.h"
#import "MainViewController.h"
#import "NSString+Utils.h"
#import "ActionButton.h"

@interface RegisterTableViewController ()

@property (weak, nonatomic) IBOutlet UITextField *name;
@property (weak, nonatomic) IBOutlet UITextField *surname;
@property (weak, nonatomic) IBOutlet UITextField *email;
@property (weak, nonatomic) IBOutlet UITextField *password;
@property (weak, nonatomic) IBOutlet UITextField *passwordConfirm;
@property (weak, nonatomic) IBOutlet ActionButton *registerButton;
@property (weak, nonatomic) IBOutlet UISwitch *receiveUpdatesSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *keepConnectedSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *agreementSwitch;

@property (strong, nonatomic) NSArray *textFields;

@end


@implementation RegisterTableViewController


#pragma mark - Initialization

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setup];
}

- (void)setup {
    self.name.delegate = self;
    self.surname.delegate = self;
    self.email.delegate = self;
    self.password.delegate = self;
    self.passwordConfirm.delegate = self;
    
    self.textFields = @[self.name, self.surname, self.email, self.password, self.passwordConfirm];
    for (UITextField *textField in self.textFields) {
        [self addInputAccessoryViewToTextField:textField];
    }
}

- (void)addInputAccessoryViewToTextField:(UITextField *)textField {
    //Next Text Field Button
    UINavigationBar *inputAccessoryView = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0,
                                                                                            self.view.frame.size.width, INPUT_ACCESSORY_VIEW_HEIGHT)];
    UIBarButtonItem *barButtonNext = [[UIBarButtonItem alloc] initWithTitle:@"Próximo"
                                                                      style:UIBarButtonItemStyleDone target:self
                                                                     action:@selector(nextTextField:)];
    
    UIBarButtonItem *barButtonPrevious = [[UIBarButtonItem alloc] initWithTitle:@"Anterior"
                                                                          style:UIBarButtonItemStyleDone target:self
                                                                         action:@selector(previousTextField:)];
    barButtonNext.tag = textField.tag+1;
    barButtonPrevious.tag = textField.tag-1;
    if (barButtonPrevious.tag < 0) [barButtonPrevious setEnabled:NO];
    if (barButtonNext.tag > self.textFields.count) [barButtonNext setEnabled:NO];
    UINavigationItem *navItem = [[UINavigationItem alloc] initWithTitle:@""];
    navItem.leftBarButtonItem = barButtonPrevious;
    navItem.rightBarButtonItem = barButtonNext;
    [inputAccessoryView setItems:@[navItem]];
    [textField setInputAccessoryView:inputAccessoryView];
}


#pragma mark - Navigation

- (IBAction)register:(id)sender {
    [self.view endEditing:YES];
    NSLog(@"👥 User pressed register button");
    [self performInputValidations];
}

- (void)performInputValidations {
    
    // alert setup
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Atenção"
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"OK"
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action) {}];
    [alert addAction:okButton];
    
    
    // mandatory input confirmation
    __block BOOL allFilled = YES;
    [self.textFields enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([((UITextField *)obj).text isEqualToString:@""]) {
            allFilled = NO;
            *stop = YES;
        }
    }];
    if (!allFilled) {
        [alert setMessage:@"Preencha todos os campos antes de prosseguir."];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    
    // valid name
    if (![NSString isValidName:self.name.text]) {
        [alert setMessage:@"Nome inválido."];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    
    // valid surname
    if (![NSString isValidName:self.surname.text]) {
        [alert setMessage:@"Sobrenome inválido."];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    
    // valid email
    if (![NSString isValidEmail:self.email.text]) {
        [alert setMessage:@"Email inválido."];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    
    // valid password length
    if ([self.password.text length] < 6) {
        [alert setMessage:@"Senha deve ter pelo menos seis caracteres."];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    
    // equal passwords
    if (![self.password.text isEqualToString:self.passwordConfirm.text]) {
        [alert setMessage:@"Confirmação de senha diferente da senha."];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    
    // accepted terms and policy
    if (!self.agreementSwitch.isOn) {
        [alert setMessage:@"Você deve aceitar os Termos de Uso e Políticas de Privacidade."];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    
    [self proceedWithRegister];
}

- (void)proceedWithRegister {
    
    // register new user
    [self.registerButton startLoadingAnimated];
    [Service registerWithName:self.name.text surname:self.surname.text email:self.email.text password:self.password.text newsletter:[self.receiveUpdatesSwitch isOn] andCompletion:^(id results, BOOL hasError, NSString *errorMessage) {
        
        if (hasError) {
            // alert setup
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Erro"
                                                                           message:nil
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"OK"
                                                               style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction * action) {}];
            [alert addAction:okButton];
            
            [self.registerButton stopLoading];
            
//            if (errorMessage != nil) {
//                [alert setMessage:errorMessage];
//            } else {
                [alert setMessage:@"Não foi possível cadastrar o usuário."];
//            }
            [self presentViewController:alert animated:YES completion:nil];
            return;
        } else {
            
            NSLog(@"✅ Registration successful");
            
            UIAlertController *successAlert = [UIAlertController alertControllerWithTitle:@"Sucesso"
                                                                                  message:(NSString *)results
                                                                           preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *successOkButton = [UIAlertAction actionWithTitle:@"OK"
                                                                      style:UIAlertActionStyleDefault
                                                                    handler:^(UIAlertAction * action) {
                                                                        [self proceedWithLogin];
                                                                    }];
            [successAlert addAction:successOkButton];
            [self presentViewController:successAlert animated:YES completion:nil];
        }
    }];
}
    
- (void)proceedWithLogin {
    // alert setup
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Erro"
                                                                   message:@"Não foi possível efetuar o login."
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"OK"
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action) {
                                                         [self.navigationController dismissViewControllerAnimated:YES completion:nil];
                                                     }];
    [alert addAction:okButton];
    
    // login with registered user
    [Service loginWithEmail:self.email.text password:self.password.text andCompletion:^(id results, BOOL hasError, NSString *errorMessage) {
        
        [self.registerButton stopLoading];
        
        if (hasError) {
            [self presentViewController:alert animated:YES completion:nil];
            return;
        } else {
            NSLog(@"✅ Login successful");
            
            // check switch to decide on saving User object through app sessions
            [Singleton loginWithUser:(User *)results withFacebookEnabled:NO andPersistent:self.keepConnectedSwitch.isOn];
            
            [self proceedAfterRegister];
        }
    }];
}

- (void)proceedAfterRegister {
    if ([Singleton isDonating]) {
        [self performSegueWithIdentifier:@"unwindToBeforeDonationValue" sender:self];
    } else {
        [self instantiateSettingsWithViewControllerIdentifier:@"settingsNavigationController"];
    }
}

- (void)instantiateSettingsWithViewControllerIdentifier:(NSString *)viewControllerIdentifier {
    NSString *storyboardIdentifier = @"Settings";
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardIdentifier bundle:nil];
    UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:viewControllerIdentifier];
    
    [self presentViewController:navigationController animated:YES completion:nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"unwindFromRegisterToAccount"]) {
        [self.view endEditing:YES];
    }
}

- (IBAction)unwindToBeforeTerms:(UIStoryboardSegue*)unwindSegue {
    
}


#pragma mark - UITextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}


#pragma mark - UIScrollView Delegate

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [self.view endEditing:YES];
}


#pragma mark - Button Actions

- (void)previousTextField:(UIBarButtonItem *)sender {
    if (sender.tag >= 0 && sender.tag < self.textFields.count) {
        [self focusTextFieldWithTag:sender.tag];
    } else {
        [sender setEnabled:NO];
    }
}

- (void)nextTextField:(UIBarButtonItem *)sender {
    if (sender.tag >= 0 && sender.tag < self.textFields.count) {
        [self focusTextFieldWithTag:sender.tag];
    } else {
        [sender setEnabled:NO];
    }
}

- (void)focusTextFieldWithTag:(NSInteger)tag {
    for (UITextField *textField in self.textFields) {
        if (textField.tag == tag) {
            [textField becomeFirstResponder];
        }
    }
}

@end

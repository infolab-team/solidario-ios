//
//  ProjectBasic.m
//
//  Created by Paula Vasconcelos on 16/06/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "ProjectBasic.h"


NSString *const kProjectBasicId = @"id";
NSString *const kProjectBasicTitle = @"title";


@interface ProjectBasic ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation ProjectBasic

@synthesize projectIdentifier = _projectIdentifier;
@synthesize title = _title;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.projectIdentifier = [self objectOrNilForKey:kProjectBasicId fromDictionary:dict];
            self.title = [self objectOrNilForKey:kProjectBasicTitle fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.projectIdentifier forKey:kProjectBasicId];
    [mutableDict setValue:self.title forKey:kProjectBasicTitle];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.projectIdentifier = [aDecoder decodeObjectForKey:kProjectBasicId];
    self.title = [aDecoder decodeObjectForKey:kProjectBasicTitle];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_projectIdentifier forKey:kProjectBasicId];
    [aCoder encodeObject:_title forKey:kProjectBasicTitle];
}

- (id)copyWithZone:(NSZone *)zone
{
    ProjectBasic *copy = [[ProjectBasic alloc] init];
    
    if (copy) {

        copy.projectIdentifier = [self.projectIdentifier copyWithZone:zone];
        copy.title = [self.title copyWithZone:zone];
    }
    
    return copy;
}


@end

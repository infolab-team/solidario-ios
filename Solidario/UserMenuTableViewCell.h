//
//  UserMenuTableViewCell.h
//  Solidario
//
//  Created by Paula Vasconcelos Gueiros on 24/8/16.
//  Copyright © 2016 Paula Vasconcelos Gueiros. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserMenuTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *userPicture;
@property (weak, nonatomic) IBOutlet UILabel *userName;

@end

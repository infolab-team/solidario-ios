//
//  NSArray+Utils.h
//  Solidario
//
//  Created by Allan Alves on 7/4/16.
//  Copyright © 2016 Infosolo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray(Utils)

+ (NSArray *)loadPlistWithName:(NSString*)name;

@end

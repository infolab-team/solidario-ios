//
//  AboutTableViewController.h
//  Solidario
//
//  Created by Paula Vasconcelos Gueiros on 26/8/16.
//  Copyright © 2016 Paula Vasconcelos Gueiros. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AboutTableViewController : UITableViewController <UIWebViewDelegate>

@end

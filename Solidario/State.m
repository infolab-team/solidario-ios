//
//  State.m
//
//  Created by Pan  on 06/03/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "State.h"

NSString *const kStateId = @"id";
NSString *const kStateName = @"name";
NSString *const kStateCount = @"count";
NSString *const kStateSelected = @"selected";

@interface State ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end


@implementation State

@synthesize internalBaseClassIdentifier = _internalBaseClassIdentifier;
@synthesize name = _name;
@synthesize count = _count;
@synthesize selected = _selected;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
        self.internalBaseClassIdentifier = [self objectOrNilForKey:kStateId fromDictionary:dict];
        self.name = [self objectOrNilForKey:kStateName fromDictionary:dict];
        self.count = [[self objectOrNilForKey:kStateCount fromDictionary:dict] doubleValue];
        self.selected = NO;
    }
    return self;
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.internalBaseClassIdentifier forKey:kStateId];
    [mutableDict setValue:self.name forKey:kStateName];
    [mutableDict setValue:[NSNumber numberWithDouble:self.count] forKey:kStateCount];
    [mutableDict setValue:[NSNumber numberWithBool:self.selected] forKey:kStateSelected];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}


#pragma mark - Helper Method

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.internalBaseClassIdentifier = [aDecoder decodeObjectForKey:kStateId];
    self.name = [aDecoder decodeObjectForKey:kStateName];
    self.count = [aDecoder decodeDoubleForKey:kStateCount];
    self.selected = [[aDecoder decodeObjectForKey:kStateSelected] boolValue];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {

    [aCoder encodeObject:_internalBaseClassIdentifier forKey:kStateId];
    [aCoder encodeObject:_name forKey:kStateName];
    [aCoder encodeDouble:_count forKey:kStateCount];
    [aCoder encodeObject:[NSNumber numberWithBool:_selected] forKey:kStateSelected];
}

- (id)copyWithZone:(NSZone *)zone {
    State *copy = [[State alloc] init];
    
    if (copy) {
        copy.internalBaseClassIdentifier = [self.internalBaseClassIdentifier copyWithZone:zone];
        copy.name = [self.name copyWithZone:zone];
        copy.count = self.count;
        copy.selected = [[[NSNumber numberWithBool:self.selected] copyWithZone:zone] boolValue];
    }
    return copy;
}


@end

//
//  CardTableViewController.h
//  Solidario
//
//  Created by Pan on 06/03/17.
//  Copyright © 2017 Paula Vasconcelos Gueiros. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CardTableViewController : UITableViewController <UITextFieldDelegate>

@end

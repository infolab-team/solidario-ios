//
//  DonationActivity.h
//
//  Created by Paula Vasconcelos on 16/06/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ProjectBasic;

@interface DonationActivity : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double amount;
@property (nonatomic, strong) NSString *status;
@property (nonatomic, strong) ProjectBasic *project;
@property (nonatomic, strong) NSString *type;
@property (nonatomic, strong) NSString *date;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end

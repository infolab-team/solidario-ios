//
//  AppDelegate.m
//  Solidario
//
//  Created by Paula Vasconcelos Gueiros on 1/8/16.
//  Copyright © 2016 Paula Vasconcelos Gueiros. All rights reserved.
//

#import "AppDelegate.h"
#import "LGSideMenuController.h"
#import "MainViewController.h"
#import "IntroMainViewController.h"
#import "UIColor+Utils.h"
#import "Singleton.h"
#import "Service.h"
#import "UIColor+Utils.h"
#import "PagarMe.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    NSString *storyboardIdentifier;
    UIStoryboard *storyboard;
    UIWindow *window = [UIApplication sharedApplication].delegate.window;

    [[UINavigationBar appearance]setBarTintColor:[UIColor defaultColor:DefaultColorNavigationBar]];
    [[UINavigationBar appearance]setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor defaultColor:DefaultColorMainText],NSForegroundColorAttributeName, nil]];
    [[UINavigationBar appearance]setTintColor:[UIColor defaultColor:DefaultColorMainText]];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [[UIBarButtonItem appearanceWhenContainedIn:[UISearchBar class], nil] setTintColor:[UIColor whiteColor]];
    
    // Facebook API
    [[FBSDKApplicationDelegate sharedInstance] application:application didFinishLaunchingWithOptions:launchOptions];
    
    // PagarMe API
    NSString *encryptionKey = (PROJECT_IS_IN_DEVELOPMENT_ENV) ?
        @"ek_test_tQu29l7wxvB1O9uNtZmFxR0JrlxoBz" : @"ek_live_ymS9WFg1NmEese2nzrSk1pQ1xP51W8";
    [[PagarMe sharedInstance] setEncryptionKey:encryptionKey];
    
    // Singleton property for navigation use
    [Singleton setDonating:NO];
    [Singleton setUpdatingTimeline:NO];

    if (![Singleton isFirstTimeUse]) {
        if ([Singleton loggedUser] == nil) { // no current logged user
            storyboardIdentifier = @"Account";
            storyboard = [UIStoryboard storyboardWithName:storyboardIdentifier bundle:nil];
            
            UIViewController *mainAccountViewController = [storyboard instantiateViewControllerWithIdentifier:@"mainAccountViewController"];
            
            window.rootViewController = mainAccountViewController;
            
        } else { // user is logged in
            storyboardIdentifier = @"Main";
            storyboard = [UIStoryboard storyboardWithName:storyboardIdentifier bundle:nil];
            
            UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"mainNavigationController"];
            MainViewController *mainViewController = [storyboard instantiateViewControllerWithIdentifier:@"mainViewController"];
            mainViewController.rootViewController = navigationController;
            [mainViewController setup];
            
            window.rootViewController = mainViewController;
        }
    } else {
        storyboardIdentifier = @"Introduction";
        storyboard = [UIStoryboard storyboardWithName:storyboardIdentifier bundle:nil];
        
        IntroMainViewController *introMainViewController = [storyboard instantiateViewControllerWithIdentifier:@"introMainViewController"];
        
        window.rootViewController = introMainViewController;
    }
    
    [UIView transitionWithView:window
                      duration:0.5
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:nil
                    completion:nil];

    return YES;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    
    BOOL handled = [[FBSDKApplicationDelegate sharedInstance] application:application
                                                                  openURL:url
                                                        sourceApplication:sourceApplication
                                                               annotation:annotation
                    ];
    
    return handled;
} 

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end

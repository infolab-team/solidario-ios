//
//  DonationValueTableViewController.h
//  Solidario
//
//  Created by Pan on 25/11/16.
//  Copyright © 2016 Paula Vasconcelos Gueiros. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProjectShort.h"

@class DonationGoalView;
@class DonationValueView;

@interface DonationValueTableViewController : UITableViewController

@property (weak, nonatomic) IBOutlet DonationGoalView *donationGoalView;

@property (strong, nonatomic) ProjectShort *project;
@property (strong, nonatomic) UIImage *mainImage;

@end

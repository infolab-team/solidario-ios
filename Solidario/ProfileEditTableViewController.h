//
//  ProfileEditTableViewController.h
//  Solidario
//
//  Created by Paula Vasconcelos Gueiros on 30/8/16.
//  Copyright © 2016 Paula Vasconcelos Gueiros. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileEditTableViewController : UITableViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate>

@end

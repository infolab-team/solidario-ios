//
//  DonationFailViewController.m
//  Solidario
//
//  Created by Paula Vasconcelos Gueiros on 20/10/16.
//  Copyright © 2016 Paula Vasconcelos Gueiros. All rights reserved.
//

#import "DonationFailViewController.h"

@interface DonationFailViewController ()
    
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImage;
@property (weak, nonatomic) IBOutlet UILabel *errorLabel;
    
@end

@implementation DonationFailViewController
    
#pragma mark - Initialization

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setup];
}

- (void)setup {
    // set image background
    NSData *stringData = [self.project.category dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *imageName = [NSString stringWithFormat:@"back %@", [[NSString alloc] initWithData:stringData encoding:NSASCIIStringEncoding]];
    self.backgroundImage.image = [UIImage imageNamed:imageName];
    
    // set feedback message
    if (self.feedbackMessage != nil && ![self.feedbackMessage isEqualToString:@""]) {
        self.errorLabel.text = self.feedbackMessage;
    }
}

- (IBAction)tappedOnContact:(id)sender {
    NSLog(@"👥 User tapped on contact link");
    
    NSString *mailString = [NSString stringWithFormat:@"mailto:contato@solidariobrasil.com.br?subject=Erro persistente no pagamento"];
    mailString = [mailString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *mailURL = [NSURL URLWithString:mailString];
    
    if ([[UIApplication sharedApplication] canOpenURL:mailURL]) {
        UIApplication *application = [UIApplication sharedApplication];
        if ([application respondsToSelector:@selector(openURL:options:completionHandler:)]) {
            [application openURL:mailURL options:@{} completionHandler:nil];
        } else if ([application respondsToSelector:@selector(openURL:)]) {
            [application openURL:mailURL];
        }
    }
}


@end

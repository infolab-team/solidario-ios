//
//  ProjectShort.h
//
//  Created by Paula Vasconcelos on 19/05/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Project.h"

@interface ProjectShort : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *status;
@property (nonatomic, strong) NSString *internalBaseClassIdentifier;
@property (nonatomic, strong) NSString *category;
@property (nonatomic, strong) NSString *goal;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *approved;
@property (nonatomic, strong) NSArray *media;
@property (nonatomic, assign) BOOL isFavorite;
@property (nonatomic, strong) NSString *internalBaseClassDescription;
@property (nonatomic, assign) double donators;
@property (nonatomic, assign) double percent ;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithProject:(Project *)project;
- (NSDictionary *)dictionaryRepresentation;

@end

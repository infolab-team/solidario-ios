//
//  DonatingAnimationViewController.h
//  Solidario
//
//  Created by Pan on 29/05/17.
//  Copyright © 2017 Paula Vasconcelos Gueiros. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProjectShort.h"
#import "Donation.h"

@interface DonatingAnimationViewController : UIViewController

@property (strong, nonatomic) ProjectShort *project;
@property (strong, nonatomic) Donation *donation;

@end

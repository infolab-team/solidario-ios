//
//  DonationValueView.m
//  Solidario
//
//  Created by Pan on 02/06/17.
//  Copyright © 2017 Paula Vasconcelos Gueiros. All rights reserved.
//

#import "DonationValueView.h"

@implementation DonationValueView

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        [self load];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self load];
    }
    return self;
}

- (void)load {
    UIView *view = [[[NSBundle bundleForClass:[self class]] loadNibNamed:@"DonationValueView" owner:self options:nil] firstObject];
    [self addSubview:view];
    view.frame = self.bounds;
}

@end

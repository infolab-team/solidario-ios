//
//  MainViewController.h
//  Solidario
//
//  Created by Paula Vasconcelos Gueiros on 11/3/16.
//  Copyright © 2016 paulinhavgueiros. All rights reserved.
//

#define LEFT_MENU_WIDTH                         280.f

#import "LGSideMenuController.h"

@interface MainViewController : LGSideMenuController <UITextViewDelegate>

- (void)setup;

@end

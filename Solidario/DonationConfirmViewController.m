//
//  DonationConfirmViewController.m
//  Solidario
//
//  Created by Paula Vasconcelos Gueiros on 20/10/16.
//  Copyright © 2016 Paula Vasconcelos Gueiros. All rights reserved.
//

#import "DonationConfirmViewController.h"
#import "BankBilletViewController.h"

@interface DonationConfirmViewController ()

@property (weak, nonatomic) IBOutlet UILabel *donorsLabel;
@property (weak, nonatomic) IBOutlet UILabel *projectConclusionLabel;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImage;
@property (weak, nonatomic) IBOutlet UIButton *printBilletButton;
@property (weak, nonatomic) IBOutlet UIButton *returnToProjectsButton;

@property (nonatomic) BOOL usedBillet;

@end

@implementation DonationConfirmViewController
    
#pragma mark - Initialization

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setup];
}

- (void)setup {
    // setup conclusion text
    
    NSString *preHelpProjectText, *postDonorsText;
    if (self.project.donators == 1)  {
        preHelpProjectText = @"que está";
        postDonorsText = @"Solidário";
    } else {
        preHelpProjectText = @"que estão";
        postDonorsText = @"Solidários";
    }
    
    self.donorsLabel.text = [NSString stringWithFormat:@"%.0f %@", self.project.donators, postDonorsText];
    
    NSString *projectTitle = self.project.title;
    NSString *helpProjectText = [NSString stringWithFormat:@"%@ ajudando o projeto %@ a virar realidade!", preHelpProjectText, projectTitle];
    NSDictionary *attributes = @{NSForegroundColorAttributeName: self.projectConclusionLabel.textColor,
                                 NSFontAttributeName: self.projectConclusionLabel.font};
    NSMutableAttributedString *attributedText =
    [[NSMutableAttributedString alloc] initWithString:helpProjectText
                                           attributes:attributes];
    UIFont *boldFont = [UIFont fontWithName:@"HelveticaNeue-Bold" size:self.projectConclusionLabel.font.pointSize];
    NSRange boldTextRange = [helpProjectText rangeOfString:projectTitle];
    [attributedText setAttributes:@{NSFontAttributeName:boldFont}
                            range:boldTextRange];
    
    self.projectConclusionLabel.attributedText = attributedText;
    
    // set image background
    NSData *stringData = [self.project.category dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *imageName = [NSString stringWithFormat:@"back %@", [[NSString alloc] initWithData:stringData encoding:NSASCIIStringEncoding]];
    self.backgroundImage.image = [UIImage imageNamed:imageName];
    
    // check if bank billet was used as payment type in order to display billet button or not
    if (self.donation.paymentType == PaymentTypeCreditCard) {
        [self.printBilletButton removeFromSuperview];
        [self.returnToProjectsButton setEnabled:YES];
        
        // post notification to let other controllers know that the project should be updated regarding donation number
        [[NSNotificationCenter defaultCenter] postNotificationName:@"donationWasMade" object:self.project.internalBaseClassIdentifier];
    }
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"modalToBankBillet"]) {
        [self.returnToProjectsButton setEnabled:YES];
        
        UINavigationController *navController = (UINavigationController *)[segue destinationViewController];
        BankBilletViewController *bankBilletViewController = (BankBilletViewController *)[navController topViewController];
        bankBilletViewController.billetStringURL = self.billetURL;
    }
}

- (IBAction)unwindToDonationConfirm:(UIStoryboardSegue *)unwindSegue {
    
}

@end

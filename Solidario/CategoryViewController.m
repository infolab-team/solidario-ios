//
//  CategoryViewController.m
//  Solidario
//
//  Created by Paula Vasconcelos Gueiros on 22/8/16.
//  Copyright © 2016 Paula Vasconcelos Gueiros. All rights reserved.
//

#import "CategoryViewController.h"
#import "StateTableViewController.h"
#import "IntroMainViewController.h"

#import "Service.h"
#import "Singleton.h"

@interface CategoryViewController ()

@property (weak, nonatomic) IBOutlet UIBarButtonItem *closeButton;
@property (weak, nonatomic) IBOutlet UIButton *socialButton;
@property (weak, nonatomic) IBOutlet UIButton *environmentButton;
@property (weak, nonatomic) IBOutlet UIButton *animalButton;
@property (weak, nonatomic) IBOutlet UIButton *healthButton;
@property (weak, nonatomic) IBOutlet UIImageView *socialSelectedCover;
@property (weak, nonatomic) IBOutlet UIImageView *environmentSelectedCover;
@property (weak, nonatomic) IBOutlet UIImageView *animalSelectedCover;
@property (weak, nonatomic) IBOutlet UIImageView *healthSelectedCover;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (strong, nonatomic) NSArray *categoryButtons;

@end

@implementation CategoryViewController

#pragma mark - Initialization 

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setup];
}

- (void)setup {
    self.categoryButtons = @[self.socialButton, self.environmentButton, self.animalButton, self.healthButton];
    
    // decide on showing close button or not, depending on app flow
    if (!self.showCloseButton) {
//        self.closeButton = nil;
        self.navigationItem.leftBarButtonItem = nil;
    }
}


#pragma mark - Button Actions

- (IBAction)selectedSocial:(id)sender {
    self.socialButton.selected = !self.socialButton.selected;
    self.socialSelectedCover.hidden = !self.socialButton.selected;
}

- (IBAction)selectedEnvironment:(id)sender {
    self.environmentButton.selected = !self.environmentButton.selected;
    self.environmentSelectedCover.hidden = !self.environmentButton.selected;
}

- (IBAction)selectedAnimal:(id)sender {
    self.animalButton.selected = !self.animalButton.selected;
    self.animalSelectedCover.hidden = !self.animalButton.selected;
}

- (IBAction)selectedHealth:(id)sender {
    self.healthButton.selected = !self.healthButton.selected;
    self.healthSelectedCover.hidden = !self.healthButton.selected;
}


#pragma mark - Navigation

- (IBAction)advance:(id)sender {
    NSLog(@"➡️ User pressed advance button");
    [self performInputValidations];
}

- (void)performInputValidations {
    NSLog(@"---");
    NSLog(@"✅ Selected category(ies):");
    if ([self.socialButton isSelected]) NSLog(@"🙌🏽 Social");
    if ([self.environmentButton isSelected]) NSLog(@"🌱 Environment");
    if ([self.animalButton isSelected]) NSLog(@"🐶 Animal");
    if ([self.healthButton isSelected]) NSLog(@"💊 Health");
    NSLog(@"---");

    // at leats one selected category confirmation
    __block BOOL oneSelected = NO;
    [self.categoryButtons enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        
        if ([((UIButton *)obj) isSelected]) {
            oneSelected = YES;
            *stop = YES;
        }
    }];
    
    if (!oneSelected) {
        // alert setup
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Atenção"
                                                                       message:@"Você deve selecionar ao menos um tema de interesse."
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"OK"
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * action) {}];
        [alert addAction:okButton];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    
    [self proceedWithAdvance];
}

- (void)proceedWithAdvance {
    // call Category service and save user options
    
    [self.activityIndicator startAnimating];
    
    [Service getAllCagetoriesWithCompletion:^(id results, BOOL hasError, NSString *errorMessage) {
        [self.activityIndicator stopAnimating];
        
        if (hasError) {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Erro"
                                                                           message:nil
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"OK"
                                                               style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction * action) {}];
            [alert addAction:okButton];
            
//            if (errorMessage != nil) {
//                [alert setMessage:errorMessage];
//            } else {
                [alert setMessage:@"Não foi possível salvar as escolhas de tema.  Tente novamente mais tarde."];
//            }
            [self presentViewController:alert animated:YES completion:nil];
            return;
        } else {
            // save Category settings to Singleton
            
            NSArray *categories = (NSArray *)results;

            [self.categoryButtons enumerateObjectsUsingBlock:^(id obj1, NSUInteger idx1, BOOL *stop1) {
                UIButton *categoryButton = ((UIButton *)obj1);
                NSString *categoryButtonID = categoryButton.restorationIdentifier;
                
                [categories enumerateObjectsUsingBlock:^(id obj2, NSUInteger idx2, BOOL *stop2) {
                    Category *category = (Category *)obj2;
                    
                    if ([category.internalBaseClassIdentifier isEqualToString:categoryButtonID]) {
                        [category setSelected:categoryButton.isSelected];
                        *stop2 = YES;
                    }
                }];
            }];
            [Singleton saveCategories:categories];
            NSLog(@"🤡 Category settings definition successful");
        }
        
        [self performSegueWithIdentifier:@"pushToLocation" sender:self];
    }];
}

- (IBAction)close:(id)sender {
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"pushToLocation"]) {
        StateTableViewController *stateTableViewController = (StateTableViewController *)[segue destinationViewController];
        stateTableViewController.fromLogin = YES;
        
    } else if ([segue.identifier isEqualToString:@"modalToIntroduction"]) {
        IntroMainViewController *introMainViewController = (IntroMainViewController *)[segue destinationViewController];
        introMainViewController.fromCategorySelection = YES;
    }
}

@end





















//
//  ProjectDetailViewController.h
//  Solidario
//
//  Created by Pan on 05/12/16.
//  Copyright © 2016 Paula Vasconcelos Gueiros. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProjectShort.h"
#import "Project.h"

@interface ProjectDetailViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UICollectionViewDataSource, UICollectionViewDelegate, UIWebViewDelegate, UIGestureRecognizerDelegate>

@property (strong, nonatomic) ProjectShort *projectShort;
@property (strong, nonatomic) Project *project;

@property (nonatomic) BOOL isFinalized;
@property (nonatomic) BOOL shouldAlwaysHideFavoriteButton;

@property CGFloat totalValueLabelPointSize;
@property NSInteger selectedBannerPage;
    
@end

//
//  About.m
//
//  Created by Paula Vasconcelos on 28/04/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "About.h"

NSString *const kAboutTitle = @"title";
NSString *const kAboutContent = @"content";

@interface About ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end


@implementation About

@synthesize title = _title;
@synthesize content = _content;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.title = [self objectOrNilForKey:kAboutTitle fromDictionary:dict];
            self.content = [self objectOrNilForKey:kAboutContent fromDictionary:dict];

    }
    return self;
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.title forKey:kAboutTitle];
    [mutableDict setValue:self.content forKey:kAboutContent];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}


#pragma mark - Helper Method

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.title = [aDecoder decodeObjectForKey:kAboutTitle];
    self.content = [aDecoder decodeObjectForKey:kAboutContent];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {

    [aCoder encodeObject:_title forKey:kAboutTitle];
    [aCoder encodeObject:_content forKey:kAboutContent];
}

- (id)copyWithZone:(NSZone *)zone {
    About *copy = [[About alloc] init];
    
    if (copy) {

        copy.title = [self.title copyWithZone:zone];
        copy.content = [self.content copyWithZone:zone];
    }
    return copy;
}

@end

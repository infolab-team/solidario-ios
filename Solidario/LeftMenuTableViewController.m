//
//  LeftMenuTableViewController.m
//  Solidario
//
//  Created by Paula Vasconcelos Gueiros on 1/8/16.
//  Copyright © 2016 Paula Vasconcelos Gueiros. All rights reserved.
//

#define MENU_CELL_TITLE_KEY              @"title"
#define MENU_CELL_ICON_IMAGE_KEY         @"iconImage"

#import "LeftMenuTableViewController.h"
#import "UIColor+Utils.h"
#import "Singleton.h"
#import "UserMenuTableViewCell.h"
#import "IconMenuTableViewCell.h"
#import "CategoryMenuTableViewCell.h"
#import "PlainMenuTableViewCell.h"
#import "NSArray+Utils.h"
#import "NSString+Utils.h"
#import "UIColor+Utils.h"
#import "Utils.h"
#import "MainViewController.h"
#import "Service.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface LeftMenuTableViewController ()

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (strong, nonatomic) NSArray <NSDictionary *> *menuEntries;
@property BOOL isThereLoggedUser;
@property (nonatomic) NSInteger numberOfRowsLoggedIn;
@property (nonatomic) NSInteger numberOfRowsLoggedOut;

@end


@implementation LeftMenuTableViewController

static const CGFloat userCellHeight = 72.f;
static const CGFloat categoryCellHeight = 100.f;
static const CGFloat defaultCellHeight = 52.f;

#pragma mark - Initialization

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setup];
}

- (void)setup {
    [self setupData];
    
    self.activityIndicator.center = CGPointMake(LEFT_MENU_WIDTH/2, self.tableView.center.y);
    [self.view addSubview:self.activityIndicator];
    [self.view bringSubviewToFront:self.activityIndicator];
    
    // setup notification observers
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userHasLoggedInMenu:) name:@"userHasLoggedInMenu" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userHasBeenUpdated:) name:@"userHasBeenUpdated" object:nil];
}

- (void)setupData {
    if ([Singleton loggedUser] == nil) { // no current logged user
        self.isThereLoggedUser = NO;
        self.menuEntries = [NSArray loadPlistWithName:@"MenuContentNotLogged"];
        self.numberOfRowsLoggedOut = [self.menuEntries count];
    } else { // user is logged in
        self.isThereLoggedUser = YES;
        self.menuEntries = [NSArray loadPlistWithName:@"MenuContentLogged"];
        self.numberOfRowsLoggedIn = [self.menuEntries count];
    }
    
    [self.tableView reloadData];
}

- (void)userHasLoggedInMenu:(NSNotification *)notification {
    [self setupData];
}

- (void)userHasBeenUpdated:(NSNotification *)notification {
    [self.tableView reloadData];
}


#pragma mark - UITableView Delegate and Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.isThereLoggedUser) {
        return self.numberOfRowsLoggedIn;
    } else {
        return self.numberOfRowsLoggedOut;
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.isThereLoggedUser) {
        if (indexPath.row == UserCellIn) {
            return userCellHeight;
        } else if (indexPath.row == CategoryCellIn) {
            return categoryCellHeight;
        } else if (indexPath.row == CardCellIn) {
            if (CREDIT_CARD_SAVE_SERVICE_ACTIVATED) {
                return defaultCellHeight;
            } else {
                return 0.f;
            }
        } else {
            return defaultCellHeight;
        }
    } else {
        if (indexPath.row == CategoryCellOut) {
            return categoryCellHeight;
        } else {
            return defaultCellHeight;
        }
    }
}

- (void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UIView *selectedBackgroundView = [[UIView alloc] initWithFrame:cell.backgroundView.frame];
    selectedBackgroundView.backgroundColor = [UIColor clearColor];
    cell.selectedBackgroundView = selectedBackgroundView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (self.isThereLoggedUser) {
        
        if (indexPath.row == UserCellIn) {
            UserMenuTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"userMenuTableViewCell"];
            
            User *loggedUser = [Singleton loggedUser];
            
            if (![loggedUser.image isEqualToString:@""]) {
                NSURL *imageURL = [NSURL URLWithString:[NSString encodedStringFromString:loggedUser.image]];
                
                // check if image src is base64 or url
                if ([loggedUser.image hasPrefix:@"data:image"]) {
                    NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
                    cell.userPicture.image = [UIImage imageWithData:imageData];
                } else {
                    [cell.userPicture sd_setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"defaultUser"]];
                }
            } else {
                cell.userPicture.image = [UIImage imageNamed:@"defaultUser"];
            }
            
            cell.userName.text = [NSString stringWithFormat:@"%@ %@", [Singleton loggedUser].name, [Singleton loggedUser].surname];
            return cell;
        } else if (indexPath.row == CategoryCellIn) {
            CategoryMenuTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"categoryMenuTableViewCell"];

            NSArray *categories = [Singleton categories];
            [cell.categoryButtons enumerateObjectsUsingBlock:^(id obj1, NSUInteger idx1, BOOL *stop1) {
                CategoryMenuButton *categoryButton = ((CategoryMenuButton *)obj1);
                NSString *categoryButtonID = categoryButton.restorationIdentifier;
                
                [categories enumerateObjectsUsingBlock:^(id obj2, NSUInteger idx2, BOOL *stop2) {
                    Category *category = (Category *)obj2;
                    
                    if ([category.internalBaseClassIdentifier isEqualToString:categoryButtonID]) {
                        [categoryButton setSelected:category.selected];
                        *stop2 = YES;
                    }
                }];
            }];
            
            return cell;
        } else if (indexPath.row >= TermsCellIn && indexPath.row <= AboutCellIn) {
            PlainMenuTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"plainMenuTableViewCell"];
            NSDictionary *menuItem = [self.menuEntries objectAtIndex:indexPath.row];
            cell.title.text = [menuItem objectForKey:MENU_CELL_TITLE_KEY];
            return cell;
        } else {
            // empty cell if save credit cards service not activated
            if (indexPath.row == CardCellIn && !CREDIT_CARD_SAVE_SERVICE_ACTIVATED) {
                return [tableView dequeueReusableCellWithIdentifier:@"emptyMenuTableViewCell"];
            }
            
            IconMenuTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"iconMenuTableViewCell"];
            NSDictionary *menuItem = [self.menuEntries objectAtIndex:indexPath.row];
            cell.title.text = [menuItem objectForKey:MENU_CELL_TITLE_KEY];
            
            // set color for all icons
            cell.iconImage.image = [UIImage imageNamed:[menuItem objectForKey:MENU_CELL_ICON_IMAGE_KEY]];
            cell.iconImage.image = [cell.iconImage.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            [cell.iconImage setTintColor:[UIColor defaultColor:DefaultColorMenuIcons]];
            
            if (indexPath.row >= LocationCellIn && indexPath.row <= CardCellIn) {
                cell.backgroundColor = [UIColor defaultColor:DefaultColorMenuLightBackground];
            } else {
                cell.backgroundColor = [UIColor defaultColor:DefaultColorMenuDarkBackground];
            }
            return cell;
        }
    } else {
        
        if (indexPath.row == CategoryCellOut) {
            CategoryMenuTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"categoryMenuTableViewCell"];
            
            NSArray *categories = [Singleton categories];
            [cell.categoryButtons enumerateObjectsUsingBlock:^(id obj1, NSUInteger idx1, BOOL *stop1) {
                CategoryMenuButton *categoryButton = ((CategoryMenuButton *)obj1);
                NSString *categoryButtonID = categoryButton.restorationIdentifier;
                
                [categories enumerateObjectsUsingBlock:^(id obj2, NSUInteger idx2, BOOL *stop2) {
                    Category *category = (Category *)obj2;
                    
                    if ([category.internalBaseClassIdentifier isEqualToString:categoryButtonID]) {
                        [categoryButton setSelected:category.selected];
                        *stop2 = YES;
                    }
                }];
            }];
            
            return cell;
        } if (indexPath.row == LocationCellOut) {
            IconMenuTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"iconMenuTableViewCell"];
            NSDictionary *menuItem = [self.menuEntries objectAtIndex:indexPath.row];
            cell.title.text = [menuItem objectForKey:MENU_CELL_TITLE_KEY];
            cell.iconImage.image = [UIImage imageNamed:[menuItem objectForKey:MENU_CELL_ICON_IMAGE_KEY]];
            cell.backgroundColor = [UIColor defaultColor:DefaultColorMenuLightBackground];
            return cell;
        } else {
            PlainMenuTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"plainMenuTableViewCell"];
            NSDictionary *menuItem = [self.menuEntries objectAtIndex:indexPath.row];
            cell.title.text = [menuItem objectForKey:MENU_CELL_TITLE_KEY];
            return cell;
        }
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (self.isThereLoggedUser) {
        if (indexPath.row == UserCellIn) {
            NSLog(@"👉🏽 Selected: 🔧 User profile edit page");
            [[NSNotificationCenter defaultCenter] postNotificationName:@"presentProfileEdit" object:nil];
        } else if (indexPath.row == CreatedCellIn) {
            NSLog(@"👉🏽 Selected: 🌎 Created projects page");
            [[NSNotificationCenter defaultCenter] postNotificationName:@"presentProjectLists" object:[NSNumber numberWithInteger:indexPath.row]];
        } else if (indexPath.row == DonatedCellIn) {
            NSLog(@"👉🏽 Selected: 💰 Supported projects page");
            [[NSNotificationCenter defaultCenter] postNotificationName:@"presentProjectLists" object:[NSNumber numberWithInteger:indexPath.row]];
        } else if (indexPath.row == FavoriteCellIn) {
            NSLog(@"👉🏽 Selected: ❤️ Favorite projects page");
            [[NSNotificationCenter defaultCenter] postNotificationName:@"presentProjectLists" object:[NSNumber numberWithInteger:indexPath.row]];
        } else if (indexPath.row == LocationCellIn) {
            NSLog(@"👉🏽 Selected: 📌 Location page");
            [[NSNotificationCenter defaultCenter] postNotificationName:@"presentLocation" object:nil];
        } else if (indexPath.row == ActivityCellIn) {
            NSLog(@"👉🏽 Selected: ⛹🏻‍♀️ Activity page");
            [[NSNotificationCenter defaultCenter] postNotificationName:@"presentActivity" object:nil];
        } else if (indexPath.row == CardCellIn) {
            NSLog(@"👉🏽 Selected: 💳 Credit card list page");
            [[NSNotificationCenter defaultCenter] postNotificationName:@"presentCardList" object:nil];
        } else if (indexPath.row == TermsCellIn) {
            NSLog(@"👉🏽 Selected: 📝 Terms page");
            [[NSNotificationCenter defaultCenter] postNotificationName:@"presentTerms" object:nil];
        } else if (indexPath.row == AboutCellIn) {
            NSLog(@"👉🏽 Selected: 📚 About page");
            [[NSNotificationCenter defaultCenter] postNotificationName:@"presentAbout" object:nil];
        } else if (indexPath.row == LogoutCellIn) {
            NSLog(@"👉🏽 Selected: ↩️ Logout button");
            
            // ask for user confirmation before calling service
            // purchase confirmation
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Confirmação"
                                                                           message:[NSString stringWithFormat:@"Tem certeza que deseja fazer o logout?"]
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"Sim"
                                                               style:UIAlertActionStyleDestructive
                                                             handler:^(UIAlertAction * action) {
                                                                 [self callLogoutService];
                                                             }];
            UIAlertAction *cancelButton = [UIAlertAction actionWithTitle:@"Cancelar"
                                                                   style:UIAlertActionStyleCancel
                                                                 handler:^(UIAlertAction * action) {}];
            
            [alert addAction:okButton];
            [alert addAction:cancelButton];
            [self.view.window.rootViewController presentViewController:alert animated:YES completion:nil];
        }
    } else {
        if (indexPath.row == LoginCellOut) {
            NSLog(@"👉🏽 Selected: ↪️ Login button");
            [self instantiateAccountPage];
        } else if (indexPath.row == LocationCellOut) {
            NSLog(@"👉🏽 Selected: 📌 Location page");
            [[NSNotificationCenter defaultCenter] postNotificationName:@"presentLocation" object:nil];
        } else if (indexPath.row == TermsCellOut) {
            NSLog(@"👉🏽 Selected: 📝 Terms page");
            [[NSNotificationCenter defaultCenter] postNotificationName:@"presentTerms" object:nil];
        } else if (indexPath.row == AboutCellOut) {
            NSLog(@"👉🏽 Selected: 📚 About page");
            [[NSNotificationCenter defaultCenter] postNotificationName:@"presentAbout" object:nil];
        }
    }
}


#pragma mark - Helper Methods

- (void)callLogoutService {
    // alert setup
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleAlert];
    __block UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"OK"
                                                               style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction * action) {}];
    [self.activityIndicator startAnimating];
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    
    [Service logoutWithCompletion:^(id results, BOOL hasError, NSString *errorMessage) {
        [self.activityIndicator stopAnimating];
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
        
        if (hasError) {
            if ([[(NSDictionary *)results valueForKey:@"statusCode"] integerValue] == 401) {
                [alert setTitle:@"Erro na autenticação"];
                [alert setMessage:@"Retornando à página de Login."];
                okButton = [UIAlertAction actionWithTitle:@"OK"
                                                    style:UIAlertActionStyleDefault
                                                  handler:^(UIAlertAction * action) {
                                                      [Singleton logout];
                                                      [kMainViewController hideLeftViewAnimated:YES completionHandler:^{
                                                          [self instantiateAccountPage];
                                                      }];
                                                  }];
            } else {
                [alert setTitle:@"Erro"];
//                if (errorMessage != nil) {
//                    [alert setMessage:errorMessage];
//                } else {
                    [alert setMessage:@"Não foi possível efetuar o logout."];
//                }
            }
            [alert addAction:okButton];
            [self.view.window.rootViewController presentViewController:alert animated:YES completion:nil];
        } else {
            [alert setTitle:@"Solidário"];
            // save User object to Singleton
            if ([results isKindOfClass:[NSString class]]) {
                [alert setMessage:(NSString *)results];
                [alert addAction:okButton];
                [self.view.window.rootViewController presentViewController:alert animated:YES completion:nil];
                [Singleton logout];
                [kMainViewController hideLeftViewAnimated:YES completionHandler:^{
                    [self setupData];
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"userHasLoggedOut" object:nil];
                }];
            }
        }
    }];
}


#pragma mark - Navigation

- (void)instantiateAccountPage {
    NSString *storyboardIdentifier = @"Account";
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardIdentifier bundle:nil];
    UIViewController *mainAccountViewController = [storyboard instantiateViewControllerWithIdentifier:@"mainAccountViewController"];
    
    [self.view.window.rootViewController presentViewController:mainAccountViewController animated:YES completion:nil];
}

@end

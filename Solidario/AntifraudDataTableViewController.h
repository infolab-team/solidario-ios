//
//  AntifraudDataTableViewController.h
//  Solidario
//
//  Created by Pan on 02/12/16.
//  Copyright © 2016 Paula Vasconcelos Gueiros. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProjectShort.h"
#import "Donation.h"
#import "DonationGoalView.h"
#import "DonationValueView.h"

@interface AntifraudDataTableViewController : UITableViewController <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet DonationGoalView *donationGoalView;
@property (weak, nonatomic) IBOutlet DonationValueView *donationValueView;

@property (strong, nonatomic) ProjectShort *project;
@property (strong, nonatomic) Donation *donation;
@property (strong, nonatomic) UIImage *mainImage;

@end

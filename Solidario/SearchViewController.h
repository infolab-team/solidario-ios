//
//  SearchViewController.h
//  Solidario
//
//  Created by Paula Vasconcelos Gueiros on 29/8/16.
//  Copyright © 2016 Paula Vasconcelos Gueiros. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@end

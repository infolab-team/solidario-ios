//
//  PlainMenuTableViewCell.h
//  Solidario
//
//  Created by Paula Vasconcelos Gueiros on 24/8/16.
//  Copyright © 2016 Paula Vasconcelos Gueiros. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlainMenuTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *title;

@end

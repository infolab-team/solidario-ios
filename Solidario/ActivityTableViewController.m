//
//  ActivityTableViewController.m
//  Solidario
//
//  Created by Pan on 26/06/17.
//  Copyright © 2017 Paula Vasconcelos Gueiros. All rights reserved.
//

#import "ActivityTableViewController.h"
#import "DonationActivityTableViewCell.h"
#import "ProjectDetailViewController.h"

#import "DonationActivity.h"
#import "Project.h"
#import "ProjectBasic.h"

#import "NSString+Utils.h"
#import "NSDictionary+Utils.h"
#import "UIColor+Utils.h"

#import "Service.h"

@interface ActivityTableViewController ()

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UIView *footerView;
@property (weak, nonatomic) IBOutlet UIButton *loadMoreButton;

@property (strong, nonatomic) NSMutableArray <DonationActivity *> *activities;
@property (strong, nonatomic) NSDictionary *projectStatusDictionary;
@property (strong, nonatomic) Project *selectedProject;

@property (nonatomic) NSInteger pageNumber;
@property (nonatomic) BOOL refreshControlIsActive;

@end


@implementation ActivityTableViewController

static const NSInteger itemsPerPage = 10;
static const CGFloat tableFooterHeight = 48;

#pragma mark - Initialization

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setup];
}

- (void) setup {
    // set estimated height for rows
    self.tableView.estimatedRowHeight = 125.f;
    
    // setup project status dictionary
    self.projectStatusDictionary = [NSDictionary loadPlistWithName:@"ProjectStatus"];
    
    // setup refresh control view and target
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl setTintColor:[UIColor defaultColor:DefaultColorMain]];
    [self.refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
    
    // hide load more button until at least one activity is found
    [self.loadMoreButton setHidden:YES];
    
    // initialize activity list
    self.activities = [NSMutableArray array];
    self.pageNumber = 1;
    [self.activityIndicator startAnimating];
    [self retrieveActivities];
}

- (void)retrieveActivities {
    
    [Service getUserActivitiesOnPage:self.pageNumber withItemsPerPage:itemsPerPage andCompletion:^(id results, BOOL hasError, NSString *errorMessage) {

        [self.refreshControl endRefreshing];
        [self.activityIndicator stopAnimating];
        
        if (hasError) {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Erro"
                                                                           message:nil
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"OK"
                                                               style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction * action) {}];
            [alert addAction:okButton];
//            if (errorMessage != nil) {
//                [alert setMessage:errorMessage];
//            } else {
                [alert setMessage:@"Não foi possível recuperar as atividades."];
//            }
            [self presentViewController:alert animated:YES completion:nil];
            
        } else {
            NSInteger numberOfResults = [(NSMutableArray *)results count];
            NSLog(@"🏓 %lu user activity(ies) found on page #️⃣%ld", (unsigned long)numberOfResults, (long)self.pageNumber);
            
            if (numberOfResults > 0) {
                // increment page number variable if call returned results
                self.pageNumber++;
                
                if (self.refreshControlIsActive) {
                    self.activities = [NSMutableArray array];
                }
                [self.activities addObjectsFromArray:(NSMutableArray *)results];

                // load new results into tableview
                NSInteger numberOfProjects = [self.activities count];
                if (numberOfResults == numberOfProjects) { // if first results, reload all data
                    [self.tableView reloadData];
                } else { // if additional results, append new rows
                    NSMutableArray *newIndexPaths = [NSMutableArray arrayWithCapacity:numberOfResults];
                    for (int i = 1; i <= numberOfResults; i++) {
                        NSIndexPath *newRowIndexPath = [NSIndexPath indexPathForRow:numberOfProjects-i inSection:0];
                        [newIndexPaths addObject:newRowIndexPath];
                    }
                    [self.tableView beginUpdates];
                    [self.tableView insertRowsAtIndexPaths:newIndexPaths withRowAnimation:UITableViewRowAnimationAutomatic];
                    [self.tableView endUpdates];
                }
                
                // unhide load more button (if hidden)
                [self.loadMoreButton setHidden:NO];
            } else {
                [self.tableView beginUpdates];
                [self.loadMoreButton setHidden:YES];
                [self.footerView setFrame:CGRectMake(self.footerView.frame.origin.x, self.footerView.frame.origin.y, self.footerView.frame.size.width, 0)];
                [self.tableView endUpdates];
            }
        }
        self.refreshControlIsActive = NO;
    }];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // setup activity indicator
    UIWindow *frontWindow = [[UIApplication sharedApplication] keyWindow];
    [frontWindow addSubview:self.activityIndicator];
    self.activityIndicator.center = frontWindow.center;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    // stop activityIndicator on cases in which it's still animating
    [self.activityIndicator stopAnimating];
    [self.activityIndicator removeFromSuperview];
}


#pragma mark - UITableView Delegate and Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.activities count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    DonationActivityTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"donationActivityCell" forIndexPath:indexPath];
    
    DonationActivity *activity = [self.activities objectAtIndex:indexPath.row];
    
    NSString *monthNumber = [activity.date substringWithRange:NSMakeRange(5, 2)];
    NSString *dayNumber = [activity.date substringWithRange:NSMakeRange(8, 2)];
    
    cell.activityDateDayLabel.text = dayNumber;
    cell.activityDateMonthLabel.text = [NSString monthShortNameFromInteger:[monthNumber integerValue]-1];
    
    // date setup for time
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    [dateFormatter setDateFormat:@"yyyy'-'MM'-'dd'T'HH':'mm':'ss.SSS'Z'"];
    NSDate *activityDate = [dateFormatter dateFromString:activity.date];
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    [dateFormatter setDateFormat:@"HH:mm"];
    cell.activityDateTimeLabel.text = [dateFormatter stringFromDate:activityDate];

    // month label spacing
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:cell.activityDateMonthLabel.text];
    float spacing = 4.4f;
    [attributedString addAttribute:NSKernAttributeName
                             value:@(spacing)
                             range:NSMakeRange(0, [attributedString length]-1)];
    cell.activityDateMonthLabel.attributedText = attributedString;

    cell.activityTitleLabel.text = activity.type;
    
    // setting attributed string for description...

    // donation value with currency format
    NSNumberFormatter *currencyFormatter = [NSNumberFormatter new];
    [currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    NSLocale *currentLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"pt_BR"];
    [currencyFormatter setLocale:currentLocale];
    NSString *donationValueString = [currencyFormatter stringFromNumber:[NSNumber numberWithFloat:activity.amount/100.f]];
    
    // plain string for description
    NSString *fullDescString = [NSString stringWithFormat:@"Você doou %@ para o projeto \"%@\"", donationValueString, activity.project.title];
    
    // attributes for description
    NSDictionary *attributes = @{NSForegroundColorAttributeName: cell.activityDescriptionLabel.textColor,
                                 NSFontAttributeName: cell.activityDescriptionLabel.font};
    NSMutableAttributedString *fullDescAttString = [[NSMutableAttributedString alloc] initWithString:fullDescString
                                                                                          attributes:attributes];
    UIFont *boldFont = [UIFont boldSystemFontOfSize:cell.activityDescriptionLabel.font.pointSize];
    NSRange valueRange = [fullDescString rangeOfString:donationValueString];
    NSRange projectTitleRange = [fullDescString rangeOfString:activity.project.title];
    
    [fullDescAttString setAttributes:@{NSFontAttributeName: boldFont}
                               range:valueRange];
    [fullDescAttString setAttributes:@{NSFontAttributeName: boldFont}
                               range:projectTitleRange];
    
    // attributed string for description
    cell.activityDescriptionLabel.attributedText = fullDescAttString;
    [cell.activityDescriptionLabel layoutIfNeeded];
    
    // donation status
    cell.activityStatusLabel.text = [NSString stringWithFormat:@"Status: %@", activity.status];
    
    // timeline top line hidden if necessary
    if (indexPath.row == 0) {
        [cell.topLineView setHidden:YES];
    } else {
        [cell.topLineView setHidden:NO];
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([self.activities count] > 0 && indexPath.row == [self.activities count]-1) {
        NSLog(@"➕ Attempting to retrieve more activities...");
        [self retrieveActivities];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:[tableView indexPathForSelectedRow] animated:YES];
    
    ProjectBasic *projectBasic = [self.activities objectAtIndex:indexPath.row].project;
    
    [self.activityIndicator startAnimating];
    [Service getProjectWithIdentifier:projectBasic.projectIdentifier andCompletion:^(id results, BOOL hasError, NSString *errorMessage) {
        [self.activityIndicator stopAnimating];
        
        if (hasError) {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Erro"
                                                                           message:nil
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"OK"
                                                               style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction * action) {}];
            [alert addAction:okButton];
//            if (errorMessage != nil) {
//                [alert setMessage:errorMessage];
//            } else {
                [alert setMessage:@"Não foi possível recuperar detalhes do projeto."];
//            }
            [self presentViewController:alert animated:YES completion:nil];
        } else {
            self.selectedProject = (Project *)results;
            [self performSegueWithIdentifier:@"modalToProjectDetail" sender:self];
        }
    }];
}

#pragma mark - UITableView Helper Methods

- (void)refreshTable {
    self.refreshControlIsActive = YES;
    self.pageNumber = 1;
    
    [self.tableView beginUpdates];
    [self.footerView setFrame:CGRectMake(self.footerView.frame.origin.x, self.footerView.frame.origin.y, self.footerView.frame.size.width, tableFooterHeight)];
    [self.tableView endUpdates];
    
    [self retrieveActivities];
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

    if ([segue.identifier isEqualToString:@"modalToProjectDetail"]) {
        UINavigationController *navController = (UINavigationController *)[segue destinationViewController];
        ProjectDetailViewController *projectDetailViewController = (ProjectDetailViewController *)[navController topViewController];
        
        ProjectShort *projectShort = [[ProjectShort alloc] initWithProject:self.selectedProject];
        projectDetailViewController.projectShort = [projectShort copy];
        projectDetailViewController.project = [self.selectedProject copy];
        projectDetailViewController.selectedBannerPage = 0;
        projectDetailViewController.isFinalized = ![[self.projectStatusDictionary objectForKey:self.selectedProject.status] isEqualToString:@"Em andamento"];
        projectDetailViewController.shouldAlwaysHideFavoriteButton = YES;
    }
}

- (IBAction)unwindToBeforeProjectDetail:(UIStoryboardSegue*)unwindSegue {
    
}

@end

//
//  UILabel+Utils.h
//  Solidario
//
//  Created by Pan on 28/12/16.
//  Copyright © 2016 Paula Vasconcelos Gueiros. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (Utils)

+ (CGFloat)heightForLabelWithText:(NSString*)text font:(UIFont*)font andWidth:(CGFloat)width;

@end

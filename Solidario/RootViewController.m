//
//  RootViewController.m
//  Solidario
//
//  Created by Paula Vasconcelos Gueiros on 11/3/16.
//  Copyright © 2016 paulinhavgueiros. All rights reserved.
//

#import "RootViewController.h"
#import "RootNavigationController.h"
#import "MainViewController.h"
#import "ProjectTableViewController.h"
#import "ProjectCardViewController.h"
#import "DonationValueTableViewController.h"
#import "ProjectDetailViewController.h"
#import "LeftMenuTableViewController.h"
#import "ProfileEditTableViewController.h"
#import "StateTableViewController.h"
#import "BannerCollectionViewCell.h"
#import "DonorListTableViewController.h"
#import "CardPlaceholderView.h"
#import "Utils.h"
#import "NSArray+Utils.h"
#import "UIColor+Utils.h"
#import "NSString+Utils.h"
#import "Service.h"
#import "Singleton.h"
#import "ProjectShort.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface RootViewController ()

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *containerHiddenConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *containerShownConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *searchBarHiddenConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *searchBarShownConstraint;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UIButton *previousButton;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;
@property (weak, nonatomic) IBOutlet UIButton *donateButton;

@property (strong, nonatomic) NSNumber *projectListOption;
@property (strong, nonatomic) NSMutableArray *cardProjects;
@property (strong, nonatomic) UIImage *currentProjectImage;
@property (strong, nonatomic) UIButton *retryButtonReference;
@property (strong, nonatomic) ProjectShort *chosenProject;

@property NSInteger pageNumber;
@property NSInteger itemsPerPage;
@property BOOL searchingForProjects;

@property BOOL isThereLoggedUser;

@property BOOL willNeedScrolling;
@property NSInteger visibleProjectIndex;

@end


@implementation RootViewController

static const NSInteger constraintPriority1 = 900;
static const NSInteger constraintPriority2 = 500;
static const NSInteger defaultItemsPerPage = 10;
static const NSInteger offsetOfDispatch = 5; // must be at least 1, and less than itemsPerPage

#pragma mark - Initialization

- (void)awakeFromNib {
    [super awakeFromNib];
    //set up data
    //your carousel should always be driven by an array of
    //data of some kind - don't store data in your item views
    //or the recycling mechanism will destroy your data once
    //your item views move off-screen

    // initalize necessary variable
    self.itemsPerPage = defaultItemsPerPage;
    [self retrieveFirstProjects];
}
    
- (void)retrieveFirstProjects {
    self.pageNumber = 1;
    
    NSArray *categories = [Singleton categories];
    NSArray *states = [Singleton states];
    
    [self.activityIndicator startAnimating];
    if (self.retryButtonReference) {
        [self.retryButtonReference setEnabled:NO];
    }
    self.searchingForProjects = YES;
    
    [Service getProjectsOnPage:self.pageNumber withItemsPerPage:self.itemsPerPage categories:categories states:states andCompletion:^(id results, BOOL hasError, NSString *errorMessage) {
        [self.activityIndicator stopAnimating];
        if (self.retryButtonReference) {
            [self.retryButtonReference setEnabled:YES];
        }
        self.searchingForProjects = NO;
        
        if (hasError) {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil
                                                                           message:nil
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"OK"
                                                               style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction * action) {}];

            if ([[(NSDictionary *)results valueForKey:@"statusCode"] integerValue] == 401) {
                [alert setTitle:@"Erro na autenticação"];
                [alert setMessage:@"Retornando à página de Login."];
                okButton = [UIAlertAction actionWithTitle:@"OK"
                                                    style:UIAlertActionStyleDefault
                                                  handler:^(UIAlertAction * action) {
                                                      [Singleton logout];
                                                      [self instantiateAccountPage];
                                                  }];
            } else {
                [alert setTitle:@"Erro"];
//                if (errorMessage != nil) {
//                    [alert setMessage:errorMessage];
//                } else {
                    [alert setMessage:@"Não foi possível recuperar os projetos."];
//                }
            }
            [alert addAction:okButton];
            [self presentViewController:alert animated:YES completion:nil];
        } else {
            self.cardProjects = (NSMutableArray *)results;
            if ([self.cardProjects count] > 0) {
                [self enableFooterButtons];
                
                // increment page number variable
                self.pageNumber++;
            } else {
                [self disableFooterButtons];
            }
            [self.carousel reloadData];

            if (self.willNeedScrolling) {
                self.willNeedScrolling = NO;
                // scroll carousel to visible project after update
                [self.carousel scrollToItemAtIndex:self.visibleProjectIndex animated:NO];
            } else {
                // scroll carousel to beginning
                [self.carousel scrollToItemAtIndex:0 animated:YES];
            }
        }
    }];
}

- (void)dealloc {
    //it's a good idea to set these to nil here to avoid
    //sending messages to a deallocated viewcontroller
    self.carousel.delegate = nil;
    self.carousel.dataSource = nil;
}


#pragma mark - View Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setup];
}

- (void)setup {
    // setup carousel
    self.carousel.type = iCarouselTypeWheel;
    self.carousel.pagingEnabled = YES;
    
    // setup search bar
    self.searchBar.delegate = self;
    self.searchBar.showsCancelButton = YES;
    UIView * view = self.searchBar.subviews[0];
    for (UIView *subView in view.subviews) {
        if ([subView isKindOfClass:[UIButton class]]) {
            UIButton * cancelButton = (UIButton*)subView;
            [cancelButton setTitle:@"Cancelar" forState:UIControlStateNormal];
        }
    }
    
    // retrieve logged user
    self.isThereLoggedUser = ([Singleton loggedUser] != nil);
    
    // activate footer buttons and retry button if first projects retrived, will be updated with service if completed later
    if ([self.cardProjects count] > 0) {
        [self enableFooterButtons];
        if (self.retryButtonReference) {
            [self.retryButtonReference setEnabled:YES];
        }
    }
    
    // setup notification observers
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(favoriteOptionChanged:) name:@"favoriteOptionChanged" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userHasLoggedOut:) name:@"userHasLoggedOut" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userHasLoggedInRoot:) name:@"userHasLoggedInRoot" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userSettingsChanged:) name:@"userSettingsChanged" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(donationWasMade:) name:@"donationWasMade" object:nil];
}

- (void)viewDidUnload {
    [super viewDidUnload];
    
    // free up memory by releasing subviews
    self.carousel = nil;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(presentLocation:) name:@"presentLocation" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(presentActivity:) name:@"presentActivity" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(presentAbout:) name:@"presentAbout" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(presentTerms:) name:@"presentTerms" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(presentProfileEdit:) name:@"presentProfileEdit" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(presentProjectLists:) name:@"presentProjectLists" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(presentCardList:) name:@"presentCardList" object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"presentActivity" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"presentLocation" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"presentAbout" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"presentTerms" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"presentProfileEdit" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"presentProjectLists" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"presentCardList" object:nil];
    
    // stop activityIndicator on cases in which it's still animating
    [self.activityIndicator stopAnimating];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    // check if user is in the middle of the donating process
    if ([Singleton isDonating]) {
        [Singleton setDonating:NO];
        if ([Singleton loggedUser] != nil) {
            [self saveCurrentProjectImageIfVisible];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"userHasLoggedInMenu" object:nil];
            [self performSegueWithIdentifier:@"modalToDonationValue" sender:self];
            
            // while user is on donating screen, update projects for logged user
            [self userHasLoggedInRoot:nil];
        }
    }
    
    // check if user has selected option to manage projects
    if ([Singleton isUpdatingTimeline]) {
        [Singleton setUpdatingTimeline:NO];
        self.projectListOption = [NSNumber numberWithInt:CreatedCellIn];
        [self performSegueWithIdentifier:@"modalToProjectLists" sender:self];
    }
}


#pragma mark - Button Actions

- (IBAction)showLeftView:(id)sender {
    [kMainViewController showLeftViewAnimated:YES completionHandler:nil];
}

- (IBAction)showSearch:(id)sender {
    NSLog(@"🔍 User clicked on search icon");
    
    [self.searchBar becomeFirstResponder];
    
    self.containerHiddenConstraint.priority = constraintPriority2;
    self.containerShownConstraint.priority = constraintPriority1;
    self.searchBarHiddenConstraint.priority = constraintPriority2;
    self.searchBarShownConstraint.priority = constraintPriority1;
    
    [UIView animateWithDuration:0.5f animations:^(void){
        [self.view layoutIfNeeded];
    }];
}

- (IBAction)goToPreviousCard:(id)sender {
    [self.carousel scrollToItemAtIndex:[self.carousel currentItemIndex]-1 animated:YES];
}

- (IBAction)goToNextCard:(id)sender {
    [self.carousel scrollToItemAtIndex:[self.carousel currentItemIndex]+1 animated:YES];
}
    
- (IBAction)startDonationProcess:(id)sender {
    if ([Singleton loggedUser] == nil) { // no current logged user
        [Singleton setDonating:YES];
        [self performSegueWithIdentifier:@"modalToMainAccount" sender:self];
    } else {
        [self performSegueWithIdentifier:@"modalToDonationValue" sender:self];
    }
}
    
- (void)wasSelectedFavoriteButton:(UIButton *)button withEvent:(UIEvent *)event {
    // update UI first
    button.selected = !button.selected;
    
    ProjectShort *project = [self.cardProjects objectAtIndex:[self.carousel currentItemIndex]];
    // toggle favorite option in server
    [Service toggleFavoriteFromProjectWithIdentifier:project.internalBaseClassIdentifier andCompletion:^(id results, BOOL hasError, NSString *errorMessage) {
        if (hasError) {
            // if request not successful, return button to original state
            button.selected = !button.selected;
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Erro"
                                                                           message:nil
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"OK"
                                                               style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction * action) {}];
            [alert addAction:okButton];
//            if (errorMessage != nil) {
//                [alert setMessage:errorMessage];
//            } else {
                [alert setMessage:@"Não foi possível alterar a opção de favoritar o projeto."];
//            }
            [self presentViewController:alert animated:YES completion:nil];
            return;
        } else {
            project.isFavorite = button.selected;
        }
    }];
}
    
- (void)wasSelectedMoreButton:(UIButton *)button withEvent:(UIEvent *)event {
    
    [self performSegueWithIdentifier:@"modalToProjectDetail" sender:self];
}
    
- (void)wasSelectedRetryButton:(UIButton *)button withEvent:(UIEvent *)event {
    
    [self retrieveFirstProjects];
}

- (void)viewDonorList:(UIButton *)button withEvent:(UIEvent *)event {
    
    [self performSegueWithIdentifier:@"modalToDonorList" sender:self];
}


#pragma mark - Navigation

- (void)presentActivity:(NSNotification *)notification {
    [kMainViewController hideLeftViewAnimated:YES completionHandler:^{
        [self performSegueWithIdentifier:@"modalToActivity" sender:self];
    }];
}

- (void)presentLocation:(NSNotification *)notification {
    [kMainViewController hideLeftViewAnimated:YES completionHandler:^{
        [self performSegueWithIdentifier:@"modalToLocation" sender:self];
    }];
}

- (void)presentAbout:(NSNotification *)notification {
    [kMainViewController hideLeftViewAnimated:YES completionHandler:^{
        [self performSegueWithIdentifier:@"modalToAbout" sender:self];
    }];
}

- (void)presentTerms:(NSNotification *)notification {
    [kMainViewController hideLeftViewAnimated:YES completionHandler:^{
        [self performSegueWithIdentifier:@"modalToTerms" sender:self];
    }];
}

- (void)presentProfileEdit:(NSNotification *)notification {
    [kMainViewController hideLeftViewAnimated:YES completionHandler:^{
        [self performSegueWithIdentifier:@"modalToProfileEdit" sender:self];
    }];
}

- (void)presentProjectLists:(NSNotification *)notification {
    self.projectListOption = (NSNumber *)notification.object;
    
    [kMainViewController hideLeftViewAnimated:YES completionHandler:^{
        [self performSegueWithIdentifier:@"modalToProjectLists" sender:self];
    }];
}

- (void)presentCardList:(NSNotification *)notification {
    [kMainViewController hideLeftViewAnimated:YES completionHandler:^{
        [self performSegueWithIdentifier:@"modalToCardList" sender:self];
    }];
}

- (IBAction)unwindToRoot:(UIStoryboardSegue*)unwindSegue {

}
    
- (IBAction)unwindToBeforeTerms:(UIStoryboardSegue*)unwindSegue {
    
}
    
- (IBAction)unwindToBeforeDonationValue:(UIStoryboardSegue*)unwindSegue {

}

- (IBAction)unwindToBeforeCardList:(UIStoryboardSegue*)unwindSegue {
    
}

- (IBAction)unwindToBeforeProjectDetail:(UIStoryboardSegue*)unwindSegue {

}

- (IBAction)unwindToBeforeDonorList:(UIStoryboardSegue*)unwindSegue {
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    UINavigationController *navController = (UINavigationController *)[segue destinationViewController];
    
    if ([segue.identifier isEqualToString:@"modalToProjectLists"]) {
        ProjectTableViewController *projectTableViewController = (ProjectTableViewController *)[navController topViewController];
        projectTableViewController.projectListOption = self.projectListOption;
        projectTableViewController.isBeingLoaded = YES;
        
    } else if ([segue.identifier isEqualToString:@"modalToDonationValue"]) {
        DonationValueTableViewController *donationValueTableViewController = (DonationValueTableViewController *)[navController topViewController];
        if (self.chosenProject != nil) {
            donationValueTableViewController.project = [self.chosenProject copy];
            self.chosenProject = nil;
        } else {
            donationValueTableViewController.project = [self.cardProjects objectAtIndex:[self.carousel currentItemIndex]];
        }
        [self saveCurrentProjectImageIfVisible];
        donationValueTableViewController.mainImage = self.currentProjectImage;
        
    } else if ([segue.identifier isEqualToString:@"modalToProjectDetail"]) {
        ProjectDetailViewController *projectDetailViewController = (ProjectDetailViewController *)[navController topViewController];
        ProjectShort *project = [[self.cardProjects objectAtIndex:[self.carousel currentItemIndex]] copy];
        projectDetailViewController.projectShort = project;
        
        ProjectCardView *cardView = (ProjectCardView *)[self.carousel currentItemView];
        UICollectionView *bannerCollectionView = cardView.bannerCollectionView;
        CGFloat pageWidth = bannerCollectionView.frame.size.width;
        float currentPage = bannerCollectionView.contentOffset.x / pageWidth;
        projectDetailViewController.selectedBannerPage = currentPage;
        
    } else if ([segue.identifier isEqualToString:@"modalToLocation"]) {
        StateTableViewController *stateTableViewController = (StateTableViewController *)[navController topViewController];
        stateTableViewController.fromLogin = NO;
        
    } else if ([segue.identifier isEqualToString:@"modalToDonorList"]) {
        DonorListTableViewController *donorListTableViewController = (DonorListTableViewController *)[navController topViewController];
        ProjectShort *project = [[self.cardProjects objectAtIndex:[self.carousel currentItemIndex]] copy];
        donorListTableViewController.projectShort = project;
    }
}

- (void)instantiateAccountPage {
    NSString *storyboardIdentifier = @"Account";
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardIdentifier bundle:nil];
    UIViewController *mainAccountViewController = [storyboard instantiateViewControllerWithIdentifier:@"mainAccountViewController"];
    
    [self.view.window.rootViewController presentViewController:mainAccountViewController animated:YES completion:nil];
}


#pragma mark - Notification Handlers

- (void)favoriteOptionChanged:(NSNotification *)notification {

    NSString *projectId = (NSString *)notification.object;
    __block NSInteger changedProjectIndex = -1;
    __block ProjectShort *changedProject;
    [self.cardProjects enumerateObjectsUsingBlock:^(ProjectShort *project, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([project.internalBaseClassIdentifier isEqualToString:projectId]) {
            changedProject = project;
            changedProjectIndex = idx;
            *stop = YES;
        }
    }];
    
    if (changedProjectIndex >= 0) {
        NSLog(@"🌳 Found changed project in RootVC (for favorite/unfavorite heart touch)");
        NSLog(@"\tProject title: %@", changedProject.title);
        NSLog(@"\tProject index in card view: %ld", (long)changedProjectIndex);
        NSLog(@"\tRootVC value for object: isFavorite = %@", (changedProject.isFavorite) ? @"YES" : @"NO");
        
        ProjectCardView *cardView = (ProjectCardView *)[self.carousel itemViewAtIndex:changedProjectIndex];
        cardView.favoriteButton.selected = !cardView.favoriteButton.selected;
        changedProject.isFavorite = cardView.favoriteButton.selected;
    }
}

- (void)donationWasMade:(NSNotification *)notification {
    
    NSString *projectId = (NSString *)notification.object;
    __block NSInteger changedProjectIndex = -1;
    __block ProjectShort *changedProject;
    [self.cardProjects enumerateObjectsUsingBlock:^(ProjectShort *project, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([project.internalBaseClassIdentifier isEqualToString:projectId]) {
            changedProject = project;
            changedProjectIndex = idx;
            *stop = YES;
        }
    }];
    
    if (changedProjectIndex >= 0) {
        NSLog(@"⚜️ Found recently donated project in RootVC");
        NSLog(@"\tProject title: %@", changedProject.title);
        NSLog(@"\tProject index in card view: %ld", (long)changedProjectIndex);
        NSLog(@"\tRootVC value for project: number of donators = %.0f", changedProject.donators);
        ProjectCardView *cardView = (ProjectCardView *)[self.carousel itemViewAtIndex:changedProjectIndex];
        
        [Service getProjectWithIdentifier:projectId andCompletion:^(id results, BOOL hasError, NSString *errorMessage) {
            if (!hasError) {
                Project *donatedProject = (Project *)results;
                changedProject.donators = donatedProject.donatorsCount;

                NSDictionary *underlineAttribute = @{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)};
                [cardView.projectNumberOfDonorsLabel setAttributedText:[[NSAttributedString alloc] initWithString:[[NSNumber numberWithDouble:changedProject.donators] stringValue] attributes:underlineAttribute]];
                cardView.projectPeopleDonatedLabel.text = (changedProject.donators == 1) ? @"PESSOA JÁ DOOU" : @"PESSOAS JÁ DOARAM";
            }
        }];
    }
}

- (void)userSettingsChanged:(NSNotification *)notification {
    
    [self retrieveFirstProjects];
}

- (void)userHasLoggedOut:(NSNotification *)notification {
    self.isThereLoggedUser = NO;
    [self.carousel reloadData];
}

- (void)userHasLoggedInRoot:(NSNotification *)notification {
    self.isThereLoggedUser = YES;
    
    // save current project index
    NSInteger visibleProjectIndex = [self.carousel currentItemIndex];
    self.chosenProject = [self.cardProjects objectAtIndex:visibleProjectIndex];
    
    NSInteger currentPageNumber = self.pageNumber;
    
    // change itemsPerPage temporarily
    self.itemsPerPage = (currentPageNumber - 1) * defaultItemsPerPage;
    
    // retrieve all previously retrieved projects at once
    self.willNeedScrolling = YES; // set tag for scrolling to visible project after update
    self.visibleProjectIndex = visibleProjectIndex;
    [self retrieveFirstProjects];
    
    // set itemsPerPage back to default
    self.itemsPerPage = defaultItemsPerPage;

    // set pageNumber back to default
    self.pageNumber = currentPageNumber;
}


#pragma mark - SearchBar Delegate

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    NSLog(@"❌ Search was dismissed");
    
    [self.searchBar resignFirstResponder];
    
    self.containerHiddenConstraint.priority = constraintPriority1;
    self.containerShownConstraint.priority = constraintPriority2;
    self.searchBarHiddenConstraint.priority = constraintPriority1;
    self.searchBarShownConstraint.priority = constraintPriority2;
    
    [UIView animateWithDuration:0.5f animations:^(void){
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished){
        self.searchBar.text = @"";
        [[NSNotificationCenter defaultCenter] postNotificationName:@"clearSearchResults" object:nil];
    }];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    NSLog(@"🔍 User performed search with argument: %@", searchBar.text);
    [self.searchBar resignFirstResponder];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"presentSearchResults" object:searchBar.text];
}


#pragma mark - iCarousel Delegate and Data Source

- (NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel {
    
    return [self.cardProjects count];
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view {
    
    ProjectShort *project = [self.cardProjects objectAtIndex:index];
    ProjectCardViewController *projectCardViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"projectCardViewController"];
    ProjectCardView *cardView = (ProjectCardView *)projectCardViewController.view;
    
    // setup each card
    
    // banner collection view data
    [cardView.bannerPageControl setNumberOfPages:[project.media count]];
    cardView.bannerCollectionView.dataSource = self;
    cardView.bannerCollectionView.delegate = self;
    
    // favorite button data
    if (self.isThereLoggedUser) {
        [cardView.favoriteButton setSelected:project.isFavorite];
    } else {
        [cardView.favoriteButton setHidden:YES];
    }
    
    // goal data
    cardView.goalLabel.text = project.goal;

    // progress label data
    cardView.projectProgressLabel.endDegree = 3.6 * project.percent;
    cardView.projectProgressLabel.trackWidth = [Singleton pLabelTrackWidth];
    cardView.projectProgressLabel.progressWidth = [Singleton pLabelProgressWidth];
    
    NSMutableAttributedString *percentageAttString = [[NSMutableAttributedString alloc] initWithString: [NSString stringWithFormat:@"%.0f%%", project.percent]];
    [percentageAttString addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Helvetica Bold" size:[Singleton pLabelPercentageFontSize]] range:NSMakeRange(0, percentageAttString.length-1)];
    [percentageAttString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:[Singleton pLabelPercentSymbolFontSize]] range:NSMakeRange(percentageAttString.length-1, 1)];
    cardView.projectProgressLabel.attributedText = percentageAttString;

    // title data
    cardView.projectTitleLabel.text = project.title;
    
    // short description data
    [cardView.projectDescriptionLabel setNumberOfLines:[Singleton cardDescriptionLabelNumberOfLines]];
    cardView.projectDescriptionLabel.text = project.internalBaseClassDescription;
    [cardView.projectDescriptionLabel sizeToFit];
    
    // number of donors data
    NSDictionary *underlineAttribute = @{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)};
    cardView.projectNumberOfDonorsLabel.attributedText =
        [[NSAttributedString alloc] initWithString:[[NSNumber numberWithDouble:project.donators] stringValue]
                                        attributes:underlineAttribute];
    cardView.projectPeopleDonatedLabel.text = (project.donators == 1) ? @"PESSOA JÁ DOOU" : @"PESSOAS JÁ DOARAM";

    // button targets
    [cardView.favoriteButton addTarget:self action:@selector(wasSelectedFavoriteButton:withEvent:) forControlEvents:UIControlEventTouchUpInside];
    [cardView.showMoreButton addTarget:self action:@selector(wasSelectedMoreButton:withEvent:) forControlEvents:UIControlEventTouchUpInside];
    [cardView.numberOfDonorsButton addTarget:self action:@selector(viewDonorList:withEvent:) forControlEvents:UIControlEventTouchUpInside];

    return cardView;
}
    
- (NSInteger)numberOfPlaceholdersInCarousel:(__unused iCarousel *)carousel {
    return [self.cardProjects count] > 0 ? 2 : 1;
}
    
- (UIView *)carousel:(__unused iCarousel *)carousel placeholderViewAtIndex:(NSInteger)index reusingView:(UIView *)view {

    //create new view if no view is available for recycling
    if (view == nil) {
        UIViewController *cardPlaceholderViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"cardPlaceholderViewController"];
        view = (CardPlaceholderView *)cardPlaceholderViewController.view;
    }
    CardPlaceholderView *cardPlaceholderView = ((CardPlaceholderView *)view);
    
    if ([self.cardProjects count] > 0) {
        [cardPlaceholderView.retryButton setHidden:YES];
        [cardPlaceholderView.pigImageView setHidden:YES];
        
        [cardPlaceholderView.messageLabel setFont:[UIFont fontWithName:cardPlaceholderView.messageLabel.font.fontName size:32.f]];
        if (index == 0) {
            cardPlaceholderView.messageLabel.text = @"Início";
        } else {
            cardPlaceholderView.messageLabel.text = @"Fim";
        }
    } else {
        [cardPlaceholderView.pigImageView setHidden:NO];
        [cardPlaceholderView.retryButton setHidden:NO];
        
        [cardPlaceholderView.messageLabel setFont:[UIFont fontWithName:cardPlaceholderView.messageLabel.font.fontName size:22.f]];
        cardPlaceholderView.messageLabel.text = @"Nenhum Projeto Encontrado";
        
        self.retryButtonReference = cardPlaceholderView.retryButton;
        // retry button target
        [cardPlaceholderView.retryButton addTarget:self action:@selector(wasSelectedRetryButton:withEvent:) forControlEvents:UIControlEventTouchUpInside];
    }
    return view;
}

- (CGFloat)carousel:(__unused iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value {
    
    //customize carousel display
    switch (option) {
        case iCarouselOptionWrap: {
            //normally you would hard-code this to YES or NO
            return NO;
        }
        case iCarouselOptionSpacing: {
            //add a bit of spacing between the item views
            //NSInteger numCards = [self.cardProjectsOld count];
            NSInteger numCards = [self.cardProjects count];
            switch (numCards) {
                case 1:
                case 2:
                case 3:
                case 4: {
                    return value * (2.3f - (numCards * 0.2f));
                }
                case 5: {
                    return value * 1.4f;
                }
                case 6:
                case 7:
                case 8: {
                    return value * 1.3f;
                }
                default: {
                    return value * 1.2f;
                }
            }
        }
        case iCarouselOptionFadeMax: {
            if (self.carousel.type == iCarouselTypeCustom) {
                //set opacity based on distance from camera
                return 0.f;
            }
            return value;
        }
        case iCarouselOptionShowBackfaces:
        case iCarouselOptionRadius:
        case iCarouselOptionAngle:
        case iCarouselOptionArc:
        case iCarouselOptionTilt:
        case iCarouselOptionCount:
        case iCarouselOptionFadeMin:
        case iCarouselOptionFadeMinAlpha:
        case iCarouselOptionFadeRange:
        case iCarouselOptionOffsetMultiplier:
        case iCarouselOptionVisibleItems: {
            return value;
        }
    }
}

- (void)carouselCurrentItemIndexDidChange:(iCarousel *)carousel {
    
    NSInteger numberOfProjects = [self.cardProjects count];
    NSInteger currentProjectIndex = [carousel currentItemIndex];

    if ((currentProjectIndex == numberOfProjects - offsetOfDispatch) && (!self.searchingForProjects)) {
        [self retrieveMoreProjects];
    }
}

- (void)retrieveMoreProjects {
    
    NSArray *categories = [Singleton categories];
    NSArray *states = [Singleton states];
    __block NSArray *newProjects;
    
    self.searchingForProjects = YES;
    NSLog(@"🔎 Attempting to retrieve projects in page %ld (%ld items per page)...", (long)self.pageNumber, (long)self.itemsPerPage);
    [Service getProjectsOnPage:self.pageNumber withItemsPerPage:self.itemsPerPage categories:categories states:states andCompletion:^(id results, BOOL hasError, NSString *errorMessage) {
        
        [self.activityIndicator stopAnimating];
        self.searchingForProjects = NO;
        
        if (!hasError) {
            newProjects = results;
            
            if ([newProjects count] > 0) {
                NSLog(@"🗂 Adding %lu projects to array", (unsigned long)[newProjects count]);
                // increment data source
                [self.cardProjects addObjectsFromArray:newProjects];
                
                // increment page number variable
                self.pageNumber++;
                
                // reload carousel
                [self.carousel reloadData];
            } else {
                NSLog(@"📂 No projects found");
            }
        }
    }];
}


#pragma mark - UICollectionView Delegate and Data Source

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {

    NSInteger collectionViewCardIndex = [self.carousel indexOfItemViewOrSubview:collectionView];
    ProjectShort *project = [self.cardProjects objectAtIndex:collectionViewCardIndex];
    return [project.media count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    NSInteger collectionViewCardIndex = [self.carousel indexOfItemViewOrSubview:collectionView];
    ProjectShort *project = [self.cardProjects objectAtIndex:collectionViewCardIndex];
    
    BannerCollectionViewCell *bannerCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"bannerCell" forIndexPath:indexPath];
    NSString *imageUrlString = [NSString encodedStringFromString:project.media[indexPath.item]];
    [bannerCell.cellImageView sd_setImageWithURL:[NSURL URLWithString:imageUrlString] placeholderImage:[UIImage imageNamed:@"webImagePlaceholder"]];
    
    return bannerCell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {

    return collectionView.frame.size;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {

    UICollectionView *collectionView = (UICollectionView *)scrollView;
    CGFloat pageWidth = collectionView.frame.size.width;
    float currentPage = collectionView.contentOffset.x / pageWidth;
    
    // get object for current Card View and update CollectionView PageControl
    ProjectCardView *cardView = (ProjectCardView *)[self.carousel currentItemView];
    [cardView.bannerPageControl setCurrentPage:currentPage];
}


#pragma mark - Helper Methods

- (void)saveCurrentProjectImageIfVisible {
    ProjectCardView *cardView = (ProjectCardView *)[self.carousel itemViewAtIndex:[self.carousel currentItemIndex]];
    if (cardView) {
        UICollectionView *bannerCollectionView = cardView.bannerCollectionView;
        CGFloat pageWidth = bannerCollectionView.frame.size.width;
        float currentPage = bannerCollectionView.contentOffset.x / pageWidth;
        BannerCollectionViewCell *bannerCell = (BannerCollectionViewCell *)[cardView.bannerCollectionView cellForItemAtIndexPath:[NSIndexPath indexPathForItem:currentPage inSection:0]];
        self.currentProjectImage = bannerCell.cellImageView.image;
    }
}

- (void)disableFooterButtons {
    [self.donateButton setEnabled:NO];
    [self.previousButton setEnabled:NO];
    [self.nextButton setEnabled:NO];
}

- (void)enableFooterButtons {
    [self.donateButton setEnabled:YES];
    [self.previousButton setEnabled:YES];
    [self.nextButton setEnabled:YES];
}

@end

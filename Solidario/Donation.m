//
//  Donation.m
//
//  Created by Paula Vasconcelos on 05/05/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "Donation.h"
#import "PagarMeCreditCard.h"
#import "AntifraudData.h"

NSString *const kDonationPaymentType = @"paymentType";
NSString *const kDonationCardHash = @"cardHash";
NSString *const kDonationValue = @"value";
NSString *const kDonationPagarMeCreditCard = @"pagarMeCreditCard";
NSString *const kDonationAntifraudData = @"antifraudData";
NSString *const kDonationIsAnonymous = @"isAnonymous";

@interface Donation ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end


@implementation Donation

@synthesize paymentType = _paymentType;
@synthesize cardHash = _cardHash;
@synthesize value = _value;
@synthesize pagarMeCreditCard = _pagarMeCreditCard;
@synthesize antifraudData = _antifraudData;
@synthesize isAnonymous = _isAnonymous;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithValue:(double)value {
    self = [super init];
    if (self) {
        self.value = value;
        self.paymentType = PaymentTypeNone;
    }
    return self;
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
        self.paymentType = PaymentTypeNone;
        self.cardHash = [self objectOrNilForKey:kDonationCardHash fromDictionary:dict];
        self.value = [[self objectOrNilForKey:kDonationValue fromDictionary:dict] doubleValue];
        self.pagarMeCreditCard = nil;
        self.antifraudData = [AntifraudData modelObjectWithDictionary:[dict objectForKey:kDonationAntifraudData]];
        self.isAnonymous = [[self objectOrNilForKey:kDonationIsAnonymous fromDictionary:dict] boolValue];
    }
    return self;
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithInt:self.paymentType] forKey:kDonationPaymentType];
    [mutableDict setValue:self.cardHash forKey:kDonationCardHash];
    [mutableDict setValue:[NSNumber numberWithDouble:self.value] forKey:kDonationValue];
    [mutableDict setValue:nil forKey:kDonationPagarMeCreditCard];
    [mutableDict setValue:[self.antifraudData dictionaryRepresentation] forKey:kDonationAntifraudData];
    [mutableDict setValue:[NSNumber numberWithBool:self.isAnonymous] forKey:kDonationIsAnonymous];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}


#pragma mark - Helper Method

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.paymentType = [[aDecoder decodeObjectForKey:kDonationPaymentType] intValue];
    self.cardHash = [aDecoder decodeObjectForKey:kDonationCardHash];
    self.value = [aDecoder decodeDoubleForKey:kDonationValue];
    self.pagarMeCreditCard = [aDecoder decodeObjectForKey:kDonationPagarMeCreditCard];
    self.antifraudData = [aDecoder decodeObjectForKey:kDonationAntifraudData];
    self.isAnonymous = [aDecoder decodeBoolForKey:kDonationIsAnonymous];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {

    [aCoder encodeObject:[NSNumber numberWithInt:_paymentType] forKey:kDonationPaymentType];
    [aCoder encodeObject:_cardHash forKey:kDonationCardHash];
    [aCoder encodeDouble:_value forKey:kDonationValue];
    [aCoder encodeObject:_pagarMeCreditCard forKey:kDonationPagarMeCreditCard];
    [aCoder encodeObject:_antifraudData forKey:kDonationAntifraudData];
    [aCoder encodeBool:_isAnonymous forKey:kDonationIsAnonymous];
}

@end

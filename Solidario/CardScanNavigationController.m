//
//  CardScanNavigationController.m
//  Solidario
//
//  Created by Pan on 19/06/17.
//  Copyright © 2017 Paula Vasconcelos Gueiros. All rights reserved.
//

#import "CardScanNavigationController.h"
#import "CompleteCardInfoViewController.h"
#import "AntifraudDataTableViewController.h"
#import "UnwindSegueToCardScan.h"
#import "UnwindSegueDissolve.h"

@implementation CardScanNavigationController

#pragma mark - Initialization

- (void)viewDidLoad {
    [super viewDidLoad];
}


#pragma mark - Navigation

- (UIStoryboardSegue *)segueForUnwindingToViewController:(UIViewController *)toViewController fromViewController:(UIViewController *)fromViewController identifier:(NSString *)identifier {
    
    if ([fromViewController isKindOfClass:[CompleteCardInfoViewController class]]) {
        return [[UnwindSegueToCardScan alloc] initWithIdentifier:identifier source:fromViewController destination:toViewController];
    } else if ([fromViewController isKindOfClass:[AntifraudDataTableViewController class]]) {
        return [[UnwindSegueDissolve alloc] initWithIdentifier:identifier source:fromViewController destination:toViewController];
    }
    
    return [super segueForUnwindingToViewController:toViewController fromViewController:fromViewController identifier:identifier];
}

@end

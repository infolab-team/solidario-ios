//
//  AboutTableViewController.m
//  Solidario
//
//  Created by Paula Vasconcelos Gueiros on 26/8/16.
//  Copyright © 2016 Paula Vasconcelos Gueiros. All rights reserved.
//

typedef enum {
    AboutCellVideoRowNum = 0,
    AboutCellPictureRowNum,
    AboutCellWebViewRowNum
} AboutCellRowNum;

#import "AboutTableViewController.h"
#import "YTPlayerView.h"
#import "UILabel+Utils.h"
#import "NSArray+Utils.h"
#import "About.h"
#import "Service.h"

@interface AboutTableViewController ()

@property (strong, nonatomic) IBOutlet UIView *activityIndicatorSuperview;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UILabel *yearVersionLabel;

@property (strong, nonatomic) UIWebView *contentWebView;
@property (strong, nonatomic) YTPlayerView *playerView;

@property (strong, nonatomic) NSMutableArray *rowsInSection;
@property (strong, nonatomic) NSMutableArray *rowHeights;

@property (strong, nonatomic) About *about;

@end


@implementation AboutTableViewController

static const CGFloat aboutTitleCellHeightOffset = 150.f;
static const CGFloat mainImageProportion = 218 / 320.f;
static const CGFloat playerProportion = 9 / 16.f;
//static const CGFloat metadataCellHeight = 120.f;
static const CGFloat defaultViewPadding = 20.f;
static const NSInteger contentCellNumber = 2;


#pragma mark - Initialization

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setup];
}

- (void)setup {
    
    // setup view sizes
    CGFloat screenWidth = [[UIScreen mainScreen] bounds].size.width;

    // setup arrays for number of rows and row heights
    // set one section with 3 rows
    self.rowsInSection = [NSMutableArray arrayWithObjects:[NSNumber numberWithInt:3], nil];
    
    // set default height of 44 for each row
    self.rowHeights = [NSMutableArray array];
    for (int i = 0; i < [self.rowsInSection count]; i++) {
        [self.rowHeights addObject:[NSMutableArray array]];
        NSInteger rows = [self.rowsInSection[i] integerValue];
        for (int j = 0; j < rows; j++) {
            [self.rowHeights[i] addObject:[NSNumber numberWithFloat:0]];
        }
    }
    
    // change for known heights
    self.rowHeights[0][AboutCellVideoRowNum] = [NSNumber numberWithFloat:screenWidth * playerProportion];
    self.rowHeights[0][AboutCellPictureRowNum] = [NSNumber numberWithFloat:(screenWidth * mainImageProportion) + aboutTitleCellHeightOffset];
    self.rowHeights[0][AboutCellWebViewRowNum] = [NSNumber numberWithFloat:44.f];
    
    // retrieve data from server
    [self retrieveData];
}

- (void)retrieveData {
    [self.activityIndicatorSuperview setHidden:NO];
    [self.activityIndicator startAnimating];

    // call About service and populate data objects
    [Service getAboutWithCompletion:^(id results, BOOL hasError, NSString *errorMessage) {
        [self.activityIndicatorSuperview setHidden:YES];
        [self.activityIndicator stopAnimating];
        if (hasError) {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Erro"
                                                                           message:nil
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"OK"
                                                               style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction * action) {}];
            [alert addAction:okButton];
            
//            if (errorMessage != nil) {
//                [alert setMessage:errorMessage];
//            } else {
                [alert setMessage:@"Não foi possível obter o documento Sobre o Solidário.  Tente novamente mais tarde."];
//            }
            [self.navigationController presentViewController:alert animated:YES completion:nil];
            return;
        } else {
            NSLog(@"📗 About retrieval successful");
            self.about = (About *)results;
            [self.tableView reloadData];
        }
    }];

    //remove comment when there is metadata to show
//#warning get actual stats here
//    NSInteger numSuccessProjects = 95432, numDonors = 980432, numProjects = 542;
//    NSNumberFormatter * formatter = [NSNumberFormatter new];
//    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
//    NSLocale *currentLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"pt_BR"];
//    [formatter setLocale:currentLocale];
//    self.numSuccessProjectsLabel.text = [formatter stringFromNumber:[NSNumber numberWithInteger:numSuccessProjects]];
//    self.numDonorsLabel.text = [formatter stringFromNumber:[NSNumber numberWithInteger:numDonors]];
//    self.numProjectsLabel.text = [formatter stringFromNumber:[NSNumber numberWithInteger:numProjects]];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // setup activityIndicator position
    UIWindow *frontWindow = [[UIApplication sharedApplication] keyWindow];
    [frontWindow addSubview:self.activityIndicatorSuperview];
    self.activityIndicatorSuperview.center = frontWindow.center;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self.activityIndicator stopAnimating];
    [self.activityIndicatorSuperview removeFromSuperview];
}


#pragma mark - UITableView Delegate and Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self.rowsInSection count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.rowsInSection[section] integerValue];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [(self.rowHeights[indexPath.section][indexPath.row]) floatValue];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.row) {
        case AboutCellVideoRowNum: {
            UITableViewCell *videoCell = [tableView dequeueReusableCellWithIdentifier:@"aboutVideoCell" forIndexPath:indexPath];
            
            if (self.playerView == nil) {
                YTPlayerView *playerView = (YTPlayerView *)[videoCell viewWithTag:100];
                [playerView loadWithVideoId:@"DQRVg5yhGA8"];
                self.playerView = playerView;
            }
            
            return videoCell;
        }
        case AboutCellPictureRowNum: {
            UITableViewCell *pictureCell = [tableView dequeueReusableCellWithIdentifier:@"aboutPictureCell" forIndexPath:indexPath];
            
            return pictureCell;
        }
        case AboutCellWebViewRowNum: {
            UITableViewCell *webViewCell = [tableView dequeueReusableCellWithIdentifier:@"aboutWebViewCell" forIndexPath:indexPath];
            
            if (self.about != nil) {
                if (self.contentWebView == nil) {
                    // restart activity indicator
                    [self.activityIndicatorSuperview setHidden:NO];
                    [self.activityIndicator startAnimating];
                    
                    // setup webview content
                    UIWebView *cellWebView = (UIWebView *)[webViewCell viewWithTag:101];
                    cellWebView.delegate = self;
                    cellWebView.scrollView.scrollEnabled = NO;
                    
                    NSArray *aboutStringArray = [NSArray loadPlistWithName:@"About"];
                    NSString *aboutString = [NSString stringWithFormat:@"%@%@%@",
                                             [aboutStringArray objectAtIndex:0],
                                             self.about.content,
                                             [aboutStringArray objectAtIndex:1]];
                    [cellWebView loadHTMLString:aboutString baseURL:[[NSBundle mainBundle] bundleURL]];
                    
                    self.contentWebView = cellWebView;
                }
            }
            
            return webViewCell;
        }
        default: {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"aboutStatisticsCell" forIndexPath:indexPath];
            return cell;
        }
    }
}


#pragma mark - UIWebView Delegate

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [self.activityIndicatorSuperview setHidden:YES];
    [self.activityIndicator stopAnimating];
}
    
- (BOOL)webView:(UIWebView*)webView shouldStartLoadWithRequest:(NSURLRequest*)request navigationType:(UIWebViewNavigationType)navigationType {
    NSURL *url = [request URL];
    
    if (navigationType == UIWebViewNavigationTypeLinkClicked) {
        UIApplication *application = [UIApplication sharedApplication];
        if ([application respondsToSelector:@selector(openURL:options:completionHandler:)]) {
            [application openURL:[request URL] options:@{} completionHandler:nil];
            return NO;
        } else if ([application respondsToSelector:@selector(openURL:)]) {
            [application openURL:[request URL]];
            return NO;
        }
    } else if (navigationType == UIWebViewNavigationTypeOther) {
        if ([[url scheme] isEqualToString:@"ready"]) {
            float contentHeight = [[url host] floatValue];
            NSLog(@"📕 About webView height after rendering: %.0f", contentHeight);
            self.rowHeights[0][contentCellNumber] = [NSNumber numberWithFloat:contentHeight + 2*defaultViewPadding];
            NSLog(@"🔄 Reloading tableView...");
            [self.tableView reloadData];
            return NO;
        }
    }
    return YES;
}

@end

//
//  UpdateEntryTitleTableViewCell.h
//  Solidario
//
//  Created by Pan on 16/02/17.
//  Copyright © 2017 Paula Vasconcelos Gueiros. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomTextField.h"

@interface UpdateEntryTitleTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet CustomTextField *titleTextField;

@end

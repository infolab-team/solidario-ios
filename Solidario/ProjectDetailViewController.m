//
//  ProjectDetailViewController.m
//  Solidario
//
//  Created by Pan on 05/12/16.
//  Copyright © 2016 Paula Vasconcelos Gueiros. All rights reserved.
//

typedef enum {ProjectDetailFirstCellNum = 0, ProjectDetailVideoCellNum, ProjectDetailWebCellNum, ProjectDetailOwnerCellNum} ProjectDetailCellFirstSectionNum;

#import "ProjectDetailViewController.h"
#import "ProjectDetailFirstCell.h"
#import "ProjectDetailVideoCell.h"
#import "ProjectDetailWebCell.h"
#import "ProjectDetailOwnerCell.h"
#import "BannerCollectionViewCell.h"
#import "ProjectUpdateCell.h"
#import "DonationValueTableViewController.h"
#import "DonorListTableViewController.h"

#import "Utils.h"
#import "UIColor+Utils.h"
#import "UILabel+Utils.h"
#import "NSString+Utils.h"
#import "NSArray+Utils.h"

#import "Singleton.h"
#import "Service.h"

#import <SDWebImage/UIImageView+WebCache.h>

@interface ProjectDetailViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *footerView;
@property (weak, nonatomic) IBOutlet UIView *loadMoreEntriesSuperview;
@property (weak, nonatomic) IBOutlet UIView *donateButtonSuperview;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (strong, nonatomic) UIWebView *contentWebView;
@property (strong, nonatomic) UIImage *visibleImage;
@property (strong, nonatomic) UIButton *favoriteButton;

@property (strong, nonatomic) NSMutableArray *rowsInSection;
@property (strong, nonatomic) NSMutableArray *rowHeights;
@property (strong, nonatomic) NSMutableArray *timelineEntries;
@property (strong, nonatomic) NSMutableArray *isButtonTextMoreForEntry;

@property (nonatomic) BOOL isThereLoggedUser;
@property (nonatomic) BOOL checkedForPlayerView;
@property (nonatomic) BOOL webViewIsDoneRendering;
@property (nonatomic) BOOL onlyFavoriteButtonNeedsUpdate;

@property (nonatomic) NSInteger timelinePageNumber;
@property (nonatomic) NSInteger activeServices;
@property (nonatomic) CGFloat footerViewHeight;

@end


@implementation ProjectDetailViewController

static const CGFloat footerViewProportion = 7.f / 16.f;
static const CGFloat bannerViewProportion = 164.f / 280.f;
static const CGFloat playerProportion = 9 / 16.f;

static const CGFloat defaultViewPadding = 20.f;

static const CGFloat loadMoreEntriesViewHeight = 48.f;
static const CGFloat projectUpdateEntryHeaderCellHeight = 48.f;

static const NSInteger numberOfEntriesPerPage = 3;

#pragma mark - Initialization

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setup];
}

- (void)setup {
    // setup table view delegate class
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.estimatedRowHeight = 100.f;
    
    // setup footer view in view and tableview
    CGFloat screenWidth;
    screenWidth = [[UIScreen mainScreen]bounds].size.width;
    if (self.isFinalized) {
        self.footerViewHeight = loadMoreEntriesViewHeight;
        [self.donateButtonSuperview setHidden:YES];
    } else {
        self.footerViewHeight = screenWidth * footerViewProportion + loadMoreEntriesViewHeight;
    }
    [self.footerView setFrame:CGRectMake(self.footerView.frame.origin.x, self.footerView.frame.origin.y, self.footerView.frame.size.width, self.footerViewHeight)];
    
    // setup collected label sizes
    if (IS_IPHONE_6_7_PLUS) { // iPhone 5.5'
        _totalValueLabelPointSize = 11.f;
    } else if (IS_IPHONE_6_7) { // iPhone 4.7'
        _totalValueLabelPointSize = 10.f;
    } else { // iPhone 4'
        _totalValueLabelPointSize = 8.f;
    }
    
    // check for current logged user
    self.isThereLoggedUser = ([Singleton loggedUser] != nil);
    
    // setup activityIndicator
    [self.view addSubview:self.activityIndicator];
    self.activityIndicator.center = self.view.center;
    
    // setup arrays for number of rows and row heights
    self.rowsInSection = [NSMutableArray arrayWithObjects:[NSNumber numberWithInt:4], [NSNumber numberWithInteger:0], nil];
    
    // row heights for first section: always 4 cells (with dynamic heights)
    self.rowHeights = [NSMutableArray arrayWithCapacity:[self.rowsInSection count]];
    self.rowHeights[0] = [NSMutableArray arrayWithObjects:
                          [NSNumber numberWithFloat:44.f],
                          [NSNumber numberWithFloat:44.f],
                          [NSNumber numberWithFloat:44.f],
                          [NSNumber numberWithFloat:projectDetailOwnerCellHeight], nil];
    
    // variable used to decide if any service calls are still waiting for response
    self.activeServices = 0;
    
    // hide load more button until at least one activity is found
    [self.loadMoreEntriesSuperview setHidden:YES];
    
    self.timelineEntries = [NSMutableArray array];
    self.isButtonTextMoreForEntry = [NSMutableArray array];
    self.timelinePageNumber = 1; // initialize timeline page number variable
    [self retrieveTimelineEntries]; // fetch first three timeline entries
    
    // call service to fetch complete data for project
    NSLog(@"#️⃣ Project ID: %@", self.projectShort.internalBaseClassIdentifier);
    
    if (self.project == nil) { // retrieve project object if it is nil (not nil if coming from Activity List)
        [self retrieveProject];
    }
}

- (void)retrieveProject {
    [self.activityIndicator startAnimating];
    self.activeServices++;
    
    [Service getProjectWithIdentifier:self.projectShort.internalBaseClassIdentifier andCompletion:^(id results, BOOL hasError, NSString *errorMessage) {
        
        self.activeServices--;
        if (!self.activeServices) {
            [self.activityIndicator stopAnimating];
        }
        
        if (hasError) {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Erro"
                                                                           message:nil
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"OK"
                                                               style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction * action) {}];
            [alert addAction:okButton];
//            if (errorMessage != nil) {
//                [alert setMessage:errorMessage];
//            } else {
                [alert setMessage:@"Não foi possível recuperar detalhes do projeto."];
//            }
            [self presentViewController:alert animated:YES completion:nil];
            return;
        } else {
            self.project = (Project *)results;
            if (self.onlyFavoriteButtonNeedsUpdate) {
                self.onlyFavoriteButtonNeedsUpdate = NO;
                
#warning correct below when isFavorite coming from service in /projects/id
//                self.projectShort.isFavorite = self.project.isFavorite;
//                [self.favoriteButton setSelected:self.projectShort.isFavorite];
                [self.favoriteButton setHidden:NO];
            } else {
                [self.tableView reloadData];
            }
        }
    }];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if ([Singleton isDonating]) {
        [Singleton setDonating:NO];
        if ([Singleton loggedUser] != nil) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"userHasLoggedInMenu" object:nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"userHasLoggedInRoot" object:nil];
            [self performSegueWithIdentifier:@"modalToDonationValue" sender:self];
            
            // while user is on donating screen, update project for logged user
            [self userHasLoggedIn];
        }
    }
}


#pragma mark - Service Requests

- (void)retrieveTimelineEntries {
    [self.activityIndicator startAnimating];
    self.activeServices++;
    
    // call service to fetch timeline entries for project
    [Service getTimelineEntriesForProjectWithIdentifier:self.projectShort.internalBaseClassIdentifier onPage:self.timelinePageNumber withItemsPerPage:numberOfEntriesPerPage andCompletion:^(id results, BOOL hasError, NSString *errorMessage) {
        
        self.activeServices--;
        if (!self.activeServices) {
            [self.activityIndicator stopAnimating];
        }
        
        if (hasError) {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Erro"
                                                                           message:nil
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"OK"
                                                               style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction * action) {}];
            [alert addAction:okButton];
//            if (errorMessage != nil) {
//                [alert setMessage:errorMessage];
//            } else {
                [alert setMessage:@"Não foi possível recuperar publicações do projeto."];
//            }
            [self presentViewController:alert animated:YES completion:nil];
            return;
        } else {
            NSInteger numberOfResults = [(NSMutableArray *)results count];
            NSLog(@"⌛️ %lu timeline entry(ies) found on page #️⃣%ld", (unsigned long)numberOfResults, (long)self.timelinePageNumber);
            if (numberOfResults > 0) {
                // increment page number variable if call returned results
                self.timelinePageNumber++;
                
                NSInteger numberOfCurrentEntries = [self.rowsInSection[1] integerValue];
                self.rowsInSection[1] = [NSNumber numberWithInteger:(numberOfCurrentEntries + numberOfResults)];
                for (int i = 0; i < numberOfResults; i++) {
                    // initialize value for more/less button text of new entries
                    [self.isButtonTextMoreForEntry addObject:[NSNumber numberWithBool:YES]];
                }
                
                if (numberOfCurrentEntries > 0) {
                    [self.timelineEntries addObjectsFromArray:results];
                    numberOfCurrentEntries = [self.timelineEntries count];
                    
                    NSMutableArray *newIndexPaths = [NSMutableArray arrayWithCapacity:numberOfResults];
                    for (int i = 1; i <= numberOfResults; i++) {
                        NSIndexPath *newRowIndexPath = [NSIndexPath indexPathForRow:numberOfCurrentEntries-i inSection:1];
                        [newIndexPaths addObject:newRowIndexPath];
                    }
                    [self.tableView beginUpdates];
                    [self.tableView insertRowsAtIndexPaths:newIndexPaths withRowAnimation:UITableViewRowAnimationAutomatic];
                    [self.tableView endUpdates];
                } else {
                    self.timelineEntries = (NSMutableArray *)results;
                    
                    [self.tableView beginUpdates];
                    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationNone];
                    [self.tableView endUpdates];
                }
                
                // unhide load more button (if hidden)
                [self.loadMoreEntriesSuperview setHidden:NO];
            } else {
                [self.tableView beginUpdates];
                [self.loadMoreEntriesSuperview setHidden:YES];
                [self.footerView setFrame:CGRectMake(self.footerView.frame.origin.x, self.footerView.frame.origin.y, self.footerView.frame.size.width, self.footerViewHeight - loadMoreEntriesViewHeight)];
                [self.tableView endUpdates];
            }
        }
    }];
}


#pragma mark - UITableView Delegate and Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self.rowsInSection count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.rowsInSection[section] integerValue];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
            
        case 0: {
            switch (indexPath.row) {
                case ProjectDetailFirstCellNum: {
                    ProjectDetailFirstCell *firstCell = [tableView dequeueReusableCellWithIdentifier:@"projectDetailFirstCell" forIndexPath:indexPath];
                    
                    // setup cell
                    
                    // banner collection view data
                    [firstCell.bannerPageControl setNumberOfPages:[self.projectShort.media count]];
                    firstCell.bannerCollectionView.dataSource = self;
                    firstCell.bannerCollectionView.delegate = self;
                    
                    firstCell.selectedBannerPage = self.selectedBannerPage;
                    [firstCell.bannerCollectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:self.selectedBannerPage inSection:0] atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:NO];
                    [firstCell.bannerPageControl setCurrentPage:self.selectedBannerPage];
                    
                    if (firstCell.gradient) {
                        [firstCell.gradient removeFromSuperlayer];
                    }
                    CAGradientLayer *gradient = [CAGradientLayer layer];
                    CGFloat viewHeight, viewWidth;
                    viewWidth = [[UIScreen mainScreen]bounds].size.width - (2 * defaultViewPadding);
                    viewHeight = viewWidth * bannerViewProportion;
                    gradient.frame = CGRectMake(0, 0, viewWidth, viewHeight);
                    gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor clearColor] CGColor],(id)[[UIColor clearColor] CGColor], (id)[[UIColor defaultColor:DefaultColorBannerShadow] CGColor], nil];
                    [firstCell.gradientSuperview.layer insertSublayer:gradient atIndex:0];
                    firstCell.gradient = gradient;
                    
                    self.favoriteButton = firstCell.favoriteButton;
                    if (self.shouldAlwaysHideFavoriteButton) {
                        [firstCell.favoriteButton setHidden:YES];
                    } else {
                        [firstCell.favoriteButton setHidden:!self.isThereLoggedUser];
                        if (self.isThereLoggedUser) {
                            // favorite button data
                            [firstCell.favoriteButton setSelected:self.projectShort.isFavorite];
                        }
                    }
                    
                    // goal data
                    firstCell.goalLabel.text = self.projectShort.goal;
                    
                    // progress label data
                    firstCell.projectProgressLabel.endDegree = 3.6 * self.projectShort.percent;
                    firstCell.projectProgressLabel.trackWidth = [Singleton pLabelTrackWidth];
                    firstCell.projectProgressLabel.progressWidth = [Singleton pLabelProgressWidth];
                    
                    NSMutableAttributedString *percentageAttString = [[NSMutableAttributedString alloc] initWithString: [NSString stringWithFormat:@"%.0f%%", self.projectShort.percent]];
                    [percentageAttString addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Helvetica Bold" size:[Singleton pLabelPercentageFontSize]] range:NSMakeRange(0, percentageAttString.length-1)];
                    [percentageAttString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:[Singleton pLabelPercentSymbolFontSize]] range:NSMakeRange(percentageAttString.length-1, 1)];
                    firstCell.projectProgressLabel.attributedText = percentageAttString;
                    
                    // title data
                    firstCell.projectTitleLabel.text = self.projectShort.title;
                    
                    // short description data
                    firstCell.projectDescriptionLabel.text = self.projectShort.internalBaseClassDescription;
                    firstCell.projectDescriptionLabel.numberOfLines = 0;
                    [firstCell.projectDescriptionLabel sizeToFit];
                    [firstCell.mainView layoutIfNeeded];
                    
                    // number of donors data
                    NSDictionary *underlineAttribute = @{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)};
                    firstCell.projectNumberOfDonorsLabel.attributedText =
                    [[NSAttributedString alloc] initWithString:[[NSNumber numberWithDouble:self.projectShort.donators] stringValue]
                                                    attributes:underlineAttribute];
                    firstCell.projectPeopleDonatedLabel.text = (self.projectShort.donators == 1) ? @"PESSOA JÁ DOOU" : @"PESSOAS JÁ DOARAM";
                    
                    if (self.project != nil) {
                        // donated value data
                        long donatedValue = self.project.donated / 100;
                        NSNumberFormatter * formatter = [NSNumberFormatter new];
                        [formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
                        [formatter setMaximumFractionDigits:0];
                        firstCell.projectDonatedValueLabel.text = [formatter stringFromNumber:[NSNumber numberWithLong:donatedValue]];
                        
                        // unhide view
                        [firstCell.projectDonatedValueLabel setHidden:NO];
                    }
                    
                    // total value data
                    NSString *goal = self.projectShort.goal;
                    NSArray *goalArray = [goal componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@","]];
                    firstCell.projectTotalValueLabel.text = [NSString stringWithFormat:@"DE %@", [goalArray firstObject]];
                    [firstCell.projectTotalValueLabel setFont:[UIFont fontWithName:firstCell.projectTotalValueLabel.font.fontName size:self.totalValueLabelPointSize]];
                    
                    // button targets
                    [firstCell.favoriteButton addTarget:self action:@selector(wasSelectedFavoriteButton:withEvent:) forControlEvents:UIControlEventTouchUpInside];
                    [firstCell.numberOfDonorsButton addTarget:self action:@selector(viewDonorList:withEvent:) forControlEvents:UIControlEventTouchUpInside];
                    
                    // setup label positioning
                    if (firstCell.rightView.frame.size.height == firstCell.metadataView.frame.size.height) {
                        [firstCell.leftView removeConstraint:firstCell.leftViewBottomConstraint];
                    }
                    
                    // update cell row height
                    self.rowHeights[indexPath.section][indexPath.row] = [NSNumber numberWithFloat:firstCell.mainView.frame.size.height];
                    
                    return firstCell;
                }
                case ProjectDetailVideoCellNum: {
                    ProjectDetailVideoCell *videoCell = [tableView dequeueReusableCellWithIdentifier:@"projectDetailVideoCell" forIndexPath:indexPath];
                    if (self.project != nil) {
                        if (!self.checkedForPlayerView) {
                            if ([self.project.video isKindOfClass:[NSString class]] && !([self.project.video isEqualToString:@""])) {
                                // load video using id
                                NSArray *urlParameters = [self.project.video componentsSeparatedByString:@"/"];
                                [videoCell.playerView loadWithVideoId:[urlParameters lastObject]];
                                
                                // update cell row height
                                CGFloat playerHeight = ([[UIScreen mainScreen]bounds].size.width - (2 * defaultViewPadding)) * playerProportion;
                                CGFloat playerCellHeight = playerHeight + defaultViewPadding;
                                self.rowHeights[indexPath.section][indexPath.row] = [NSNumber numberWithFloat:playerCellHeight];\
                                
                                self.checkedForPlayerView = YES;
                            } else {
                                [videoCell.playerView removeFromSuperview];
                                
                                // update cell row height
                                self.rowHeights[indexPath.section][indexPath.row] = [NSNumber numberWithFloat:0.f];
                            }
                        }
                    }
                    return videoCell;
                }
                case ProjectDetailWebCellNum: {
                    ProjectDetailWebCell *webCell = [tableView dequeueReusableCellWithIdentifier:@"projectDetailWebCell" forIndexPath:indexPath];
                    
                    if (self.project != nil) {
                        if (self.contentWebView == nil) {
                            // restart activity indicator
                            [self.activityIndicator startAnimating];
                            
                            // setup webview content
                            webCell.webView.delegate = self;
                            webCell.webView.scrollView.scrollEnabled = NO;
                            
                            NSArray *detailStringArray = [NSArray loadPlistWithName:@"ProjectDetail"];
                            NSString *detailString = [NSString stringWithFormat:@"%@%@%@",
                                                      [detailStringArray objectAtIndex:0],
                                                      self.project.information,
                                                      [detailStringArray objectAtIndex:1]];
                            [webCell.webView loadHTMLString:detailString baseURL:[[NSBundle mainBundle] bundleURL]];
                            
                            self.contentWebView = webCell.webView;
                        }
                    }
                    return webCell;
                }
                case ProjectDetailOwnerCellNum: {
                    ProjectDetailOwnerCell *ownerCell = [tableView dequeueReusableCellWithIdentifier:@"projectDetailOwnerCell" forIndexPath:indexPath];
                    
                    if (self.project != nil) {
                        
                        if (![self.project.creator.image isEqualToString:@""]) {
                            NSURL *url = [NSURL URLWithString:[NSString encodedStringFromString:self.project.creator.image]];
                            
                            // check if image src is base64 or url
                            if ([self.project.creator.image hasPrefix:@"data:image"]) {
                                NSData *imageData = [NSData dataWithContentsOfURL:url];
                                ownerCell.ownerImage.image = [UIImage imageWithData:imageData];
                            } else {
                                [ownerCell.ownerImage sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"defaultUser"]];
                            }
                        } else {
                            ownerCell.ownerImage.image = [UIImage imageNamed:@"defaultUser"];
                        }
                        
                        ownerCell.ownerNameLabel.text = [NSString stringWithFormat:@"%@ %@", self.project.creator.name, self.project.creator.surname];
                        
                        NSMutableString *subtitle = [NSMutableString string];
                        if ([self.project.contact.email length] > 0) {
                            [subtitle appendString:self.project.contact.email];
                        }
                        if ([self.project.contact.tel length] > 0) {
                            if ([subtitle length] > 0) {
                                [subtitle appendString:@" | "];
                            }
                            [subtitle appendString:self.project.contact.tel];
                        }
                        ownerCell.subtitleLabel.text = subtitle;
                        
                        if ([self.project.social.facebook length] > 0) {
                            // set facebook button target
                            [ownerCell.facebookButton addTarget:self action:@selector(openProjectFacebookPage:withEvent:) forControlEvents:UIControlEventTouchUpInside];
                        } else {
                            [ownerCell.facebookButton removeFromSuperview];
                        }
                        
                        if ([self.project.social.twitter length] > 0) {
                            // set twitter button target
                            [ownerCell.twitterButton addTarget:self action:@selector(openProjectTwitterPage:withEvent:) forControlEvents:UIControlEventTouchUpInside];
                        } else {
                            [ownerCell.twitterButton removeFromSuperview];
                        }
                        
                        [ownerCell.contentView setHidden:NO];
                    }
                    return ownerCell;
                }
                default: {
                    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
                    self.rowHeights[indexPath.section][indexPath.row] = [NSNumber numberWithFloat:44.f];
                    return cell;
                }
            }
        }
        case 1: {
            ProjectUpdateCell *cell = [tableView dequeueReusableCellWithIdentifier:@"projectUpdateCell" forIndexPath:indexPath];
            TimelineEntry *entry = [self.timelineEntries objectAtIndex:indexPath.row];
            
            NSString *day = (entry.day < 10) ? [NSString stringWithFormat:@"0%.0f", entry.day] :
            [NSString stringWithFormat:@"%.0f", entry.day];
            cell.entryDateDayLabel.text = day;
            cell.entryDateMonthLabel.text = [NSString monthShortNameFromInteger:(NSInteger)entry.month];
            
            // date setup for time
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
            [dateFormatter setDateFormat:@"yyyy'-'MM'-'dd'T'HH':'mm':'ss.SSS'Z'"];
            NSDate *entryDate = [dateFormatter dateFromString:entry.date];
            [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
            [dateFormatter setDateFormat:@"HH:mm"];
            cell.entryDateTimeLabel.text = [dateFormatter stringFromDate:entryDate];
            
            cell.entryTitleLabel.text = entry.title;
            cell.entryDescriptionLabel.text = [entry.internalBaseClassDescription stringByTrimmingCharactersInSet:
                                               [NSCharacterSet whitespaceAndNewlineCharacterSet]];
            // retrieve and set up image
            if (entry.img != nil && ![entry.img isEqualToString:@""]) {
                
                [cell.entryImageView sd_setImageWithURL:[NSURL URLWithString:entry.img] placeholderImage:[UIImage imageNamed:@"webImagePlaceholder"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                    
                    if (error) {
                        [cell.entryImageView removeFromSuperview];
                    } else {
                        CGFloat imageViewWidth = cell.entryTitleLabel.frame.size.width;
                        CGFloat imageViewHeight = (image.size.height / image.size.width) * imageViewWidth;
                        CGFloat constant = imageViewHeight - imageViewWidth;
                        cell.imageAspectRatioConstraint.constant = constant;
                    }
                }];
            } else {
                [cell.entryImageView removeFromSuperview];
            }
            
            // month label spacing
            NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:cell.entryDateMonthLabel.text];
            float spacing = 4.4f;
            [attributedString addAttribute:NSKernAttributeName
                                     value:@(spacing)
                                     range:NSMakeRange(0, [attributedString length]-1)];
            cell.entryDateMonthLabel.attributedText = attributedString;
            
            // timeline top line hidden if necessary
            if (indexPath.row == 0) {
                [cell.topLineView setHidden:YES];
            } else {
                [cell.topLineView setHidden:NO];
            }
            
            // see more/less button
            cell.isButtonTextMore = [self.isButtonTextMoreForEntry[indexPath.row] boolValue];
            [self setupLayoutOfUpdateCell:cell];
            
            // set button tag for identifying which button was selected
            [cell.viewMoreOrLessButton setTag:indexPath.row];
            
            return cell;
        }
        default: {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
            return cell;
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return [(self.rowHeights[indexPath.section][indexPath.row]) floatValue];
    }
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return [(self.rowHeights[indexPath.section][indexPath.row]) floatValue];
    }
    return UITableViewAutomaticDimension;
}

- (void)tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([cell isKindOfClass:[ProjectDetailFirstCell class]]) {
        // store last visible image in banner
        BannerCollectionViewCell *bannerCell = (BannerCollectionViewCell *)[((ProjectDetailFirstCell*)cell).bannerCollectionView cellForItemAtIndexPath:[NSIndexPath indexPathForItem:self.selectedBannerPage inSection:0]];
        self.visibleImage = bannerCell.cellImageView.image;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (section == 1) {
        if ([self.timelineEntries count] > 0) {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"projectUpdateHeaderCell"];
            return cell.contentView;
        }
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 1) {
        if ([self.timelineEntries count] > 0) {
            return projectUpdateEntryHeaderCellHeight;
        }
    }
    return 0;
}


#pragma mark - Button Actions

- (void)wasSelectedFavoriteButton:(UIButton *)button withEvent:(UIEvent *)event {
    // update UI first
    button.selected = !button.selected;
    
    // toggle favorite option in server
    [Service toggleFavoriteFromProjectWithIdentifier:self.projectShort.internalBaseClassIdentifier andCompletion:^(id results, BOOL hasError, NSString *errorMessage) {
        if (hasError) {
            // if request not successful, return button to original state
            button.selected = !button.selected;
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Erro"
                                                                           message:nil
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"OK"
                                                               style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction * action) {}];
            [alert addAction:okButton];
//            if (errorMessage != nil) {
//                [alert setMessage:errorMessage];
//            } else {
                [alert setMessage:@"Não foi possível alterar a opção de favoritar o projeto."];
//            }
            [self presentViewController:alert animated:YES completion:nil];
            return;
        } else {
            self.projectShort.isFavorite = button.selected;
            [[NSNotificationCenter defaultCenter] postNotificationName:@"favoriteOptionChanged" object:self.projectShort.internalBaseClassIdentifier];
        }
    }];
}

- (IBAction)loadMoreUpdateEntries:(id)sender {
    NSLog(@"↪️ Attempting to load more Update Entries");
    [self retrieveTimelineEntries];
}

- (IBAction)seeMoreOrLessOfUpdateEntry:(id)sender {
    ProjectUpdateCell *cell = (ProjectUpdateCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:((UIButton *)sender).tag inSection:1]];
    
    // show log message for action
    if (cell.isButtonTextMore) {
        NSLog(@"🔃 See More of Update Entry: %ld", (long)cell.viewMoreOrLessButton.tag);
    } else {
        NSLog(@"🔃 See Less of Update Entry: %ld", (long)cell.viewMoreOrLessButton.tag);
    }
    
    // update value for isButtonTextMoreForEntry array
    cell.isButtonTextMore = !cell.isButtonTextMore;
    self.isButtonTextMoreForEntry[cell.viewMoreOrLessButton.tag] = [NSNumber numberWithBool:cell.isButtonTextMore];
    [self setupLayoutOfUpdateCell:cell];

    [self.tableView beginUpdates];
    [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:(long)cell.viewMoreOrLessButton.tag inSection:1]] withRowAnimation:UITableViewRowAnimationAutomatic];
    [self.tableView endUpdates];
}

- (void)setupLayoutOfUpdateCell:(ProjectUpdateCell *)cell {
    NSMutableAttributedString *mutableAttributedString = [cell.viewMoreOrLessButton.currentAttributedTitle mutableCopy];
    if (cell.isButtonTextMore) {
        [mutableAttributedString.mutableString setString:@"Ver Mais"];
        [cell.entryArrowImage setImage:[UIImage imageNamed:@"updateArrowRight"]];
        [cell.entryDescriptionLabel setNumberOfLines:5];
        [cell.imageHiddenConstraint setPriority:900];
    } else {
        [mutableAttributedString.mutableString setString:@"Ver Menos"];
        [cell.entryArrowImage setImage:[UIImage imageNamed:@"updateArrowUp"]];
        [cell.entryDescriptionLabel setNumberOfLines:0];
        [cell.imageHiddenConstraint setPriority:500];
    }
    [cell.viewMoreOrLessButton setAttributedTitle:mutableAttributedString forState:UIControlStateNormal];
}

- (IBAction)startDonationProcess:(id)sender {
    if ([Singleton loggedUser] == nil) { // no current logged user
        [Singleton setDonating:YES];
        [self performSegueWithIdentifier:@"modalToMainAccount" sender:self];
    } else {
        [self performSegueWithIdentifier:@"modalToDonationValue" sender:self];
    }
}

- (void)viewDonorList:(UIButton *)button withEvent:(UIEvent *)event {
    [self performSegueWithIdentifier:@"modalToDonorList" sender:self];
}

- (void)openProjectFacebookPage:(UIButton *)button withEvent:(UIEvent *)event {
    NSString *facebookURL = [self.project.social.facebook stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:facebookURL]];
}

- (void)openProjectTwitterPage:(UIButton *)button withEvent:(UIEvent *)event {
    NSString *twitterURL = [self.project.social.twitter stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:twitterURL]];
}


#pragma mark - UICollectionView Delegate and Data Source

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return [self.projectShort.media count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    BannerCollectionViewCell *bannerCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"bannerCell" forIndexPath:indexPath];
    
    NSString *imageUrlString = [NSString encodedStringFromString:self.projectShort.media[indexPath.item]];
    [bannerCell.cellImageView sd_setImageWithURL:[NSURL URLWithString:imageUrlString] placeholderImage:[UIImage imageNamed:@"webImagePlaceholder"]];
    return bannerCell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    return collectionView.frame.size;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    
    if ([scrollView isKindOfClass:[UICollectionView class]]) {
        UICollectionView *collectionView = (UICollectionView *)scrollView;
        CGFloat pageWidth = collectionView.frame.size.width;
        float currentPage = collectionView.contentOffset.x / pageWidth;
        
        // get object for current Card View and update CollectionView PageControl
        ProjectDetailFirstCell *firstCell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:ProjectDetailFirstCellNum inSection:0]];
        [firstCell.bannerPageControl setCurrentPage:currentPage];
        self.selectedBannerPage = currentPage;
        
        // update current visible image for use in donation page
        BannerCollectionViewCell *bannerCell = (BannerCollectionViewCell *)[collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForItem:self.selectedBannerPage inSection:0]];
        self.visibleImage = bannerCell.cellImageView.image;
    }
}


#pragma mark - UIWebView Delegate

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    
    [self.activityIndicator stopAnimating];
}

- (BOOL)webView:(UIWebView*)webView shouldStartLoadWithRequest:(NSURLRequest*)request navigationType:(UIWebViewNavigationType)navigationType {
    NSURL *url = [request URL];
    
    if (navigationType == UIWebViewNavigationTypeLinkClicked) {
        UIApplication *application = [UIApplication sharedApplication];
        if ([application respondsToSelector:@selector(openURL:options:completionHandler:)]) {
            [application openURL:[request URL] options:@{} completionHandler:nil];
            return NO;
        } else if ([application respondsToSelector:@selector(openURL:)]) {
            [application openURL:[request URL]];
            return NO;
        }
    } else if (navigationType == UIWebViewNavigationTypeOther) {
        if ([[url scheme] isEqualToString:@"ready"]) {
            float contentHeight = [[url host] floatValue];
            NSLog(@"📏 Project content webView height after rendering: %.0f", contentHeight);
            self.rowHeights[0][ProjectDetailWebCellNum] = [NSNumber numberWithFloat:contentHeight];
            NSLog(@"🔄 Reloading tableView...");
            [self.tableView reloadData];
            return NO;
        }
    }
    return YES;
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    UINavigationController *navController = (UINavigationController *)[segue destinationViewController];
    
    if ([segue.identifier isEqualToString:@"modalToDonationValue"]) {
        
        DonationValueTableViewController *donationValueTableViewController = (DonationValueTableViewController *)[navController topViewController];
        donationValueTableViewController.project = self.projectShort;
        
        if (!self.visibleImage) {
            ProjectDetailFirstCell *firstCell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:ProjectDetailFirstCellNum inSection:0]];
            BannerCollectionViewCell *bannerCell = (BannerCollectionViewCell *)[firstCell.bannerCollectionView cellForItemAtIndexPath:[NSIndexPath indexPathForItem:self.selectedBannerPage inSection:0]];
            donationValueTableViewController.mainImage = bannerCell.cellImageView.image;
        } else {
            donationValueTableViewController.mainImage = self.visibleImage;
        }
    } else if ([segue.identifier isEqualToString:@"modalToDonorList"]) {
        
        DonorListTableViewController *donorListTableViewController = (DonorListTableViewController *)[navController topViewController];
        donorListTableViewController.projectShort = self.projectShort;
        donorListTableViewController.project = self.project;
    }
}

- (IBAction)unwindToBeforeDonationValue:(UIStoryboardSegue*)unwindSegue {
    
}

- (IBAction)unwindToBeforeDonorList:(UIStoryboardSegue*)unwindSegue {
    
}

- (void)userHasLoggedIn {
    self.isThereLoggedUser = YES;
    if (!self.shouldAlwaysHideFavoriteButton) {
        // retrieve project from service again
        self.onlyFavoriteButtonNeedsUpdate = YES; // set tag for updating only heart button, not complete project
        [self retrieveProject];
    }
}

@end

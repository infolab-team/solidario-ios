//
//  ProjectTabsTableViewCell.h
//  Solidario
//
//  Created by Paula Vasconcelos Gueiros on 14/9/16.
//  Copyright © 2016 Paula Vasconcelos Gueiros. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProjectTabsTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *createdImage;
@property (weak, nonatomic) IBOutlet UIImageView *donatedImage;
@property (weak, nonatomic) IBOutlet UIImageView *favoriteImage;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *selectedViewLeadingConstraint;

@end

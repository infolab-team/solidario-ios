//
//  CardScanViewController.m
//  Solidario
//
//  Created by Pan on 21/12/16.
//  Copyright © 2016 Paula Vasconcelos Gueiros. All rights reserved.
//

#import "CardScanViewController.h"
#import "CardIO.h"
#import "UIColor+Utils.h"
#import "CompleteCardInfoViewController.h"

@interface CardScanViewController ()

@property (strong, nonatomic) CardIOCreditCardInfo *creditCardInfo;

@end

@implementation CardScanViewController

#pragma mark - Initialization

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setup];
}

- (void)setup {
    // left bar button item
    if (self.userIsSavingCard) {
        UIBarButtonItem *stopButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemStop target:self action:@selector(close:)];
        self.navigationItem.leftBarButtonItem = stopButton;
    }
    
    // cardIO view
    self.cardView.delegate = self;
    self.cardView.hideCardIOLogo = YES;
    self.cardView.guideColor = [UIColor defaultColor:DefaultColorMain];
    self.cardView.languageOrLocale = @"pt_BR";
    self.cardView.scanInstructions = @"Posicione seu cartão.  Ele será escaneado automaticamente.";
}


#pragma mark - CardIOView Delegate

- (void)cardIOView:(CardIOView *)cardIOView didScanCard:(CardIOCreditCardInfo *)cardInfo {
    self.creditCardInfo = cardInfo;
    [self performSegueWithIdentifier:@"customToCompleteCardInfo" sender:self];
}


#pragma mark - Navigation

- (void)close:(UIBarButtonItem *)sender {
    [self performSegueWithIdentifier:@"unwindToBeforeCardScan" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"customToCompleteCardInfo"]) {
        CompleteCardInfoViewController *completeCardInfoViewController = (CompleteCardInfoViewController *)[segue destinationViewController];
        
        completeCardInfoViewController.creditCardInfo = self.creditCardInfo;
        completeCardInfoViewController.project = self.project;
        completeCardInfoViewController.mainImage = self.mainImage;
        completeCardInfoViewController.donation = self.donation;
        completeCardInfoViewController.userIsSavingCard = self.userIsSavingCard;
    }
}

- (IBAction)unwindToCardScan:(UIStoryboardSegue*)unwindSegue {
    
}

@end

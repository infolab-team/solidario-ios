//
//  DonatingAnimationViewController.m
//  Solidario
//
//  Created by Pan on 29/05/17.
//  Copyright © 2017 Paula Vasconcelos Gueiros. All rights reserved.
//

#import "DonatingAnimationViewController.h"
#import "DonationConfirmViewController.h"
#import "DonationFailViewController.h"

#import "UIImage+animatedGIF.h"

#import "Service.h"

#import "AntifraudData.h"


@interface DonatingAnimationViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *animatedImage;

@property (strong, nonatomic) NSString *billetURL;
@property (strong, nonatomic) NSString *feedbackMessage;

@end


@implementation DonatingAnimationViewController

#pragma mark - Initialization

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setup];
}

- (void)setup {
    // setup gif animation
    NSURL *url = [[NSBundle mainBundle] URLForResource:@"donating" withExtension:@"gif"];
    self.animatedImage.image = [UIImage animatedImageWithAnimatedGIFURL:url];
    
    // call purchase service
    [self callPurchaseService];
}


#pragma mark - Purchase Conclusion

- (void)callPurchaseService {
    if (self.donation.paymentType == PaymentTypeCreditCard) {
        [self callCreditCardService];
    } else if (self.donation.paymentType == PaymentTypeBankBillet) {
        [self callBilletService];
    }
}

- (void)callCreditCardService {
    
    // make antifraud data a dictionary
    [Service purchaseAmount:[NSNumber numberWithDouble:self.donation.value] anonymously:self.donation.isAnonymous toProjectWithIdentifier:self.project.internalBaseClassIdentifier usingCardHash:self.donation.cardHash antifraudData:[self.donation.antifraudData dictionaryRepresentation] andCompletion:^(id results, BOOL hasError, NSString *errorMessage) {
        
        if (hasError) {
            // linha removida enquanto textos de retorno estiverem sem tratamento
            // if (errorMessage != nil) self.feedbackMessage = errorMessage;
            [self performSegueWithIdentifier:@"modalToDonationFailure" sender:self];
        } else {
            NSLog(@"👐🏼 Donation successful!");
            [self performSegueWithIdentifier:@"modalToDonationSuccess" sender:self];
        }
    }];
}

- (void)callBilletService {
    
    [Service generateBilletForDonationOfAmount:[NSNumber numberWithDouble:self.donation.value] anonymously:self.donation.isAnonymous toProjectWithIdentifier:self.project.internalBaseClassIdentifier withCompletion:^(id results, BOOL hasError, NSString *errorMessage) {
        
        if (hasError) {
            // linha removida enquanto textos de retorno estiverem sem tratamento
            // self.feedbackMessage = (errorMessage != nil) ? errorMessage : @"Não foi possível gerar o boleto.";
            self.feedbackMessage = @"Não foi possível gerar o boleto.";
            [self performSegueWithIdentifier:@"modalToDonationFailure" sender:self];
        } else {
            self.billetURL = (NSString *)results;
            NSLog(@"🗒 Billet generation successful!");
            [self performSegueWithIdentifier:@"modalToDonationSuccess" sender:self];
        }
    }];
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    UINavigationController *navController = (UINavigationController *)[segue destinationViewController];
    
    if ([segue.identifier isEqualToString:@"modalToDonationSuccess"]) {
        DonationConfirmViewController *donationConfirmViewController = (DonationConfirmViewController *)[navController topViewController];
        donationConfirmViewController.project = self.project;
        donationConfirmViewController.donation = self.donation;
        if (self.donation.paymentType == PaymentTypeBankBillet) {
            donationConfirmViewController.billetURL = self.billetURL;
        }
    } else if ([segue.identifier isEqualToString:@"modalToDonationFailure"]) {
        DonationFailViewController *donationFailViewController = (DonationFailViewController *)[navController topViewController];
        donationFailViewController.project = self.project;
        donationFailViewController.donation = self.donation;
        donationFailViewController.feedbackMessage = self.feedbackMessage;
    }
}

@end

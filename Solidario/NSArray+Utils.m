//
//  NSArray+Utils.m
//  Solidario
//
//  Created by Allan Alves on 7/4/16.
//  Copyright © 2016 Infosolo. All rights reserved.
//

#import "NSArray+Utils.h"

@implementation NSArray(Utils)

+ (NSArray *)loadPlistWithName:(NSString *)name {
    NSURL *url = [[NSBundle mainBundle] URLForResource:name withExtension:@"plist"];
    return [NSArray arrayWithContentsOfURL:url];
}

@end

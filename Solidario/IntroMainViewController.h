//
//  IntroMainViewController.h
//  Solidário
//
//  Created by Allan Alves on 7/4/16.
//  Copyright © 2016 Infosolo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IntroMainViewController : UIViewController <UIPageViewControllerDelegate, UIPageViewControllerDataSource>

@property (nonatomic) BOOL fromCategorySelection;

@end

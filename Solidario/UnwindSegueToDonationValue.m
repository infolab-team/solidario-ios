//
//  UnwindSegueToDonationValue.m
//  Solidario
//
//  Created by Pan on 05/06/17.
//  Copyright © 2017 Paula Vasconcelos Gueiros. All rights reserved.
//

#import "UnwindSegueToDonationValue.h"

#import "DonationValueTableViewController.h"
#import "DonationDataTableViewController.h"
#import "AntifraudDataTableViewController.h"
#import "DonationFailViewController.h"
#import "DonationNavigationController.h"

#import "DonationGoalView.h"
#import "DonationValueView.h"

#import "Utils.h"
#import "UIColor+Utils.h"

@implementation UnwindSegueToDonationValue

- (void)perform {
    
    SourceTVC idSourceTVC = -1;
    if ([self.sourceViewController isKindOfClass:[DonationDataTableViewController class]]) {
        idSourceTVC = DonationDataTVC;
    } else if ([self.sourceViewController isKindOfClass:[AntifraudDataTableViewController class]]) {
        idSourceTVC = AntifraudDataTVC;
    } else if ([self.sourceViewController isKindOfClass:[DonationFailViewController class]]) {
        idSourceTVC = DonationFailVC;
    }
    
    DonationValueTableViewController *destinationTVC = (DonationValueTableViewController *)self.destinationViewController;

    CGFloat screenWidth = UIScreen.mainScreen.bounds.size.width;
    CGFloat screenHeight = UIScreen.mainScreen.bounds.size.height;
    CGFloat yOffset = self.sourceViewController.navigationController.navigationBar.frame.size.height + 20.f;
    
    // add destination tableview to current view, initially transparent
    UITableView *destinationTableView = destinationTVC.tableView;
    [destinationTableView setFrame:CGRectMake(0, yOffset, screenWidth, screenHeight)];
    [destinationTableView setAlpha:0];

    // add donation superview on top of the current hidden banner
    DonationValueView *donationSuperview = [[DonationValueView alloc] initWithFrame:destinationTVC.donationGoalView.frame];
    [donationSuperview setFrame:CGRectOffset(donationSuperview.frame, 0, yOffset + donationSuperview.frame.size.height)];
    
    // setup donation superview data
    NSNumberFormatter *currencyFormatter = [NSNumberFormatter new];
    [currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    NSLocale *currentLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"pt_BR"];
    [currencyFormatter setLocale:currentLocale];
    
    // prepare for processing inside switch
    UIWindow *window = UIApplication.sharedApplication.keyWindow;
    DonationGoalView *goalSuperview = [[DonationGoalView alloc] initWithFrame:destinationTVC.donationGoalView.frame];

    // stores height of tableview slide
    CGFloat heightToSlide;
    
    switch (idSourceTVC) {
        case DonationDataTVC: {
            DonationDataTableViewController *sourceTVC = (DonationDataTableViewController *)self.sourceViewController;
            donationSuperview.donationValueLabel.text = [currencyFormatter stringFromNumber:[NSNumber numberWithFloat:sourceTVC.donation.value/100.f]];
            
            // add views to window
            [window insertSubview:destinationTableView aboveSubview:sourceTVC.view];
            [window insertSubview:donationSuperview aboveSubview:sourceTVC.view];
            [window insertSubview:goalSuperview aboveSubview:sourceTVC.view];
            
            // calculate height of tableview slide
            heightToSlide = -destinationTVC.donationGoalView.frame.size.height;
            
            break;
        }
        case AntifraudDataTVC: {
            AntifraudDataTableViewController *sourceTVC = (AntifraudDataTableViewController *)self.sourceViewController;
            donationSuperview.donationValueLabel.text = [currencyFormatter stringFromNumber:[NSNumber numberWithFloat:sourceTVC.donation.value/100.f]];
            
            // add views to window
            [window insertSubview:destinationTableView aboveSubview:sourceTVC.view];
            [window insertSubview:donationSuperview aboveSubview:sourceTVC.view];
            [window insertSubview:goalSuperview aboveSubview:sourceTVC.view];
            
            // calculate height of tableview slide
            heightToSlide = -destinationTVC.donationGoalView.frame.size.height;
            
            break;
        }
        case DonationFailVC: {
            DonationFailViewController *sourceVC = (DonationFailViewController *)self.sourceViewController;
            donationSuperview.donationValueLabel.text = [currencyFormatter stringFromNumber:[NSNumber numberWithFloat:sourceVC.donation.value/100.f]];
            
            // add views to window
            [window insertSubview:destinationTableView aboveSubview:sourceVC.view];
            [window insertSubview:donationSuperview aboveSubview:sourceVC.view];
            [window insertSubview:goalSuperview aboveSubview:sourceVC.view];
            
            // calculate height of tableview slide
            heightToSlide = -destinationTVC.donationGoalView.frame.size.height;
            
            break;
        }
        default:
            break;
    }
    
    // complement calculation of height of tableview slide
    UIEdgeInsets tableViewInset = destinationTableView.contentInset;
    tableViewInset.top = 0.f;
    
    // add goal superview on top of donation superview
    [goalSuperview setFrame:CGRectOffset(goalSuperview.frame, 0, yOffset)];
    
    // setup goal superview data
    goalSuperview.goalValueLabel.text = destinationTVC.project.goal;
    goalSuperview.mainImageView.image = destinationTVC.mainImage;
    
    CGFloat viewHeight, viewWidth;
    viewWidth = [[UIScreen mainScreen]bounds].size.width - (2 * defaultViewPadding);
    viewHeight = viewWidth * bannerViewProportion;
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = CGRectMake(0, 0, viewWidth, viewHeight);
    gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor defaultColor:DefaultColorBannerShadowTop] CGColor], (id)[[UIColor defaultColor:DefaultColorBannerShadowTop] CGColor], (id)[[UIColor blackColor] CGColor], nil];
    [goalSuperview.mainImageView.layer insertSublayer:gradient atIndex:0];
    
    // animate views
    [UIView animateWithDuration: 0.3f animations:^{
        [destinationTableView setAlpha:1];
        destinationTableView.contentInset = tableViewInset;
        [donationSuperview setFrame:CGRectOffset(donationSuperview.frame, 0, heightToSlide)];
        
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.3f animations:^{
            // unhide banner
            [destinationTVC.donationGoalView setHidden:NO];
            [donationSuperview removeFromSuperview];
            [goalSuperview removeFromSuperview];
            
            [self dismissStacksRecursivelyFromController:self.sourceViewController.navigationController];
        } completion:nil];
    }];
}

- (void)dismissStacksRecursivelyFromController:(UIViewController *)viewController {

    UINavigationController *navigationController;
    if (![viewController isKindOfClass:[UINavigationController class]]) {
        navigationController = viewController.navigationController;
    } else {
        navigationController = (UINavigationController *)viewController;
    }
    
    if ([navigationController isKindOfClass:[DonationNavigationController class]]) {
        [navigationController popToRootViewControllerAnimated:NO];
    } else {
        UIViewController *presentingViewController = [navigationController presentingViewController];
        [navigationController dismissViewControllerAnimated:NO completion:^{
            [self dismissStacksRecursivelyFromController:presentingViewController];
        }];
    }
}

@end

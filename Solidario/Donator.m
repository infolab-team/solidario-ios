//
//  Donator.m
//
//  Created by Paula Vasconcelos on 11/05/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "Donator.h"

NSString *const kDonatorId = @"id";
NSString *const kDonatorName = @"name";
NSString *const kDonatorImage = @"image";


@interface Donator ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end


@implementation Donator

@synthesize donatorIdentifier = _donatorIdentifier;
@synthesize name = _name;
@synthesize image = _image;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.donatorIdentifier = [self objectOrNilForKey:kDonatorId fromDictionary:dict];
            self.name = [self objectOrNilForKey:kDonatorName fromDictionary:dict];
            self.image = [self objectOrNilForKey:kDonatorImage fromDictionary:dict];

    }
    return self;
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.donatorIdentifier forKey:kDonatorId];
    [mutableDict setValue:self.name forKey:kDonatorName];
    [mutableDict setValue:self.image forKey:kDonatorImage];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}


#pragma mark - Helper Method

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.donatorIdentifier = [aDecoder decodeObjectForKey:kDonatorId];
    self.name = [aDecoder decodeObjectForKey:kDonatorName];
    self.image = [aDecoder decodeObjectForKey:kDonatorImage];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {

    [aCoder encodeObject:_donatorIdentifier forKey:kDonatorId];
    [aCoder encodeObject:_name forKey:kDonatorName];
    [aCoder encodeObject:_image forKey:kDonatorImage];
}

- (id)copyWithZone:(NSZone *)zone {
    Donator *copy = [[Donator alloc] init];
    
    if (copy) {
        copy.donatorIdentifier = [self.donatorIdentifier copyWithZone:zone];
        copy.name = [self.name copyWithZone:zone];
        copy.image = [self.image copyWithZone:zone];
    }
    return copy;
}

@end

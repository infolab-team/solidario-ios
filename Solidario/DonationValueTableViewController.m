//
//  DonationValueTableViewController.m
//  Solidario
//
//  Created by Pan on 25/11/16.
//  Copyright © 2016 Paula Vasconcelos Gueiros. All rights reserved.
//

#import "DonationValueTableViewController.h"
#import "DonationDataTableViewController.h"
#import "AntifraudDataTableViewController.h"
#import "CardListTableViewController.h"
#import "DonationGoalView.h"

#import "CustomTextField.h"
#import "Utils.h"
#import "UIColor+Utils.h"
#import "NSString+Utils.h"

#import "Donation.h"

@interface DonationValueTableViewController ()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet CustomTextField *donationValue;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIView *savedCardSwitchSuperview;
@property (weak, nonatomic) IBOutlet UISwitch *savedCardSwitch;
@property (weak, nonatomic) IBOutlet UIView *savedCardDataSuperview;
@property (weak, nonatomic) IBOutlet UIImageView *savedCardFlagImageView;
@property (weak, nonatomic) IBOutlet UILabel *savedCardNumberEndingLabel;

@property (strong, nonatomic) Donation *donation;
    
@property BOOL userHasSavedCreditCard;
    
@end


@implementation DonationValueTableViewController

static const CGFloat headerViewDefaultHeight = 206.f;

#pragma mark - Initialization

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setup];
}

- (void)setup {
    self.titleLabel.text = self.project.title;
    self.donationGoalView.goalValueLabel.text = self.project.goal;
    self.donationGoalView.mainImageView.image = self.mainImage;
    
    CGFloat viewHeight, viewWidth;
    viewWidth = [[UIScreen mainScreen]bounds].size.width - (2 * defaultViewPadding);
    viewHeight = viewWidth * bannerViewProportion;
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = CGRectMake(0, 0, viewWidth, viewHeight);
    gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor defaultColor:DefaultColorBannerShadowTop] CGColor], (id)[[UIColor defaultColor:DefaultColorBannerShadowTop] CGColor], (id)[[UIColor blackColor] CGColor], nil];
    [self.donationGoalView.mainImageView.layer insertSublayer:gradient atIndex:0];
    
    self.donationValue.clearButtonMode = UITextFieldViewModeWhileEditing;
    
    // setup Header view height
    if (IS_IPHONE_6_7_PLUS) { // iPhone 5.5'
        [self.headerView setFrame:CGRectMake(self.headerView.frame.origin.x, self.headerView.frame.origin.y, self.headerView.frame.size.width, headerViewDefaultHeight + 10.f)];
    } else if (IS_IPHONE_6_7) { // iPhone 4.7'
        [self.headerView setFrame:CGRectMake(self.headerView.frame.origin.x, self.headerView.frame.origin.y, self.headerView.frame.size.width, headerViewDefaultHeight)];
    } else { // iPhone 4'
        [self.headerView setFrame:CGRectMake(self.headerView.frame.origin.x, self.headerView.frame.origin.y, self.headerView.frame.size.width, headerViewDefaultHeight - 15.f)];
    }
    
#warning for prototype only. perform test later
    self.userHasSavedCreditCard = YES;
    
    // setup views for user saved cards
    [self.savedCardSwitch addTarget:self action:@selector(switchChanged:) forControlEvents:UIControlEventValueChanged];
    if (!CREDIT_CARD_SAVE_SERVICE_ACTIVATED || !self.userHasSavedCreditCard) {
        [self.savedCardSwitchSuperview setHidden:YES];
    }
    [self.savedCardDataSuperview setHidden:YES];
    
    // setup notification observers
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cardWasSelectedForDonation:) name:@"cardWasSelectedForDonation" object:nil];
}


#pragma mark - Navigation

- (IBAction)goToNextPage:(id)sender {
    [self.view endEditing:YES];
    [self performInputValidations];
}

- (void)performInputValidations {
    
    NSMutableString *valueString = [NSMutableString stringWithString:self.donationValue.text];
    valueString = [NSMutableString stringWithString:[valueString stringByReplacingOccurrencesOfString:@"." withString:@""]];
    
    NSNumberFormatter * formatter = [NSNumberFormatter new];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    NSLocale *currentLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"pt_BR"];
    [formatter setLocale:currentLocale];
    
    NSNumber *donationValue = [formatter numberFromString:valueString];

    if ([self.donationValue.text isEqualToString:@""] || [donationValue floatValue] < 10.f) {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Atenção"
                                                                       message:@"Valor de doação mínimo: R$10,00."
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"OK"
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * action) {}];
        
        [alert addAction:okButton];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    
    self.donation = [[Donation alloc] initWithValue:[[NSString plainStringFromString:self.donationValue.text] doubleValue]];
    
    if ([self.savedCardSwitch isOn]) {
        // if saved card switch is on, payment type can only be credit card
        self.donation.paymentType = PaymentTypeCreditCard;
        [self performSegueWithIdentifier:@"customToAntifraudDataFromDonationValue" sender:self];
    } else {
        [self performSegueWithIdentifier:@"customToDonationData" sender:self];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"customToDonationData"]) {
        DonationDataTableViewController *donorDataTableViewController = (DonationDataTableViewController *)[segue destinationViewController];
        
        donorDataTableViewController.project = self.project;
        donorDataTableViewController.mainImage = self.mainImage;
        donorDataTableViewController.donation = self.donation;
    } else if ([segue.identifier isEqualToString:@"customToAntifraudDataFromDonationValue"]) {
        AntifraudDataTableViewController *antifraudDataTableViewController = (AntifraudDataTableViewController *)[segue destinationViewController];
        
        antifraudDataTableViewController.project = self.project;
        antifraudDataTableViewController.mainImage = self.mainImage;
        antifraudDataTableViewController.donation = self.donation;
    } else if ([segue.identifier isEqualToString:@"modalToCardList"]) {
        UINavigationController *navController = (UINavigationController *)[segue destinationViewController];
        CardListTableViewController *cardListTableViewController = (CardListTableViewController *)[navController topViewController];
        cardListTableViewController.navigationItem.rightBarButtonItem = nil;
        cardListTableViewController.userCameFromDonationPage = YES;
    }
}

- (void)cardWasSelectedForDonation:(NSNotification *)notification {
    NSDictionary *selectedCard = (NSDictionary *)notification.object;
    self.savedCardFlagImageView.image = [UIImage imageNamed:[selectedCard objectForKey:@"flag"]];
    if (self.savedCardFlagImageView.image == nil) {
        self.savedCardFlagImageView.image = [UIImage imageNamed:@"Generic Card"];
    }
    NSString *cardNumber = [selectedCard objectForKey:@"cardNumber"];
    self.savedCardNumberEndingLabel.text = [cardNumber substringWithRange:(NSMakeRange([cardNumber length] - 4, 4))];
}

- (IBAction)unwindToDonationValue:(UIStoryboardSegue*)unwindSegue {

}

- (IBAction)unwindToBeforeCardList:(UIStoryboardSegue*)unwindSegue {
    
}


#pragma mark - UIScrollView Delegate

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [self.view endEditing:YES];
}
    
    
#pragma mark - UISwitch Helper Methods
    
- (void)switchChanged:(id)sender {
    if ([sender isOn]){
        [self.savedCardDataSuperview setHidden:NO];
    } else {
        [self.savedCardDataSuperview setHidden:YES];
    }
}

@end

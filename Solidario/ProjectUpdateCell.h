//
//  ProjectUpdateCell.h
//  Solidario
//
//  Created by Pan on 28/12/16.
//  Copyright © 2016 Paula Vasconcelos Gueiros. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProjectUpdateCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *topLineView;
@property (weak, nonatomic) IBOutlet UILabel *entryDateDayLabel;
@property (weak, nonatomic) IBOutlet UILabel *entryDateMonthLabel;
@property (weak, nonatomic) IBOutlet UILabel *entryDateTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *entryTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *entryDescriptionLabel;
@property (strong, nonatomic) IBOutlet UIImageView *entryImageView;
@property (weak, nonatomic) IBOutlet UIImageView *entryArrowImage;
@property (weak, nonatomic) IBOutlet UIButton *viewMoreOrLessButton;
@property (weak, nonatomic) IBOutlet UIButton *deleteEntryButton;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageHiddenConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageAspectRatioConstraint;

@property BOOL isButtonTextMore;

@end

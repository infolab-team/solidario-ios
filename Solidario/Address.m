//
//  Address.m
//
//  Created by Pan  on 18/01/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "Address.h"

NSString *const kAddressBairro = @"bairro";
NSString *const kAddressUf = @"uf";
NSString *const kAddressLocalidade = @"localidade";
NSString *const kAddressCep = @"cep";
NSString *const kAddressUnidade = @"unidade";
NSString *const kAddressIbge = @"ibge";
NSString *const kAddressLogradouro = @"logradouro";
NSString *const kAddressComplemento = @"complemento";
NSString *const kAddressGia = @"gia";


@interface Address ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation Address

@synthesize bairro = _bairro;
@synthesize uf = _uf;
@synthesize localidade = _localidade;
@synthesize cep = _cep;
@synthesize unidade = _unidade;
@synthesize ibge = _ibge;
@synthesize logradouro = _logradouro;
@synthesize complemento = _complemento;
@synthesize gia = _gia;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.bairro = [self objectOrNilForKey:kAddressBairro fromDictionary:dict];
            self.uf = [self objectOrNilForKey:kAddressUf fromDictionary:dict];
            self.localidade = [self objectOrNilForKey:kAddressLocalidade fromDictionary:dict];
            self.cep = [self objectOrNilForKey:kAddressCep fromDictionary:dict];
            self.unidade = [self objectOrNilForKey:kAddressUnidade fromDictionary:dict];
            self.ibge = [self objectOrNilForKey:kAddressIbge fromDictionary:dict];
            self.logradouro = [self objectOrNilForKey:kAddressLogradouro fromDictionary:dict];
            self.complemento = [self objectOrNilForKey:kAddressComplemento fromDictionary:dict];
            self.gia = [self objectOrNilForKey:kAddressGia fromDictionary:dict];

    }
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.bairro forKey:kAddressBairro];
    [mutableDict setValue:self.uf forKey:kAddressUf];
    [mutableDict setValue:self.localidade forKey:kAddressLocalidade];
    [mutableDict setValue:self.cep forKey:kAddressCep];
    [mutableDict setValue:self.unidade forKey:kAddressUnidade];
    [mutableDict setValue:self.ibge forKey:kAddressIbge];
    [mutableDict setValue:self.logradouro forKey:kAddressLogradouro];
    [mutableDict setValue:self.complemento forKey:kAddressComplemento];
    [mutableDict setValue:self.gia forKey:kAddressGia];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}


#pragma mark - Helper Method

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.bairro = [aDecoder decodeObjectForKey:kAddressBairro];
    self.uf = [aDecoder decodeObjectForKey:kAddressUf];
    self.localidade = [aDecoder decodeObjectForKey:kAddressLocalidade];
    self.cep = [aDecoder decodeObjectForKey:kAddressCep];
    self.unidade = [aDecoder decodeObjectForKey:kAddressUnidade];
    self.ibge = [aDecoder decodeObjectForKey:kAddressIbge];
    self.logradouro = [aDecoder decodeObjectForKey:kAddressLogradouro];
    self.complemento = [aDecoder decodeObjectForKey:kAddressComplemento];
    self.gia = [aDecoder decodeObjectForKey:kAddressGia];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {

    [aCoder encodeObject:_bairro forKey:kAddressBairro];
    [aCoder encodeObject:_uf forKey:kAddressUf];
    [aCoder encodeObject:_localidade forKey:kAddressLocalidade];
    [aCoder encodeObject:_cep forKey:kAddressCep];
    [aCoder encodeObject:_unidade forKey:kAddressUnidade];
    [aCoder encodeObject:_ibge forKey:kAddressIbge];
    [aCoder encodeObject:_logradouro forKey:kAddressLogradouro];
    [aCoder encodeObject:_complemento forKey:kAddressComplemento];
    [aCoder encodeObject:_gia forKey:kAddressGia];
}

- (id)copyWithZone:(NSZone *)zone {
    Address *copy = [[Address alloc] init];
    
    if (copy) {

        copy.bairro = [self.bairro copyWithZone:zone];
        copy.uf = [self.uf copyWithZone:zone];
        copy.localidade = [self.localidade copyWithZone:zone];
        copy.cep = [self.cep copyWithZone:zone];
        copy.unidade = [self.unidade copyWithZone:zone];
        copy.ibge = [self.ibge copyWithZone:zone];
        copy.logradouro = [self.logradouro copyWithZone:zone];
        copy.complemento = [self.complemento copyWithZone:zone];
        copy.gia = [self.gia copyWithZone:zone];
    }
    return copy;
}

@end

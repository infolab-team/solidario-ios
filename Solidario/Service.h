//
//  Service.h
//  Solidario
//
//  Created by Allan on 4/6/16.
//  Copyright © 2016 Infosolo. All rights reserved.
//

// Project environment type
#define PROJECT_IS_IN_DEVELOPMENT_ENV           0

// Server constants for url path in plist file
#define kServerPlistFilename                    @"Service"

#define kServerUrlRootKeyHmg                    @"url-root-hmg"
#define kServerUrlRootKeyProd                   @"url-root-prod"
#define kServerUrlRootKeyCep                    @"url-root-cep"

#define kServerUrlPartKeyRegister               @"url-part-register"
#define kServerUrlPartKeyResetPassword          @"url-part-reset-password"
#define kServerUrlPartKeyLogin                  @"url-part-login"
#define kServerUrlPartKeyFacebookLogin          @"url-part-fb-login"
#define kServerUrlPartKeyLogout                 @"url-part-logout"
#define kServerUrlPartKeyUpdateUserProfile      @"url-part-update-user-profile"

#define kServerUrlPartKeyTimeline               @"url-part-timeline"
#define kServerUrlPartKeyTimelineDelete         @"url-part-timeline-delete"
#define kServerUrlPartKeyTimelineInsert         @"url-part-timeline-insert"
#define kServerUrlPartKeyTimelineTitle          @"url-part-timeline-title"
#define kServerUrlPartKeyTimelineDescription    @"url-part-timeline-description"

#define kServerUrlPartKeyProjects               @"url-part-projects"
#define kServerUrlPartKeyProjectsPage           @"url-part-projects-page"
#define kServerUrlPartKeyProjectsPerPage        @"url-part-projects-per-page"
#define kServerUrlPartKeyProjectsCategories     @"url-part-projects-categories"
#define kServerUrlPartKeyProjectsQuery          @"url-part-projects-query"
#define kServerUrlPartKeyProject                @"url-part-project"
#define kServerUrlPartKeyProjectDonators        @"url-part-project-donators"

#define kServerUrlPartKeyGenerateBillet         @"url-part-generate-billet"
#define kServerUrlPartKeyPurchase               @"url-part-purchase"

#define kServerUrlPartKeyMyProjectsCreated      @"url-part-myprojects-created"
#define kServerUrlPartKeyMyProjectsDonated      @"url-part-myprojects-donated"
#define kServerUrlPartKeyMyProjectsFavorite     @"url-part-myprojects-favorite"

#define kServerUrlPartKeyActivities             @"url-part-activities"
#define kServerUrlPartKeyCategories             @"url-part-categories"
#define kServerUrlPartKeyStates                 @"url-part-states"
#define kServerUrlPartKeyTerms                  @"url-part-terms"
#define kServerUrlPartKeyAbout                  @"url-part-about"
#define kServerUrlPartKeyFavoriteToggle         @"url-part-favorite-toggle"

#define kServerUrlSuffixKeyCep                  @"url-suffix-cep"


#import <Foundation/Foundation.h>

typedef void(^GenericFullCompletion)(id results, BOOL hasError, NSString *errorMessage);
typedef void(^FullCompletion)(NSDictionary *results, BOOL hasError, NSString *errorMessage);

typedef enum {
    ServiceMethodPost = 0,
    ServiceMethodGet = 1,
    ServiceMethodDelete = 2,
    ServiceMethodPut = 3
} ServiceMethod;

@interface Service : NSObject


#pragma mark - Address

+ (void)getAddressWithCEP:(NSString *)cep andCompletion:(GenericFullCompletion)completion;

#pragma mark - User

+ (void)registerWithName:(NSString *)name surname:(NSString *)surname email:(NSString *)email password:(NSString *)password newsletter:(BOOL)newsletter andCompletion:(GenericFullCompletion)completion;
+ (void)loginWithEmail:(NSString *)email password:(NSString *)password andCompletion:(GenericFullCompletion)completion;
+ (void)loginWithFacebookAccessToken:(NSString *)accessToken name:(NSString *)name lastName:(NSString *)lastName email:(NSString *)email andCompletion:(GenericFullCompletion)completion;
+ (void)logoutWithCompletion:(GenericFullCompletion)completion;
+ (void)resetPasswordForEmail:(NSString *)email withCompletion:(GenericFullCompletion)completion;
+ (void)updateUserWithName:(NSString *)name surname:(NSString *)surname email:(NSString *)email newPassword:(NSString *)newPassword currentPassword:(NSString *)currentPassword imageString:(NSString *)imageString andCompletion:(GenericFullCompletion)completion;

#pragma mark - Projects

+ (void)getProjectsOnPage:(NSInteger)page withItemsPerPage:(NSInteger)itemsPerPage categories:(NSArray *)categories states:(NSArray *)states andCompletion:(GenericFullCompletion)completion;
+ (void)getProjectsOnPage:(NSInteger)page withItemsPerPage:(NSInteger)itemsPerPage searchArgument:(NSString *)searchArgument andCompletion:(GenericFullCompletion)completion;
+ (void)getProjectWithIdentifier:(NSString *)identifier andCompletion:(GenericFullCompletion)completion;

#pragma mark - Timeline

+ (void)getTimelineEntriesForProjectWithIdentifier:(NSString *)identifier onPage:(NSInteger)page withItemsPerPage:(NSInteger)itemsPerPage andCompletion:(GenericFullCompletion)completion;
+ (void)deleteEntryFromTimelineWithIdentifier:(NSString *)identifier andCompletion:(GenericFullCompletion)completion;
+ (void)insertEntryWithTitle:(NSString *)title description:(NSString *)description andImageString:(NSString *)imageString toProjectWithIdentifier:(NSString *)identifier andCompletion:(GenericFullCompletion)completion;

#pragma mark - Donators

+ (void)getDonatorsForProjectWithIdentifier:(NSString *)identifier andCompletion:(GenericFullCompletion)completion;

#pragma mark - Donation

+ (void)generateBilletForDonationOfAmount:(NSNumber *)amount anonymously:(BOOL)anonymous toProjectWithIdentifier:(NSString *)identifier withCompletion:(GenericFullCompletion)completion;
+ (void)purchaseAmount:(NSNumber *)amount anonymously:(BOOL)anonymous toProjectWithIdentifier:(NSString *)identifier usingCardHash:(NSString *)cardHash antifraudData:(NSDictionary *)antifraudData andCompletion:(GenericFullCompletion)completion;

#pragma mark - My Projects

+ (void)getMyProjectListWithOptionNumber:(NSInteger)number onPage:(NSInteger)page withItemsPerPage:(NSInteger)itemsPerPage andCompletion:(GenericFullCompletion)completion;

#pragma mark - Activities

+ (void)getUserActivitiesOnPage:(NSInteger)page withItemsPerPage:(NSInteger)itemsPerPage andCompletion:(GenericFullCompletion)completion;

#pragma mark - Categories

+ (void)getAllCagetoriesWithCompletion:(GenericFullCompletion)completion;

#pragma mark - States

+ (void)getAllStatesWithCompletion:(GenericFullCompletion)completion;

#pragma mark - Terms

+ (void)getTermsWithCompletion:(GenericFullCompletion)completion;

#pragma mark - About

+ (void)getAboutWithCompletion:(GenericFullCompletion)completion;

#pragma mark - Favorite/Unfavorite

+ (void)toggleFavoriteFromProjectWithIdentifier:(NSString *)identifier andCompletion:(GenericFullCompletion)completion;

@end

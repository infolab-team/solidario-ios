//
//  StateTableViewCell.h
//  Solidario
//
//  Created by Paula Vasconcelos Gueiros on 23/8/16.
//  Copyright © 2016 Paula Vasconcelos Gueiros. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StateTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *checkImage;
@property (weak, nonatomic) IBOutlet UILabel *stateNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *projectQuantityLabel;
@property (weak, nonatomic) IBOutlet UIView *projectQuantitySuperview;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leadingSpaceLabelToImageConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leadingSpaceLabelToSuperviewConstraint;

@end

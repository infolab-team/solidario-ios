//
//  AntifraudData.m
//
//  Created by Paula Vasconcelos on 05/05/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "AntifraudData.h"

NSString *const kAntifraudDataNumero = @"numero";
NSString *const kAntifraudDataRua = @"rua";
NSString *const kAntifraudDataComplemento = @"complemento";
NSString *const kAntifraudDataName = @"name";
NSString *const kAntifraudDataEmail = @"email";
NSString *const kAntifraudDataBairro = @"bairro";
NSString *const kAntifraudDataCep = @"cep";
NSString *const kAntifraudDataCpf = @"cpf";
NSString *const kAntifraudDataTelefone = @"telefone";
NSString *const kAntifraudDataDDD = @"ddd";

@interface AntifraudData ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end


@implementation AntifraudData

@synthesize numero = _numero;
@synthesize rua = _rua;
@synthesize complemento = _complemento;
@synthesize name = _name;
@synthesize email = _email;
@synthesize bairro = _bairro;
@synthesize cep = _cep;
@synthesize cpf = _cpf;
@synthesize telefone = _telefone;
@synthesize ddd = _ddd;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
        self.numero = [[self objectOrNilForKey:kAntifraudDataNumero fromDictionary:dict] doubleValue];
        self.rua = [self objectOrNilForKey:kAntifraudDataRua fromDictionary:dict];
        self.complemento = [self objectOrNilForKey:kAntifraudDataComplemento fromDictionary:dict];
        self.name = [self objectOrNilForKey:kAntifraudDataName fromDictionary:dict];
        self.email = [self objectOrNilForKey:kAntifraudDataEmail fromDictionary:dict];
        self.bairro = [self objectOrNilForKey:kAntifraudDataBairro fromDictionary:dict];
        self.cep = [self objectOrNilForKey:kAntifraudDataCep fromDictionary:dict];
        self.cpf = [self objectOrNilForKey:kAntifraudDataCpf fromDictionary:dict];
        self.telefone = [self objectOrNilForKey:kAntifraudDataTelefone fromDictionary:dict];
        self.ddd = [self objectOrNilForKey:kAntifraudDataDDD fromDictionary:dict];

    }
    return self;
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.numero] forKey:kAntifraudDataNumero];
    [mutableDict setValue:self.rua forKey:kAntifraudDataRua];
    [mutableDict setValue:self.complemento forKey:kAntifraudDataComplemento];
    [mutableDict setValue:self.name forKey:kAntifraudDataName];
    [mutableDict setValue:self.email forKey:kAntifraudDataEmail];
    [mutableDict setValue:self.bairro forKey:kAntifraudDataBairro];
    [mutableDict setValue:self.cep forKey:kAntifraudDataCep];
    [mutableDict setValue:self.cpf forKey:kAntifraudDataCpf];
    [mutableDict setValue:self.telefone forKey:kAntifraudDataTelefone];
    [mutableDict setValue:self.ddd forKey:kAntifraudDataDDD];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}


#pragma mark - Helper Method

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.numero = [aDecoder decodeDoubleForKey:kAntifraudDataNumero];
    self.rua = [aDecoder decodeObjectForKey:kAntifraudDataRua];
    self.complemento = [aDecoder decodeObjectForKey:kAntifraudDataComplemento];
    self.name = [aDecoder decodeObjectForKey:kAntifraudDataName];
    self.email = [aDecoder decodeObjectForKey:kAntifraudDataEmail];
    self.bairro = [aDecoder decodeObjectForKey:kAntifraudDataBairro];
    self.cep = [aDecoder decodeObjectForKey:kAntifraudDataCep];
    self.cpf = [aDecoder decodeObjectForKey:kAntifraudDataCpf];
    self.telefone = [aDecoder decodeObjectForKey:kAntifraudDataTelefone];
    self.ddd = [aDecoder decodeObjectForKey:kAntifraudDataDDD];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {

    [aCoder encodeDouble:_numero forKey:kAntifraudDataNumero];
    [aCoder encodeObject:_rua forKey:kAntifraudDataRua];
    [aCoder encodeObject:_complemento forKey:kAntifraudDataComplemento];
    [aCoder encodeObject:_name forKey:kAntifraudDataName];
    [aCoder encodeObject:_email forKey:kAntifraudDataEmail];
    [aCoder encodeObject:_bairro forKey:kAntifraudDataBairro];
    [aCoder encodeObject:_cep forKey:kAntifraudDataCep];
    [aCoder encodeObject:_cpf forKey:kAntifraudDataCpf];
    [aCoder encodeObject:_telefone forKey:kAntifraudDataTelefone];
    [aCoder encodeObject:_ddd forKey:kAntifraudDataDDD];
}

- (id)copyWithZone:(NSZone *)zone {
    AntifraudData *copy = [[AntifraudData alloc] init];
    
    if (copy) {
        copy.numero = self.numero;
        copy.rua = [self.rua copyWithZone:zone];
        copy.complemento = [self.complemento copyWithZone:zone];
        copy.name = [self.name copyWithZone:zone];
        copy.email = [self.email copyWithZone:zone];
        copy.bairro = [self.bairro copyWithZone:zone];
        copy.cep = [self.cep copyWithZone:zone];
        copy.cpf = [self.cpf copyWithZone:zone];
        copy.telefone = [self.telefone copyWithZone:zone];
        copy.ddd = [self.ddd copyWithZone:zone];
    }
    return copy;
}

@end

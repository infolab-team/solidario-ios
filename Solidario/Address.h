//
//  Address.h
//
//  Created by Pan  on 18/01/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface Address : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *bairro;
@property (nonatomic, strong) NSString *uf;
@property (nonatomic, strong) NSString *localidade;
@property (nonatomic, strong) NSString *cep;
@property (nonatomic, strong) NSString *unidade;
@property (nonatomic, strong) NSString *ibge;
@property (nonatomic, strong) NSString *logradouro;
@property (nonatomic, strong) NSString *complemento;
@property (nonatomic, strong) NSString *gia;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end

//
//  TermsTableViewController.h
//  Solidario
//
//  Created by Paula Vasconcelos Gueiros on 12/9/16.
//  Copyright © 2016 Paula Vasconcelos Gueiros. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TermsTableViewController : UITableViewController <UIWebViewDelegate>

@end

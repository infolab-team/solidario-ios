//
//  CategoryMenuTableViewCell.m
//  Solidario
//
//  Created by Paula Vasconcelos Gueiros on 24/8/16.
//  Copyright © 2016 Paula Vasconcelos Gueiros. All rights reserved.
//

#import "CategoryMenuTableViewCell.h"
#import "Singleton.h"


@implementation CategoryMenuTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];

    // setup array with category buttons
    self.categoryButtons = @[self.socialButton, self.environmentButton, self.animalButton, self.healthButton];
    
    // fetch categories from singleton
    self.categories = [Singleton categories];
}

- (IBAction)selectedSocial:(id)sender {
    [self.socialButton setUserInteractionEnabled:NO];
    [self.socialButton setSelected:!self.socialButton.selected];
    [self updateCategoriesArray];
}

- (IBAction)selectedEnvironment:(id)sender {
    [self.environmentButton setUserInteractionEnabled:NO];
    [self.environmentButton setSelected:!self.environmentButton.selected];
    [self updateCategoriesArray];
}

- (IBAction)selectedAnimal:(id)sender {
    [self.animalButton setUserInteractionEnabled:NO];
    [self.animalButton setSelected:!self.animalButton.selected];
    [self updateCategoriesArray];
}

- (IBAction)selectedHealth:(id)sender {
    [self.healthButton setUserInteractionEnabled:NO];
    [self.healthButton setSelected:!self.healthButton.selected];
    [self updateCategoriesArray];
}

- (void)updateCategoriesArray {
    // update button selection options in categories array
    
    [self.categoryButtons enumerateObjectsUsingBlock:^(id obj1, NSUInteger idx1, BOOL *stop1) {
        CategoryMenuButton *categoryButton = ((CategoryMenuButton *)obj1);
        NSString *categoryButtonID = categoryButton.restorationIdentifier;
        
        [self.categories enumerateObjectsUsingBlock:^(id obj2, NSUInteger idx2, BOOL *stop2) {
            Category *category = (Category *)obj2;
            
            if ([category.internalBaseClassIdentifier isEqualToString:categoryButtonID]) {
                [category setSelected:categoryButton.isSelected];
                *stop2 = YES;
            }
        }];
    }];
    [Singleton saveCategories:self.categories];
    NSLog(@"🤡 Settings for categories updated successfully");
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"userSettingsChanged" object:nil];
}

@end

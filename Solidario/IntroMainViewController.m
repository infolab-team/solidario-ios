//
//  IntroMainViewController.m
//  Solidário
//
//  Created by Allan Alves on 7/4/16.
//  Copyright © 2016 Infosolo. All rights reserved.
//

#import "IntroMainViewController.h"
#import "IntroPageItemViewController.h"
#import "MainViewController.h"
#import "NSArray+Utils.h"
#import "UIColor+Utils.h"
#import "Singleton.h"

@interface IntroMainViewController ()

@property (strong, nonatomic) UIPageViewController *pageViewController;
@property (weak, nonatomic) IBOutlet UIPageControl *introPageControl;
@property (weak, nonatomic) IBOutlet UIView *skipView;
@property (weak, nonatomic) IBOutlet UIView *separatorView;
@property (weak, nonatomic) IBOutlet UIButton *skipButton;
@property (weak, nonatomic) IBOutlet UIButton *arrowButton;

@property (strong, nonatomic) NSArray *introItems;

@end


@implementation IntroMainViewController

static const CGFloat pageControlScale = 2.f;
static const CGFloat pageControlViewScale = 1.4f;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setup];
}

- (void)setup {
    [self setNeedsStatusBarAppearanceUpdate];
    
    //Get Plist with items
    self.introItems = [NSArray loadPlistWithName:@"IntroPages"];
    
    //Create Page View Controller
    _pageViewController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll
                                                          navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal
                                                                        options:nil];
    
    _pageViewController.delegate = self;
    _pageViewController.dataSource = self;
    
    IntroPageItemViewController *firstViewController = [self viewControllerWithIndex:0];
    
    [_pageViewController setViewControllers:@[firstViewController]
                                  direction:UIPageViewControllerNavigationDirectionForward
                                   animated:YES completion:nil];
    
    [self addChildViewController:self.pageViewController];
    [self.view addSubview:self.pageViewController.view];
    [self didMoveToParentViewController:self.pageViewController];
    [self.view bringSubviewToFront:self.skipView];
    
    [self.introPageControl setNumberOfPages:[self.introItems count]];
    [self.introPageControl setCurrentPageIndicatorTintColor:[UIColor defaultColor:DefaultColorIntroSelectedPageDot]];
    self.introPageControl.transform = CGAffineTransformMakeScale(pageControlScale, pageControlScale);
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self transform];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)transform {
    
    for (UIView *view in self.introPageControl.subviews) {
        [UIView animateWithDuration:1 animations:^{
            view.transform = CGAffineTransformMakeScale(pageControlViewScale, pageControlViewScale);
            view.center = CGPointMake(self.view.center.x - 50, view.center.y);
        }];
    }
}

- (IntroPageItemViewController*)viewControllerWithIndex:(NSInteger)index {
    
    IntroPageItemViewController *page = [self.storyboard instantiateViewControllerWithIdentifier:@"IntroPageItemViewController"];
    if (self.introItems.count && index <= self.introItems.count) {
        NSDictionary *dict = [self.introItems objectAtIndex:index];
        page.pageIndex = index;
        page.pageImageName = [dict objectForKey:@"imageName"];
        page.pageDescription = [dict objectForKey:@"description"];
        page.pageTitle = [dict objectForKey:@"title"];
        return page;
    }
    return nil;
}


#pragma mark - Page View Controller Delegate & Data Source

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
    
    NSInteger pageIndex = [(IntroPageItemViewController*)viewController pageIndex];
    if (pageIndex > 0 && pageIndex != NSNotFound) {
        pageIndex--;
        return [self viewControllerWithIndex:pageIndex];
    }
    return nil;
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
    
    NSInteger pageIndex = [(IntroPageItemViewController*)viewController pageIndex];
    if (pageIndex != NSNotFound && pageIndex < self.introItems.count-1) {
        pageIndex++;
        return [self viewControllerWithIndex:pageIndex];
    }
    return nil;
}

- (void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray *)previousViewControllers transitionCompleted:(BOOL)completed {

    IntroPageItemViewController *viewController = [self.pageViewController.viewControllers lastObject];
    [self.introPageControl setCurrentPage:viewController.pageIndex];

    [UIView animateWithDuration:0.3 animations:^{
        if (viewController.pageIndex == self.introItems.count-1) {
            [self.arrowButton setHidden:YES];
            [self.separatorView setHidden:YES];
            [self.skipButton setTitle:@"Vem com a gente?" forState:UIControlStateNormal];
            [self.skipButton setBackgroundColor:[UIColor defaultColor:DefaultColorMain]];
            
        } else {
            [self.arrowButton setHidden:NO];
            [self.separatorView setHidden:NO];
            [self.skipButton setTitle:@"Pular" forState:UIControlStateNormal];
            [self.skipButton setBackgroundColor:[UIColor clearColor]];
            
        }
    }];
}


# pragma mark - Navigation Methods

- (IBAction)goToNextPage:(id)sender {
    NSLog(@"➡️ Go to next page in Intro");
    
    NSUInteger pageIndex = ((IntroPageItemViewController *)[self.pageViewController.viewControllers objectAtIndex:0]).pageIndex + 1;
    
    if (pageIndex == self.introItems.count) {
        return;
    }
    
    IntroPageItemViewController *nextViewController = [self viewControllerWithIndex:pageIndex];
    __weak typeof(self) weakSelf = self;
    [self.pageViewController setViewControllers:@[nextViewController]
                                      direction:UIPageViewControllerNavigationDirectionForward
                                       animated:YES
                                     completion:^(BOOL finished){
                                         if (finished) {
                                             [weakSelf pageViewController:weakSelf.pageViewController didFinishAnimating:YES previousViewControllers:@[[weakSelf viewControllerWithIndex:pageIndex-1]] transitionCompleted:YES];
                                         }
                                     }];
}

- (IBAction)skip:(id)sender {
    NSLog(@"↗️ Skip Intro / Continue");

    if (self.fromCategorySelection) {
        [self dismissViewControllerAnimated:YES completion:nil];
    } else {
        [Singleton setNotFirstTimeUse];
        
        NSString *storyboardIdentifier = @"Account";
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardIdentifier bundle:nil];
        UIViewController *mainAccountViewController = [storyboard instantiateViewControllerWithIdentifier:@"mainAccountViewController"];
        
        [self presentViewController:mainAccountViewController animated:YES completion:nil];
    }
}

@end

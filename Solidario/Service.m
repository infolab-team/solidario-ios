//
//  Service.m
//  Solidario
//
//  Created by Allan on 4/6/16.
//  Copyright © 2016 Infosolo. All rights reserved.
//

#import "Service.h"
#import "AFNetworking.h"
#import "Singleton.h"
#import "DataModels.h"
#import "Utils.h"

@implementation Service

#pragma mark - URL Paths

+ (NSString *)urlPartWithRootForKey:(NSString *)key {
    NSString *root;
    
    if (PROJECT_IS_IN_DEVELOPMENT_ENV) root = kServerUrlRootKeyHmg;
    else root = kServerUrlRootKeyProd;
    
    return [NSString stringWithFormat:@"%@/%@", [self urlForKey:root], [self urlForKey:key]];
}

+ (NSString *)urlForKey:(NSString *)key {
    NSString *fileName = kServerPlistFilename;
    NSString *path = [[NSBundle mainBundle] pathForResource:fileName ofType:@"plist"];
    NSDictionary *config = [[NSDictionary alloc] initWithContentsOfFile:path];
    
    return [config valueForKey:key];
}


#pragma mark - Address

+ (void)getAddressWithCEP:(NSString *)cep andCompletion:(GenericFullCompletion)completion {
    // setup url for request
    NSString *url = [NSString stringWithFormat:@"%@/%@/%@", [self urlForKey:kServerUrlRootKeyCep], cep, [self urlForKey:kServerUrlSuffixKeyCep]];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializerWithWritingOptions:NSJSONWritingPrettyPrinted];
    
    NSMutableIndexSet *setCodes = [[NSMutableIndexSet alloc] init];
    [setCodes addIndex:200];
    [setCodes addIndex:400];
    manager.responseSerializer.acceptableStatusCodes = setCodes;
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html", @"application/json", nil];
    
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id _Nullable responseObject) {
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            NSDictionary *response = responseObject;
            if ([response.allKeys containsObject:@"erro"]) {
                completion(response, YES, nil);
            } else {
                if ([response.allKeys containsObject:@"cep"]) {
                    Address *address = [[Address alloc] initWithDictionary:response];
                    completion(address, NO, nil);
                } else {
                    completion(response, YES, nil);
                }
            }
        } else {
            completion(nil, YES, nil);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completion(nil, YES, [error localizedDescription]);
    }];
}


#pragma mark - User
    
+ (void)registerWithName:(NSString *)name surname:(NSString *)surname email:(NSString *)email password:(NSString *)password newsletter:(BOOL)newsletter andCompletion:(GenericFullCompletion)completion {
    // setup url for request
    NSString *url = [self urlPartWithRootForKey:kServerUrlPartKeyRegister];
    
    // create dictionary with user input data to be sent to web server
    NSMutableDictionary *userData = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                          name, @"name",
                                          surname, @"surname",
                                          email, @"email",
                                          password, @"password",
                                          nil];
    if (newsletter) {
        [userData setObject:@"true" forKey:@"newsletter"];
    }
    
    [self requestUrl:url withMethod:ServiceMethodPost parameters:userData andCompletion:^(NSDictionary *results, BOOL hasError, NSString *errorMessage) {
        if (hasError) {
            completion(results, YES, errorMessage);
        } else {
            NSString *successMessage = [results valueForKeyPath:@"message"];
            completion(successMessage, NO, nil);
        }
    }];
}

+ (void)loginWithEmail:(NSString *)email password:(NSString *)password andCompletion:(GenericFullCompletion)completion {
    // setup url for request
    NSString *url = [self urlPartWithRootForKey:kServerUrlPartKeyLogin];

    // create dictionary with user input data to be sent to web server
    NSDictionary *userData = [NSDictionary dictionaryWithObjectsAndKeys:
                                 email, @"email",
                                 password, @"password", nil];
    
    [self requestUrl:url withMethod:ServiceMethodPost parameters:userData andCompletion:^(NSDictionary *results, BOOL hasError, NSString *errorMessage) {
        if (hasError) {
            completion(results, YES, errorMessage);
        } else {
            if ([results.allKeys containsObject:@"data"]) {
                User *user = [[User alloc] initWithDictionary:results[@"data"]];
                completion(user, NO, nil);
            } else {
                completion(results, YES, nil);
            }
        }
    }];
}

+ (void)loginWithFacebookAccessToken:(NSString *)accessToken name:(NSString *)name lastName:(NSString *)lastName email:(NSString *)email andCompletion:(GenericFullCompletion)completion; {
    // setup url for request
    NSString *url = [self urlPartWithRootForKey:kServerUrlPartKeyFacebookLogin];
    
    // create dictionary with user input data to be sent to web server
    NSDictionary *userData = [NSDictionary dictionaryWithObjectsAndKeys:
                                  accessToken, @"accessToken",
                                  name, @"name",
                                  lastName, @"lastname",
                                  email, @"email", nil];
    
    [self requestUrl:url withMethod:ServiceMethodPost parameters:userData andCompletion:^(NSDictionary *results, BOOL hasError, NSString *errorMessage) {
        if (hasError) {
            completion(results, YES, errorMessage);
        } else {
            if ([results.allKeys containsObject:@"data"]) {
                User *user = [[User alloc] initWithDictionary:results[@"data"]];
                completion(user, NO, nil);
            } else {
                completion(results, YES, nil);
            }
        }
    }];
}

+ (void)logoutWithCompletion:(GenericFullCompletion)completion {
    // setup url for request
    NSString *url = [self urlPartWithRootForKey:kServerUrlPartKeyLogout];

    [self requestUrl:url withMethod:ServiceMethodPost parameters:nil andCompletion:^(NSDictionary *results, BOOL hasError, NSString *errorMessage) {
        if (hasError) {
            completion(results, YES, errorMessage);
        } else {
            NSString *successMessage = [results valueForKeyPath:@"data.message"];
            completion(successMessage, NO, nil);
        }
    }];
}

+ (void)resetPasswordForEmail:(NSString *)email withCompletion:(GenericFullCompletion)completion {
    // setup url for request
    NSString *url = [self urlPartWithRootForKey:kServerUrlPartKeyResetPassword];
    
    // create dictionary with data to be sent to web server
    NSDictionary *userData = [NSDictionary dictionaryWithObjectsAndKeys:email, @"email", nil];

    [self requestUrl:url withMethod:ServiceMethodPost parameters:userData andCompletion:^(NSDictionary *results, BOOL hasError, NSString *errorMessage) {
        if (hasError) {
            completion(results, YES, errorMessage);
        } else {
            NSString *successMessage = [results valueForKeyPath:@"message"];
            completion(successMessage, NO, nil);
        }
    }];
}

+ (void)updateUserWithName:(NSString *)name surname:(NSString *)surname email:(NSString *)email newPassword:(NSString *)newPassword currentPassword:(NSString *)currentPassword imageString:(NSString *)imageString andCompletion:(GenericFullCompletion)completion {
    // setup url for request
    NSString *url = [self urlPartWithRootForKey:kServerUrlPartKeyUpdateUserProfile];
    
    // create dictionary with user input data to be sent to web server
    NSMutableDictionary *userData = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                     name, @"name",
                                     surname, @"surname",
                                     nil];
    if (email) [userData setObject:email forKey:@"email"];
    if (newPassword) [userData setObject:newPassword forKey:@"newpassword"];
    if (currentPassword) [userData setObject:currentPassword forKey:@"currentpassword"];
    if (imageString) [userData setObject:imageString forKey:@"file"];

    [self requestUrl:url withMethod:ServiceMethodPost parameters:userData andCompletion:^(NSDictionary *results, BOOL hasError, NSString *errorMessage) {
        if (hasError) {
            completion(results, YES, errorMessage);
        } else {
            if ([results.allKeys containsObject:@"profile"]) {
                UserUpdated *user = [[UserUpdated alloc] initWithDictionary:results[@"profile"]];
                completion(user, NO, nil);
            } else {
                completion(results, YES, nil);
            }
        }
    }];
}


#pragma mark - Projects

+ (void)getProjectsOnPage:(NSInteger)page withItemsPerPage:(NSInteger)itemsPerPage categories:(NSArray *)categories states:(NSArray *)states andCompletion:(GenericFullCompletion)completion {

    // setup url and parameters with selected categories by user
    NSString *categoriesParam = @"";
    for (Category *category in categories) {
        if (category.selected) {
            categoriesParam = [[categoriesParam stringByAppendingString:category.internalBaseClassIdentifier] stringByAppendingString:@","];
        }
    }
    categoriesParam = [categoriesParam stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *urlParams = [NSString stringWithFormat:@"%@%ld%@%ld%@%@",
                           [self urlForKey:kServerUrlPartKeyProjectsPage],
                           (long)page,
                           [self urlForKey:kServerUrlPartKeyProjectsPerPage],
                           (long)itemsPerPage,
                           [self urlForKey:kServerUrlPartKeyProjectsCategories],
                           categoriesParam];
    NSString *url = [NSString stringWithFormat:@"%@%@", [self urlPartWithRootForKey:kServerUrlPartKeyProjects], urlParams];

    // setup request body with array of selected states, if any
    NSDictionary *bodyParam;
    if (states != nil) {
        NSMutableArray *selectedStates = [[NSMutableArray alloc] init];
        [states enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            State *state = (State *)obj;
            if (state.selected) {
                [selectedStates addObject:state.internalBaseClassIdentifier];
            }
        }];
        bodyParam = [NSDictionary dictionaryWithObjectsAndKeys:selectedStates, @"states", nil];
    }

    // request projects with sent parameters
    [self requestUrl:url withMethod:ServiceMethodPost parameters:bodyParam andCompletion:^(NSDictionary *results, BOOL hasError, NSString *errorMessage) {
        if (hasError) {
            completion(results, YES, errorMessage);
        } else {
            if ([results.allKeys containsObject:@"data"]) {
                NSArray *dataArray = [results objectForKey:@"data"];
                NSMutableArray *projects = [[NSMutableArray alloc] init];
                
                for (NSDictionary *item in dataArray){
                    ProjectShort *project = [[ProjectShort alloc] initWithDictionary:item];
                    [projects addObject:project];
                }
                completion(projects, NO, nil);
            } else {
                completion(results, YES, nil);
            }
        }
    }];
}

+ (void)getProjectsOnPage:(NSInteger)page withItemsPerPage:(NSInteger)itemsPerPage searchArgument:(NSString *)searchArgument andCompletion:(GenericFullCompletion)completion {
    
    // query all categories
    NSArray *categories = [Singleton categories];
    NSString *categoriesParam = @"";
    for (Category *category in categories) {
        categoriesParam = [[categoriesParam stringByAppendingString:category.internalBaseClassIdentifier] stringByAppendingString:@","];
    }
    categoriesParam = [categoriesParam stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSString *queryParam = [searchArgument stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *urlParams = [NSString stringWithFormat:@"%@%ld%@%ld%@%@%@%@",
                           [self urlForKey:kServerUrlPartKeyProjectsPage],
                           (long)page,
                           [self urlForKey:kServerUrlPartKeyProjectsPerPage],
                           (long)itemsPerPage,
                           [self urlForKey:kServerUrlPartKeyProjectsCategories],
                           categoriesParam,
                           [self urlForKey:kServerUrlPartKeyProjectsQuery],
                           queryParam];
    NSString *url = [NSString stringWithFormat:@"%@%@", [self urlPartWithRootForKey:kServerUrlPartKeyProjects], urlParams];
    
    [self requestUrl:url withMethod:ServiceMethodPost parameters:nil andCompletion:^(NSDictionary *results, BOOL hasError, NSString *errorMessage) {
        if (hasError) {
            completion(results, YES, errorMessage);
        } else {
            if ([results.allKeys containsObject:@"data"]) {
                NSArray *dataArray = [results objectForKey:@"data"];
                NSMutableArray *projects = [[NSMutableArray alloc] init];
                
                for (NSDictionary *item in dataArray){
                    ProjectShort *project = [[ProjectShort alloc] initWithDictionary:item];
                    [projects addObject:project];
                }
                completion(projects, NO, nil);
            } else {
                completion(results, YES, nil);
            }
        }
    }];
}

+ (void)getProjectWithIdentifier:(NSString *)identifier andCompletion:(GenericFullCompletion)completion {
    
    // setup url for request
    NSString *url = [NSString stringWithFormat:@"%@/%@", [self urlPartWithRootForKey:kServerUrlPartKeyProjects], identifier];
    
    [self requestUrl:url withMethod:ServiceMethodGet parameters:nil andCompletion:^(NSDictionary *results, BOOL hasError, NSString *errorMessage) {
        if (hasError) {
            completion(results, YES, errorMessage);
        } else {
            if ([results.allKeys containsObject:@"data"]) {
                NSDictionary *data = [results objectForKey:@"data"];
                Project *project = [[Project alloc] initWithDictionary:data];
                completion(project, NO, nil);
            } else {
                completion(results, YES, nil);
            }
        }
    }];
}


#pragma mark - Timeline

+ (void)getTimelineEntriesForProjectWithIdentifier:(NSString *)identifier onPage:(NSInteger)page withItemsPerPage:(NSInteger)itemsPerPage andCompletion:(GenericFullCompletion)completion {
    
    // setup page parameters
    NSString *urlParams = [NSString stringWithFormat:@"%@%ld%@%ld",
                           [self urlForKey:kServerUrlPartKeyProjectsPage],
                           (long)page,
                           [self urlForKey:kServerUrlPartKeyProjectsPerPage],
                           (long)itemsPerPage];
    
    // setup url for request
    NSString *url = [NSString stringWithFormat:@"%@/%@%@", [self urlPartWithRootForKey:kServerUrlPartKeyTimeline], identifier, urlParams];
    
    [self requestUrl:url withMethod:ServiceMethodGet parameters:nil andCompletion:^(NSDictionary *results, BOOL hasError, NSString *errorMessage) {
        if (hasError) {
            completion(results, YES, errorMessage);
        } else {
            if ([results.allKeys containsObject:@"data"]) {
                NSArray *dataArray = [results objectForKey:@"data"];
                NSMutableArray *timelineEntries = [[NSMutableArray alloc] init];
                
                for (NSDictionary *item in dataArray){
                    TimelineEntry *entry = [[TimelineEntry alloc] initWithDictionary:item];
                    [timelineEntries addObject:entry];
                }
                completion(timelineEntries, NO, nil);
            } else {
                completion(results, YES, nil);
            }
        }
    }]; 
}

+ (void)deleteEntryFromTimelineWithIdentifier:(NSString *)identifier andCompletion:(GenericFullCompletion)completion {

    // setup url for request
    NSString *url = [NSString stringWithFormat:@"%@/%@", [self urlPartWithRootForKey:kServerUrlPartKeyTimelineDelete], identifier];
    
    [self requestUrl:url withMethod:ServiceMethodDelete parameters:nil andCompletion:^(NSDictionary *results, BOOL hasError, NSString *errorMessage) {
        if (hasError) {
            completion(results, YES, errorMessage);
        } else {
            if ([results.allKeys containsObject:@"message"]) {
                NSString *successMessage = [results valueForKey:@"message"];
                completion(successMessage, NO, nil);
            } else {
                completion(results, YES, nil);
            }
        }
    }];
}

+ (void)insertEntryWithTitle:(NSString *)title description:(NSString *)description andImageString:(NSString *)imageString toProjectWithIdentifier:(NSString *)identifier andCompletion:(GenericFullCompletion)completion {
    // setup url for request
    NSString *url = [NSString stringWithFormat:@"%@/%@", [self urlPartWithRootForKey:kServerUrlPartKeyTimelineInsert], identifier];
    
    // create dictionary with entry input data to be sent to web server
    NSMutableDictionary *entryData = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                     title, @"title",
                                     description, @"description",
                                     nil];
    if (imageString) [entryData setObject:imageString forKey:@"file"];

    [self requestUrl:url withMethod:ServiceMethodPost parameters:entryData andCompletion:^(NSDictionary *results, BOOL hasError, NSString *errorMessage) {
        if (hasError) {
            completion(results, YES, errorMessage);
        } else {
            if ([results.allKeys containsObject:@"data"]) {
                NSDictionary *data = [results objectForKey:@"data"];
                TimelineEntry *entry = [[TimelineEntry alloc] initWithDictionary:data];
                completion(entry, NO, nil);
            } else {
                completion(results, YES, nil);
            }
        }
    }];
}


#pragma mark - Donators

+ (void)getDonatorsForProjectWithIdentifier:(NSString *)identifier andCompletion:(GenericFullCompletion)completion {
    
    // setup url for request
    NSString *url = [NSString stringWithFormat:@"%@/%@/%@", [self urlPartWithRootForKey:kServerUrlPartKeyProject], identifier, [self urlForKey:kServerUrlPartKeyProjectDonators]];
    
    [self requestUrl:url withMethod:ServiceMethodGet parameters:nil andCompletion:^(NSDictionary *results, BOOL hasError, NSString *errorMessage) {
        if (hasError) {
            completion(results, YES, errorMessage);
        } else {
            if ([results.allKeys containsObject:@"data"]) {
                NSDictionary *data = [results objectForKey:@"data"];
                DonatorList *donatorList = [[DonatorList alloc] initWithDictionary:data];
                completion(donatorList, NO, nil);
            } else {
                completion(results, YES, nil);
            }
        }
    }];
}


#pragma mark - Donation

+ (void)generateBilletForDonationOfAmount:(NSNumber *)amount anonymously:(BOOL)anonymous toProjectWithIdentifier:(NSString *)identifier withCompletion:(GenericFullCompletion)completion {

    // setup url for request
    NSString *url = [self urlPartWithRootForKey:kServerUrlPartKeyGenerateBillet];
    
    // create dictionary with user input data to be sent to web server
    NSMutableDictionary *donationData = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                         identifier, @"projectId",
                                         amount, @"amount",
                                         nil];
    if (anonymous) {
        [donationData setObject:@"true" forKey:@"anon"];
    } else {
        [donationData setObject:@"false" forKey:@"anon"];
    }
    
    [self requestUrl:url withMethod:ServiceMethodPost parameters:donationData andCompletion:^(NSDictionary *results, BOOL hasError, NSString *errorMessage) {
        if (hasError) {
            completion(results, YES, errorMessage);
        } else {
            NSString *billetURL = [results valueForKeyPath:@"boletoUrl"];
            completion(billetURL, NO, nil);
        }
    }];
}

+ (void)purchaseAmount:(NSNumber *)amount anonymously:(BOOL)anonymous toProjectWithIdentifier:(NSString *)identifier usingCardHash:(NSString *)cardHash antifraudData:(NSDictionary *)antifraudData andCompletion:(GenericFullCompletion)completion {
    
    // setup url for request
    NSString *url = [self urlPartWithRootForKey:kServerUrlPartKeyPurchase];
    
    // create dictionary with user input data to be sent to web server
    NSMutableDictionary *donationData = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                         identifier, @"projectId",
                                         amount, @"amount",
                                         cardHash, @"cardHash",
                                         nil];
    [donationData addEntriesFromDictionary:antifraudData];

    if (anonymous) {
        [donationData setObject:@"true" forKey:@"anon"];
    } else {
        [donationData setObject:@"false" forKey:@"anon"];
    }
    
    [self requestUrl:url withMethod:ServiceMethodPost parameters:donationData andCompletion:^(NSDictionary *results, BOOL hasError, NSString *errorMessage) {
        if (hasError) {
            completion(results, YES, errorMessage);
        } else {
            completion(results, NO, nil);
        }
    }];
}


#pragma mark - My Projects

+ (void)getMyProjectListWithOptionNumber:(NSInteger)number onPage:(NSInteger)page withItemsPerPage:(NSInteger)itemsPerPage andCompletion:(GenericFullCompletion)completion {

    // setup page parameters
    NSString *urlParams = [NSString stringWithFormat:@"%@%ld%@%ld",
                           [self urlForKey:kServerUrlPartKeyProjectsPage],
                           (long)page,
                           [self urlForKey:kServerUrlPartKeyProjectsPerPage],
                           (long)itemsPerPage];
    
    // setup url for request
    NSString *url = [NSString stringWithFormat:@"%@%@", [self urlPartWithRootForKey:[self urlPartForMyProjectListOptionNumber:number]], urlParams];
    
    [self requestUrl:url withMethod:ServiceMethodGet parameters:nil andCompletion:^(NSDictionary *results, BOOL hasError, NSString *errorMessage) {
        if (hasError) {
            completion(results, YES, errorMessage);
        } else {
            if ([results.allKeys containsObject:@"data"]) {
                NSArray *dataArray = [results objectForKey:@"data"];
                NSMutableArray *projects = [[NSMutableArray alloc] init];
                
                for (NSDictionary *item in dataArray){
                    ProjectShort *project = [[ProjectShort alloc] initWithDictionary:item];
                    [projects addObject:project];
                }
                completion(projects, NO, nil);
            } else {
                completion(results, YES, nil);
            }
        }
    }];
}

+ (NSString *)urlPartForMyProjectListOptionNumber:(NSInteger)number {
    // decide which constant to use depending on project list identifier
    switch (number) {
            
        case CreatedOption:
            return kServerUrlPartKeyMyProjectsCreated;
            
        case DonatedOption:
            return kServerUrlPartKeyMyProjectsDonated;

        case FavoriteOption:
            return kServerUrlPartKeyMyProjectsFavorite;
            
        default:
            return nil;
    }
}


#pragma mark - Activities

+ (void)getUserActivitiesOnPage:(NSInteger)page withItemsPerPage:(NSInteger)itemsPerPage andCompletion:(GenericFullCompletion)completion {
    
    // setup page parameters
    NSString *urlParams = [NSString stringWithFormat:@"%@%ld%@%ld",
                           [self urlForKey:kServerUrlPartKeyProjectsPage],
                           (long)page,
                           [self urlForKey:kServerUrlPartKeyProjectsPerPage],
                           (long)itemsPerPage];
    
    // setup url for request
    NSString *url = [NSString stringWithFormat:@"%@%@", [self urlPartWithRootForKey:kServerUrlPartKeyActivities], urlParams];
    
    [self requestUrl:url withMethod:ServiceMethodGet parameters:nil andCompletion:^(NSDictionary *results, BOOL hasError, NSString *errorMessage) {
        if (hasError) {
            completion(results, YES, errorMessage);
        } else {
            if ([results.allKeys containsObject:@"data"]) {
                NSArray *dataArray = [results objectForKey:@"data"];
                NSMutableArray *activityEntries = [[NSMutableArray alloc] init];
                
                for (NSDictionary *item in dataArray){
                    DonationActivity *activity = [[DonationActivity alloc] initWithDictionary:item];
                    [activityEntries addObject:activity];
                }
                completion(activityEntries, NO, nil);
            } else {
                completion(results, YES, nil);
            }
        }
    }];
}


#pragma mark - Categories

+ (void)getAllCagetoriesWithCompletion:(GenericFullCompletion)completion {
    // setup url for request
    NSString *url = [self urlPartWithRootForKey:kServerUrlPartKeyCategories];
    
    [self requestUrl:url withMethod:ServiceMethodGet parameters:nil andCompletion:^(NSDictionary *results, BOOL hasError, NSString *errorMessage) {
        if (hasError) {
            completion(results, YES, errorMessage);
        } else {
            if ([results.allKeys containsObject:@"data"]) {
                NSArray *dataArray = [results objectForKey:@"data"];
                NSMutableArray *categories = [[NSMutableArray alloc] init];
                
                for (NSDictionary *item in dataArray){
                    Category *category = [[Category alloc] initWithDictionary:item];
                    [categories addObject:category];
                }
                completion(categories, NO, nil);
            } else {
                completion(results, YES, nil);
            }
        }
    }];
}


#pragma mark - States

+ (void)getAllStatesWithCompletion:(GenericFullCompletion)completion {
    // setup url for request
    NSString *url = [self urlPartWithRootForKey:kServerUrlPartKeyStates];
    
    [self requestUrl:url withMethod:ServiceMethodGet parameters:nil andCompletion:^(NSDictionary *results, BOOL hasError, NSString *errorMessage) {
        if (hasError) {
            completion(results, YES, errorMessage);
        } else {
            if ([results.allKeys containsObject:@"data"]) {
                NSArray *dataArray = [results objectForKey:@"data"];
                NSMutableArray *states = [[NSMutableArray alloc] init];
                
                for (NSDictionary *item in dataArray){
                    State *state = [[State alloc] initWithDictionary:item];
                    [states addObject:state];
                }
                completion(states, NO, nil);
            } else {
                completion(results, YES, nil);
            }
        }
    }];
}


#pragma mark - Terms

+ (void)getTermsWithCompletion:(GenericFullCompletion)completion {
    // setup url for request
    NSString *url = [self urlPartWithRootForKey:kServerUrlPartKeyTerms];
    
    [self requestUrl:url withMethod:ServiceMethodGet parameters:nil andCompletion:^(NSDictionary *results, BOOL hasError, NSString *errorMessage) {
        if (hasError) {
            completion(results, YES, errorMessage);
        } else {
            if ([results.allKeys containsObject:@"data"]) {
                NSDictionary *data = [results objectForKey:@"data"];
                Terms *terms = [[Terms alloc] initWithDictionary:data];
                completion(terms, NO, nil);
            } else {
                completion(results, YES, nil);
            }
        }
    }];
}


#pragma mark - About

+ (void)getAboutWithCompletion:(GenericFullCompletion)completion {
    // setup url for request
    NSString *url = [self urlPartWithRootForKey:kServerUrlPartKeyAbout];
    
    [self requestUrl:url withMethod:ServiceMethodGet parameters:nil andCompletion:^(NSDictionary *results, BOOL hasError, NSString *errorMessage) {
        if (hasError) {
            completion(results, YES, errorMessage);
        } else {
            if ([results.allKeys containsObject:@"data"]) {
                NSDictionary *data = [results objectForKey:@"data"];
                About *about = [[About alloc] initWithDictionary:data];
                completion(about, NO, nil);
            } else {
                completion(results, YES, nil);
            }
        }
    }];
}


#pragma mark - Favorite/Unfavorite

+ (void)toggleFavoriteFromProjectWithIdentifier:(NSString *)identifier andCompletion:(GenericFullCompletion)completion {
    // setup url for request
    NSString *url = [NSString stringWithFormat:@"%@/%@", [self urlPartWithRootForKey:kServerUrlPartKeyFavoriteToggle], identifier];
    
    [self requestUrl:url withMethod:ServiceMethodPost parameters:nil andCompletion:^(NSDictionary *results, BOOL hasError, NSString *errorMessage) {
        if (hasError) {
            completion(results, YES, errorMessage);
        } else {
            NSString *successMessage = [results valueForKeyPath:@"message"];
            completion(successMessage, NO, nil);
        }
    }];
}


#pragma mark - Base Methods

+ (void)requestUrl:(NSString *)url withMethod:(ServiceMethod)method parameters:(NSDictionary *)parameters andCompletion:(FullCompletion)completion {
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializerWithWritingOptions:NSJSONWritingPrettyPrinted];
    
    NSMutableIndexSet *setCodes = [[NSMutableIndexSet alloc] init];
    [setCodes addIndex:200];
    manager.responseSerializer.acceptableStatusCodes = setCodes;
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html", @"application/json", nil];
    
    // User ID
    if ([Singleton isThereLoggedUserID]) {
        [manager.requestSerializer setValue:[Singleton loggedUserID] forHTTPHeaderField:@"X-User-Id"];
    }
    // User Authentication Token
    if ([Singleton isThereLoggedUserAuthToken]) {
        [manager.requestSerializer setValue:[Singleton loggedUserAuthToken] forHTTPHeaderField:@"X-Auth-Token"];
    }
    
    void (^ successBlock)(NSURLSessionDataTask * _Nonnull task, id _Nullable responseObject) = ^(NSURLSessionDataTask * _Nonnull task, id _Nullable responseObject) {
        
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            NSDictionary *response = responseObject;
            if ([response.allKeys containsObject:@"status"]) {
                if ([[response objectForKey:@"status"] isEqualToString:@"success"]) {
                    completion(response, NO, nil);
                } else { // status == error
                    completion(response, YES, [response objectForKey:@"message"]);
                }
            } else {
                completion(response, YES, nil);
            }
        } else {
            completion(nil, YES, nil);
        }
    };

    void (^ failureBlock)(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) = ^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        if ([[[error userInfo] objectForKey:AFNetworkingOperationFailingURLResponseErrorKey] statusCode] == 401) {
            NSDictionary *errorDictionary = [NSDictionary dictionaryWithObject:[NSNumber numberWithInteger:401] forKey:@"statusCode"];
            completion(errorDictionary, YES, [error localizedDescription]);
        } else {
            completion(nil, YES, [error localizedDescription]);
        }
    };
    
    if (method == ServiceMethodPost) {
        [manager POST:url parameters:parameters progress:nil success:successBlock failure:failureBlock];
    } else if (method == ServiceMethodGet) {
        [manager GET:url parameters:nil progress:nil success:successBlock failure:failureBlock];
    } else if (method == ServiceMethodDelete) {
        [manager DELETE:url parameters:parameters success:successBlock failure:failureBlock];
    }
}

@end

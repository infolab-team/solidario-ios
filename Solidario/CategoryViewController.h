//
//  CategoryViewController.h
//  Solidario
//
//  Created by Paula Vasconcelos Gueiros on 22/8/16.
//  Copyright © 2016 Paula Vasconcelos Gueiros. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CategoryViewController : UIViewController

@property (nonatomic) BOOL showCloseButton;

@end

//
//  IconMenuTableViewCell.m
//  Solidario
//
//  Created by Paula Vasconcelos Gueiros on 24/8/16.
//  Copyright © 2016 Paula Vasconcelos Gueiros. All rights reserved.
//

#import "IconMenuTableViewCell.h"

@implementation IconMenuTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

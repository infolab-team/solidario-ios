//
//  AntifraudDataTableViewController.m
//  Solidario
//
//  Created by Pan on 02/12/16.
//  Copyright © 2016 Paula Vasconcelos Gueiros. All rights reserved.
//

#import "AntifraudDataTableViewController.h"
#import "DonatingAnimationViewController.h"
#import "DonationValueTableViewController.h"
#import "DonationDataTableViewController.h"
#import "CompleteCardInfoViewController.h"

#import "CustomTextField.h"
#import "Utils.h"
#import "UIColor+Utils.h"
#import "NSString+Utils.h"

#import "Address.h"
#import "AntifraudData.h"
#import "User.h"

#import "Service.h"

#import "Singleton.h"

@interface AntifraudDataTableViewController ()

@property (weak, nonatomic) IBOutlet UIBarButtonItem *confirmationButton;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIView *headerView;

@property (weak, nonatomic) IBOutlet UITextField *fullName;
@property (weak, nonatomic) IBOutlet CustomTextField *cpf;
@property (weak, nonatomic) IBOutlet UITextField *email;
@property (weak, nonatomic) IBOutlet CustomTextField *phoneNumber;
@property (weak, nonatomic) IBOutlet CustomTextField *cep;
@property (weak, nonatomic) IBOutlet UITextField *streetName;
@property (weak, nonatomic) IBOutlet UITextField *streetNumber;
@property (weak, nonatomic) IBOutlet UITextField *complement;
@property (weak, nonatomic) IBOutlet UITextField *neighborhood;

@property (weak, nonatomic) IBOutlet UISwitch *agreementSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *anonymousSwitch;

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (strong, nonatomic) NSArray *textFields;
@property (nonatomic) BOOL showAntifraudData;

@end


@implementation AntifraudDataTableViewController

static const CGFloat headerViewDefaultHeight = 240.f;

#pragma mark - Initialization

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setup];
}

- (void)setup {
    // project title
    self.titleLabel.text = self.project.title;

    // goal and donation values
    self.donationGoalView.goalValueLabel.text = self.project.goal;
    
    NSNumberFormatter *currencyFormatter = [NSNumberFormatter new];
    [currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    NSLocale *currentLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"pt_BR"];
    [currencyFormatter setLocale:currentLocale];
    
    self.donationValueView.donationValueLabel.text = [currencyFormatter stringFromNumber:[NSNumber numberWithFloat:self.donation.value/100.f]];

    // banner image
    self.donationGoalView.mainImageView.image = self.mainImage;
    
    // gradient layer over banner
    CGFloat viewHeight, viewWidth;
    viewWidth = [[UIScreen mainScreen]bounds].size.width - (2 * defaultViewPadding);
    viewHeight = viewWidth * bannerViewProportion;
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = CGRectMake(0, 0, viewWidth, viewHeight);
    gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor defaultColor:DefaultColorBannerShadowTop] CGColor], (id)[[UIColor defaultColor:DefaultColorBannerShadowTop] CGColor], (id)[[UIColor blackColor] CGColor], nil];
    [self.donationGoalView.mainImageView.layer insertSublayer:gradient atIndex:0];
    
    // header view height
    if (IS_IPHONE_6_7_PLUS) { // iPhone 5.5'
        [self.headerView setFrame:CGRectMake(self.headerView.frame.origin.x, self.headerView.frame.origin.y, self.headerView.frame.size.width, headerViewDefaultHeight + 20.f)];
    } else if (IS_IPHONE_6_7) { // iPhone 4.7'
        [self.headerView setFrame:CGRectMake(self.headerView.frame.origin.x, self.headerView.frame.origin.y, self.headerView.frame.size.width, headerViewDefaultHeight)];
    } else { // iPhone 4'
        [self.headerView setFrame:CGRectMake(self.headerView.frame.origin.x, self.headerView.frame.origin.y, self.headerView.frame.size.width, headerViewDefaultHeight - 30.f)];
    }
    
    // text fields
    self.fullName.delegate = self;
    self.email.delegate = self;
    self.streetName.delegate = self;
    self.streetNumber.delegate = self;
    self.complement.delegate = self;
    self.neighborhood.delegate = self;
    
    self.textFields = @[self.fullName, self.cpf, self.email, self.phoneNumber, self.cep, self.streetName, self.streetNumber, self.complement, self.neighborhood];
    for (UITextField *textField in self.textFields) {
        [self addInputAccessoryViewToTextField:textField];
    }
    
    // pre-fill text fields with logged user data
    User *loggedUser = [Singleton loggedUser];
    self.fullName.text = [NSString stringWithFormat:@"%@ %@", loggedUser.name, loggedUser.surname];
    self.email.text = loggedUser.email;
    
    // confirmation bar button
    if (self.donation.paymentType == PaymentTypeBankBillet) {
        [self.confirmationButton setTitle:@"Gerar Boleto"];
    }
    
    // setup selector for cep text field event editing changed
    [self.cep addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    // decide on showing antifraud input fields
    self.showAntifraudData = (ANTIFRAUD_PAYMENT_ACTIVATED && self.donation.paymentType == PaymentTypeCreditCard);
}

- (void)addInputAccessoryViewToTextField:(UITextField *)textField {
    //Next Text Field Button
    UINavigationBar *inputAccessoryView = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0,
                                                                                            self.view.frame.size.width, INPUT_ACCESSORY_VIEW_HEIGHT)];
    UIBarButtonItem *barButtonNext = [[UIBarButtonItem alloc] initWithTitle:@"Próximo"
                                                                      style:UIBarButtonItemStyleDone target:self
                                                                     action:@selector(nextTextField:)];
    
    UIBarButtonItem *barButtonPrevious = [[UIBarButtonItem alloc] initWithTitle:@"Anterior"
                                                                          style:UIBarButtonItemStyleDone target:self
                                                                         action:@selector(previousTextField:)];
    barButtonNext.tag = textField.tag+1;
    barButtonPrevious.tag = textField.tag-1;
    if (barButtonPrevious.tag < 0) [barButtonPrevious setEnabled:NO];
    if (barButtonNext.tag > self.textFields.count) [barButtonNext setEnabled:NO];
    UINavigationItem *navItem = [[UINavigationItem alloc] initWithTitle:@""];
    navItem.leftBarButtonItem = barButtonPrevious;
    navItem.rightBarButtonItem = barButtonNext;
    [inputAccessoryView setItems:@[navItem]];
    [textField setInputAccessoryView:inputAccessoryView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // activityIndicator
    UIWindow *frontWindow = [[UIApplication sharedApplication] keyWindow];
    [frontWindow addSubview:self.activityIndicator];
    self.activityIndicator.center = frontWindow.center;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    // stop activityIndicator on cases in which it's still animating
    [self.activityIndicator stopAnimating];
    [self.activityIndicator removeFromSuperview];
}


#pragma mark - UITableView Delegate and Data Source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.showAntifraudData) {
        return [super tableView:tableView numberOfRowsInSection:section];
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.showAntifraudData) {
        return [super tableView:tableView heightForRowAtIndexPath:indexPath];
    }
    return 0;
}


#pragma mark - UITextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidChange:(CustomTextField *)textField  {

    if (textField.text.length == textField.maxLength) {
        NSString *cep = [NSString plainStringFromString:textField.text];
        [self.activityIndicator startAnimating];
        
        [Service getAddressWithCEP:cep andCompletion:^(id results, BOOL hasError, NSString *errorMessage) {
            [self.activityIndicator stopAnimating];
            if (hasError) {
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Erro"
                                                                               message:nil
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"OK"
                                                                   style:UIAlertActionStyleDefault
                                                                 handler:^(UIAlertAction * action) {}];
                [alert addAction:okButton];
                
                self.streetName.text = @"";
                self.neighborhood.text = @"";
//                if (errorMessage != nil) {
//                    [alert setMessage:errorMessage];
//                } else {
                    [alert setMessage:@"Endereço não encontrado para o CEP."];
//                }
                [self presentViewController:alert animated:YES completion:nil];
                return;
            } else {
                Address *addressForCEP = (Address *)results;
                self.streetName.text = addressForCEP.logradouro;
                self.neighborhood.text = addressForCEP.bairro;
            }
        }];
    }
}


#pragma mark - UIScrollView Delegate

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [self.view endEditing:YES];
}


#pragma mark - Button Actions

- (void)previousTextField:(UIBarButtonItem *)sender {
    if (sender.tag >= 0 && sender.tag < self.textFields.count) {
        [self focusTextFieldWithTag:sender.tag];
    } else {
        [sender setEnabled:NO];
    }
}

- (void)nextTextField:(UIBarButtonItem *)sender {
    if (sender.tag >= 0 && sender.tag < self.textFields.count) {
        [self focusTextFieldWithTag:sender.tag];
    } else {
        [sender setEnabled:NO];
    }
}

- (void)focusTextFieldWithTag:(NSInteger)tag {
    for (UITextField *textField in self.textFields) {
        if (textField.tag == tag) {
            [textField becomeFirstResponder];
        }
    }
}


#pragma mark - Purchase Validation and Confirmation

- (IBAction)confirmDonation:(id)sender {
    [self.view endEditing:YES];
    
    if (self.showAntifraudData) {
        [self performInputValidations];
    } else {
        [self checkIfTermsAccepted];
    }
}

- (void)performInputValidations {

    // alert setup
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Atenção"
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"OK"
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action) {}];
    [alert addAction:okButton];
    
    
    // mandatory input confirmation
    __block BOOL allFilled = YES;
    [self.textFields enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([((UITextField *)obj).text isEqualToString:@""]) {
            if ((!((UITextField *)obj == self.streetNumber) && !((UITextField *)obj == self.complement))) {
                allFilled = NO;
                *stop = YES;
            }
        }
    }];
    if (!allFilled) {
        [alert setMessage:@"Preencha todos os campos obrigatórios antes de prosseguir."];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    
    // valid CPF
    if (![NSString isValidCPF:[NSString plainStringFromString:self.cpf.text]]) {
        [alert setMessage:@"Número de CPF inválido."];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    
    // valid email
    if (![NSString isValidEmail:self.email.text]) {
        [alert setMessage:@"Email inválido."];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    
    // valid phone number
    NSUInteger phoneNumberLength = [self.phoneNumber.text length];
    if (phoneNumberLength < 14 || phoneNumberLength > 15) {
        [alert setMessage:@"Número de telefone inválido."];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    
    // valid CEP
    if ([self.cep.text length] != self.cep.maxLength) {
        [alert setMessage:@"CEP inválido."];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    
    [self checkIfTermsAccepted];
}

- (void)checkIfTermsAccepted {
    
    // check if accepted terms and policy
    if (!self.agreementSwitch.isOn) {
        // alert setup
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Atenção"
                                                                       message:@"Você deve aceitar os Termos de Uso e Políticas de Privacidade."
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"OK"
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * action) {}];
        [alert addAction:okButton];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    
    [self confirmPurchase];
}

- (void)confirmPurchase {
    
    NSString *confirmationMessageComplement = (self.donation.paymentType == PaymentTypeBankBillet) ?
        @"gerar um boleto de": @"doar";

    // purchase confirmation
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Confirmação"
                                                                   message:[NSString stringWithFormat:@"Você deseja %@ %@ para o projeto \"%@\"?", confirmationMessageComplement, self.donationValueView.donationValueLabel.text, self.titleLabel.text]
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"Sim"
                                                       style:UIAlertActionStyleDestructive
                                                     handler:^(UIAlertAction * action) {
                                                         [self setupFinalUserData];
                                                     }];
    UIAlertAction *cancelButton = [UIAlertAction actionWithTitle:@"Cancelar"
                                                           style:UIAlertActionStyleCancel
                                                         handler:^(UIAlertAction * action) {}];
    [alert addAction:okButton];
    [alert addAction:cancelButton];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void) setupFinalUserData {

    NSDictionary *antifraudDataDict;
    if (self.showAntifraudData) {
        NSString *phoneNumberPrefix = [self.phoneNumber.text substringToIndex:3];
        NSString *phoneNumberEnding = [self.phoneNumber.text substringFromIndex:5];
        
        antifraudDataDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                             self.fullName.text, @"name",
                             self.email.text, @"email",
                             [NSString plainStringFromString:self.cpf.text], @"cpf",
                             [NSString plainStringFromString:self.cep.text], @"cep",
                             self.neighborhood.text, @"bairro",
                             self.streetName.text, @"rua",
                             self.streetNumber.text, @"numero",
                             [NSString plainStringFromString:phoneNumberEnding], @"telefone",
                             [NSString plainStringFromString:phoneNumberPrefix], @"ddd",
                             nil];
    } else {
        antifraudDataDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                             @"", @"name",
                             @"", @"email",
                             @"", @"cpf",
                             @"", @"cep",
                             @"", @"bairro",
                             @"", @"rua",
                             @"", @"numero",
                             @"", @"telefone",
                             @"", @"ddd",
                             nil];
    }

    AntifraudData *antifraudData = [[AntifraudData alloc] initWithDictionary:antifraudDataDict];
    self.donation.antifraudData = antifraudData;
    
    self.donation.isAnonymous = self.anonymousSwitch.isOn;
    
    [self performSegueWithIdentifier:@"modalToDonatingAnimation" sender:self];
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    UINavigationController *navController = (UINavigationController *)[segue destinationViewController];
    
    if ([segue.identifier isEqualToString:@"modalToDonatingAnimation"]) {
        DonatingAnimationViewController *donatingAnimationViewController = (DonatingAnimationViewController *)[navController topViewController];
        donatingAnimationViewController.project = self.project;
        donatingAnimationViewController.donation = self.donation;
    }
}

- (IBAction)returnToPreviousPage:(UIBarButtonItem *)sender {
    // check for previous controller in navigation controller
    
    NSUInteger nControllers = [[self.navigationController viewControllers] count];
    UIViewController *previousViewController = [self.navigationController viewControllers][nControllers-2];
    
    if ([previousViewController isKindOfClass:[DonationValueTableViewController class]]) {
        [self performSegueWithIdentifier:@"unwindToDonationValue" sender:self];  
    } else if ([previousViewController isKindOfClass:[DonationDataTableViewController class]]) {
        [self performSegueWithIdentifier:@"unwindToDonationData" sender:self];
    } else if ([previousViewController isKindOfClass:[CompleteCardInfoViewController class]]) {
        [self performSegueWithIdentifier:@"unwindToCompleteCardInfo" sender:self];
    }
}

- (IBAction)unwindToBeforeTerms:(UIStoryboardSegue*)unwindSegue {
    
}

@end

//
//  DonorListTableViewController.h
//  Solidario
//
//  Created by Pan on 03/05/17.
//  Copyright © 2017 Paula Vasconcelos Gueiros. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Project.h"
#import "ProjectShort.h"

@interface DonorListTableViewController : UITableViewController

@property (strong, nonatomic) Project *project;
@property (strong, nonatomic) ProjectShort *projectShort;

@end

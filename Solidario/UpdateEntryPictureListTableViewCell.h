//
//  UpdateEntryPictureListTableViewCell.h
//  Solidario
//
//  Created by Pan on 16/02/17.
//  Copyright © 2017 Paula Vasconcelos Gueiros. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UpdateEntryPictureListTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *pictureImageView;
@property (weak, nonatomic) IBOutlet UIButton *removePictureButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *pictureWidthConstraint;

- (void)setImageViewWidthFromProportion:(CGFloat)proportion;

@end

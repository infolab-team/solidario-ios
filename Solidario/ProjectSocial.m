//
//  ProjectSocial.m
//
//  Created by Paula Vasconcelos on 02/05/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "ProjectSocial.h"


NSString *const kProjectSocialTwitter = @"twitter";
NSString *const kProjectSocialFacebook = @"facebook";


@interface ProjectSocial ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation ProjectSocial

@synthesize twitter = _twitter;
@synthesize facebook = _facebook;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.twitter = [self objectOrNilForKey:kProjectSocialTwitter fromDictionary:dict];
            self.facebook = [self objectOrNilForKey:kProjectSocialFacebook fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.twitter forKey:kProjectSocialTwitter];
    [mutableDict setValue:self.facebook forKey:kProjectSocialFacebook];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.twitter = [aDecoder decodeObjectForKey:kProjectSocialTwitter];
    self.facebook = [aDecoder decodeObjectForKey:kProjectSocialFacebook];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_twitter forKey:kProjectSocialTwitter];
    [aCoder encodeObject:_facebook forKey:kProjectSocialFacebook];
}

- (id)copyWithZone:(NSZone *)zone
{
    ProjectSocial *copy = [[ProjectSocial alloc] init];
    
    if (copy) {

        copy.twitter = [self.twitter copyWithZone:zone];
        copy.facebook = [self.facebook copyWithZone:zone];
    }
    
    return copy;
}


@end

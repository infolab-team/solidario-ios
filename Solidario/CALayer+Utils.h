//
//  CALayer+Utils.h
//  Solidario
//
//  Created by Paula Vasconcelos Gueiros on 18/3/16.
//  Copyright © 2016 paulinhavgueiros. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import <UIKit/UIKit.h>

@interface CALayer(Utils)

// This assigns a UIColor to borderColor.
@property (nonatomic, assign) UIColor* borderUIColor;

@end

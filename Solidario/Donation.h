//
//  Donation.h
//
//  Created by Paula Vasconcelos on 05/05/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

typedef enum {
    PaymentTypeNone = -1,
    PaymentTypeBankBillet = 0,
    PaymentTypeCreditCard = 1
} PaymentType;

#import <Foundation/Foundation.h>

@class PagarMeCreditCard, AntifraudData;

@interface Donation : NSObject <NSCoding>

@property (nonatomic) PaymentType paymentType;
@property (nonatomic, strong) NSString *cardHash;
@property (nonatomic, assign) double value;
@property (nonatomic, strong) PagarMeCreditCard *pagarMeCreditCard;
@property (nonatomic, strong) AntifraudData *antifraudData;
@property (nonatomic, assign) BOOL isAnonymous;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithValue:(double)value;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end

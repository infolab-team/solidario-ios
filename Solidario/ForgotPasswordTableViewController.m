//
//  ForgotPasswordTableViewController.m
//  Solidario
//
//  Created by Paula Vasconcelos Gueiros on 19/8/16.
//  Copyright © 2016 Paula Vasconcelos Gueiros. All rights reserved.
//

#import "ForgotPasswordTableViewController.h"
#import "ActionButton.h"
#import "NSString+Utils.h"
#import "Service.h"

@interface ForgotPasswordTableViewController ()

@property (weak, nonatomic) IBOutlet UITextField *email;
@property (weak, nonatomic) IBOutlet ActionButton *resetButton;

@end


@implementation ForgotPasswordTableViewController


#pragma mark - Initialization

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setup];
}

- (void)setup {
    self.email.delegate = self;
}


#pragma mark - Navigation

- (IBAction)reset:(id)sender {
    [self.view endEditing:YES];
    NSLog(@"👤 User pressed login button");
    [self performInputValidations];
}

- (void)performInputValidations {
    
    // alert setup
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Atenção"
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"OK"
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action) {}];
    [alert addAction:okButton];
    
    // mandatory input confirmation
    if ([self.email.text isEqualToString:@""]) {
        [alert setMessage:@"Informe o email."];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    
    // valid email
    if (![NSString isValidEmail:self.email.text]) {
        [alert setMessage:@"Email inválido."];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }

    [self proceedWithReset];
}

- (void)proceedWithReset {
    // alert setup
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleAlert];
    __block UIAlertAction *okButton;
    
    [self.resetButton startLoadingAnimated];
    
    [Service resetPasswordForEmail:self.email.text withCompletion:^(id results, BOOL hasError, NSString *errorMessage) {
        [self.resetButton stopLoading];
        
        if (hasError) {
            [alert setTitle:@"Erro"];
            NSLog(@"❌ Password reset error: %@", errorMessage);
//            if (errorMessage != nil) {
//                [alert setMessage:errorMessage];
//            } else {
                [alert setMessage:@"Não foi possível enviar email de redefinição."];
//            }
            okButton = [UIAlertAction actionWithTitle:@"OK"
                                                style:UIAlertActionStyleDefault
                                              handler:nil];
            [alert addAction:okButton];
            [self presentViewController:alert animated:YES completion:nil];
        } else {
            [alert setTitle:@"Sucesso"];
            okButton = [UIAlertAction actionWithTitle:@"OK"
                                                style:UIAlertActionStyleDefault
                                              handler:^(UIAlertAction * action) {
                                                  [self.navigationController popViewControllerAnimated:YES];
                                              }];
            [alert addAction:okButton];
            if ([results isKindOfClass:[NSString class]]) {
                [alert setMessage:(NSString *)results];
            }
            NSLog(@"✅ Password reset mail sent");
            [self presentViewController:alert animated:YES completion:nil];
        }
    }];
}


#pragma mark - UITextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}


#pragma mark - UIScrollView Delegate

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [self.view endEditing:YES];
}

@end

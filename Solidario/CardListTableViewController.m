//
//  CardListTableViewController.m
//  Solidario
//
//  Created by Pan on 21/02/17.
//  Copyright © 2017 Paula Vasconcelos Gueiros. All rights reserved.
//

#import "CardListTableViewController.h"
#import "CardListTableViewCell.h"
#import "NSArray+Utils.h"

@interface CardListTableViewController ()

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UIView *footerView;
    
@property (strong, nonatomic) NSMutableArray <NSDictionary *> *creditCards;

@end

@implementation CardListTableViewController
    
static const CGFloat cardCellHeight = 52.f;
static const CGFloat emptyCellHeight = 108.f;

    
#pragma mark - Initialization
    
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setup];
}
    
- (void)setup {
#warning call service fetch cards
    // initialize credit card list
    //self.creditCards = [NSArray array];
    self.creditCards = [[NSArray loadPlistWithName:@"CreditCards"] mutableCopy];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // setup activityIndicator position
    UIWindow *frontWindow = [[UIApplication sharedApplication] keyWindow];
    [frontWindow addSubview:self.activityIndicator];
    self.activityIndicator.center = frontWindow.center;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    // stop activityIndicator on cases in which it's still animating
    [self.activityIndicator stopAnimating];
    [self.activityIndicator removeFromSuperview];
}

    
#pragma mark - UITableView Delegate and Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if ([self.creditCards count]) {
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        [self.footerView setHidden:NO];
    } else {
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [self.footerView setHidden:YES];
    }
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.creditCards count] ? [self.creditCards count] : 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [self.creditCards count] ? cardCellHeight : emptyCellHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (![self.creditCards count]) {
        return [tableView dequeueReusableCellWithIdentifier:@"noCardsCell"];
    }
    
    CardListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cardCell" forIndexPath:indexPath];
    
    NSDictionary *creditCard = self.creditCards[indexPath.row];
    cell.flagImageView.image = [UIImage imageNamed:[creditCard valueForKeyPath:@"flag"]];
    
#warning optimize with pagarme properties
    if (cell.flagImageView.image == nil) {
        cell.flagImageView.image = [UIImage imageNamed:@"Generic Card"];
    }
    NSString *cardNumber = [creditCard valueForKeyPath:@"cardNumber"];
    cell.cardNumberEndingLabel.text = [cardNumber substringWithRange:NSMakeRange([cardNumber length] - 4, 4)];
    [cell.isMainLabel setHidden:![[creditCard valueForKeyPath:@"isMain"] boolValue]];
    
    return cell;
}
    
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Atenção"
                                                                   message:@"Tem certeza que excluir deletar o cartão?"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"OK"
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action) {
                                                         [self deleteCardEntryAtIndexPath:indexPath];
                                                     }];
    [alert addAction:okButton];
    [self presentViewController:alert animated:YES completion:nil];
}

- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath {
    return @"Excluir";
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:[tableView indexPathForSelectedRow] animated:YES];
    if (self.userCameFromDonationPage) {
        // fetch selected card and post notification to donation page
        NSDictionary *selectedCard = [self.creditCards objectAtIndex:indexPath.row];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"cardWasSelectedForDonation" object:selectedCard];
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    } else {
        [self performSegueWithIdentifier:@"pushToCard" sender:self];
    }
}
    
    
#pragma mark - Helper Methods
    
- (void)deleteCardEntryAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"❌ User deleted card %ld", indexPath.row + 1);
    
    NSDictionary *removedCard = [self.creditCards objectAtIndex:indexPath.row];
    [self.creditCards removeObject:removedCard];
#warning call service remove card
    if ([[removedCard objectForKey:@"isMain"] boolValue] && [self.creditCards count]) {
        NSMutableDictionary *newMainCard = [[self.creditCards objectAtIndex:0] mutableCopy];
        [newMainCard setObject:@YES forKey:@"isMain"];
        [self.creditCards replaceObjectAtIndex:0 withObject:newMainCard];
    }
    
    [self.tableView reloadData];
}


#pragma mark - Navigation

- (IBAction)unwindToCardList:(UIStoryboardSegue*)unwindSegue {
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"pushToCard"]) {
        UIViewController *vc = [segue destinationViewController];
        [vc.navigationItem setLeftBarButtonItem:nil];
    }
}

@end

//
//  CategoryMenuButton.m
//  Solidario
//
//  Created by Pan on 26/05/17.
//  Copyright © 2017 Paula Vasconcelos Gueiros. All rights reserved.
//

#import "CategoryMenuButton.h"

@interface CategoryMenuButton()

@property (strong, nonatomic) NSLayoutConstraint *widthConstraint;
@property (strong, nonatomic) UIImageView *selectedImageView;

@property (nonatomic) CGFloat widthConstraintConstant;

@end


@implementation CategoryMenuButton

#pragma mark - Initialization

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    self.widthConstraintConstant = (self.selected) ? self.frame.size.width : 0.f;
    [self setupSubviews];
    return self;
}

- (void)setupSubviews {
    
    self.translatesAutoresizingMaskIntoConstraints = NO;
    
    self.selectedImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"categoryBackgroundActive"]];
    self.selectedImageView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.selectedImageView setUserInteractionEnabled:NO];
    [self addSubview:self.selectedImageView];
    
    NSLayoutConstraint *centerHorizontallyConstraint = [NSLayoutConstraint constraintWithItem:self.selectedImageView
                                                                                    attribute:NSLayoutAttributeCenterX
                                                                                    relatedBy:NSLayoutRelationEqual
                                                                                       toItem:self
                                                                                    attribute:NSLayoutAttributeCenterX
                                                                                   multiplier:1.0
                                                                                     constant:0];
    
    NSLayoutConstraint *centerVerticallyConstraint = [NSLayoutConstraint constraintWithItem:self.selectedImageView
                                                                                  attribute:NSLayoutAttributeCenterY
                                                                                  relatedBy:NSLayoutRelationEqual
                                                                                     toItem:self
                                                                                  attribute:NSLayoutAttributeCenterY
                                                                                 multiplier:1.0
                                                                                   constant:0];
    [self addConstraints:[NSArray arrayWithObjects:centerHorizontallyConstraint, centerVerticallyConstraint, nil]];
    
    
    self.widthConstraint = [NSLayoutConstraint constraintWithItem:self.selectedImageView
                                                        attribute:NSLayoutAttributeWidth
                                                        relatedBy:NSLayoutRelationEqual
                                                           toItem:nil
                                                        attribute:NSLayoutAttributeNotAnAttribute
                                                       multiplier:1.0
                                                         constant:self.widthConstraintConstant];
    
    NSLayoutConstraint *aspectRatioConstraint = [NSLayoutConstraint constraintWithItem:self.selectedImageView
                                                                             attribute:NSLayoutAttributeHeight
                                                                             relatedBy:NSLayoutRelationEqual
                                                                                toItem:self.selectedImageView
                                                                             attribute:NSLayoutAttributeWidth
                                                                            multiplier:1.0
                                                                              constant:0];
    [self.selectedImageView addConstraints:[NSArray arrayWithObjects:aspectRatioConstraint, self.widthConstraint, nil]];
}


#pragma mark - Delegate Methods

- (void)setSelected:(BOOL)selected {
    [super setSelected:selected];
    
    // disable clipsToBounds to allow button selection animation beyond button bounds
    [self setClipsToBounds:NO];

    self.widthConstraintConstant = (self.selected) ? self.frame.size.width : 0.f;
    self.widthConstraint.constant = self.widthConstraintConstant;
    [self.selectedImageView setNeedsLayout];
    
    [UIView animateWithDuration:0.3f animations:^{
        [self.selectedImageView layoutIfNeeded];
        if (self.selected) self.selectedImageView.transform = CGAffineTransformMakeScale(1.1f, 1.1f);
        
    } completion:^(BOOL finished) {
        if (self.selected) {
            [UIView animateWithDuration:0.2f animations:^{
                self.selectedImageView.transform = CGAffineTransformIdentity;
            } completion:^(BOOL finished) {
                [self setUserInteractionEnabled:YES];
            }];
        } else {
            [self setUserInteractionEnabled:YES];
        }
    }];
}

@end

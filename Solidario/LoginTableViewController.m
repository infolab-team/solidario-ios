//
//  LoginTableViewController.m
//  Solidario
//
//  Created by Paula Vasconcelos Gueiros on 18/8/16.
//  Copyright © 2016 Paula Vasconcelos Gueiros. All rights reserved.
//

#import "LoginTableViewController.h"
#import "MainViewController.h"
#import "Singleton.h"
#import "Service.h"
#import "ActionButton.h"
#import "User.h"
#import "NSString+Utils.h"

@interface LoginTableViewController ()

@property (weak, nonatomic) IBOutlet UITextField *email;
@property (weak, nonatomic) IBOutlet UITextField *password;
@property (weak, nonatomic) IBOutlet ActionButton *loginButton;
@property (weak, nonatomic) IBOutlet UISwitch *keepConnectedSwitch;

@property (strong, nonatomic) NSArray *textFields;

@end

@implementation LoginTableViewController


#pragma mark - Initialization

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setup];
}

- (void)setup {
    self.email.delegate = self;
    self.password.delegate = self;
    self.textFields = @[self.email, self.password];
}


#pragma mark - Navigation

- (IBAction)login:(id)sender {
    [self.view endEditing:YES];
    NSLog(@"👤 User pressed login button");
    [self performInputValidations];
}

- (void)performInputValidations {
    
    // alert setup
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Atenção"
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"OK"
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action) {}];
    [alert addAction:okButton];
    
    
    // mandatory input confirmation
    __block BOOL allFilled = YES;
    [self.textFields enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([((UITextField *)obj).text isEqualToString:@""]) {
            allFilled = NO;
            *stop = YES;
        }
    }];
    if (!allFilled) {
        [alert setMessage:@"Preencha todos os campos antes de prosseguir."];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    
    // valid email
    if (![NSString isValidEmail:self.email.text]) {
        [alert setMessage:@"Email inválido."];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    
    // valid password length
    if ([self.password.text length] < 6) {
        [alert setMessage:@"Senha deve ter pelo menos seis caracteres."];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    
    [self proceedWithLogin];
}

- (void)proceedWithLogin {
    [self.loginButton startLoadingAnimated];

    [Service loginWithEmail:self.email.text password:self.password.text andCompletion:^(id results, BOOL hasError, NSString *errorMessage) {
        
        [self.loginButton stopLoading];
        
        if (hasError) {
            // alert setup
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Erro"
                                                                           message:nil
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"OK"
                                                               style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction * action) {}];
            [alert addAction:okButton];
            
//            if (errorMessage != nil) {
//                [alert setMessage:errorMessage];
//            } else {
                [alert setMessage:@"Não foi possível efetuar o login."];
//            }
            [self presentViewController:alert animated:YES completion:nil];
            return;
        } else {
            // check switch to decide on saving User object through app sessions
            [Singleton loginWithUser:(User *)results withFacebookEnabled:NO andPersistent:self.keepConnectedSwitch.isOn];
            NSLog(@"✅ Login successful");
            [self proceedAfterLogin];
        }
    }];
}
    
- (void)proceedAfterLogin {
    if ([Singleton isDonating]) {
        [self performSegueWithIdentifier:@"unwindToBeforeDonationValue" sender:self];
    } else {
        [self instantiateSettingsWithViewControllerIdentifier:@"settingsNavigationController"];
    }
}

- (void)instantiateSettingsWithViewControllerIdentifier:(NSString *)viewControllerIdentifier {
    NSString *storyboardIdentifier = @"Settings";
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardIdentifier bundle:nil];
    UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:viewControllerIdentifier];
    
    [self presentViewController:navigationController animated:YES completion:nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"unwindFromLoginToAccount"]) {
        [self.view endEditing:YES];
    }
}


#pragma mark - UITextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}


#pragma mark - UIScrollView Delegate

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [self.view endEditing:YES];
}

@end

//
//  Utils.h
//  Solidario
//
//  Created by Paula Vasconcelos Gueiros on 25/4/16.
//  Copyright © 2016 paulinhavgueiros. All rights reserved.
//

#define kMainViewController                 (MainViewController *)[UIApplication sharedApplication].delegate.window.rootViewController
#define kNavigationController               (NavigationController *)[(MainViewController *)[UIApplication sharedApplication].delegate.window.rootViewController rootViewController]

#define IS_IPHONE_5                         (fabs((double)[[UIScreen mainScreen]bounds].size.height - (double)568) < DBL_EPSILON)
#define IS_IPHONE_6_7                       (fabs((double)[[UIScreen mainScreen]bounds].size.height - (double)667) < DBL_EPSILON)
#define IS_IPHONE_6_7_PLUS                  (fabs((double)[[UIScreen mainScreen]bounds].size.height - (double)736) < DBL_EPSILON)

#define INPUT_ACCESSORY_VIEW_HEIGHT         44.f

#define ANTIFRAUD_PAYMENT_ACTIVATED         1
#define CREDIT_CARD_SAVE_SERVICE_ACTIVATED  0

#import <UIKit/UIKit.h>

typedef enum {
    TextMaskWithout = 0,
    TextMaskDate = 1,
    TextMaskPhone = 2,
    TextMaskCPFCNPJ = 3,
    TextMaskCurrency = 4,
    TextMaskCreditCardNumber = 5,
    TextMaskCreditCardValidationDate = 6,
    TextMaskCPF = 7,
    TextMaskCEP = 8
} TextMask;

typedef enum {
    CreditCardNone = 0,
    CreditCardVisa = 1,
    CreditCardMasterCard = 2,
    CreditCardAmex = 3,
    CreditCardDinersClub = 4,
    CreditCardDiscover = 5,
    CreditCardJCB = 6
} CreditCardFlag;

typedef enum {
    CreatedOption = 1,
    DonatedOption,
    FavoriteOption
} ProjectListOption;

typedef enum {
    DonationDataTVC = 0,
    AntifraudDataTVC,
    DonationFailVC
} SourceTVC;


@interface Utils : NSObject

@end








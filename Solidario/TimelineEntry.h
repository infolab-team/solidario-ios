//
//  TimelineEntry.h
//
//  Created by Paula Vasconcelos on 16/06/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TimelineEntry : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *internalBaseClassIdentifier;
@property (nonatomic, strong) NSString *img;
@property (nonatomic, assign) double day;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, assign) double month;
@property (nonatomic, strong) NSString *date;
@property (nonatomic, strong) NSString *internalBaseClassDescription;
@property (nonatomic, strong) NSString *projectId;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end

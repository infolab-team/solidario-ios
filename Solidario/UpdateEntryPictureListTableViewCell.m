//
//  UpdateEntryPictureListTableViewCell.m
//  Solidario
//
//  Created by Pan on 16/02/17.
//  Copyright © 2017 Paula Vasconcelos Gueiros. All rights reserved.
//

#import "UpdateEntryPictureListTableViewCell.h"

@implementation UpdateEntryPictureListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setImageViewWidthFromProportion:(CGFloat)proportion {
    CGFloat pictureWidth = self.pictureImageView.frame.size.height * proportion;
    [self.pictureWidthConstraint setConstant:pictureWidth];
}

@end

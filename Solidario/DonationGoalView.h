//
//  DonationGoalView.h
//  Solidario
//
//  Created by Pan on 02/06/17.
//  Copyright © 2017 Paula Vasconcelos Gueiros. All rights reserved.
//

#import <UIKit/UIKit.h>

static const CGFloat bannerViewProportion = 64.f / 280.f;
static const CGFloat defaultViewPadding = 20.f;

IB_DESIGNABLE
@interface DonationGoalView : UIView

@property (weak, nonatomic) IBOutlet UIImageView *mainImageView;
@property (weak, nonatomic) IBOutlet UILabel *goalValueLabel;

@end

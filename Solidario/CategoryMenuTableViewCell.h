//
//  CategoryMenuTableViewCell.h
//  Solidario
//
//  Created by Paula Vasconcelos Gueiros on 24/8/16.
//  Copyright © 2016 Paula Vasconcelos Gueiros. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Category.h"
#import "CategoryMenuButton.h"

@interface CategoryMenuTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet CategoryMenuButton *socialButton;
@property (weak, nonatomic) IBOutlet CategoryMenuButton *environmentButton;
@property (weak, nonatomic) IBOutlet CategoryMenuButton *animalButton;
@property (weak, nonatomic) IBOutlet CategoryMenuButton *healthButton;

@property (strong, nonatomic) NSArray *categoryButtons;
@property (strong, nonatomic) NSArray <Category *> *categories;

@end

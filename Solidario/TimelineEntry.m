//
//  TimelineEntry.m
//
//  Created by Paula Vasconcelos on 16/06/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "TimelineEntry.h"

NSString *const kTimelineEntryId = @"_id";
NSString *const kTimelineEntryImg = @"img";
NSString *const kTimelineEntryDay = @"day";
NSString *const kTimelineEntryTitle = @"title";
NSString *const kTimelineEntryMonth = @"month";
NSString *const kTimelineEntryDate = @"date";
NSString *const kTimelineEntryDescription = @"description";
NSString *const kTimelineEntryProjectId = @"projectId";


@interface TimelineEntry ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end


@implementation TimelineEntry

@synthesize internalBaseClassIdentifier = _internalBaseClassIdentifier;
@synthesize img = _img;
@synthesize day = _day;
@synthesize title = _title;
@synthesize month = _month;
@synthesize date = _date;
@synthesize internalBaseClassDescription = _internalBaseClassDescription;
@synthesize projectId = _projectId;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.internalBaseClassIdentifier = [self objectOrNilForKey:kTimelineEntryId fromDictionary:dict];
            self.img = [self objectOrNilForKey:kTimelineEntryImg fromDictionary:dict];
            self.day = [[self objectOrNilForKey:kTimelineEntryDay fromDictionary:dict] doubleValue];
            self.title = [self objectOrNilForKey:kTimelineEntryTitle fromDictionary:dict];
            self.month = [[self objectOrNilForKey:kTimelineEntryMonth fromDictionary:dict] doubleValue];
            self.date = [self objectOrNilForKey:kTimelineEntryDate fromDictionary:dict];
            self.internalBaseClassDescription = [self objectOrNilForKey:kTimelineEntryDescription fromDictionary:dict];
            self.projectId = [self objectOrNilForKey:kTimelineEntryProjectId fromDictionary:dict];

    }
    return self;
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.internalBaseClassIdentifier forKey:kTimelineEntryId];
    [mutableDict setValue:self.img forKey:kTimelineEntryImg];
    [mutableDict setValue:[NSNumber numberWithDouble:self.day] forKey:kTimelineEntryDay];
    [mutableDict setValue:self.title forKey:kTimelineEntryTitle];
    [mutableDict setValue:[NSNumber numberWithDouble:self.month] forKey:kTimelineEntryMonth];
    [mutableDict setValue:self.date forKey:kTimelineEntryDate];
    [mutableDict setValue:self.internalBaseClassDescription forKey:kTimelineEntryDescription];
    [mutableDict setValue:self.projectId forKey:kTimelineEntryProjectId];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}


#pragma mark - Helper Method

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.internalBaseClassIdentifier = [aDecoder decodeObjectForKey:kTimelineEntryId];
    self.img = [aDecoder decodeObjectForKey:kTimelineEntryImg];
    self.day = [aDecoder decodeDoubleForKey:kTimelineEntryDay];
    self.title = [aDecoder decodeObjectForKey:kTimelineEntryTitle];
    self.month = [aDecoder decodeDoubleForKey:kTimelineEntryMonth];
    self.date = [aDecoder decodeObjectForKey:kTimelineEntryDate];
    self.internalBaseClassDescription = [aDecoder decodeObjectForKey:kTimelineEntryDescription];
    self.projectId = [aDecoder decodeObjectForKey:kTimelineEntryProjectId];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {

    [aCoder encodeObject:_internalBaseClassIdentifier forKey:kTimelineEntryId];
    [aCoder encodeObject:_img forKey:kTimelineEntryImg];
    [aCoder encodeDouble:_day forKey:kTimelineEntryDay];
    [aCoder encodeObject:_title forKey:kTimelineEntryTitle];
    [aCoder encodeDouble:_month forKey:kTimelineEntryMonth];
    [aCoder encodeObject:_date forKey:kTimelineEntryDate];
    [aCoder encodeObject:_internalBaseClassDescription forKey:kTimelineEntryDescription];
    [aCoder encodeObject:_projectId forKey:kTimelineEntryProjectId];
}

- (id)copyWithZone:(NSZone *)zone {
    TimelineEntry *copy = [[TimelineEntry alloc] init];
    
    if (copy) {

        copy.internalBaseClassIdentifier = [self.internalBaseClassIdentifier copyWithZone:zone];
        copy.img = [self.img copyWithZone:zone];
        copy.day = self.day;
        copy.title = [self.title copyWithZone:zone];
        copy.month = self.month;
        copy.date = [self.date copyWithZone:zone];
        copy.internalBaseClassDescription = [self.internalBaseClassDescription copyWithZone:zone];
        copy.projectId = [self.projectId copyWithZone:zone];
    }
    return copy;
}

@end

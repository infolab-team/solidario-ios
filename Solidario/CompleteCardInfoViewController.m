//
//  CompleteCardInfoViewController.m
//  Solidario
//
//  Created by Pan on 21/12/16.
//  Copyright © 2016 Paula Vasconcelos Gueiros. All rights reserved.
//

#import "CompleteCardInfoViewController.h"
#import "AntifraudDataTableViewController.h"

#import "CustomTextField.h"
#import "NSString+Utils.h"

#import "PagarMe.h"
#import "PagarMeCreditCard.h"

@interface CompleteCardInfoViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *cardTypeLogo;
@property (weak, nonatomic) IBOutlet UILabel *cardNumberLabel;
@property (weak, nonatomic) IBOutlet UITextField *cardholderName;
@property (weak, nonatomic) IBOutlet CustomTextField *cardValidation;
@property (weak, nonatomic) IBOutlet CustomTextField *cardCVN;
@property (weak, nonatomic) IBOutlet UIView *memorizeSuperview;

@property (weak, nonatomic) IBOutlet UISwitch *memorizeCardSwitch;
@property (strong, nonatomic) IBOutlet UIView *activityIndicatorSuperview;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (strong, nonatomic) UITextField *activeField;
@property (strong, nonatomic) NSArray *textFields;

@end

@implementation CompleteCardInfoViewController

#pragma mark - Initialization

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setup];
}

- (void)setup {
    // scroll view
    self.scrollView.delegate = self;
    [self registerForKeyboardNotifications];
    
    // card info on view
    self.cardImageView.image = self.creditCardInfo.cardImage;
    self.cardTypeLogo.image = [CardIOCreditCardInfo logoForCardType:self.creditCardInfo.cardType];
    self.cardNumberLabel.text = [NSString maskedStringFromString:self.creditCardInfo.cardNumber andMask:TextMaskCreditCardNumber];
    
    // text fields
    self.cardholderName.delegate = self;
    self.textFields = @[self.cardholderName, self.cardValidation, self.cardCVN];
    for (UITextField *textField in self.textFields) {
        [self addInputAccessoryViewToTextField:textField];
    }
    
    // tap gesture recognizer to dismiss keyboard
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    
    // setup right bar button item title - user may be donating or saving a credit card
    if (self.userIsSavingCard) {
        [self.navigationItem.rightBarButtonItem setTitle:@"Salvar"];
        [self.memorizeSuperview setHidden:YES];
    }
    
    // hide save card option if service not activated
    if (!CREDIT_CARD_SAVE_SERVICE_ACTIVATED) {
        [self.memorizeSuperview setHidden:YES];
    }
}

- (void)registerForKeyboardNotifications {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void)addInputAccessoryViewToTextField:(UITextField *)textField {
    //Next Text Field Button
    UINavigationBar *inputAccessoryView = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0,
                                                                                            self.view.frame.size.width, INPUT_ACCESSORY_VIEW_HEIGHT)];
    UIBarButtonItem *barButtonNext = [[UIBarButtonItem alloc] initWithTitle:@"Próximo"
                                                                      style:UIBarButtonItemStyleDone target:self
                                                                     action:@selector(nextTextField:)];
    
    UIBarButtonItem *barButtonPrevious = [[UIBarButtonItem alloc] initWithTitle:@"Anterior"
                                                                          style:UIBarButtonItemStyleDone target:self
                                                                         action:@selector(previousTextField:)];
    barButtonNext.tag = textField.tag+1;
    barButtonPrevious.tag = textField.tag-1;
    if (barButtonPrevious.tag < 0) [barButtonPrevious setEnabled:NO];
    if (barButtonNext.tag > self.textFields.count) [barButtonNext setEnabled:NO];
    UINavigationItem *navItem = [[UINavigationItem alloc] initWithTitle:@""];
    navItem.leftBarButtonItem = barButtonPrevious;
    navItem.rightBarButtonItem = barButtonNext;
    [inputAccessoryView setItems:@[navItem]];
    [textField setInputAccessoryView:inputAccessoryView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // setup activityIndicator
    [self.view addSubview:self.activityIndicatorSuperview];
    self.activityIndicatorSuperview.center = self.view.center;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    // stop activityIndicator on cases in which it's still animating
    [self.activityIndicatorSuperview setHidden:YES];
    [self.activityIndicator stopAnimating];
}


#pragma mark - Helper Methods

- (void)dismissKeyboard {
    [self.view endEditing:YES];
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification {
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    CGRect bkgndRect = self.activeField.superview.frame;
    bkgndRect.size.height += kbSize.height;
    [self.activeField.superview setFrame:bkgndRect];
    [self.scrollView setContentOffset:CGPointMake(0.0, -self.activeField.frame.origin.y+kbSize.height) animated:YES];
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification {
    [self.scrollView setContentOffset:CGPointZero animated:YES];
}


#pragma mark - UITextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    self.activeField = textField;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    self.activeField = nil;
}


#pragma mark - Button Actions

- (void)previousTextField:(UIBarButtonItem *)sender {
    if (sender.tag >= 0 && sender.tag < self.textFields.count) {
        [self focusTextFieldWithTag:sender.tag];
    } else {
        [sender setEnabled:NO];
    }
}

- (void)nextTextField:(UIBarButtonItem *)sender {
    if (sender.tag >= 0 && sender.tag < self.textFields.count) {
        [self focusTextFieldWithTag:sender.tag];
    } else {
        [sender setEnabled:NO];
    }
}

- (void)focusTextFieldWithTag:(NSInteger)tag {
    for (UITextField *textField in self.textFields) {
        if (textField.tag == tag) {
            [textField becomeFirstResponder];
        }
    }
}

- (IBAction)cvnHelp:(id)sender {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Ajuda"
                                                                   message:@"O CVN, ou número de verificação, é um número de três dígitos que geralmente fica na parte de trás do cartão."
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"OK"
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action) {}];
    
    [alert addAction:okButton];
    [self presentViewController:alert animated:YES completion:nil];
}


#pragma mark - Navigation

- (IBAction)goToNextPageOrSave:(id)sender {
    [self.view endEditing:YES];
    [self performInputValidations];
}

- (void)performInputValidations {
    
    // alert setup
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Atenção"
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"OK"
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action) {}];
    [alert addAction:okButton];
    
    
    // mandatory input confirmation
    __block BOOL allFilled = YES;
    [self.textFields enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([((UITextField *)obj).text isEqualToString:@""]) {
            allFilled = NO;
            *stop = YES;
        }
    }];
    if (!allFilled) {
        [alert setMessage:@"Preencha todos os campos antes de prosseguir."];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    
    // valid expiration date
    if ([self.cardValidation.text length] != self.cardValidation.maxLength) {
        [alert setMessage:@"Vencimento no formato incorreto. Utilize o formato mm/aa."];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    NSDateComponents *dateComponents = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
    NSArray *dateArray = [self.cardValidation.text componentsSeparatedByString:@"/"];
    NSInteger validationYear = [dateArray[1] integerValue] + 2000;
    if (validationYear < [dateComponents year]) {
        [alert setMessage:@"Vencimento anterior à data atual."];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    NSInteger validationMonth = [dateArray[0] integerValue];
    if (validationMonth < 1 || validationMonth > 12) {
        [alert setMessage:@"Data inválida."];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    if (validationYear == [dateComponents year] && validationMonth < [dateComponents month]) {
        [alert setMessage:@"Vencimento anterior à data atual."];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    
    // valid CVN
    if ([self.cardCVN.text length] != self.cardCVN.maxLength) {
        [alert setMessage:@"Número CVN deve ter 3 dígitos."];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    
    if (self.userIsSavingCard) {
        [self performSegueWithIdentifier:@"unwindToCardList" sender:self];
    } else {
        [self generateCardHash];
    }
}

- (void)generateCardHash {
    // alert setup
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Erro"
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"OK"
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action) {
                                                         [self.memorizeCardSwitch setOn:NO animated:YES];
                                                     }];
    [alert addAction:okButton];
    
    NSArray *expirationDateComponents = [self.cardValidation.text componentsSeparatedByString:@"/"];
    PagarMeCreditCard *pagarMeCreditCard = [[PagarMeCreditCard alloc] initWithCardNumber:[NSString plainStringFromString:self.cardNumberLabel.text]
                                                                          cardHolderName:self.cardholderName.text
                                                                     cardExpirationMonth:expirationDateComponents[0]
                                                                      cardExpirationYear:expirationDateComponents[1]
                                                                                 cardCvv:self.cardCVN.text];
    if ([pagarMeCreditCard hasErrorCardNumber]) {
        [alert setMessage:@"Erro no número do cartão."];
    }
    else if ([pagarMeCreditCard hasErrorCardHolderName]) {
        [alert setMessage:@"Erro no nome impresso no cartão."];
    }
    else if ([pagarMeCreditCard hasErrorCardCVV]) {
        [alert setMessage:@"Erro no CVN."];
    }
    else if ([pagarMeCreditCard hasErrorCardExpirationMonth] || [pagarMeCreditCard hasErrorCardExpirationYear]) {
        [alert setMessage:@"Erro na data de validade."];
    }
    else {
        // todos os campos estão válidos - gerar hash do cartão
        [self.activityIndicatorSuperview setHidden:NO];
        [self.activityIndicator startAnimating];
        
        [pagarMeCreditCard generateHash:^(NSError *error, NSString *cardHash) {
            [self.activityIndicatorSuperview setHidden:YES];
            [self.activityIndicator stopAnimating];
            if (error) {
                NSLog(@"❗️ Error generating card hash: %@", error);
                [alert setMessage:@"Erro na geração do hash do cartão."];
                [self presentViewController:alert animated:YES completion:nil];
                return;
            }
            
            // this point is only reached in case of transaction success
            self.donation.cardHash = cardHash;
            
            // proceed with flow
            if (self.memorizeCardSwitch.isOn) {
                [self callSaveCardService];
            } else {
                [self performSegueWithIdentifier:@"customToAntifraudDataFromCompleteCardInfo" sender:self];
            }
        }];
        return;
    }
    
    // this point is only reached in case of error in card data
    [self presentViewController:alert animated:YES completion:nil];
}


- (void)callSaveCardService {
#warning complete call when ready

    [self.activityIndicatorSuperview setHidden:NO];
    [self.activityIndicator startAnimating];
//    [Service saveCardXForUserY... andCompletion:^(id results, BOOL hasError, NSString *errorMessage) {

        [self.activityIndicatorSuperview setHidden:YES];
        [self.activityIndicator stopAnimating];

//        if (hasError) {
//            // alert setup
//            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Erro"
//                                                                           message:nil
//                                                                    preferredStyle:UIAlertControllerStyleAlert];
//            UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"OK"
//                                                               style:UIAlertActionStyleDefault
//                                                             handler:^(UIAlertAction * action) {
//                                                                 [self.memorizeCardSwitch setOn:NO animated:YES];
//                                                             }];
//            [alert addAction:okButton];
//            
////            if (errorMessage != nil) {
////                [alert setMessage:errorMessage];
////            } else {
//                [alert setMessage:@"Não foi possível salvar os dados do cartão."];
////            }
//            [self presentViewController:alert animated:YES completion:nil];
//            return;
//        } else {
            NSLog(@"💳 Credit Card data saved");
            [self performSegueWithIdentifier:@"customToAntifraudDataFromCompleteCardInfo" sender:self];
//        }
//    }];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"customToAntifraudDataFromCompleteCardInfo"]) {
        AntifraudDataTableViewController *antifraudDataTableViewController = (AntifraudDataTableViewController *)[segue destinationViewController];
        
        antifraudDataTableViewController.project = self.project;
        antifraudDataTableViewController.mainImage = self.mainImage;
        antifraudDataTableViewController.donation = self.donation;
    }
}

- (IBAction)unwindToCompleteCardInfo:(UIStoryboardSegue*)unwindSegue {
    
}

@end

//
//  PlaceholderTextView.h
//  Solidario
//
//  Created by Pan on 15/02/17.
//  Copyright © 2017 Paula Vasconcelos Gueiros. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlaceholderTextView : UITextView <UITextViewDelegate>

@property (nonatomic, retain) IBInspectable NSString *placeholder;
@property (nonatomic, retain) IBInspectable UIColor *placeholderColor;
@property (nonatomic) IBInspectable NSInteger maxLength;

- (void)textChanged:(NSNotification*)notification;

@end

//
//  TermsTableViewController.m
//  Solidario
//
//  Created by Paula Vasconcelos Gueiros on 12/9/16.
//  Copyright © 2016 Paula Vasconcelos Gueiros. All rights reserved.
//

#import "TermsTableViewController.h"
#import "NSArray+Utils.h"
#import "UILabel+Utils.h"
#import "Service.h"
#import "Terms.h"

@interface TermsTableViewController ()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIWebView *contentWebView;
@property (weak, nonatomic) IBOutlet UIView *footerView;
@property (weak, nonatomic) IBOutlet UILabel *yearVersionLabel;

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (strong, nonatomic) NSMutableArray *rowsInSection;
@property (strong, nonatomic) NSMutableArray *rowHeights;

@property (strong, nonatomic) Terms *terms;

@end


@implementation TermsTableViewController

static const NSInteger titleCellNumber = 0;
static const NSInteger contentCellNumber = 1;
static const CGFloat defaultViewPadding = 20.f;

#pragma mark - Initialization

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setup];
}

- (void)setup {
    // initialize data before request
    self.titleLabel.text = @"";
    
    // setup arrays for number of rows and row heights
    // set one section with 3 rows
    self.rowsInSection = [NSMutableArray arrayWithObjects:[NSNumber numberWithInt:3], nil];

    // set default height of 44 for each row
    self.rowHeights = [NSMutableArray array];
    for (int i = 0; i < [self.rowsInSection count]; i++) {
        [self.rowHeights addObject:[NSMutableArray array]];
        NSInteger rows = [self.rowsInSection[i] integerValue];
        for (int j = 0; j < rows; j++) {
            [self.rowHeights[i] addObject:[NSNumber numberWithFloat:44.f]];
        }
    }
    
    // retrieve data from server
    [self retrieveData];
}

- (void)retrieveData {
    
    // call Terms service and populate data objects
    [Service getTermsWithCompletion:^(id results, BOOL hasError, NSString *errorMessage) {
        if (hasError) {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Erro"
                                                                           message:nil
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"OK"
                                                               style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction * action) {}];
            [alert addAction:okButton];
            
//            if (errorMessage != nil) {
//                [alert setMessage:errorMessage];
//            } else {
                [alert setMessage:@"Não foi possível obter o documento de Termos e Políticas de Privacidade.  Tente novamente mais tarde."];
//            }
            [self presentViewController:alert animated:YES completion:nil];
            return;
        } else {
            NSLog(@"📜 Terms retrieval successful");
            
            self.terms = (Terms *)results;
            
            // setup title cell content and height
            self.titleLabel.text = self.terms.title;
            CGFloat labelHeight = [UILabel heightForLabelWithText:self.titleLabel.text font:self.titleLabel.font andWidth:self.tableView.frame.size.width - 2*defaultViewPadding];
            self.rowHeights[0][titleCellNumber] = [NSNumber numberWithFloat:labelHeight + defaultViewPadding];
            [self.tableView reloadData];
            
            // setup webview content
            self.contentWebView.delegate = self;
            self.contentWebView.scrollView.scrollEnabled = NO;
            
            NSArray *termsStringArray = [NSArray loadPlistWithName:@"Terms"];
            NSString *termsString = [NSString stringWithFormat:@"%@%@%@",
                                        [termsStringArray objectAtIndex:0],
                                        self.terms.content,
                                        [termsStringArray objectAtIndex:1]];

            [self.contentWebView loadHTMLString:termsString baseURL:[[NSBundle mainBundle] bundleURL]];
        }
    }];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // setup activityIndicator position
    UIWindow *frontWindow = [[UIApplication sharedApplication] keyWindow];
    [frontWindow addSubview:self.activityIndicator];
    self.activityIndicator.center = frontWindow.center;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    // stop activityIndicator on cases in which it's still animating
    [self.activityIndicator stopAnimating];
    [self.activityIndicator removeFromSuperview];
}


#pragma mark - UITableView Delegate and Data Source

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [(self.rowHeights[indexPath.section][indexPath.row]) floatValue];
}


#pragma mark - UIWebView Delegate

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    
    [self.activityIndicator stopAnimating];
}

- (BOOL)webView:(UIWebView*)webView shouldStartLoadWithRequest:(NSURLRequest*)request navigationType:(UIWebViewNavigationType)navigationType {
    NSURL *url = [request URL];
    
    if (navigationType == UIWebViewNavigationTypeLinkClicked) {
        UIApplication *application = [UIApplication sharedApplication];
        if ([application respondsToSelector:@selector(openURL:options:completionHandler:)]) {
            [application openURL:[request URL] options:@{} completionHandler:nil];
            return NO;
        } else if ([application respondsToSelector:@selector(openURL:)]) {
            [application openURL:[request URL]];
            return NO;
        }
    } else if (navigationType == UIWebViewNavigationTypeOther) {
        if ([[url scheme] isEqualToString:@"ready"]) {
            float contentHeight = [[url host] floatValue];
            NSLog(@"📖 Terms webView height after rendering: %.0f", contentHeight);
            self.rowHeights[0][contentCellNumber] = [NSNumber numberWithFloat:contentHeight + 2*defaultViewPadding];
            NSLog(@"🔄 Reloading tableView...");
            [self.footerView setHidden:NO];
            [self.tableView reloadData];
            return NO;
        }
    }
    return YES;
}

@end

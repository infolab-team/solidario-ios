//
//  ActionButton.m
//  BelievePrototype
//
//  Created by Allan Alves on 9/30/15.
//  Copyright © 2015 Infosolo. All rights reserved.
//

#import "ActionButton.h"

@implementation ActionButton

- (void)startLoadingAnimated {
    UIActivityIndicatorView *loading = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    [loading setColor:self.currentTitleColor];
    loading.center = self.center;
    loading.tag = 3523;
    loading.alpha = 0;
    [self setTitleColor:self.backgroundColor forState:UIControlStateNormal];
    [self setEnabled:NO];
    [self.superview addSubview:loading];
    //fade in animating
    [UIView animateWithDuration:0.3 animations:^{
        loading.alpha = 1;
        [loading startAnimating];
    }];
}

- (void)stopLoading {
    UIActivityIndicatorView *loading = (UIActivityIndicatorView*)[self.superview viewWithTag:3523];
    if (loading) {
        //fade out
        [UIView animateWithDuration:0.3 animations:^{
            [self setTitleColor:loading.color forState:UIControlStateNormal];
            loading.alpha = 0;
        } completion:^(BOOL finished) {
            [loading stopAnimating];
            [loading removeFromSuperview];
            [self setEnabled:YES];
        }];
    }
}

@end

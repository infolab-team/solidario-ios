//
//  Singleton.h
//  Solidario
//
//  Created by Paula Vasconcelos Gueiros on 17/8/16.
//  Copyright © 2016 Paula Vasconcelos Gueiros. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataModels.h"

@interface Singleton : NSObject

#pragma mark - Singleton instance

+ (Singleton *)instance;


#pragma mark - First Time Use

+ (BOOL)isFirstTimeUse;
+ (void)setNotFirstTimeUse;


#pragma mark - PLabel and Card Project Properties

+ (double)pLabelPercentageFontSize;
+ (double)pLabelPercentSymbolFontSize;
+ (double)pLabelTrackWidth;
+ (double)pLabelProgressWidth;
+ (NSInteger)cardDescriptionLabelNumberOfLines;


#pragma mark - Card Flag Types

+ (NSArray *)acceptedCardFlags;


#pragma mark - App Flow

+ (BOOL)isDonating;
+ (void)setDonating:(BOOL)donating;
+ (BOOL)isUpdatingTimeline;
+ (void)setUpdatingTimeline:(BOOL)updatingTimeline;


#pragma mark - User Data

+ (User *)loggedUser;
+ (void)loginWithUser:(User *)user withFacebookEnabled:(BOOL)facebookEnabled andPersistent:(BOOL)persistent;
+ (void)logout;

+ (NSString *)loggedUserAuthToken;
+ (BOOL)isThereLoggedUserAuthToken;
+ (NSString *)loggedUserID;
+ (BOOL)isThereLoggedUserID;

+ (BOOL)isFacebookUser;

+ (void)updateLoggedUserWithUser:(UserUpdated *)user;


#pragma mark - Category

+ (NSArray *)categories;
+ (void)saveCategories:(NSArray *)categories;


#pragma mark - State

+ (NSArray *)states;
+ (void)saveStates:(NSArray *)states;


@end
